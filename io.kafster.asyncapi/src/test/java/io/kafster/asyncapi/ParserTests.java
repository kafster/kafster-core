/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URL;
import java.util.Collections;
import java.util.Optional;

import javax.activation.URLDataSource;

import org.junit.jupiter.api.Test;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.io.Resource;
import io.kafster.io.util.DataSourceResource;
import io.kafster.util.ObjectMapperFactoryBean;

public class ParserTests {

	private AsyncAPIParser getParser() throws Exception {
		
		ObjectMapperFactoryBean factory = new ObjectMapperFactoryBean();
		factory.setFormat("yaml");
		
		AsyncAPIParser result = new AsyncAPIParser();
		result.setYaml(factory.getObject());
		
		return result;
	}
	
	@Test
	public void testSimpleParse() throws Exception {
		
		Resource document = new DataSourceResource(new URLDataSource(new URL("https://raw.githubusercontent.com/project-flogo/asyncapi/master/examples/kafka/asyncapi.yml")), "application/yaml");
	
		AsyncAPIParser parser = getParser();
		Optional<EndpointProvider> parsed = parser.parse(document, Collections.emptyMap());
		
		assertTrue(parsed.isPresent());
		
		EndpointProvider provider = parsed.get();
		
		assertEquals("Kafka Application", provider.getPresentationName());
		assertEquals("urn:com:kafka:server", provider.getName());
		assertEquals("1.0.0", provider.getVersion());
		
		Iterable<? extends Endpoint> endpoints = provider.getEndpoints(null);
		assertNotNull(endpoints);
		
		for (Endpoint endpoint : endpoints) {
			assertEquals("kafka://localhost:9092", endpoint.getUri());
			
			EndpointMetadata metadata = endpoint.getMetadata();
			assertNotNull(metadata);
			assertNotNull(metadata.getPayloadSchema());
			assertEquals("application/json", metadata.getContentType());
		}
	}
}
