/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi.model;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.text.StringSubstitutor;

import com.asyncapi.v2.model.channel.message.Message;
import com.asyncapi.v2.model.channel.operation.Operation;
import com.asyncapi.v2.model.server.Server;

import io.kafster.asyncapi.AsyncAPIParserContext;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.error.InvalidDefinitionException;

public class AsyncAPIEndpoint implements Endpoint {
	
	private AsyncAPIParserContext context;
	private Operation operation;
	private EndpointMetadata metadata;
	private Server server;
	
	private Message message;
	private Instant timestamp = Instant.now();

	public AsyncAPIEndpoint(Operation operation, EndpointMetadata metadata, AsyncAPIParserContext context) {
		this(operation, metadata, context, null);
	}
	
	public AsyncAPIEndpoint(Operation operation, EndpointMetadata metadata, AsyncAPIParserContext context, Server server) {
		this.operation = operation;
		this.context = context;
		this.metadata = metadata;
		this.server = server;
	}
	
	protected Operation getOperation() {
		return operation;
	}
	
	protected Message getMessage() throws InvalidDefinitionException {
		if (message == null) {
			Operation operation = getOperation();
			if (operation != null) {
				message = context.resolve(operation.getMessage(), Message.class);
			}
		}
		return message;
	}
	
	@Override
	public String getKey() {
		return null;
	}

	@Override
	public String getName() {
		Operation op = getOperation();
		if (op != null) {
			String result = op.getOperationId();
			if (result == null) {
				result = op.getSummary();
			}
		}
		return null;
	}

	@Override
	public String getDescription() {
		Operation op = getOperation();
		if (op != null) {
			return op.getDescription();
		}
		return null;
	}

	@Override
	public String getUri() {
		if (server != null) {
			StringSubstitutor sub = new StringSubstitutor(getConfiguration(), "{", "}");
			return server.getProtocol() + "://" + sub.replace(server.getUrl());
		}
		return null;
	}

	@Override
	public Instant getModified() {
		return timestamp;
	}

	@Override
	public Instant getCreated() {
		return timestamp;
	}

	@Override
	public EndpointMetadata getMetadata() {
		return metadata;
	}

	@Override
	public Map<String, Object> getConfiguration() {
		Map<String, Object> result = new HashMap<>();
		
		if ((server != null) && (server.getVariables() != null)) {
			server.getVariables().forEach((k,v) -> {
				if (v.getDefaultValue() != null) {
					result.put(k, v.getDefaultValue());
				}
			});
		}
		
		return result;
	}

}
