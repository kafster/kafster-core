/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.asyncapi.v2.model.channel.ChannelItem;
import com.asyncapi.v2.model.channel.operation.Operation;

import io.kafster.asyncapi.AsyncAPIParserContext;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.io.Resource;

public class AsyncAPIEndpointProvider implements EndpointProvider {
	
	private AsyncAPIParserContext context;
	private List<AsyncAPIEndpoint> endpoints;
	
	public AsyncAPIEndpointProvider(AsyncAPIParserContext context) {
		this.context = context;
	}

	@Override
	public Optional<Endpoint> getByKey(String key) throws Exception {
		if (key != null) {
			for (Endpoint endpoint : getEndpoints(null)) {
				if (StringUtils.equals(key, endpoint.getKey())) {
					return Optional.of(endpoint);
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<Endpoint> getByUri(String uri, Map<String, Object> props) throws Exception {
		if (uri != null) {
			for (Endpoint endpoint : getEndpoints(null)) {
				if (StringUtils.equals(uri, endpoint.getUri())) {
					return Optional.of(endpoint);
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public Iterable<? extends Endpoint> getEndpoints(EndpointFilter filter) throws Exception {
		
		if (endpoints == null) {
			List<AsyncAPIEndpoint> result = new ArrayList<>();
			
			Map<String,ChannelItem> channels = context.getAsyncAPI().getChannels();
			if (channels != null) {
				channels.forEach((name, channel) -> result.addAll(createEndpoints(name, channel)));
			}
			
			this.endpoints = result;
		}
		
		return endpoints;
	}
	
	protected List<AsyncAPIEndpoint> createEndpoints(String name, ChannelItem channel) {
		
		List<AsyncAPIEndpoint> result = new ArrayList<>();
		
		if (channel.getPublish() != null) {
			AsyncAPIEndpointMetadata metadata = new AsyncAPIEndpointMetadata(channel.getPublish(), context);
			metadata.setName(name);
			metadata.setDescription(channel.getDescription());
			metadata.setRole(Role.SINK);
			metadata.setProvider(this);
			
			result.addAll(getServerEndpoints(channel.getPublish(), metadata));
		}
		
		if (channel.getSubscribe() != null) {
			AsyncAPIEndpointMetadata metadata = new AsyncAPIEndpointMetadata(channel.getSubscribe(), context);
			metadata.setName(name);
			metadata.setDescription(channel.getDescription());
			metadata.setRole(Role.SOURCE);
			metadata.setProvider(this);
			
			result.addAll(getServerEndpoints(channel.getSubscribe(), metadata));
		}
		
		return result;
	}
	
	protected List<AsyncAPIEndpoint> getServerEndpoints(Operation operation, AsyncAPIEndpointMetadata metadata) {
		
		if (context.getServers().isEmpty()) {
			return Collections.singletonList(new AsyncAPIEndpoint(operation, metadata, context));
		}
		
		List<AsyncAPIEndpoint> result = new ArrayList<>();
		context.getServers().forEach((key, server) -> {
			result.add(new AsyncAPIEndpoint(operation, metadata, context, server));
		});
		return result;
	}

	@Override
	public String getName() {
		return context.getAsyncAPI().getId();
	}

	@Override
	public String getPresentationName() {
		return (context.getAsyncAPI().getInfo() == null) ? null : context.getAsyncAPI().getInfo().getTitle();
	}

	@Override
	public String getDescription() {
		return (context.getAsyncAPI().getInfo() == null) ? null : context.getAsyncAPI().getInfo().getDescription();
	}

	@Override
	public String getVersion() {
		return (context.getAsyncAPI().getInfo() == null) ? null : context.getAsyncAPI().getInfo().getVersion();
	}

	@Override
	public Resource getImage() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getKey() {
		// Always allow the system to assign the key
		//
		return null;
	}

}
