/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi.model;

import com.asyncapi.v2.model.channel.message.Message;
import com.asyncapi.v2.model.channel.operation.Operation;
import com.asyncapi.v2.model.schema.Schema;
import com.asyncapi.v2.model.server.Server;

import io.kafster.asyncapi.AsyncAPIParserContext;
import io.kafster.asyncapi.schema.AsyncAPISchemaType;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.error.InvalidDefinitionException;
import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaType;
import io.kafster.schema.json.JSONSchemaPrimitiveTypes;
import io.kafster.schema.json.model.JSONSchemaType;
import io.kafster.schema.simple.SimpleSchemaType;

public class AsyncAPIEndpointMetadata implements EndpointMetadata {
	
	private Operation operation;
	private AsyncAPIParserContext context;
	
	private Message message;
	
	private Role role;
	private String name;
	private String description;
	
	private EndpointProvider provider;
	private Server server;
	
	public AsyncAPIEndpointMetadata(Operation operation, AsyncAPIParserContext context) {
		this.operation = operation;
		this.context = context;
	}
	
	public AsyncAPIEndpointMetadata(AsyncAPIEndpointMetadata metadata, Server server) {
		this.operation = metadata.operation;
		this.context = metadata.context;
		this.message = metadata.message;
		this.role = metadata.role;
		this.name = metadata.name;
		this.description = metadata.description;
		this.provider = metadata.provider;
		this.server = server;
	}
	
	protected Operation getOperation() {
		return operation;
	}
	
	protected Message getMessage() throws InvalidDefinitionException {
		if (message == null) {
			Operation operation = getOperation();
			if (operation != null) {
				message = context.resolve(operation.getMessage(), Message.class);
			}
		}
		return message;
	}

	@Override
	public Role getRole() {
		return role;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getContentType() throws InvalidDefinitionException {
		Message message = getMessage();
		if (message != null) {
			String result = message.getContentType();
			if (result != null) {
				return result;
			}
		}
		
		return context.getAsyncAPI().getDefaultContentType();
	}

	@Override
	public SchemaType getEndpointSchema() {
		
		if ((server == null) || (server.getVariables() == null)) {
			return null;
		}
		
		SimpleSchemaType result = new SimpleSchemaType();
		server.getVariables().forEach((k,v) -> {
			
			MutableSchemaType type = JSONSchemaType.derivedFrom(JSONSchemaPrimitiveTypes.STRING);
			type.setDescription(v.getDescription());
			
			if (v.getEnumValues() != null) {
				type.setEnum(true);
				type.setEnumValues(v.getEnumValues().toArray(new String[] {}));
			}
			
			MutableSchemaProperty property = result.addProperty(k);
			property.setType(type);
		});
		
		return result;
	}

	@Override
	public SchemaType getPayloadSchema() throws InvalidDefinitionException {
		Message message = getMessage();
		if (message != null) {
			Schema schema = context.resolve(message.getPayload(), Schema.class);
			if (schema != null) {
				return new AsyncAPISchemaType(schema);
			}
		}
		return null;
	}

	@Override
	public EndpointProvider getProvider() {
		return provider;
	}
	
	public void setProvider(EndpointProvider provider) {
		this.provider = provider;
	}

	@Override
	public SchemaType getHeaderSchema() throws InvalidDefinitionException {
		Message message = getMessage();
		if (message != null) {
			Schema schema = context.resolve(message.getHeaders(), Schema.class);
			if (schema != null) {
				return new AsyncAPISchemaType(schema);
			}
		}
		return null;
	}

	@Override
	public SchemaType getKeySchema() {
		return null;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getUriTemplate() throws Exception {
		return (server == null) ? null : server.getUrl();
	}

}
