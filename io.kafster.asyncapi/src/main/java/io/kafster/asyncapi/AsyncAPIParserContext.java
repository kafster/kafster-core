/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.asyncapi.v2.binding.ChannelBinding;
import com.asyncapi.v2.binding.MessageBinding;
import com.asyncapi.v2.binding.OperationBinding;
import com.asyncapi.v2.binding.ServerBinding;
import com.asyncapi.v2.model.AsyncAPI;
import com.asyncapi.v2.model.Reference;
import com.asyncapi.v2.model.channel.Parameter;
import com.asyncapi.v2.model.channel.message.CorrelationId;
import com.asyncapi.v2.model.channel.message.Message;
import com.asyncapi.v2.model.channel.message.MessageTrait;
import com.asyncapi.v2.model.channel.operation.OperationTrait;
import com.asyncapi.v2.model.schema.Schema;
import com.asyncapi.v2.model.security_scheme.SecurityScheme;
import com.asyncapi.v2.model.server.Server;

import io.kafster.asyncapi.model.AsyncAPIEndpointProvider;
import io.kafster.error.InvalidDefinitionException;

public class AsyncAPIParserContext {

	private AsyncAPI asyncAPI;
	private Map<String,Server> servers;
	
	private int maxDepth = 16;
	
	public AsyncAPIParserContext(AsyncAPI api) {
		this.asyncAPI = api;
	}
	
	public Map<String,Server> getServers() {
		
		if ((servers == null) && (asyncAPI.getServers() != null)) {
			
			Map<String,Server> result = new HashMap<>();
			
			asyncAPI.getServers().forEach((name,server) -> {
				if (ArrayUtils.contains(AsyncAPIParser.SUPPORTED_BINDINGS, server.getProtocol())) {
					result.put(name, server);
				}
			});
			
			servers = result;
		}
		return servers;
	}
	
	public <T> T resolve(Object ref, Class<? extends T> type) throws InvalidDefinitionException {
		return resolve(ref, type, 0);
	}
	
	protected <T> T resolve(Object ref, Class<? extends T> type, int depth) throws InvalidDefinitionException {
		
		if (depth++ > maxDepth) {
			throw new InvalidDefinitionException("Stack depth [" + depth + "] exceeded");
		}
		
		Object result = ref;
			
		if (ref instanceof Reference) {
			
			String reference = ((Reference)ref).getRef();
			
			String name = StringUtils.substringAfterLast(reference, '/');
			if (name != null) {
			
				if (Schema.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getSchemas().get(name);
				}
				else if (ChannelBinding.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getChannelBindings().get(name);
				}
				else if (CorrelationId.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getCorrelationIds().get(name);
				}
				else if (MessageBinding.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getMessageBindings().get(name);
				}
				else if (Message.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getMessages().get(name);
				}
				else if (MessageTrait.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getMessageTraits().get(name);
				}
				else if (OperationBinding.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getOperationBindings().get(name);
				}
				else if (OperationTrait.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getOperationTraits().get(name);
				}
				else if (Parameter.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getParameters().get(name);
				}
				else if (SecurityScheme.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getSecuritySchemes().get(name);
				}
				else if (ServerBinding.class.isAssignableFrom(type)) {
					result = asyncAPI.getComponents().getServerBindings().get(name);
				}
				else {
					throw new IllegalArgumentException("Components of type [" + type.getName() + "] not supported");
				}
			}
		}
			
		if (result != null) {
			
			if (result instanceof Reference) {
				return resolve(result, type, depth);
			}
			
			return type.cast(result);
		}
		
		return null;
	}

	public AsyncAPI getAsyncAPI() {
		return asyncAPI;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}
}
