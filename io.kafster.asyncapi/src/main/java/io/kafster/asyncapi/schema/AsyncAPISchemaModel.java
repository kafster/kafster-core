/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi.schema;

import javax.xml.namespace.QName;

import com.asyncapi.v2.model.schema.Schema;

import io.kafster.schema.SchemaModel;

public class AsyncAPISchemaModel implements SchemaModel {
	
	private Schema schema;
	
	protected AsyncAPISchemaModel(Schema schema) {
		this.schema = schema;
	}
	
	protected Schema getSchema() {
		return schema;
	}

	@Override
	public QName getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitle() {
		return schema.getTitle();
	}

	@Override
	public String getDescription() {
		return schema.getDescription();
	}

}
