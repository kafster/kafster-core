/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi.schema;

import java.util.HashMap;
import java.util.Map;

import com.asyncapi.v2.model.schema.Schema;

import io.kafster.schema.Dependency;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;

public class AsyncAPISchemaType extends AsyncAPISchemaModel implements SchemaType {
	
	private Map<String, SchemaProperty> properties;
	
	public AsyncAPISchemaType(Schema schema) {
		super(schema);
	}

	@Override
	public boolean isCollection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAny() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isRef() {
		return getSchema().getRef() != null;
	}

	@Override
	public boolean isEnum() {
		return getSchema().getEnumValue() != null;
	}

	@Override
	public Object[] getEnumValues() {
		if (getSchema().getEnumValue() != null) {
			return getSchema().getEnumValue().toArray(new Object[] {});
		}
		return null;
	}

	@Override
	public SchemaType getCollectionType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReference() {
		return getSchema().getRef();
	}

	@Override
	public Number getMaxInclusive() {
		return getSchema().getMaximum();
	}

	@Override
	public Number getMaxExclusive() {
		return getSchema().getExclusiveMaximum();
	}

	@Override
	public Number getMinInclusive() {
		return getSchema().getMinimum();
	}

	@Override
	public Number getMinExclusive() {
		return getSchema().getExclusiveMinimum();
	}

	@Override
	public Number getMultipleOf() {
		return getSchema().getMultipleOf();
	}

	@Override
	public Integer getMinLength() {
		return getSchema().getMinLength();
	}

	@Override
	public Integer getMaxLength() {
		return getSchema().getMaxLength();
	}

	@Override
	public String getPattern() {
		return getSchema().getPattern();
	}

	@Override
	public String getFormat() {
		return (getSchema().getFormat() == null) ? null : getSchema().getFormat().toString();
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		if (properties == null) {
			Map<String, SchemaProperty> result = new HashMap<>();
			if (getSchema().getProperties() != null) {
				getSchema().getProperties().forEach((k,v) -> result.put(k, new AsyncAPISchemaProperty(v)));
			}
			properties = result;
		}
		return properties;
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		return null;
	}

}
