/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.asyncapi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.input.CloseShieldInputStream;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asyncapi.v2.model.AsyncAPI;
import com.asyncapi.v2.model.server.Server;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.asyncapi.model.AsyncAPIEndpointProvider;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderBuilder;
import io.kafster.endpoint.EndpointProviderParser;
import io.kafster.error.InvalidDefinitionException;
import io.kafster.io.Resource;

public class AsyncAPIParser implements EndpointProviderParser {
	
	private static final Logger log = LoggerFactory.getLogger(AsyncAPIParser.class);
	
	private static final int DEFAULT_MAX_DOC_SIZE = 5_000_000;
	public static final String[] SUPPORTED_BINDINGS = {"kafka","kafka-secure"};
	public static final String[] SUPPORTED_CONTENT_TYPES = {"application/json","text/json","application/x-yaml","application/yaml","text/yaml"};
	
	private ObjectMapper json;
	private ObjectMapper yaml;
	
	private int maxSize = DEFAULT_MAX_DOC_SIZE;

	@Override
	public Optional<EndpointProvider> parse(Resource resource, Map<String, String> options) throws Exception {
		
		if (shouldParse(resource)) {
			
			AsyncAPI asyncapi = read(resource);
			if (validate(asyncapi)) {
				
				AsyncAPIParserContext context = new AsyncAPIParserContext(asyncapi);
				
				Map<String,Server> servers = context.getServers();
				if ((servers != null) && !servers.isEmpty()) {
					return Optional.of(new AsyncAPIEndpointProvider(context));
				}
				else {
					// We require at least one supported server type
					//
					throw new InvalidDefinitionException("At least one supported server type is required");
				}
			}
		}
		
		return Optional.empty();
	}
	
	protected AsyncAPI read(Resource resource) throws Exception {
		
		InputStream istrm = resource.getInputStream();
		istrm = CloseShieldInputStream.wrap(new BufferedInputStream(istrm));
		
		AsyncAPI result = read(resource, istrm, yaml);
		
		if (result == null) {
			result = read(resource, istrm, json);
		}
		
		return result;
	}
	
	protected AsyncAPI read(Resource resource, InputStream istrm, ObjectMapper mapper) throws Exception {
		
		try {
			
			if (istrm.markSupported()) {
				istrm.mark(maxSize);
			}
			
			return mapper.readValue(istrm, AsyncAPI.class);
		}
		catch (Exception e) {
			log.error("Could not parse [" + resource.getName() + "]", e);
		}
		finally {
			if (istrm.markSupported()) {
				istrm.reset();
			}
		}
		
		return null;
	}
	
	protected boolean shouldParse(Resource resource) throws IOException, Exception {
		return ArrayUtils.contains(SUPPORTED_CONTENT_TYPES, resource.getContentType()) &&
				(resource.getInputStream() != null);
	}
	
	protected boolean validate(AsyncAPI asyncapi) {
		
		if (asyncapi != null) {
			return true;
		}
		
		return false;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public void setJson(ObjectMapper json) {
		this.json = json;
	}

	public void setYaml(ObjectMapper yaml) {
		this.yaml = yaml;
	}

}
