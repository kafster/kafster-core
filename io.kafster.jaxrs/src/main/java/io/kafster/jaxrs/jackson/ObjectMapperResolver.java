/*
 * Copyright (c) 2016 osgitools.org
 *
 * This file is licensed to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.jackson;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ObjectMapperResolver implements ContextResolver<ObjectMapper> {
	
	private ObjectMapper objectMapper;

	public ObjectMapperResolver() throws Exception {
	    //SimpleModule module = new SimpleModule("CollectionOfAny");
	    //module.addSerializerModifier(new CollectionOfAnySerializer());
	    //objectMapper.registerModule(module);	    
	}
	
	public ObjectMapper getContext(Class<?> objectType) {
		if (objectMapper == null) {
		    objectMapper = new ObjectMapper()
		    		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		    		.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
		    		.setSerializationInclusion(Include.NON_EMPTY)
		    		.setSerializationInclusion(Include.NON_NULL);
		}		
		return objectMapper;
    }
	
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
}
