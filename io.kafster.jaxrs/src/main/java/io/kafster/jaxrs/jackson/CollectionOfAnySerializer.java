/*
 * Copyright 2012 osgitools.org
 *
 * This file is licensed to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.jackson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

/**
 * A BeanSerializerModifier that takes a Collection of Any and writes them as
 * top-level JSON fields in the containing object.  This results in JSON that is 
 * more similar to the XML serialization where the Any items appear as top-level
 * elements.  Without this, the Any items appear in a JSON property named "any",
 * because it uses regular bean properties, which looks kinda crummy.  With this,
 * any items in an Any collection will be put into a property corresponding to the
 * XmlRootElement "name" property for the object.  If none is specified, the simple
 * name of the class is used.  The JSON property will ALWAYS be an array, even if 
 * there is only one item of a given type in the collection.    
 * @author Copyright 2012 osgitools.org
 */
public class CollectionOfAnySerializer extends BeanSerializerModifier {
	
	private static final Logger log = LoggerFactory.getLogger(CollectionOfAnySerializer.class);

	@Override
	public List<BeanPropertyWriter> changeProperties(SerializationConfig config, BeanDescription beanDesc, List<BeanPropertyWriter> writers) {
		List<BeanPropertyWriter> result = new ArrayList<BeanPropertyWriter>();
		
		for (BeanPropertyWriter writer : writers) {
			AnnotatedMember member = writer.getMember();
			if (member.hasAnnotation(XmlAnyElement.class) && 
				!member.hasAnnotation(XmlElementRefs.class) &&
				writer.getType().isCollectionLikeType()) {
				
				// It's a Collection of Any 
				//
				result.add(new AnyCollectionWriter(writer));
			}
			else {
				result.add(writer);
			}
		}
		
		return result;
	}
	
	private interface Writer {
		public void write(JsonGenerator gen) throws Exception;
	}
	
	private class ArrayWriter implements Writer {

		private List<Writer> writers = new ArrayList<Writer>();
		private String name;
		public ArrayWriter(String name) {
			this.name = name;
		}
		
		public void addWriter(Writer writer) {
			writers.add(writer);
		}
		
		@Override
		public void write(JsonGenerator gen) throws Exception {
			gen.writeArrayFieldStart(name);
			for (Writer writer : writers) {
				writer.write(gen);
			}
			gen.writeEndArray();
		}
		
	}
	
	private class ObjectWriter implements Writer {
		
		private Object obj;
		public ObjectWriter(Object obj) {
			this.obj = obj;
		}

		@Override
		public void write(JsonGenerator gen) throws Exception {
			gen.writeObject(obj);
		}
		
	}
	
	private class ElementWriter implements Writer {
		
		private Element element;
		public ElementWriter(Element element) {
			this.element = element;
		}

		@Override
		public void write(JsonGenerator gen) throws Exception {
			gen.writeString(element.getTextContent());
		}
		
	}	
	
	private class AnyCollectionWriter extends BeanPropertyWriter {

		protected AnyCollectionWriter(BeanPropertyWriter base) {
			super(base);
		}
		
		protected ArrayWriter getWriter(String name, Map<String,ArrayWriter> writers) {
			ArrayWriter writer = writers.get(name);
			if (writer == null) {
				writer = new ArrayWriter(name);
				writers.put(name, writer);
			}
			return writer;
		}

		@Override
		public void serializeAsField(Object bean, JsonGenerator gen, SerializerProvider provider) throws Exception {
			Object target = get(bean);
			if (target != null) {
				Map<String,ArrayWriter> writers = new HashMap<String,ArrayWriter>();
				
				// The target is the actual collection
				//
				for (Object item : (Collection)target) {
					if (item != null) {
						if (item.getClass().isAnnotationPresent(XmlRootElement.class)) {
							// It's a JAXB object.  Always write each item as an array in case there
							// are multiple items of the same type.
							//
							XmlRootElement element = item.getClass().getAnnotation(XmlRootElement.class);
							String name = (element.name() != null) ? element.name() : item.getClass().getSimpleName();
							
							ArrayWriter writer = getWriter(name, writers);
							writer.addWriter(new ObjectWriter(item));
						}
						else if (item instanceof Element) {
							Element element = (Element)item;
							
							ArrayWriter writer = getWriter(element.getLocalName(), writers);
							writer.addWriter(new ElementWriter(element));
						}
						else {
							log.warn("Cannot serialize Any type [" + item.getClass().getName() + "]");
						}
					}
				}
				
				if (!writers.isEmpty()) {
					for (Writer writer : writers.values()) {
						writer.write(gen);
					}
				}
			}
		}
	}

}
