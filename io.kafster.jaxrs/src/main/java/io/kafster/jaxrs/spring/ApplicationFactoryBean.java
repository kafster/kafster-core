/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.spring;

import java.util.Set;

import javax.ws.rs.core.Application;

import org.springframework.beans.factory.FactoryBean;

public class ApplicationFactoryBean implements FactoryBean<Application> {

	private Set<Class<?>> classes;
	private Set<Object> singletons;
	
	@Override
	public Application getObject() throws Exception {
		return new Application() {
			@Override
			public Set<Class<?>> getClasses() {
				return classes;
			}
			@Override
			public Set<Object> getSingletons() {
				return singletons;
			}
		};
	}

	@Override
	public Class<?> getObjectType() {
		return Application.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
	
	public void setClasses(Set<Class<?>> classes) {
		this.classes = classes;
	}
	
	public void setSingletons(Set<Object> singletons) {
		this.singletons = singletons;
	}

}