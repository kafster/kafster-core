/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

public class DataSourceStreamingOutput implements StreamingOutput {
	
	private DataSource source;
	private boolean buffer;
	
	public DataSourceStreamingOutput(DataSource source) {
		this(source, false);
	}
	
	public DataSourceStreamingOutput(DataSource source, boolean buffer) {
		this.source = source;
		this.buffer = buffer;
	}	

	public void write(OutputStream ostrm) throws IOException, WebApplicationException {
		
		InputStream in = source.getInputStream();
		if (buffer && (in != null) && in.markSupported()) {
			in.mark(Integer.MAX_VALUE);
		}
		
		DataHandler handler = new DataHandler(source);
		handler.writeTo(ostrm);
		
		if (buffer && (in != null) && in.markSupported()) {
			in.reset();
		}
	}
	
}
