/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.multipart.provider;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemHeaders;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.UploadContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.io.Resource;
import io.kafster.io.util.SimpleResource;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.jaxrs.multipart.Part;

@Provider
@Consumes("multipart/form-data")
public class MultipartProvider implements MessageBodyReader<Multipart> {
	
	private static Logger log = LoggerFactory.getLogger(MultipartProvider.class);
	
	private int sizeThreshold = 10000;
	private String repository = System.getProperty("java.io.tmpdir");
	private FileItemFactory factory; 
	
	public synchronized void setSizeThreshold(int size) {
		this.sizeThreshold = size;
		this.factory = null;
	}
	
	public synchronized void setRepository(String repository) {
		this.repository = repository;
		this.factory = null;
	}
	
	protected synchronized FileItemFactory getFactory() {
		if (factory == null) {
			factory = new DiskFileItemFactory(sizeThreshold, new File(repository));
		}
		return factory;
	}

	@Override
	public boolean isReadable(Class<?> clazz, Type type, Annotation[] annotations, MediaType mediaType) {
		return Multipart.class.isAssignableFrom(clazz);
	}

	@Override
	public Multipart readFrom(Class<Multipart> type, 
								Type genericType, 
								Annotation[] annotations, 
								MediaType mediaType,
								MultivaluedMap<String, String> headers, 
								InputStream istrm) throws IOException, WebApplicationException {
		
		try {
			FileUpload upload = new ServletFileUpload(getFactory());
			List<FileItem> items = upload.parseRequest(new ProviderUploadContext(mediaType, headers, istrm));
			
			return new ListMultiPart(items);
		}
		catch (FileUploadException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new IOException(e);
		}
	}
	
	protected class ListMultiPart implements Multipart {
		
		private Iterable<FileItem> items;
		public ListMultiPart(Iterable<FileItem> items) {
			this.items = items;
		}

		@Override
		public Iterator<Part> iterator() {
			return new PartIterator(items.iterator());
		}
	}
	
	protected class PartIterator implements Iterator<Part> {
		
		private Iterator<FileItem> iterator;
		public PartIterator(Iterator<FileItem> iterator) {
			this.iterator = iterator;
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public Part next() {
			FileItem item = iterator.next();
			return new FileItemPart(item);
		}
	}
	
	protected class FileItemPart implements Part {
		
		private FileItem item;
		private MultivaluedMap<String, String> headers;
		
		public FileItemPart(FileItem item) {
			this.item = item;
		}

		@Override
		public MultivaluedMap<String, String> getHeaders() throws IOException {
			if (headers == null) {
				FileItemHeaders incoming = item.getHeaders();
				if (incoming != null) {
					headers = new MultivaluedHashMap<String, String>();
					for (String name : IteratorUtils.asIterable(incoming.getHeaderNames())) {
						Iterator<String> values = incoming.getHeaders(name);
						if (values != null) {
							List<String> v = new ArrayList<String>();
							for (String value : IteratorUtils.asIterable(values)) {
								v.add(value);
							}
							headers.put(name, v);
						}
					}
				}
			}
			return headers;
		}

		@Override
		public Resource getContent() throws IOException {
			InputStream istrm = item.getInputStream();
			
			FileItemHeaders headers = item.getHeaders();
			if (headers != null) {
				String encoding = headers.getHeader("Content-Transfer-Encoding");
				if (StringUtils.equalsIgnoreCase(encoding, "base64")) {
					istrm = new Base64InputStream(istrm);
				}
			}
			
			return new SimpleResource(istrm, item.getContentType(), item.getName());
		}

		@Override
		public Type getType() {
			return item.isFormField() ? Type.FORM : Type.FILE;
		}
	}
	
	protected class ProviderUploadContext implements UploadContext {
		
		private MediaType mediaType;
		private MultivaluedMap<String, String> headers;
		private InputStream istrm;
		
		public ProviderUploadContext(MediaType mediaType, MultivaluedMap<String, String> headers, InputStream istrm) {
			this.mediaType = mediaType;
			this.headers = headers;
			this.istrm = istrm;
		}

		@Override
		public String getCharacterEncoding() {
			Map<String,String> params = mediaType.getParameters();
			if (params != null) {
				return params.get("encoding");
			}
			return null;
		}

		@Override
		public int getContentLength() {
			return (int)contentLength();
		}

		@Override
		public String getContentType() {
			return mediaType.toString();
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return istrm;
		}

		@Override
		public long contentLength() {
	        try {
	            return Long.parseLong(headers.getFirst(FileUploadBase.CONTENT_LENGTH));
	        } catch (NumberFormatException e) {
	            if (log.isTraceEnabled()) {
	            	log.error(e.getLocalizedMessage(), e);
	            }
	        }
	        return -1;
		}
		
	}

}

