/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kaftser.persistence.bitsy;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.text.StringSubstitutor;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lambdazen.bitsy.BitsyGraph;

import io.kafster.persistence.graph.tx.GraphTransactionContext;
import io.kafster.persistence.transaction.TransactionContext;

public class BitsyGraphFactory implements ManagedServiceFactory, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(BitsyGraphFactory.class);
	
	public static final String PID = "kafster.bitsygraph";
	private static final String PATH_PROP = "path";
	
	private BundleContext context;
	private Map<String,Service> graphs = new HashMap<>();
	
	private boolean allowFullGraphScans = true;
	private long txLogThreshold = BitsyGraph.DEFAULT_TX_LOG_THRESHOLD;
	private double reorgFactor = BitsyGraph.DEFAULT_REORG_FACTOR;
	private boolean createDirIfMissing = true;

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.context = ctx;
	}

	@Override
	public String getName() {
		return "Kafster BitsyGraph Configuration";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		deleted(pid);
		
		try {
			Service svcs = new Service(properties);
			svcs.register();
			
			graphs.put(pid, svcs);	
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public void deleted(String pid) {
		Service svc = graphs.remove(pid);
		if (svc != null) {
			svc.close();
		}		
	}
	
	public void shutdown() {
		for (Service service : graphs.values()) {
			try {
				service.close();
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		
		graphs.clear();
	}
	
	@SuppressWarnings("rawtypes")
	protected class Service {
		
		private ServiceRegistration<Graph> registration;
		private ServiceRegistration<TransactionContext> transactions;
		
		private Dictionary props;
		
		private BitsyGraph graph;
		
		public Service(Dictionary props) {
			this.props = props;
		}
		
		public void close() {
			
			if (registration != null) {
				registration.unregister();
			}
			
			if (transactions != null) {
				transactions.unregister();
			}
			
			if (graph != null) {
				graph.shutdown();
			}
				
		}
		
		@SuppressWarnings({ "unchecked" })
		public void register() {
			
			String path = (String)props.get(PATH_PROP);
			if (path != null) {
				
				// Allow system properties in path
				//
				path = StringSubstitutor.replaceSystemProperties(path);
				
				boolean allowScans = allowFullGraphScans;
				long txThreashold = txLogThreshold;
				double reorg = reorgFactor;
				boolean create = createDirIfMissing;
				
				String allowScansConfig = (String)props.get(BitsyGraph.ALLOW_FULL_GRAPH_SCANS_KEY);
				if (allowScansConfig != null) {
					allowScans = Boolean.parseBoolean(allowScansConfig);
				}
				
				String thresholdConfig = (String)props.get(BitsyGraph.TX_LOG_THRESHOLD_KEY);
				if (thresholdConfig != null) {
					txThreashold = Long.parseLong(thresholdConfig);
				}
				
				String reorgConfig = (String)props.get(BitsyGraph.REORG_FACTOR_KEY);
				if (reorgConfig != null) {
					reorg = Double.parseDouble(reorgConfig);
				}
				
				String createConfig = (String)props.get(BitsyGraph.CREATE_DIR_IF_MISSING_KEY);
				if (createConfig != null) {
					create = Boolean.parseBoolean(createConfig);
					
					// Bitsy doesn't create subdirs
					//
					if (create) {
						File file = new File(path);
						if (!file.exists()) {
							file.mkdirs();
						}
					}
				}
				
		        this.graph = new BitsyGraph(FileSystems.getDefault().getPath(path), allowScans, txThreashold, reorg, create);	
		        log.trace("Created BitsyGraph [" + graph + "]");
		        
				registration = context.registerService(Graph.class, graph, props);
				transactions = context.registerService(TransactionContext.class, new GraphTransactionContext(graph), props);
			}
		}

	}	
	
	public void setAllowFullGraphScans(boolean allowFullGraphScans) {
		this.allowFullGraphScans = allowFullGraphScans;
	}

	public void setTxLogThreshold(long txLogThreshold) {
		this.txLogThreshold = txLogThreshold;
	}

	public void setReorgFactor(double reorgFactor) {
		this.reorgFactor = reorgFactor;
	}

	public void setCreateDirIfMissing(boolean createDirIfMissing) {
		this.createDirIfMissing = createDirIfMissing;
	}	

}


