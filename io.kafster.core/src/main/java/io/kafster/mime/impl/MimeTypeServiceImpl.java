/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.mime.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.activation.FileTypeMap;
import javax.activation.MimetypesFileTypeMap;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.mime.MimeTypeService;

public class MimeTypeServiceImpl implements MimeTypeService, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(MimeTypeServiceImpl.class);
	
	private static final String MIME_TYPES_FILE = "META-INF/mime.types";
	
	private BundleContext ctx;
	private FileTypeMap TYPES; 
	
	private String location = MIME_TYPES_FILE;
	
	protected FileTypeMap getTypeMap() {
		if (TYPES == null) {
			URL resource = ctx.getBundle().getResource(location);
			if (resource != null) {
				try (InputStream istrm = resource.openStream()) {
					TYPES = new MimetypesFileTypeMap(istrm);
				} 
				catch (IOException e) {
					log.error("Error Reading [" + location + "]", e);
				}
			}
		}
		return TYPES;
	}

	@Override
	public String getMimeType(String name) {
		FileTypeMap types = getTypeMap();
		if (types != null) {
			return types.getContentType(name);
		}
		return null;
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

}
