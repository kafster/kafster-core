/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jexl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.introspection.JexlSandbox;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.FactoryBean;

import io.kafster.jexl.annotation.JexlNamespace;

public class JexlEngineFactoryBean implements FactoryBean<JexlEngine> {
	
	private int cache = 256;
	private boolean strict = true;
	private boolean sandbox = false;
	private boolean open = false;
	private Map<String,Object> namespaces = new ConcurrentHashMap<>();
	
	private JexlEngine engine;

	@Override
	public JexlEngine getObject() throws Exception {
		if (engine == null) {
			JexlBuilder builder = new JexlBuilder()
								.cache(cache)
								.strict(strict)
								.namespaces(namespaces);
			if (sandbox) {
				builder.sandbox(new JexlSandbox(open));
			}
			
			engine = builder.create();
		}
		return engine;
	}

	@Override
	public Class<?> getObjectType() {
		return JexlEngine.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setCacheSize(int cache) {
		this.cache = cache;
	}

	public void setStrict(boolean strict) {
		this.strict = strict;
	}

	public void setNamespaces(Map<String, Object> namespaces) {
		this.namespaces.putAll(namespaces);
	}
	
	protected Map<String,Object> getNamespaces(NamespaceRegistration registration) {
		
		Map<String,Object> result = new HashMap<>();
		
		for (Object namespace : registration.getNamespaces()) {
			if (namespace.getClass().isAnnotationPresent(JexlNamespace.class)) {
				
				JexlNamespace annotation = namespace.getClass().getAnnotation(JexlNamespace.class);
				
				String prefix = StringUtils.trimToNull(annotation.prefix());
				if (prefix != null) {
					result.put(prefix, namespace);
				}
			}
		}
		
		return result;
	}
	
	public synchronized void addNamespace(NamespaceRegistration registration, Map<String,Object> props) {
		
		if ((registration == null) || (registration.getNamespaces() == null)) {
			return;
		}
		
		Map<String,Object> map = getNamespaces(registration);
		if ((map != null) && !map.isEmpty()) {
			namespaces.putAll(map);
		}
	}
	
	public synchronized void removeNamespace(NamespaceRegistration registration, Map<String,Object> props) {
		
		if ((registration == null) || (registration.getNamespaces() == null)) {
			return;
		}
		
		Map<String,Object> map = getNamespaces(registration);
		if ((map != null) && !map.isEmpty()) {
			map.keySet().forEach(k -> namespaces.remove(k));
		}
	}	

}
