/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.DataStreamLookup;

public class CompositeDataStreamLookup implements DataStreamLookup {
	
	private static final Logger log = LoggerFactory.getLogger(CompositeDataStreamLookup.class);
	
	private List<DataStreamLookup> lookups;

	@Override
	public Optional<DataStream> getDataStream(String key) throws Exception {
		for (DataStreamLookup lookup : lookups) {
			Optional<DataStream> result = lookup.getDataStream(key);
			if (result.isPresent()) {
				return result;
			}
		}
		return null;
	}

	@Override
	public Iterable<? extends DataStream> getDataStreams() throws Exception {
		Iterable<? extends DataStream> result = IterableUtils.emptyIterable();
		for (DataStreamLookup lookup : lookups) {
			try {
				Iterable<? extends DataStream> streams = lookup.getDataStreams();
				if (streams != null) {
					result = IterableUtils.chainedIterable(result, streams);
				}
			}
			catch (Exception e) {
				log.error("DataStream Error", e);
			}
		}
		return result;
	}

	public void setLookups(List<DataStreamLookup> lookups) {
		this.lookups = lookups;
	}

}
