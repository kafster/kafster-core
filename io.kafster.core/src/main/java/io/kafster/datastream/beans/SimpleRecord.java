/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream.beans;

import java.util.List;
import java.util.Map;

import io.kafster.datastream.Record;

public class SimpleRecord implements Record {
	
	private String key;
	private Long timestamp;
	private Long offset;
	private Map<String,List<String>> headers;
	private Object content;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public Long getTimestamp() {
		return timestamp;
	}

	@Override
	public Map<String,List<String>> getHeaders() {
		return headers;
	}

	@Override
	public Object getContent() {
		return content;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setHeaders(Map<String,List<String>> headers) {
		this.headers = headers;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	@Override
	public Long getOffset() {
		return offset;
	}

	public void setOffset(Long offset) {
		this.offset = offset;
	}

}
