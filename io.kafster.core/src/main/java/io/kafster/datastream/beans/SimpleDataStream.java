/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream.beans;

import io.kafster.datastream.DataStream;

public class SimpleDataStream implements DataStream {
	
	private String key;
	private String name;
	private String description;
	private String contentType;
	
	private ReaderBuilder reader;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public ReaderBuilder getReader() {
		return reader;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setReader(ReaderBuilder reader) {
		this.reader = reader;
	}

}
