/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream.beans;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.io.output.CountingOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.DataStream.Reader;
import io.kafster.datastream.DataStream.ReaderBuilder;
import io.kafster.datastream.Record;
import io.kafster.io.Resource;

public class JacksonResourceReaderBuilder implements DataStream.ReaderBuilder {
	
	private static final Logger log = LoggerFactory.getLogger(JacksonResourceReaderBuilder.class);
	
	private Resource resource;
	private ObjectMapper mapper;
	
	protected Long notBefore;
	protected Long notAfter;
	protected Long notEarlierThan;
	protected Long notLaterThan;
	protected Integer notMoreThan;
	protected Set<String> fields;
	
	private int maxSize = 200000;
	
	private Reader reader;
	
	public JacksonResourceReaderBuilder(Resource resource, ObjectMapper mapper) {
		this.resource = resource;
		this.mapper = mapper;
	}

	@Override
	public ReaderBuilder notBefore(long offset) {
		notBefore = offset;
		return this;
	}

	@Override
	public ReaderBuilder notAfter(long offset) {
		notAfter = offset;
		return this;
	}

	@Override
	public ReaderBuilder notEarlierThan(long timestamp) {
		notEarlierThan = timestamp;
		return this;
	}

	@Override
	public ReaderBuilder notLaterThan(long timestamp) {
		notLaterThan = timestamp;
		return this;
	}

	@Override
	public ReaderBuilder notMoreThan(int count) {
		notMoreThan = count;
		return this;
	}
	
	@Override
	public ReaderBuilder field(String field) {
		if (fields == null) {
			fields = new HashSet<>();
		}
		fields.add(field);
		return this;
	}
	

	@Override
	public ReaderBuilder fields(Collection<String> added) {
		if (fields == null) {
			fields = new HashSet<>();
		}
		fields.addAll(added);
		return this;
	}	
	
	protected JsonNode parseAsArray(InputStream istrm) throws IOException {
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(istrm))) {
			JsonNode result = mapper.readTree(reader);
			if ((fields != null) && (result instanceof ArrayNode)) {
				for (JsonNode node : result) {
					if (node instanceof ObjectNode) {
						((ObjectNode)node).retain(fields);
					}
				}
			}
			return result;
		}		
	}
	
	protected JsonNode parseAsSeries(InputStream istrm) throws IOException {
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(istrm))) {
			
			ArrayNode array = mapper.getNodeFactory().arrayNode();
			
			String line = reader.readLine();
			while (line != null)  {
				
				JsonNode node = mapper.readTree(line);
				if ((fields != null) && (node instanceof ObjectNode)) {
					((ObjectNode)node).retain(fields);
				}
				
				array.add(node);
				line = reader.readLine();
			}
				
			return array;
		}		
	}	
	
	protected JsonNode parse(Resource resource) throws IOException {
		
		InputStream istrm = new BufferedInputStream(resource.getInputStream());
		
		istrm.mark(8);
		char start = (char)istrm.read();
		istrm.reset();
		
		switch (start) {
			case '{':
				return parseAsSeries(istrm);
			case '[':
				return parseAsArray(istrm);
		}

		return null;
	}

	@Override
	public Reader build() throws IOException {
		if (reader == null) {
			reader = new JsonNodeReader(parse(resource));
		}
		return reader;
	}
	
	protected class JsonNodeReader implements Reader {
		
		private JsonNode node;
		
		public JsonNodeReader(JsonNode node) {
			this.node = node;
		}

		@Override
		public Integer getCount() {
			if (node.isArray()) {
				return IterableUtils.size(node);
			}
			return 0;
		}

		@Override
		public Long getStartOffset() {
			return null;
		}

		@Override
		public Long getEndOffset() {
			return null;
		}

		@Override
		public Long getStartTimestamp() {
			return null;
		}

		@Override
		public Long getEndTimestamp() {
			return null;
		}

		@Override
		public Iterable<? extends Record> getRecords() throws Exception {
			if (node.isArray()) {
				return IterableUtils.transformedIterable(node, n -> new JsonNodeRecord(n));
			}
			return IterableUtils.emptyIterable();
		}

		@Override
		public int writeTo(OutputStream ostrm) throws IOException {
			try (CountingOutputStream counter = new CountingOutputStream(ostrm)) {
				mapper.writeValue(counter, node);
				return counter.getCount();
			}
		}
		
	}
	
	protected class JsonNodeRecord implements Record {
		
		private JsonNode node;
		
		public JsonNodeRecord(JsonNode node) {
			this.node = node;
		}

		@Override
		public String getKey() {
			return null;
		}

		@Override
		public Long getOffset() {
			return null;
		}

		@Override
		public Long getTimestamp() {
			return null;
		}

		@Override
		public Map<String,List<String>> getHeaders() {
			return null;
		}

		@Override
		public Object getContent() {
			return node;
		}
		
	}

}
