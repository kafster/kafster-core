/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream;

import java.io.InputStream;

public interface DataStreamFactory {
	
	DataStreamBuilder create() throws Exception;

	interface DataStreamBuilder {
		DataStream build() throws Exception;
		
		DataStreamBuilder key(String key);
		DataStreamBuilder name(String name);
		DataStreamBuilder description(String description);
		DataStreamBuilder contentType(String contentType);
		DataStreamBuilder record(Record record);
		DataStreamBuilder records(Iterable<? extends Record> records);
		DataStreamBuilder records(InputStream records);
	}
}
