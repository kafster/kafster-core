/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.datastream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

public interface DataStream {

	String getKey();
	String getName();
	String getDescription();
	String getContentType();
	
	ReaderBuilder getReader();
	
	interface Reader {
		Integer getCount();
		Long getStartOffset();
		Long getEndOffset();
		Long getStartTimestamp();
		Long getEndTimestamp();
		
		Iterable<? extends Record> getRecords() throws Exception;
		int writeTo(OutputStream ostrm) throws IOException;
	}
	
	interface ReaderBuilder {
		
		ReaderBuilder notBefore(long offset);
		ReaderBuilder notAfter(long offset);
		ReaderBuilder notEarlierThan(long timestamp);
		ReaderBuilder notLaterThan(long timestamp);
		ReaderBuilder notMoreThan(int count);
		ReaderBuilder field(String field);
		ReaderBuilder fields(Collection<String> fields);
		
		Reader build() throws Exception;
	}
}
