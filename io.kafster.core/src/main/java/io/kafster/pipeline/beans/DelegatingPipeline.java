/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.time.Instant;

import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;

public class DelegatingPipeline implements Pipeline {
	
	private Pipeline pipeline;
	
	public DelegatingPipeline(Pipeline pipeline) {
		this.pipeline = pipeline;
	}
	
	public DelegatingPipeline() {
	}	

	public void setPipeline(Pipeline pipeline) {
		this.pipeline = pipeline;
	}
	
	protected Pipeline getDelegate() {
		return pipeline;
	}
	
	@Override
	public String getKey() {
		return getDelegate().getKey();
	}

	@Override
	public String getName() {
		return getDelegate().getName();
	}

	@Override
	public String getVersion() {
		return getDelegate().getVersion();
	}

	@Override
	public String getDescription() {
		return getDelegate().getDescription();
	}

	@Override
	public Instant getCreated() {
		return getDelegate().getCreated();
	}

	@Override
	public Instant getModified() {
		return getDelegate().getModified();
	}

	@Override
	public PipelineDestination getSource() {
		return getDelegate().getSource();
	}

	@Override
	public PipelineDestination getTarget() {
		return getDelegate().getTarget();
	}

	@Override
	public Iterable<? extends PipelineStage> getStages() {
		return getDelegate().getStages();
	}


}
