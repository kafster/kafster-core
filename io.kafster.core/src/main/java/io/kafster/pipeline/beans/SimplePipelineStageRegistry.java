/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.pipeline.PipelineStageRegistry;

public class SimplePipelineStageRegistry implements PipelineStageRegistry {
	
	private Map<String,PipelineStageMetadata> stages = new ConcurrentHashMap<>();

	@Override
	public PipelineStageMetadata getStage(String query) throws Exception {
		return stages.get(query);
	}

	@Override
	public Iterable<PipelineStageMetadata> getStages() throws Exception {
		return stages.values();
	}
	
	public void addStage(PipelineStageMetadata stage) {
		stages.put(stage.getPresentationName(), stage);
	}
	
	public PipelineStageMetadata removeStage(PipelineStageMetadata stage) {
		return stages.remove(stage.getPresentationName());
	}
	
	public void setStages(List<PipelineStageMetadata> stages) {
		if (stages != null) {
			for (PipelineStageMetadata stage : stages) {
				this.stages.put(stage.getName(), stage);
			}
		}
	}
	
	public void stageAdded(PipelineStageMetadata stage, Map<String,Object> properties) {
		if (stage != null) {
			addStage(stage);
		}
	}
	
	public void stageRemoved(PipelineStageMetadata stage, Map<String,Object> properties) {
		if (stage != null) {
			removeStage(stage);
		}
	}	

}
