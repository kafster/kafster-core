/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import io.kafster.pipeline.MutablePipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;

public class SimplePipeline implements MutablePipeline {
	
	private String key;
	private String name;
	private String version;
	private String description;
	private Instant created;
	private Instant modified;
	private PipelineDestination source;
	private PipelineDestination target;
	private List<PipelineStage> stages;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Instant getCreated() {
		return created;
	}

	@Override
	public Instant getModified() {
		return modified;
	}

	@Override
	public PipelineDestination getSource() {
		return source;
	}

	@Override
	public PipelineDestination getTarget() {
		return target;
	}

	@Override
	public Iterable<PipelineStage> getStages() {
		return stages;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setCreated(Instant created) {
		this.created = created;
	}

	@Override
	public void setModified(Instant modified) {
		this.modified = modified;
	}

	@Override
	public void setSource(PipelineDestination source) {
		this.source = source;
	}

	@Override
	public void setTarget(PipelineDestination target) {
		this.target = target;
	}

	public void setStages(List<PipelineStage> stages) {
		this.stages = stages;
	}
	
	@Override
	public PipelineStageBuilder createStage(PipelineStageMetadata metadata) {
		if (stages == null) {
			stages = new ArrayList<>();
		}
		
		return new SimplePipelineStageBuilder(metadata) {

			@Override
			public PipelineStage build() {
				PipelineStage result = super.build();
				stages.add(result);
				return result;
			}
		};
	}
	
	public PipelineStage addStage(PipelineStage stage) {
		if (stages == null) {
			stages = new ArrayList<>();
		}
		
		stages.add(stage);
		
		return stage;
	}

	@Override
	public boolean removeStage(PipelineStage stage) throws Exception {
		if (stages != null) {
			return stages.remove(stage);
		}
		return false;
	}

}
