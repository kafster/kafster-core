/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineBuilder;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.schema.SchemaType;

public class SimplePipelineBuilder implements PipelineBuilder {
	
	private SimplePipeline pipeline;
	
	public SimplePipelineBuilder() {
		this.pipeline = new SimplePipeline();
	}	
	
	public SimplePipelineBuilder(SimplePipeline pipeline) {
		this.pipeline = pipeline;
	}		

	@Override
	public PipelineBuilder key(String key) {
		pipeline.setKey(key);
		return this;
	}

	@Override
	public PipelineBuilder name(String name) {
		pipeline.setName(name);
		return this;
	}

	@Override
	public PipelineBuilder description(String description) {
		pipeline.setDescription(description);
		return this;
	}

	@Override
	public PipelineBuilder version(String version) {
		pipeline.setVersion(version);
		return this;
	}

	@Override
	public PipelineBuilder modified(Instant modified) {
		pipeline.setModified(modified);
		return this;
	}

	@Override
	public PipelineBuilder created(Instant created) {
		pipeline.setCreated(created);
		return this;
	}

	public PipelineBuilder source(PipelineDestination source) {
		pipeline.setSource(source);
		return this;
	}

	public PipelineBuilder target(PipelineDestination target) {
		pipeline.setTarget(target);
		return this;
	}

	@Override
	public PipelineBuilder stage(PipelineStage stage) throws Exception {
		if (pipeline.getStages() == null) {
			pipeline.setStages(new ArrayList<>());
		}
		((List<PipelineStage>)pipeline.getStages()).add(stage);
		return this;
	}

	@Override
	public Pipeline build() throws Exception {
		return pipeline;
	}

	@Override
	public PipelineStageBuilder stage(PipelineStageMetadata metadata) throws Exception {
		return new SimplePipelineStageBuilder(metadata);
	}

	@Override
	public PipelineBuilder source(String as) {
		return source(new SimplePipelineDestination(as));
	}

	@Override
	public PipelineBuilder source(String as, SchemaType type) {
		return source(new SimplePipelineDestination(as, type));
	}

	@Override
	public PipelineBuilder target(String as) {
		return target(new SimplePipelineDestination(as));
	}

	@Override
	public PipelineBuilder target(String as, SchemaType type) {
		return target(new SimplePipelineDestination(as, type));
	}

}
