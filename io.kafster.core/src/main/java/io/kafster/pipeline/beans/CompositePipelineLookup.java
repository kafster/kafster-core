/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;

public class CompositePipelineLookup implements PipelineLookup {
	
	private List<PipelineLookup> lookups;
	
	public void setLookups(List<PipelineLookup> lookups) {
		this.lookups = lookups;
	}	

	@Override
	public Optional<Pipeline> getPipeline(String query) throws Exception {
		for (PipelineLookup registry : lookups) {
			Optional<Pipeline> pipeline = registry.getPipeline(query);
			if (pipeline.isPresent()) {
				return pipeline;
			}
		}
		return Optional.empty();
	}

	@Override
	public Iterable<? extends Pipeline> getPipelines() throws Exception {
		Iterable<Pipeline> result = IterableUtils.emptyIterable();
		for (PipelineLookup registry : lookups) {
			Iterable<? extends Pipeline> pipelines = registry.getPipelines();
			if (pipelines != null) {
				result = IterableUtils.chainedIterable(pipelines, result);
			}
		}
		return result;
	}
	
}
