/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;

public class SimplePipelineLookup implements PipelineLookup {
	
	private Map<String,Pipeline> pipelines = new ConcurrentHashMap<>();

	@Override
	public Optional<Pipeline> getPipeline(String query) {
		return Optional.ofNullable(pipelines.get(query));
	}
	
	public void addPipeline(Pipeline pipeline) {
		pipelines.put(pipeline.getName(), pipeline);
	}
	
	public Pipeline removePipeline(Pipeline pipeline) {
		return pipelines.remove(pipeline.getName());
	}
	
	public void setPipelines(List<Pipeline> pipelines) {
		if (pipelines != null) {
			for (Pipeline pipeline : pipelines) {
				this.pipelines.put(pipeline.getKey(), pipeline);
			}
		}
	}
	
	public void pipelineAdded(Pipeline pipeline, Map<String,Object> properties) {
		if (pipeline != null) {
			addPipeline(pipeline);
		}
	}
	
	public void pipelineRemoved(Pipeline pipeline, Map<String,Object> properties) {
		if (pipeline != null) {
			removePipeline(pipeline);
		}
	}

	@Override
	public Iterable<Pipeline> getPipelines() throws Exception {
		return pipelines.values();
	}	

}
