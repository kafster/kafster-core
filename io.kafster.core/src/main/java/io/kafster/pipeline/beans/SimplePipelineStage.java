/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.Map;

import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageMetadata;

public class SimplePipelineStage implements PipelineStage {
	
	private String key;
	private String condition;
	private Map<String,Object> configuration;
	private PipelineStageMetadata metadata;
	
	public SimplePipelineStage() {
	}

	public SimplePipelineStage(PipelineStageMetadata metadata) {
		this.metadata = metadata;
	}
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getCondition() {
		return condition;
	}

	@Override
	public Map<String, Object> getConfiguration() {
		return configuration;
	}

	@Override
	public PipelineStageMetadata getMetadata() {
		return metadata;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setConfiguration(Map<String, Object> configuration) {
		this.configuration = configuration;
	}

	public void setMetadata(PipelineStageMetadata metadata) {
		this.metadata = metadata;
	}

}
