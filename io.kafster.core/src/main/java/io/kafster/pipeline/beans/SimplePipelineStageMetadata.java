/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.net.URL;

import javax.activation.URLDataSource;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.io.Resource;
import io.kafster.io.util.DataSourceResource;
import io.kafster.mime.MimeTypeService;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.schema.SchemaType;
import io.kafster.schema.util.SchemaUtils;

public class SimplePipelineStageMetadata implements PipelineStageMetadata, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(SimplePipelineStageMetadata.class);
	
	private String name;
	private String presentationName;
	private String description;
	private String imageLocation;
	private Object defaultConfiguration;
	
	private BundleContext ctx;
	private SchemaType schema;
	
	private MimeTypeService mimeTypes;
	
	private Resource image;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getPresentationName() {
		return presentationName;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public synchronized Resource getImage() {
		
		if (image == null) {
			
			if ((imageLocation != null) && (ctx != null)) {
				URL url = ctx.getBundle().getEntry(imageLocation);
				if (url != null) {
					DataSourceResource result = new DataSourceResource(new URLDataSource(url));
					result.setContentType(getContentType(imageLocation));
					result.setModified(ctx.getBundle().getLastModified());
					image = result;
				}
			} 
		}
		
		return image;
	}
	
	protected String getContentType(String location) {
		return mimeTypes.getMimeType(location);
	}

	@Override
	public synchronized SchemaType getSchema() throws Exception {
		if ((schema == null) && (defaultConfiguration != null)) {
			try {
				schema = SchemaUtils.getSchema(defaultConfiguration.getClass());
			}
			catch (Exception e) {
				log.error("Could not load Schema", e);
				throw e;
			}
		}
		return schema;
	}

	public void setPresentationName(String displayName) {
		this.presentationName = displayName;
	}

	public String getImageLocation() {
		return imageLocation;
	}

	public void setImageLocation(String imageLocation) {
		this.imageLocation = imageLocation;
	}

	public Object getDefaultConfiguration() {
		return defaultConfiguration;
	}

	public void setDefaultConfiguration(Object defaultConfiguration) {
		this.defaultConfiguration = defaultConfiguration;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}

}
