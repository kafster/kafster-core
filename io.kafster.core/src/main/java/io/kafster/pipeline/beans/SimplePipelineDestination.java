/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import io.kafster.pipeline.PipelineDestination;
import io.kafster.schema.SchemaType;

public class SimplePipelineDestination implements PipelineDestination {
	
	private String as;
	private SchemaType type;
	
	public SimplePipelineDestination() {
	}

	public SimplePipelineDestination(String as) {
		this.as = as;
	}
	
	public SimplePipelineDestination(String as, SchemaType type) {
		this.as = as;
		this.type = type;
	}	
	
	@Override
	public String getAs() {
		return as;
	}

	@Override
	public void setAs(String as) {
		this.as = as;
	}

	@Override
	public SchemaType getType() {
		return type;
	}

	@Override
	public void setType(SchemaType type) {
		this.type = type;
	}

}
