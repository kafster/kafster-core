/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.Map;

import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;

public class SimplePipelineStageBuilder implements PipelineStageBuilder {
	
	private SimplePipelineStage stage;
	
	public SimplePipelineStageBuilder() {
	}

	public SimplePipelineStageBuilder(PipelineStageMetadata metadata) {
		stage.setMetadata(metadata);
	}
	
	@Override
	public PipelineStageBuilder condition(String expression) {
		stage.setCondition(expression);
		return this;
	}

	@Override
	public PipelineStageBuilder key(String key) {
		stage.setKey(key);
		return this;
	}

	@Override
	public PipelineStageBuilder configuration(Map<String, Object> props) {
		stage.setConfiguration(props);
		return this;
	}

	@Override
	public PipelineStage build() {
		return stage;
	}

}
