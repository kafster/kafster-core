/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline.beans;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.pipeline.PipelineStageRegistry;

public class CompositePipelineStageRegistry implements PipelineStageRegistry {
	
	private List<PipelineStageRegistry> registries;

	@Override
	public PipelineStageMetadata getStage(String query) throws Exception {
		for (PipelineStageRegistry registry : registries) {
			PipelineStageMetadata result = registry.getStage(query);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	@Override
	public Iterable<PipelineStageMetadata> getStages() throws Exception {
		Iterable<PipelineStageMetadata> result = IterableUtils.emptyIterable();
		for (PipelineStageRegistry registry : registries) {
			Iterable<PipelineStageMetadata> metadata = registry.getStages();
			if (metadata != null) {
				result = IterableUtils.chainedIterable(metadata, result);
			}
		}
		return result;
	}

	public void setRegistries(List<PipelineStageRegistry> registries) {
		this.registries = registries;
	}

}
