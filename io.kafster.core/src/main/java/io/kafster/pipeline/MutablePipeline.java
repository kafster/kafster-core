/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline;

import java.time.Instant;

public interface MutablePipeline extends Pipeline {

	void setKey(String key);	
	void setName(String name);
	void setDescription(String description);
	void setVersion(String version);
	
	void setModified(Instant modified);
	void setCreated(Instant created);
	
	void setSource(PipelineDestination source);
	void setTarget(PipelineDestination target);
	
	PipelineStageBuilder createStage(PipelineStageMetadata metadata) throws Exception;
	
	PipelineStage addStage(PipelineStage stage) throws Exception;
	boolean removeStage(PipelineStage stage) throws Exception;	
}
