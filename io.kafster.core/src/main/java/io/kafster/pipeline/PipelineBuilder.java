/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.pipeline;

import java.time.Instant;

import io.kafster.schema.SchemaType;

public interface PipelineBuilder {

	PipelineBuilder key(String key);	
	PipelineBuilder name(String name);
	PipelineBuilder description(String description);
	PipelineBuilder version(String version);
	
	PipelineBuilder modified(Instant modified);
	PipelineBuilder created(Instant created);
	
	PipelineBuilder source(String as);
	PipelineBuilder source(String as, SchemaType type);
	
	PipelineBuilder target(String as);
	PipelineBuilder target(String as, SchemaType type);	
	
	PipelineStageBuilder stage(PipelineStageMetadata metadata) throws Exception;
	PipelineBuilder stage(PipelineStage stage) throws Exception;
	
	Pipeline build() throws Exception;
}
