/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.time.Instant;
import java.util.Map;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.pipeline.Pipeline;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionStatus;

public class DelegatingSubscription implements Subscription {
	
	private Subscription delegate;
	
	public DelegatingSubscription() {
	}
	
	public DelegatingSubscription(Subscription delegate) {
		this.delegate = delegate;
	}
	
	protected Subscription getDelegate() {
		return delegate;
	}

	@Override
	public String getKey() {
		return getDelegate().getKey();
	}

	@Override
	public String getName() {
		return getDelegate().getName();
	}

	@Override
	public String getDescription() {
		return getDelegate().getDescription();
	}

	@Override
	public Subject getOwner() {
		return getDelegate().getOwner();
	}

	@Override
	public Instant getModified() {
		return getDelegate().getModified();
	}

	@Override
	public Instant getCreated() {
		return getDelegate().getCreated();
	}

	@Override
	public Map<String, Object> getConfiguration() {
		return getDelegate().getConfiguration();
	}

	@Override
	public Map<String, Object> getOverrides() {
		return getDelegate().getOverrides();
	}

	@Override
	public Endpoint getSource() {
		return getDelegate().getSource();
	}

	@Override
	public Endpoint getTarget() {
		return getDelegate().getTarget();
	}

	@Override
	public Pipeline getPipeline() {
		return getDelegate().getPipeline();
	}

	@Override
	public SubscriptionStatus getStatus() {
		return getDelegate().getStatus();
	}

}
