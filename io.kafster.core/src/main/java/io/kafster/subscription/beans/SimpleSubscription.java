/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.time.Instant;
import java.util.Map;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.pipeline.Pipeline;
import io.kafster.subscription.MutableSubscription;
import io.kafster.subscription.SubscriptionStatus;

public class SimpleSubscription implements MutableSubscription {
	
	private String key;
	private String name;
	private String description;
	private Subject owner;
	private Instant modified;
	private Instant created;
	private Endpoint source;
	private Endpoint target;
	private Pipeline pipeline;
	private Map<String,Object> configuration;
	private Map<String,Object> overrides;
	private SubscriptionStatus status;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Subject getOwner() {
		return owner;
	}

	@Override
	public Instant getModified() {
		return modified;
	}

	@Override
	public Instant getCreated() {
		return created;
	}

	@Override
	public Endpoint getSource() {
		return source;
	}

	@Override
	public Endpoint getTarget() {
		return target;
	}

	@Override
	public Pipeline getPipeline() {
		return pipeline;
	}
	
	@Override
	public Map<String, Object> getConfiguration() {
		return configuration;
	}	

	public void setKey(String key) {
		this.key = key;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOwner(Subject owner) {
		this.owner = owner;
	}

	public void setModified(Instant modified) {
		this.modified = modified;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public void setSource(Endpoint source) {
		this.source = source;
	}

	public void setTarget(Endpoint target) {
		this.target = target;
	}

	public void setPipeline(Pipeline pipeline) {
		this.pipeline = pipeline;
	}

	public void setConfiguration(Map<String,Object> configuration) {
		this.configuration = configuration;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Map<String, Object> getOverrides() {
		return overrides;
	}

	public void setOverrides(Map<String, Object> overrides) {
		this.overrides = overrides;
	}

	@Override
	public SubscriptionStatus getStatus() {
		return status;
	}
	

	@Override
	public void setStatus(SubscriptionStatus status) {
		this.status = status;
	}

}
