/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import io.kafster.subscription.SubscriptionStatus;

public class SimpleSubscriptionStatus implements SubscriptionStatus {
	
	private State state;
	private String detail;
	
	public SimpleSubscriptionStatus() {
	}

	public SimpleSubscriptionStatus(State state) {
		this.state = state;
	}
	
	public SimpleSubscriptionStatus(State state, String detail) {
		this.state = state;
		this.detail = detail;
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public String getStateDetail() {
		return detail;
	}

	public String getDetail() {
		return detail;
	}

	public void setStateDetail(String detail) {
		this.detail = detail;
	}

	public void setState(State state) {
		this.state = state;
	}

}
