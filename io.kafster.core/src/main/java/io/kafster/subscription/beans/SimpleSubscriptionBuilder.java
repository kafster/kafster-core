/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.pipeline.Pipeline;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionBuilder;
import io.kafster.subscription.SubscriptionStatus;

public class SimpleSubscriptionBuilder implements SubscriptionBuilder {
	
	private SimpleSubscription subscription;
	
	public SimpleSubscriptionBuilder() {
		this.subscription = new SimpleSubscription();
	}

	public SimpleSubscriptionBuilder(SimpleSubscription subscription) {
		this.subscription = subscription;
	}
	
	protected SimpleSubscription getSubscription() {
		return subscription;
	}
	
	@Override
	public SubscriptionBuilder configuration(Map<String, Object> configuration) {
		subscription.setConfiguration(configuration);
		return this;
	}

	@Override
	public SubscriptionBuilder configuration(String key, Object value) {
		if (subscription.getConfiguration() == null) {
			subscription.setConfiguration(new HashMap<>());
		}
		subscription.getConfiguration().put(key, value);
		return this;
	}

	@Override
	public SubscriptionBuilder created(Instant created) {
		subscription.setCreated(created);
		return this;
	}

	@Override
	public SubscriptionBuilder key(String key) {
		subscription.setKey(key);
		return this;
	}

	@Override
	public SubscriptionBuilder modified(Instant modified) {
		subscription.setModified(modified);
		return this;
	}

	@Override
	public SubscriptionBuilder name(String name) {
		subscription.setName(name);
		return this;
	}

	@Override
	public SubscriptionBuilder owner(Subject subject) {
		subscription.setOwner(subject);
		return this;
	}

	@Override
	public SubscriptionBuilder pipeline(Pipeline pipeline) {
		subscription.setPipeline(pipeline);
		return this;
	}

	@Override
	public SubscriptionBuilder source(Endpoint source) {
		subscription.setSource(source);
		return this;
	}

	@Override
	public SubscriptionBuilder target(Endpoint target) {
		subscription.setTarget(target);
		return this;
	}

	@Override
	public SubscriptionBuilder description(String description) {
		subscription.setDescription(description);
		return this;
	}	
	

	@Override
	public SubscriptionBuilder status(SubscriptionStatus status) {
		subscription.setStatus(status);
		return this;
	}	

	@Override
	public Subscription build() throws Exception {
		return subscription;
	}



}
