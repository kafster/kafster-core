/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriptionConfiguration {

	@JsonProperty("retention.ms")
	private Integer retentionTime;
	private Short replicationFactor;
	private Integer partitions;
	private Map<String,String> properties;
	
	public Integer getRetentionTime() {
		return retentionTime;
	}
	
	public void setRetentionTime(Integer retentionTime) {
		this.retentionTime = retentionTime;
	}
	
	public Short getReplicationFactor() {
		return replicationFactor;
	}
	
	public void setReplicationFactor(Short replicationFactor) {
		this.replicationFactor = replicationFactor;
	}
	
	public Integer getPartitions() {
		return partitions;
	}
	
	public void setPartitions(Integer partitions) {
		this.partitions = partitions;
	}
	
	@JsonAnyGetter
	public Map<String, String> getProperties() {
		return properties;
	}
	
	@JsonAnySetter
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}
