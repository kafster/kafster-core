/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.time.Instant;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.pipeline.Pipeline;
import io.kafster.subscription.MutableSubscription;
import io.kafster.subscription.SubscriptionStatus;

public class DelegatingMutableSubscription extends DelegatingSubscription implements MutableSubscription {
	
	private MutableSubscription delegate;
	
	public DelegatingMutableSubscription(MutableSubscription delegate) {
		super(delegate);
		this.delegate = delegate;
	}
	
	@Override
	public void setKey(String key) {
		delegate.setKey(key);
	}

	@Override
	public void setName(String name) {
		delegate.setName(name);
	}

	@Override
	public void setDescription(String description) {
		delegate.setDescription(description);
	}

	@Override
	public void setOwner(Subject owner) {
		delegate.setOwner(owner);
	}

	@Override
	public void setModified(Instant timestamp) {
		delegate.setModified(timestamp);
	}

	@Override
	public void setCreated(Instant timestamp) {
		delegate.setCreated(timestamp);
	}

	@Override
	public void setSource(Endpoint source) {
		delegate.setSource(source);
	}

	@Override
	public void setTarget(Endpoint target) {
		delegate.setTarget(target);
	}

	@Override
	public void setPipeline(Pipeline pipeline) {
		delegate.setPipeline(pipeline);
	}

	@Override
	public void setStatus(SubscriptionStatus status) {
		delegate.setStatus(status);
	}

}
