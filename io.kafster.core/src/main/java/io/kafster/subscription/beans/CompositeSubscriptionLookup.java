/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionLookup;

public class CompositeSubscriptionLookup implements SubscriptionLookup {
	
	private List<SubscriptionLookup> registries;

	@Override
	public Optional<Subscription> getSubscription(String query) throws Exception {
		for (SubscriptionLookup registry : registries) {
			Optional<Subscription> result = registry.getSubscription(query);
			if (result.isPresent()) {
				return result;
			}
		}
		return Optional.empty();
	}

	@Override
	public Iterable<? extends Subscription> getSubscriptions() throws Exception {
		Iterable<Subscription> result = IterableUtils.emptyIterable();
		for (SubscriptionLookup registry : registries) {
			Iterable<? extends Subscription> subscriptions = registry.getSubscriptions();
			if (subscriptions != null) {
				result = IterableUtils.chainedIterable(subscriptions, result);
			}
		}
		return result;
	}

	public void setLookups(List<SubscriptionLookup> registries) {
		this.registries = registries;
	}


}
