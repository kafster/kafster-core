/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.subscription;

import java.time.Instant;
import java.util.Map;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.pipeline.Pipeline;

public interface SubscriptionBuilder {

	SubscriptionBuilder configuration(Map<String,Object> configuration);
	SubscriptionBuilder configuration(String key, Object value);
	SubscriptionBuilder created(Instant created);
	SubscriptionBuilder key(String key);
	SubscriptionBuilder modified(Instant modified);
	SubscriptionBuilder name(String name);
	SubscriptionBuilder description(String description);
	SubscriptionBuilder owner(Subject subject);
	SubscriptionBuilder pipeline(Pipeline pipeline);
	SubscriptionBuilder source(Endpoint source);
	SubscriptionBuilder target(Endpoint target);
	SubscriptionBuilder status(SubscriptionStatus status);
	
	Subscription build() throws Exception;
}
