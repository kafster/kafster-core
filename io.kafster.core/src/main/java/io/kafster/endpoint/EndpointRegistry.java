/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint;

import java.util.Map;
import java.util.Optional;

import io.kafster.endpoint.Endpoint.Role;

public interface EndpointRegistry {
	
	Optional<Endpoint> getByKey(String key) throws Exception;
	Optional<Endpoint> getByUri(String uri, Map<String,Object> props) throws Exception;
	Iterable<? extends Endpoint> getEndpoints(EndpointFilter filter) throws Exception;
	
	interface EndpointFilter {
		String getFilter();
		Integer getOffset();
		Integer getCount();
		Role getRole();
		boolean isRecurse();
	}
}
