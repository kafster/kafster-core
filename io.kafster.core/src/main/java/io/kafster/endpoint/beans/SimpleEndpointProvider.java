/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointBuilder;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.MutableEndpointProvider;
import io.kafster.io.Resource;

public class SimpleEndpointProvider implements MutableEndpointProvider, EndpointFactory {
	
	private String name;
	private String key;
	private String presentationName;
	private String description;
	private String version;
	private Resource image;
	
	private Set<EndpointMetadata> endpointTypes;
	private Map<String, Endpoint> endpointsByKey;
	private Map<String, Endpoint> endpointsByUri;

	@Override
	public Set<EndpointMetadata> getEndpointTypes() throws Exception {
		return endpointTypes;
	}

	@Override
	public EndpointBuilder createEndpoint(EndpointMetadata metadata) throws Exception {
		
		if ((endpointTypes != null) && endpointTypes.contains(metadata)) {
			return new SimpleEndpointBuilder() {

				@Override
				public Endpoint build() {
					Endpoint result = super.build();
					if (result != null) {
						endpointsByKey.put(result.getKey(), result);
					}
					return result;
				}
				
			};
		}
		
		return null;
	}

	@Override
	public Optional<Endpoint> getByKey(String key) throws Exception {
		if (endpointsByKey != null) {
			return Optional.ofNullable(endpointsByKey.get(key));
		}
		return null;
	}

	@Override
	public Optional<Endpoint> getByUri(String uri, Map<String, Object> props) throws Exception {
		if (endpointsByUri != null) {
			return Optional.ofNullable(endpointsByUri.get(uri));
		}
		return null;
	}

	@Override
	public Iterable<? extends Endpoint> getEndpoints(EndpointFilter filter) throws Exception {
		Iterable<? extends Endpoint> result = IterableUtils.emptyIterable();
		
		if (endpointsByKey != null) {
			result = IterableUtils.chainedIterable(result, endpointsByKey.values());
		}
		
		if (endpointsByUri != null) {
			result = IterableUtils.chainedIterable(result, endpointsByUri.values());
		}
		
		return result;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getPresentationName() {
		return presentationName;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public Resource getImage() throws IOException {
		return image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPresentationName(String presentationName) {
		this.presentationName = presentationName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setImage(Resource image) {
		this.image = image;
	}

	public void setEndpointTypes(Set<EndpointMetadata> endpointTypes) {
		this.endpointTypes = endpointTypes;
	}

	public void setEndpointsByKey(Map<String, Endpoint> endpointsByKey) {
		this.endpointsByKey = endpointsByKey;
	}

	public void setEndpointsByUri(Map<String, Endpoint> endpointsByUri) {
		this.endpointsByUri = endpointsByUri;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void addEndpoint(Endpoint endpoint) throws Exception {
		
		if ((endpoint == null) || (endpoint.getKey() == null)) {
			return;
		}
		
		if (endpointsByKey == null) {
			endpointsByKey = new ConcurrentHashMap<>();
		}
		
		endpointsByKey.put(endpoint.getKey(), endpoint);
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}
}
