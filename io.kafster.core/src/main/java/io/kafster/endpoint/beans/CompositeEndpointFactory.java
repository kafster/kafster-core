/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.kafster.endpoint.EndpointBuilder;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;

public class CompositeEndpointFactory implements EndpointFactory {
	
	private List<EndpointFactory> factories;

	@Override
	public Set<EndpointMetadata> getEndpointTypes() throws Exception {
		Set<EndpointMetadata> result = new HashSet<>();
		for (EndpointFactory factory : factories) {
			Set<EndpointMetadata> metadata = factory.getEndpointTypes();
			if (metadata != null) {
				result.addAll(metadata);
			}
		}
		return result;
	}

	@Override
	public EndpointBuilder createEndpoint(EndpointMetadata metadata) throws Exception {
		for (EndpointFactory factory : factories) {
			EndpointBuilder result = factory.createEndpoint(metadata);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	public void setFactories(List<EndpointFactory> factories) {
		this.factories = factories;
	}

}
