/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import org.apache.commons.lang3.StringUtils;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.endpoint.HierarchicalEndpointProvider;

public class SimpleProviderLookup implements EndpointProviderLookup {

	private List<EndpointProvider> providers;
	private boolean recurse = true;

	@Override
	public Optional<EndpointProvider> getProvider(String query) throws Exception {
		
		// Breadth-first lookup 
		//
		Queue<EndpointProvider> queue = new LinkedList<>(providers);
		
		while (!queue.isEmpty()) {
			EndpointProvider provider = queue.remove();
			
			if (StringUtils.equals(query,  provider.getName())) {
				return Optional.of(provider);
			}
			
			if (recurse && (provider instanceof HierarchicalEndpointProvider)) {
				List<? extends EndpointProvider> children = ((HierarchicalEndpointProvider) provider).getChildren();
				if (children != null) {
					queue.addAll(children);
				}
			}
		}
		
		return null;
	}

	@Override
	public Iterable<? extends EndpointProvider> getProviders() throws Exception {
		return providers;
	}
	
	public void setProviders(List<EndpointProvider> providers) {
		this.providers = providers;
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}

}
