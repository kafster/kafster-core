/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointResolver;
import io.kafster.subscription.Subscription;

public class CompositeEndpointResolver implements EndpointResolver {
	
	private List<EndpointResolver> resolvers =  new CopyOnWriteArrayList<>();

	@Override
	public ResolvedEndpoint resolve(Subscription subscription, Endpoint destination) throws Exception {
		
		for (EndpointResolver resolver : resolvers) {
			ResolvedEndpoint result = resolver.resolve(subscription, destination);
			if (result != null) {
				return result;
			}
		}
		
		return null;
	}

	public void setResolvers(List<EndpointResolver> resolvers) {
		this.resolvers.addAll(resolvers);
	}
	
	public void resolverAdded(EndpointResolver resolver, Map<String,Object> props) {
		
		if (resolver == null) {
			return;
		}
		
		resolvers.add(resolver);
	}
	
	public void resolverRemoved(EndpointResolver resolver, Map<String,Object> props) {
		
		if (resolver == null) {
			return;
		}
		
		resolvers.remove(resolver);
	}

}
