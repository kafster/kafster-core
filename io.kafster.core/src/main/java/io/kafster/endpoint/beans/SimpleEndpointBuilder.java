/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointBuilder;
import io.kafster.endpoint.EndpointMetadata;

public class SimpleEndpointBuilder implements EndpointBuilder {
	
	private SimpleEndpoint destination;
	
	public SimpleEndpointBuilder() {
		this.destination = new SimpleEndpoint();
	}

	public SimpleEndpointBuilder(SimpleEndpoint destination) {
		this.destination = destination;
	}
	
	@Override
	public EndpointBuilder configuration(Map<String, Object> configuration) {
		destination.setConfiguration(configuration);
		return this;
	}

	@Override
	public EndpointBuilder created(Instant created) {
		destination.setCreated(created);
		return this;
	}

	@Override
	public EndpointBuilder key(String key) {
		destination.setKey(key);
		return this;
	}

	@Override
	public EndpointBuilder modified(Instant modified) {
		destination.setModified(modified);
		return this;
	}

	@Override
	public EndpointBuilder name(String name) {
		destination.setName(name);
		return this;
	}

	@Override
	public EndpointBuilder uri(String uri) {
		destination.setUri(uri);
		return this;
	}

	@Override
	public Endpoint build() {
		return destination;
	}

	@Override
	public EndpointBuilder property(String key, Object value) {
		try {
			if (destination.getConfiguration() == null) {
				destination.setConfiguration(new HashMap<>());
			}
			destination.getConfiguration().put(key, value);
		} 
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return this;
	}

}
