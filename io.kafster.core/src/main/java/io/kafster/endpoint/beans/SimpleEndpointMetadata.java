/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.schema.SchemaType;

public class SimpleEndpointMetadata implements EndpointMetadata {
	
	private String name;
	private String description;
	private String contentType;
	private EndpointProvider provider;
	private Role role;
	private String uriTemplate;
	
	private SchemaType endpointSchema;
	private SchemaType payloadSchema;
	private SchemaType headerSchema;
	private SchemaType keySchema;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public SchemaType getEndpointSchema() {
		return endpointSchema;
	}
	
	public void setEndpointSchema(SchemaType schema) {
		this.endpointSchema = schema;
	}
	
	public SchemaType getPayloadSchema() {
		return payloadSchema;
	}
	
	public void setPayloadSchema(SchemaType contentModel) {
		this.payloadSchema = contentModel;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	@Override
	public EndpointProvider getProvider() {
		return provider;
	}
	
	public void setProvider(EndpointProvider provider) {
		this.provider = provider;
	}
	
	@Override
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public SchemaType getHeaderSchema() {
		return headerSchema;
	}

	public void setHeaderSchema(SchemaType headerSchema) {
		this.headerSchema = headerSchema;
	}

	@Override
	public SchemaType getKeySchema() {
		return keySchema;
	}

	public void setKeySchema(SchemaType keySchema) {
		this.keySchema = keySchema;
	}

	@Override
	public String getUriTemplate() throws Exception {
		return uriTemplate;
	}
	
	public void setUriTemplate(String template) {
		this.uriTemplate = template;
	}

}
