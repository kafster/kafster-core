/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointRegistry;

public class CompositeEndpointRegistry implements EndpointRegistry {
	
	private List<EndpointRegistry> registries;

	@Override
	public Optional<Endpoint> getByUri(String uri, Map<String, Object> props) throws Exception {
		for (EndpointRegistry lookup : registries) {
			Optional<Endpoint> result = lookup.getByUri(uri, props);
			if (result.isPresent()) {
				return result;
			}
		}
		return null;
	}

	@Override
	public Iterable<? extends Endpoint> getEndpoints(EndpointFilter filter) throws Exception {
		Iterable<? extends Endpoint> result = IterableUtils.emptyIterable();
		for (EndpointRegistry lookup : registries) {
			Iterable<? extends Endpoint> destinations = lookup.getEndpoints(filter);
			if (destinations != null) {
				result = IterableUtils.chainedIterable(result, destinations);
			}
		}
		return result;
	}

	@Override
	public Optional<Endpoint> getByKey(String key) throws Exception {
		for (EndpointRegistry lookup : registries) {
			Optional<Endpoint> result = lookup.getByKey(key);
			if (result.isPresent()) {
				return result;
			}
		}
		return null;
	}

	public void setRegistries(List<EndpointRegistry> registries) {
		this.registries = registries;
	}

}
