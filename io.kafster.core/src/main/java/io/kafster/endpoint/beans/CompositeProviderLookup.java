/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;

public class CompositeProviderLookup implements EndpointProviderLookup {
	
	private List<EndpointProviderLookup> lookups;

	@Override
	public Optional<EndpointProvider> getProvider(String query) throws Exception {
		
		for (EndpointProviderLookup lookup : lookups) {
			Optional<EndpointProvider> result = lookup.getProvider(query);
			if (result.isPresent()) {
				return result;
			}
		}
		
		return Optional.empty();
	}

	@Override
	public Iterable<? extends EndpointProvider> getProviders() throws Exception {
		Iterable<? extends EndpointProvider> result = IterableUtils.emptyIterable();
		
		for (EndpointProviderLookup lookup : lookups) {
			Iterable<? extends EndpointProvider> providers = lookup.getProviders();
			if (providers != null) {
				result = IterableUtils.chainedIterable(providers, result);
			}
		}
		
		return result;
	}

	public void setLookups(List<EndpointProviderLookup> lookups) {
		this.lookups = lookups;
	}
	

}
