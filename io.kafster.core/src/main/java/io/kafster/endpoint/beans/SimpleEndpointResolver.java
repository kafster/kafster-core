/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.KafsterConstants;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointResolver;
import io.kafster.subscription.Subscription;

public class SimpleEndpointResolver implements EndpointResolver {
	
	private static final Logger log = LoggerFactory.getLogger(SimpleEndpointResolver.class);

	@Override
	public ResolvedEndpoint resolve(Subscription subscription, Endpoint endpoint) {
		
		if (endpoint.getUri() != null) {
			
			// Eat any exception here because other resolvers might be able
			// to handle it.
			//
			try {
				
				URI uri = new URI(endpoint.getUri());
				
				if (uri.getScheme().equals(KafsterConstants.KSTREAM_SCHEME)) {
					return new ResolvedEndpoint() {
	
						@Override
						public String getName() {
							return uri.getHost();
						}
	
						@Override
						public String getFilter() {
							return null;
						}
					};
				}
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		
		return null;
	}

}
