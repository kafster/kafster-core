/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint.beans;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.MutableEndpoint;

public class SimpleEndpoint implements MutableEndpoint {
	
	private String key;
	private String name;
	private String description;
	private String uri;
	private Instant modified;
	private Instant created;
	private EndpointMetadata metadata;
	private Map<String, Object> configuration;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public Instant getModified() {
		return modified;
	}

	@Override
	public Instant getCreated() {
		return created;
	}

	@Override
	public Map<String, Object> getConfiguration() throws Exception {
		return configuration;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setModified(Instant modified) {
		this.modified = modified;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public void setConfiguration(Map<String, Object> configuration) {
		this.configuration = configuration;
	}

	public EndpointMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(EndpointMetadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setConfiguration(String name, String value) throws Exception {
		if (configuration == null) {
			configuration = new HashMap<>();
		}
		configuration.put(name, value);
	}

}
