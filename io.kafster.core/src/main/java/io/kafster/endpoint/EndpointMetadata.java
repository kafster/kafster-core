/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint;

import io.kafster.endpoint.Endpoint.Role;
import io.kafster.schema.SchemaType;

public interface EndpointMetadata {
	
	Role getRole();
	
	String getName();
	String getDescription();
	String getContentType() throws Exception;
	String getUriTemplate() throws Exception;
	
	SchemaType getEndpointSchema() throws Exception;
	SchemaType getPayloadSchema() throws Exception;
	SchemaType getHeaderSchema() throws Exception;
	SchemaType getKeySchema() throws Exception;
	
	EndpointProvider getProvider() throws Exception;
}
