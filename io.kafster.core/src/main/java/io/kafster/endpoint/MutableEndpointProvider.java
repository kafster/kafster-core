/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.endpoint;

import io.kafster.io.Resource;

public interface MutableEndpointProvider extends EndpointProvider, MutableEndpointRegistry {

	void setDescription(String description);
	void setImage(Resource image) throws Exception;
	void setName(String name);
	void setKey(String key);
	void setPresentationName(String name);
	void setVersion(String version);
}
