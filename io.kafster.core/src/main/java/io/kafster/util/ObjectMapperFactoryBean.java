/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.springframework.beans.factory.FactoryBean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class ObjectMapperFactoryBean implements FactoryBean<ObjectMapper> {
	
	private ObjectMapper mapper;
	private String format = "json";
	private boolean failOnUnknown = false;
	
	private static final Map<String, Supplier<JsonFactory>> FACTORIES = new HashMap<>();
	static {
		FACTORIES.put("json", () -> null);
		FACTORIES.put("yaml", () -> new YAMLFactory());
	}

	@Override
	public ObjectMapper getObject() throws Exception {
		if (mapper == null) {
			mapper = new ObjectMapper(FACTORIES.get(format).get());
		    mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector());
		    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, failOnUnknown);
		}
		return mapper;
	}

	@Override
	public Class<?> getObjectType() {
		return ObjectMapper.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public void setFailOnUnknown(boolean failOnUnknown) {
		this.failOnUnknown = failOnUnknown;
	}

}
