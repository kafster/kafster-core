/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.util;

import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.IteratorUtils;

public class UriTemplate {
	
	private org.springframework.web.util.UriTemplate processor;
	
	private UriTemplate(String uri) {
		processor = new org.springframework.web.util.UriTemplate(uri);
	}
	
	public static UriTemplate of(String uri) {
		return new UriTemplate(uri);
	}
	
	public boolean matches(String uri) {
		return processor.matches(uri);
	}
	
	public Map<String,String> match(String uri) {
		return processor.match(uri);
	}
	
	public URI expand(Map<String,Object> params) {
		return processor.expand(params);
	}

	public URI expand(Object... props) {
		
		Map<String,Object> params = new HashMap<>();
		
		if (props != null) {
			List<String> names = processor.getVariableNames();
			if (names != null) {
				Iterator<Object> propsIt = IteratorUtils.arrayIterator(props);
				Iterator<String> namesIt = names.iterator();
				
				while (namesIt.hasNext() && propsIt.hasNext()) {
					params.put(namesIt.next(), propsIt.next());
				}
			}
		}
		
		return processor.expand(params);
	}
}
