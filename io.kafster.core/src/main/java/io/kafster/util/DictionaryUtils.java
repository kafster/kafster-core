/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.util;

import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.function.Predicate;

public class DictionaryUtils {

	public static Map<String,Object> asMap(Dictionary<String,?> dictionary) {
		Map<String,Object> result = new HashMap<>();
		Collections.list(dictionary.keys()).forEach(key -> result.put(key, dictionary.get(key)));
		return result;
	}
	
	public static Dictionary<String,?> filter(Dictionary<String,?> dictionary, Predicate<String> predicate) {
		Hashtable<String,Object> result = new Hashtable<>();
		Collections.list(dictionary.keys()).stream().filter(predicate).forEach(key -> result.put(key, dictionary.get(key)));
		return result;
	}	
}
