/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view;

import java.time.Instant;
import java.util.Map;

public interface ViewFactory {

	ViewBuilder createView();
	
	interface ViewBuilder {
		View build() throws Exception;
		
		ViewBuilder metadata(ViewMetadata metadata);
		ViewBuilder configuration(String name, Object value);
		ViewBuilder configuration(Map<String,Object> configuration);
		ViewBuilder created(Instant created);
		ViewBuilder modified(Instant modified);
		ViewBuilder key(String key);
		ViewBuilder name(String name);
		ViewBuilder description(String description);
	}
}
