/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.view.View;
import io.kafster.view.ViewLookup;

public class CompositeViewLookup implements ViewLookup {
	
	private List<ViewLookup> lookups;

	@Override
	public Optional<View> getView(String key) throws Exception {
		for (ViewLookup lookup : lookups) {
			Optional<View> result = lookup.getView(key);
			if (result.isPresent()) {
				return result;
			}
		}
		return Optional.empty();
	}

	@Override
	public Iterable<? extends View> getViews() throws Exception {
		Iterable<? extends View> result = IterableUtils.emptyIterable();
		for (ViewLookup lookup : lookups) {
			Iterable<? extends View> v = lookup.getViews();
			if (v != null) {
				result = IterableUtils.chainedIterable(v, result);
			}
		}
		return result;
	}

	public void setLookups(List<ViewLookup> lookups) {
		this.lookups = lookups;
	}

}
