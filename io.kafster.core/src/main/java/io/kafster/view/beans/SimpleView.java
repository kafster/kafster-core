/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view.beans;

import java.time.Instant;
import java.util.Map;

import io.kafster.datastream.DataStream;
import io.kafster.view.MutableView;
import io.kafster.view.ViewMetadata;

public class SimpleView implements MutableView {
	
	private String key;
	private String name;
	private String description;
	private Instant created;
	private Instant modified;
	private ViewMetadata metadata;
	private DataStream stream;
	private Map<String, Object> configuration;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Instant getCreated() {
		return created;
	}

	@Override
	public Instant getModified() {
		return modified;
	}

	@Override
	public ViewMetadata getMetadata() {
		return metadata;
	}

	@Override
	public Map<String, Object> getConfiguration() {
		return configuration;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setCreated(Instant created) {
		this.created = created;
	}

	@Override
	public void setModified(Instant modified) {
		this.modified = modified;
	}

	@Override
	public void setMetadata(ViewMetadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public void setConfiguration(Map<String, Object> configuration) {
		this.configuration = configuration;
	}

	@Override
	public DataStream getDataStream() {
		return stream;
	}

	@Override
	public void setDataStream(DataStream stream) {
		this.stream = stream;
	}

}
