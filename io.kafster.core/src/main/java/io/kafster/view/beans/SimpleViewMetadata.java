/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.kafster.io.Resource;
import io.kafster.view.MutableViewMetadata;

public class SimpleViewMetadata implements MutableViewMetadata {
	
	private String key;
	private String name;
	private String description;
	private Resource preview;
	private List<String> accepts;
	private List<Parameter> parameters;
	private Map<String, Object> specification;
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public List<String> getAccepts() {
		return accepts;
	}

	@Override
	public Map<String, Object> getSpecification() {
		return specification;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setAccepts(List<String> accepts) {
		this.accepts = accepts;
	}
	
	public void addAccepts(String accepts) {
		if (this.accepts == null) {
			this.accepts = new ArrayList<>();
		}
		this.accepts.add(accepts);
	}

	@Override
	public void setSpecification(Map<String, Object> specification) {
		this.specification = specification;
	}

	@Override
	public Resource getPreview() {
		return preview;
	}

	@Override
	public List<Parameter> getParameters() {
		return parameters;
	}

	@Override
	public void setPreview(Resource preview) {
		this.preview = preview;
	}

	@Override
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

}
