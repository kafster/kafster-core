/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view.beans;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.view.ViewMetadata;
import io.kafster.view.ViewMetadataLookup;

public class CompositeViewMetadataLookup implements ViewMetadataLookup {
	
	private List<ViewMetadataLookup> lookups;

	@Override
	public Iterable<? extends ViewMetadata> getMetadata() throws Exception {
		Iterable<? extends ViewMetadata> result = IterableUtils.emptyIterable();
		for (ViewMetadataLookup lookup : lookups) {
			Iterable<? extends ViewMetadata> m = lookup.getMetadata();
			if (m != null) {
				result = IterableUtils.chainedIterable(m, result);
			}
		}
		return result;
	}

	@Override
	public Optional<ViewMetadata> getMetadata(String key) throws Exception {
		for (ViewMetadataLookup lookup : lookups) {
			Optional<ViewMetadata> result = lookup.getMetadata(key);
			if (result.isPresent()) {
				return result;
			}
		}
		return null;
	}

	public void setLookups(List<ViewMetadataLookup> lookups) {
		this.lookups = lookups;
	}

}
