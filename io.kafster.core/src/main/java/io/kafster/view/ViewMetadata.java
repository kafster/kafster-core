/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.view;

import java.util.List;
import java.util.Map;

import io.kafster.io.Resource;
import io.kafster.schema.SchemaType;

public interface ViewMetadata {

	String getKey();
	String getName();
	String getDescription();
	Resource getPreview();
	List<String> getAccepts();
	List<Parameter> getParameters();
	
	Map<String,Object> getSpecification();
	
	interface Parameter {
		
		enum ParameterType {
			quantitative, ordinal, temporal, nominal, geojson
		}
		
		enum ParameterSource {
			field, constant, option
		}
		
		String getName();
		
		SchemaType getSchema();
		ParameterType getType();
		ParameterSource getSource();
	}
	
}
