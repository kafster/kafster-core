/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.util.List;
import java.util.Optional;

import io.kafster.io.Resource;
import io.kafster.io.ResourceLookup;

public class CompositeResourceLookup implements ResourceLookup {
	
	private List<ResourceLookup> lookups;

	@Override
	public Optional<Resource> lookup(String location) throws Exception {
		for (ResourceLookup lookup : lookups) {
			Optional<Resource> result = lookup.lookup(location);
			if (result.isPresent()) {
				return result;
			}
		}
		return Optional.empty();
	}

	public void setLookups(List<ResourceLookup> lookups) {
		this.lookups = lookups;
	}

}
