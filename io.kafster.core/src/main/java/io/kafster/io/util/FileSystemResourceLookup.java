/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import io.kafster.io.Resource;
import io.kafster.io.ResourceLookup;
import io.kafster.mime.MimeTypeService;

public class FileSystemResourceLookup implements ResourceLookup {
	
	private Path root;
	private MimeTypeService mimeTypes;
	
	private boolean create = true;

	@Override
	public Optional<Resource> lookup(String location) throws Exception {
		Path resource = root.resolve(location);
		if (resource != null) {
			return Optional.of(new FileSystemResource(resource));
		}
		return Optional.empty();
	}
	
	public void setRoot(String root) {
		this.root = FileSystems.getDefault().getPath(root);
	}
	
	public void setCreate(boolean create) {
		this.create = create;
	}
	
	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}
	
	protected class FileSystemResource implements Resource {
		
		private Path path;
		
		public FileSystemResource(Path path) {
			this.path = path;
		}

		@Override
		public URI getUri() {
			return path.toUri();
		}

		@Override
		public String getName() {
			return path.getFileName().toString();
		}

		@Override
		public String getContentType() {
			if (mimeTypes != null) {
				return mimeTypes.getMimeType(getName());
			}
			return null;
		}

		@Override
		public Long getModified() throws IOException {
			FileTime time = Files.getLastModifiedTime(path);
			if (time != null) {
				return time.toMillis();
			}
			return null;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			
			if (Files.exists(path) && Files.isReadable(path)) {
				return Files.newInputStream(path);
			}
			
			return null;
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			
			if (!Files.exists(root) && create) {
				Files.createDirectories(root);
			}
			
			if (!Files.exists(path) && create) {
				Files.createFile(path);
			}
			
			if (Files.exists(path) && Files.isWritable(path)) {
				return Files.newOutputStream(path);
			}
			
			return null;
		}

		@Override
		public void writeTo(OutputStream ostrm) throws IOException {
			
			if (Files.exists(path)) {
				try (InputStream istrm = Files.newInputStream(path)) {
					IOUtils.copy(istrm, ostrm);
				}
			}
			
		}
		
	}

}
