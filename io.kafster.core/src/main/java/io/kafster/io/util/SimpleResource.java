/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import org.apache.commons.io.IOUtils;

import io.kafster.io.Resource;

public class SimpleResource implements Resource {
	
	private URI uri;
	private String name;
	private String contentType;
	private Long modified;
	private InputStream inputStream;
	private OutputStream outputStream;
	
	public SimpleResource() {
	}

	public SimpleResource(InputStream istrm, String contentType) {
		this.inputStream = istrm;
		this.contentType = contentType;
	}
	
	public SimpleResource(InputStream istrm, String contentType, String name) {
		this.inputStream = istrm;
		this.contentType = contentType;
		this.name = name;
	}	
	
	@Override
	public URI getUri() throws Exception {
		return uri;
	}

	@Override
	public String getName() throws Exception {
		return name;
	}

	@Override
	public String getContentType() throws Exception {
		return contentType;
	}

	@Override
	public Long getModified() throws Exception {
		return modified;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return outputStream;
	}

	@Override
	public void writeTo(OutputStream ostrm) throws IOException {
		InputStream istrm = getInputStream();
		if (istrm != null) {
			IOUtils.copy(istrm, ostrm);
		}
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setModified(Long modified) {
		this.modified = modified;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

}
