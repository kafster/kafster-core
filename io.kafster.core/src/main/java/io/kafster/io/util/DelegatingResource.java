/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import io.kafster.io.Resource;

public class DelegatingResource implements Resource {
	
	private Resource delegate;
	
	public DelegatingResource() {
	}
	
	public DelegatingResource(Resource resource) {
		this.delegate = resource;
	}
	
	protected Resource getDelegate() {
		return delegate;
	}
	
	public void setDelegate(Resource delegate) {
		this.delegate = delegate;
	}

	@Override
	public URI getUri() throws Exception {
		return getDelegate().getUri();
	}

	@Override
	public String getName() throws Exception {
		return getDelegate().getName();
	}

	@Override
	public String getContentType() throws Exception {
		return getDelegate().getContentType();
	}

	@Override
	public Long getModified() throws Exception {
		return getDelegate().getModified();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return getDelegate().getInputStream();
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return getDelegate().getOutputStream();
	}

	@Override
	public void writeTo(OutputStream ostrm) throws IOException {
		getDelegate().writeTo(ostrm);
	}

}
