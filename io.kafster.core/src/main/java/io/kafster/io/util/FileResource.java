/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import io.kafster.io.Resource;

public class FileResource implements Resource {
	
	private File file;
	private String contentType;
	
	public FileResource(File file, String contentType) {
		this.file = file;
		this.contentType = contentType;
	}

	@Override
	public URI getUri() throws Exception {
		return file.toURI();
	}

	@Override
	public String getName() throws Exception {
		return file.getName();
	}

	@Override
	public String getContentType() throws Exception {
		return contentType;
	}

	@Override
	public Long getModified() throws Exception {
		return file.lastModified();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new FileInputStream(file);
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return new FileOutputStream(file);
	}

	@Override
	public void writeTo(OutputStream ostrm) throws IOException {
		new DataHandler(new FileDataSource(file)).writeTo(ostrm);
	}

}
