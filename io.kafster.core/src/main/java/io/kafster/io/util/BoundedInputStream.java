/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import org.apache.commons.io.input.ProxyInputStream;

/**
 * This is like the Commons IO BoundedInputStream but it allows the
 * behavior when the limit is exceeded to be configured.  By default
 * it will throw an IOException, whereas the Commons IO BoundedInputStream
 * just stops pulling any more data.
 */
public class BoundedInputStream extends ProxyInputStream {
	
	private long limit;
	private Consumer<Long> handler = this::handleLimit;
	
	private long count = 0;

	public BoundedInputStream(InputStream proxy, long limit) {
		super(proxy);
		this.limit = limit;
	}
	
	public BoundedInputStream(InputStream proxy, long limit, Consumer<Long> handler) {
		super(proxy);
		this.limit = limit;
		this.handler = handler;
	}
	
	protected void handleLimit(Long count) {
		throw new RuntimeException(new IOException("Count [" + count + "] exceeded"));
	}

	@Override
	public int read() throws IOException {
		
        int result = super.read();
        
        if ((result != -1) && (++count > limit)) {
            handler.accept(count);
            result = -1;
        }
        
        return result;
	}

	@Override
	public int read(byte[] destination, int offset, int length) throws IOException {
		
        int result = 0;
        
        if (length > 0) {
            
	        if ((count + length) > limit) {
	            length = (int) (limit - count);
	        }
	        
	        if (length <= 0) {
	        	handler.accept(count);
	        	result = -1;
	        }
	        else {
	            result = super.read(destination, offset, length);
	            if (result > 0) {
	            	count += result;	
	            }
	        }
        }
        
        return result;
	}

	@Override
	public int read(byte[] destination) throws IOException {
		return read(destination, 0, destination.length);
	}

	@Override
	public long skip(long skip) throws IOException {
		
		long result = 0;

        if ((count + skip) > limit) {
            skip = (int) (limit - count);
        }
        
        if (skip <= 0) {
        	handler.accept(count);
        }
        else {
            result = super.skip(skip);
            count += result;	           
        }        
        
		return result;
	}

	@Override
	public int available() throws IOException {
		int available = super.available();
        return (int) Math.min(available, limit - count);
	}

}
