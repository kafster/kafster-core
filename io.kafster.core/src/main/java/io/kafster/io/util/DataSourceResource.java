/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import io.kafster.io.Resource;

public class DataSourceResource implements Resource {
	
	private DataSource source;
	private URI uri;
	private Long modified;
	private String contentType;
	
	public DataSourceResource() {
	}
	
	public DataSourceResource(DataSource source) {
		this.source = source;
	}
	
	public DataSourceResource(DataSource source, String contentType) {
		this.source = source;
		this.contentType = contentType;
	}	

	@Override
	public URI getUri() {
		return uri;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public String getContentType() {
		return contentType == null ? source.getContentType() : contentType;
	}

	@Override
	public Long getModified() {
		return modified;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return source.getInputStream();
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return source.getOutputStream();
	}

	@Override
	public void writeTo(OutputStream ostrm) throws IOException {
		DataHandler handler = new DataHandler(source);
		handler.writeTo(ostrm);
	}

	public DataSource getSource() {
		return source;
	}

	public void setSource(DataSource source) {
		this.source = source;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public void setModified(Long modified) {
		this.modified = modified;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
