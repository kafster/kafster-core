/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.io.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

public class StreamTests {
	
	private static final InputStream ALPHANUM = new ByteArrayInputStream("0123456789abcdefghijklmnopqrstuvwxyz".getBytes());

	@Test
	public void testReadBoundedStream() throws Exception {
		
		ConsumerSupplier<Long> consumer = new ConsumerSupplier<>();
		
		try (InputStream istrm = new BoundedInputStream(ALPHANUM, 8, consumer)) {
		
			// Exhaust up to the limit
			//
			int count = istrm.read(new byte[12]);
			assertEquals(8, count);
			assertNull(consumer.get());
			
			// Now trigger the handler
			//
			count = istrm.read(new byte[12]);
			assertEquals(-1, count);
			assertEquals(8, consumer.get());
		}
		
	}
	
	@Test
	public void testSkipBoundedStream() throws Exception {
		
		ConsumerSupplier<Long> consumer = new ConsumerSupplier<>();
		
		try (InputStream istrm = new BoundedInputStream(ALPHANUM, 8, consumer)) {
		
			// Try to Skip past the limit
			//
			long count = istrm.skip(12);
			assertEquals(8, count);
			assertNull(consumer.get());
			
			// Now trigger the handler
			//
			count = istrm.skip(12);
			assertEquals(0, count);
			assertEquals(8, consumer.get());
		}
		
	}	
	
	@Test
	public void testReadExceptionBoundedStream() throws Exception {
		
		ConsumerSupplier<Throwable> consumer = new ConsumerSupplier<>();
		
		try (InputStream istrm = new BoundedInputStream(ALPHANUM, 8)) {
		
			// Exhaust up to the limit
			//
			int count = istrm.read(new byte[12]);
			assertEquals(8, count);
			
			// Now trigger the handler
			//
			istrm.read(new byte[12]);
		}
		catch (Throwable e) {
			consumer.accept(e);
		}
		
		assertNotNull(consumer.get());
	}
	
	@Test
	public void testAvailableBoundedStream() throws Exception {
		
		ConsumerSupplier<Long> consumer = new ConsumerSupplier<>();
		
		try (InputStream istrm = new BoundedInputStream(ALPHANUM, 8, consumer)) {
			
			assertEquals(8, istrm.available());
		
			// Try to read past the limit
			//
			long count = istrm.read(new byte[12]);
			assertEquals(8, count);
			assertEquals(0, istrm.available());
			assertNull(consumer.get());
			
			// Now trigger the handler
			//
			count = istrm.read(new byte[12]);
			assertEquals(-1, count);
			assertEquals(0, istrm.available());
			assertEquals(8, consumer.get());
		}
		
	}	
	
	private class ConsumerSupplier<T> implements Consumer<T>, Supplier<T> {
		
		private T t;

		@Override
		public T get() {
			return t;
		}

		@Override
		public void accept(T t) {
			if (this.t == null) {
				this.t = t;
			}
		}
		
	}
}
