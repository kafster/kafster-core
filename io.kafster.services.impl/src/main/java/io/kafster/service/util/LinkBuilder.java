/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.util;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.HierarchicalEndpointProvider;
import io.kafster.model.DashboardList;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.DataStreamList;
import io.kafster.model.EndpointDetail;
import io.kafster.model.EndpointList;
import io.kafster.model.HierarchicalEndpointList;
import io.kafster.model.Link;
import io.kafster.model.Model;
import io.kafster.model.ModelList;
import io.kafster.model.PipelineDetail;
import io.kafster.model.PipelineList;
import io.kafster.model.PipelineStageDescriptorList;
import io.kafster.model.PipelineStageList;
import io.kafster.model.Session;
import io.kafster.model.SubscriptionDetail;
import io.kafster.model.SubscriptionList;
import io.kafster.model.ViewConfiguration;
import io.kafster.model.ViewDefinition;
import io.kafster.model.ViewList;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.service.dashboard.DashboardService;
import io.kafster.service.datastream.DataStreamService;
import io.kafster.service.endpoint.EndpointProviderService;
import io.kafster.service.endpoint.EndpointService;
import io.kafster.service.model.ModelService;
import io.kafster.service.pipeline.PipelineService;
import io.kafster.service.session.SessionService;
import io.kafster.service.subscription.SubscriptionService;
import io.kafster.service.views.ViewService;
import io.kafster.view.ViewMetadata;

public class LinkBuilder {
	
	private static final Logger log = LoggerFactory.getLogger(LinkBuilder.class);
	
	private static final String SELF_REL = "self";
	private static final String VIEWS_REL = "views";
	private static final String PROVIDERS_REL = "providers";
	private static final String ENDPOINTS_REL = "endpoints";
	private static final String IMAGE_REL = "image";
	private static final String SOURCE_REL = "source";
	private static final String PIPELINE_REL = "pipeline";
	private static final String TARGET_REL = "target";
	private static final String CHILDREN_REL = "children";
	private static final String SCHEMA_REL = "schema";
	private static final String PEEK_REL = "peek";
	private static final String STAGES_REL = "stages";
	private static final String STREAM_REL = "datastream";
	private static final String STREAMS_REL = "datastreams";
	private static final String LOGIN_REL = "login";
	private static final String SUBSCRIPTIONS_REL = "subscriptions";
	private static final String PIPELNES_REL = "pipelines";
	private static final String DASHBOARDS_REL = "dashboards";
	private static final String HOME_REL = "home";
	private static final String DEFINITIONS_REL = "definitions";
	
	public static List<Link> getLinks(SubscriptionDetail subscription, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(SubscriptionService.class, "getSubscription").build(subscription.getKey());
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(ViewService.class, "getViews").queryParam("subscription", subscription.getKey()).build();
		result.add(createLink(uri, VIEWS_REL));		

		uri = base(uris).path(SubscriptionService.class, "getSubscriptionSource").build(subscription.getKey());
		result.add(createLink(uri, SOURCE_REL));	
		
		uri = base(uris).path(SubscriptionService.class, "getSubscriptionTarget").build(subscription.getKey());
		result.add(createLink(uri, TARGET_REL));
		
		uri = base(uris).path(SubscriptionService.class, "getSubscriptionPipeline").build(subscription.getKey());
		result.add(createLink(uri, PIPELINE_REL));	
		
		uri = base(uris).path(DataStreamService.class, "getDataStreams").build(subscription.getKey());
		result.add(createLink(uri, STREAMS_REL));		
		
		uri = base(uris).path(EndpointProviderService.class, "getProviders").build();
		result.add(createLink(uri, PROVIDERS_REL));	
		
		return result;
	}
	
	public static List<Link> getLinks(SubscriptionList subscriptions, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(SubscriptionService.class, "getSubscriptions").build();
		result.add(createLink(uri, SELF_REL));
		
		return result;
	}
	
	public static List<Link> getLinks(Session session, UriInfo uris) {
		
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(SessionService.class, "getActiveSession").build();
		result.add(createLink(uri, SELF_REL));	
		
		Boolean authenticated = Optional.ofNullable(session.getAuthenticated()).orElse(false);
		if (!authenticated) {
			uri = base(uris).path(SessionService.class, "login").build();
			result.add(createLink(uri, LOGIN_REL));			
		}
		else {
			
			uri = base(uris).path(DashboardService.class, "getDashboards").build();
			result.add(createLink(uri, DASHBOARDS_REL));	
			
			uri = base(uris).path(SessionService.class, "setHomeDashboard").build();
			result.add(createLink(uri, HOME_REL));			
			
			uri = base(uris).path(DataStreamService.class, "getDataStreams").build();
			result.add(createLink(uri, STREAMS_REL));	
			
			uri = base(uris).path(SubscriptionService.class, "getSubscriptions").build();
			result.add(createLink(uri, SUBSCRIPTIONS_REL));		
			
			uri = base(uris).path(PipelineService.class, "getPipelines").build();
			result.add(createLink(uri, PIPELNES_REL));	
			
			uri = base(uris).path(EndpointProviderService.class, "getProviders").build();
			result.add(createLink(uri, PROVIDERS_REL));				
		}
		
		return result;		
	}	
	
	public static List<Link> getLinks(DashboardList session, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(DashboardService.class, "getDashboards").build();
		result.add(createLink(uri, SELF_REL));	
		
		return result;		
	}	
	
	public static List<Link> getLinks(PipelineDetail pipeline, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(PipelineService.class, "getPipeline").build(pipeline.getKey());
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(PipelineService.class, "getStageProviders").build(pipeline.getKey());
		result.add(createLink(uri, PROVIDERS_REL));	
		
		uri = base(uris).path(PipelineService.class, "getPipelineStages").build(pipeline.getKey());
		result.add(createLink(uri, STAGES_REL));		
		
		uri = base(uris).path(PipelineService.class, "getPipelineInput").build(pipeline.getKey());
		result.add(createLink(uri, SOURCE_REL));	
		
		uri = base(uris).path(PipelineService.class, "getPipelineOutput").build(pipeline.getKey());
		result.add(createLink(uri, TARGET_REL));			
		
		return result;		
	}	
	
	public static List<Link> getLinks(PipelineList pipelines, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(PipelineService.class, "getPipelines").build();
		result.add(createLink(uri, SELF_REL));
		
		return result;
	}	
	
	public static List<Link> getLinks(Pipeline pipeline, PipelineStage stage, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(PipelineService.class, "getPipelineStage").build(pipeline.getKey(), stage.getKey());
		result.add(createLink(uri, SELF_REL));
		
		if (stage.getMetadata() != null) {
			PipelineStageMetadata metadata = stage.getMetadata();
			
			try  {
				if (metadata.getImage() != null) {
					uri = base(uris).path(PipelineService.class, "getStageProviderImage").build(pipeline.getKey(), metadata.getName());
					result.add(createLink(uri, IMAGE_REL));				
				}
			}
			catch (IOException e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		
		return result;		
	}
	
	public static List<Link> getLinks(Pipeline pipeline, PipelineStageList stages, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(PipelineService.class, "getPipelineStages").build(pipeline.getKey());
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}	
	
	public static List<Link> getLinks(String pipeline, PipelineStageDescriptorList stages, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(PipelineService.class, "getStageProviders").build(pipeline);
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}	
	
	public static List<Link> getLinks(EndpointList destinations, UriInfo uris) {
		List<Link> result = new ArrayList<>();

		URI uri = base(uris).path(EndpointService.class, "getEndpoints").build();
		result.add(createLink(uri, SELF_REL));	
		
		uri = base(uris).path(EndpointProviderService.class, "getProviders").build();
		result.add(createLink(uri, PROVIDERS_REL));		

		return result;		
	}
	
	public static List<Link> getLinks(HierarchicalEndpointList destinations, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		UriBuilder builder = base(uris).path(EndpointService.class, "getEndpoints");
		
		MultivaluedMap<String, String> query = uris.getQueryParameters();
		if ((query != null) && !query.isEmpty()) {
			query.forEach((k,v) -> builder.queryParam(k, v));
		}
		
		result.add(createLink(builder.build(), SELF_REL));
		
		return result;		
	}
	
	public static List<Link> getLinks(EndpointDetail destination, HierarchicalEndpointProvider provider, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(EndpointService.class, "getEndpoint").build(destination.getKey());
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}
	
	public static List<Link> getLinks(EndpointDetail destination, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(EndpointService.class, "getEndpoint").build(destination.getKey());
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}	
	
	public static List<Link> getLinks(EndpointProvider provider, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(EndpointProviderService.class, "getProvider").build(provider.getKey());
		result.add(createLink(uri, SELF_REL));
		
		if (provider instanceof HierarchicalEndpointProvider) {
			uri = base(uris).path(EndpointProviderService.class, "getProviderChildren").build(provider.getKey());
			result.add(createLink(uri, CHILDREN_REL));	
		}
		
		try {
			if (provider.getImage() != null) {
				uri = base(uris).path(EndpointProviderService.class, "getProviderImage").build(provider.getKey());
				result.add(createLink(uri, IMAGE_REL));			
			}
		}
		catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		uri = base(uris).path(EndpointService.class, "getEndpoints").queryParam("provider", provider.getKey()).build();
		result.add(createLink(uri, ENDPOINTS_REL));	
		
		return result;		
	}	
	
	public static List<Link> getLinks(ModelList models, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(ModelService.class, "getModels").build();
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}	
	
	public static List<Link> getLinks(ViewDefinition view, ViewMetadata metadata, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(ViewService.class, "getViewDefinition").build(view.getKey());
		result.add(createLink(uri, SELF_REL));
		
		if (metadata.getPreview() != null) {
			uri = base(uris).path(ViewService.class, "getViewDefinitionImage").build(metadata.getKey());
			result.add(createLink(uri, IMAGE_REL));				
		}
		
		return result;		
	}	
	
	public static List<Link> getLinks(ViewList views, UriInfo uris) {
		
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(ViewService.class, "getViews").build();
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(ViewService.class, "getViewDefinitions").build();
		result.add(createLink(uri, DEFINITIONS_REL));
		
		return result;		
	}		
	
	public static List<Link> getLinks(ViewConfiguration view, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(ViewService.class, "getView").build(view.getKey());
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(ViewService.class, "getViewDataStream").build(view.getKey());
		result.add(createLink(uri, STREAM_REL));		
		
		return result;		
	}	
	
	public static List<Link> getLinks(Model model, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(ModelService.class, "getModel").build(new QName(model.getNamespace(), model.getName()).toString());
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(ModelService.class, "getModelSchema").build(new QName(model.getNamespace(), model.getName()).toString());
		result.add(createLink(uri, SCHEMA_REL));
		
		return result;
	}	
	
	public static List<Link> getLinks(DataStreamDetail stream, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(DataStreamService.class, "getDataStream").build(stream.getKey());
		result.add(createLink(uri, SELF_REL));
		
		uri = base(uris).path(DataStreamService.class, "peek").build(stream.getKey());
		result.add(createLink(uri, PEEK_REL));		
		
		return result;		
	}	
	
	public static List<Link> getLinks(DataStreamList stream, UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = base(uris).path(DataStreamService.class, "getDataStreams").build();
		result.add(createLink(uri, SELF_REL));
		
		return result;		
	}	
	
	public static Link createLink(URI uri, String rel, String... type) {
		Link link = new Link();
		link.setRel(rel);
		
		link.setHref(uri.getPath());
		if (uri.getQuery() != null) {
			link.setHref(uri.getPath() + "?" + uri.getQuery());
		}
		
		if ((type != null) && (type.length > 0)) {
			link.setType(type[0]);
		}
		return link;
	}	
	
	protected static UriBuilder base(UriInfo uris) {
		return uris.getBaseUriBuilder();
	}
	
}
