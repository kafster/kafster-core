/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.session.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import io.kafster.model.Credentials;
import io.kafster.model.Dashboard;
import io.kafster.model.Link;
import io.kafster.model.Session;
import io.kafster.service.endpoint.EndpointProviderService;
import io.kafster.service.endpoint.EndpointService;
import io.kafster.service.pipeline.PipelineService;
import io.kafster.service.session.SessionService;
import io.kafster.service.subscription.SubscriptionService;
import io.kafster.service.util.LinkBuilder;

public class SessionServiceImpl implements SessionService {
	
	private static final Logger log = LoggerFactory.getLogger(SessionServiceImpl.class);
	
	private static final String SUBSCRIPTION_REL = "subscription";
	private static final String PIPELINE_REL = "pipeline";
	private static final String ENDPOINT_REL = "endpoint";
	private static final String PROVIDER_REL = "provider";
	
	private AuthenticationManager manager;
	
	@Override
	public Session getActiveSession(UriInfo uris) {
		
		Session result = new Session();
		result.setAuthenticated(true);
		
		try {
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		result.setTemplates(getTemplates(uris));
		return result;
	}
	
	protected List<Link> getTemplates(UriInfo uris) {
		List<Link> result = new ArrayList<>();
		
		URI uri = uris.getBaseUriBuilder().path(SubscriptionService.class, "getSubscription").build("{key}");
		result.add(LinkBuilder.createLink(uri, SUBSCRIPTION_REL));	
		
		uri = uris.getBaseUriBuilder().path(PipelineService.class, "getPipeline").build("{key}");
		result.add(LinkBuilder.createLink(uri, PIPELINE_REL));	
		
		uri = uris.getBaseUriBuilder().path(EndpointProviderService.class, "getEndpoint").build("{provider}","{key}");
		result.add(LinkBuilder.createLink(uri, ENDPOINT_REL));		
		
		uri = uris.getBaseUriBuilder().path(EndpointProviderService.class, "getProvider").build("{key}");
		result.add(LinkBuilder.createLink(uri, PROVIDER_REL));			
		
		return result;
	}

	@Override
	public Session logout(String key, UriInfo uris) {
		
		Session result = new Session();
		result.setAuthenticated(false);
		
		try {
			SecurityContextHolder.clearContext();
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return result;
	}

	@Override
	public Session login(Credentials request, UriInfo uris) {
		
		if (StringUtils.isEmpty(request.getUsername()) || StringUtils.isEmpty(request.getPassword())) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		Session result = new Session();
		result.setAuthenticated(false);
		
		try {
			Authentication auth = manager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
			result.setAuthenticated(auth.isAuthenticated());
			
			if (auth.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} 
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}

	@Override
	public Session loginForm(String username, String password, UriInfo uris) {
		Credentials credentials = new Credentials();
		credentials.setUsername(username);
		credentials.setPassword(password);
		return login(credentials, uris);
	}

	@Override
	public Response setHomeDashboard(Dashboard dashboard, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response unsetHomeDashboard(UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setManager(AuthenticationManager manager) {
		this.manager = manager;
	}


}
