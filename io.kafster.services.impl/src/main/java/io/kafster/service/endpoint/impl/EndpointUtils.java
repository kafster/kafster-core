/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.endpoint.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.HierarchicalEndpointProvider;

public class EndpointUtils {
	
	private static final int MAX_RECURSION = 8;
	
	public static Pair<EndpointProvider,Endpoint> findEndpoint(EndpointProvider provider, String key) {
		return findEndpoint(provider, key, MAX_RECURSION);
	}
	
	public static Pair<EndpointProvider,Endpoint> findEndpoint(EndpointProvider provider, String key, int depth) {
		
		try {
			return provider.getByKey(key)
					.map(destination -> Pair.of(provider, destination))
					.orElseGet(() ->  findChild(getChildren(provider), key, depth));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static Pair<EndpointProvider,Endpoint> findChild(List<HierarchicalEndpointProvider> children, String key, int depth) {
		
		if (depth-- > 0) {
			for (HierarchicalEndpointProvider child : children) {
				Pair<EndpointProvider,Endpoint> result = findEndpoint(child, key, depth);
				if (result != null) {
					return result;
				}
			}
		}
		
		return null;
	}
	
	protected static List<HierarchicalEndpointProvider> getChildren(EndpointProvider provider) {
		
		try {
			if (provider instanceof HierarchicalEndpointProvider) {
				return ((HierarchicalEndpointProvider)provider).getChildren();
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return Collections.emptyList(); 
	}
}
