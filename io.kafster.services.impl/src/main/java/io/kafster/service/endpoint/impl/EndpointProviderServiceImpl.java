/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.endpoint.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.activation.URLDataSource;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.Removable;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.endpoint.EndpointProviderParser;
import io.kafster.endpoint.EndpointRegistry.EndpointFilter;
import io.kafster.endpoint.HierarchicalEndpointProvider;
import io.kafster.endpoint.MutableEndpoint;
import io.kafster.endpoint.MutableEndpointMetadata;
import io.kafster.endpoint.MutableEndpointProvider;
import io.kafster.io.Resource;
import io.kafster.io.util.DataSourceResource;
import io.kafster.jaxrs.io.ResourceStreamingOutput;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.jaxrs.multipart.Part;
import io.kafster.jaxrs.multipart.Part.Type;
import io.kafster.model.EndpointDetail;
import io.kafster.model.EndpointProviderDetail;
import io.kafster.model.EndpointProviderList;
import io.kafster.model.ExternalReference;
import io.kafster.model.HierarchicalEndpointList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.persistence.EndpointMetadataRepository;
import io.kafster.persistence.EndpointProviderRepository;
import io.kafster.persistence.EndpointRepository;
import io.kafster.service.endpoint.EndpointProviderService;
import io.kafster.service.util.LinkBuilder;

public class EndpointProviderServiceImpl implements EndpointProviderService {
	
	private static final Logger log = LoggerFactory.getLogger(EndpointProviderServiceImpl.class);
	
	// TODO: Should be io.kafster.kafka.connect.ConnectConstants.CONNECTOR_NAME
	//
	private static final String CONNECTOR_NAME = "name";
	
	private EndpointProviderLookup providers;
	private ModelConverter converter;
	private EndpointProviderParser parser;
	
	private EndpointRepository endpointRepository;
	private EndpointProviderRepository providerRepository;
	private EndpointMetadataRepository metadataRepository;

	@Override
	public EndpointProviderList getProviders(Role role, UriInfo uris) {
		
		try {
			EndpointProviderList result = new EndpointProviderList();
			result.setProviders(new ArrayList<>());
			
			for (EndpointProvider provider : providers.getProviders()) {
				
				try {
						
					EndpointProviderDetail detail = converter.convert(provider, EndpointProviderDetail.class, Option.CHILDREN, Option.SCHEMA);
					detail.setLinks(LinkBuilder.getLinks(provider, uris));
					
					result.getProviders().add(detail);
				}
				catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}
			
			return result;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public EndpointProviderDetail getProvider(String key, UriInfo uris) {
		try {
			return providers.getProvider(key)
					.map(p -> convert(p, uris, Option.CHILDREN, Option.SCHEMA))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointProviderDetail convert(EndpointProvider provider, UriInfo uris, Option... options) {
		try {
			EndpointProviderDetail result = converter.convert(provider, EndpointProviderDetail.class, options);
			result.setLinks(LinkBuilder.getLinks(provider, uris));
			
			// Condition the endpoints
			//
			if (result.getEndpoints() != null) {
				result.getEndpoints().forEach(endpoint -> {
					//endpoint.setProvider(null);
					endpoint.setLinks(LinkBuilder.getLinks(endpoint, uris));
				});
			}

			return result;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public Response getProviderImage(String name, UriInfo uris, Request request) {
		
		try {
			return providers.getProvider(name)
					.map(provider -> getProviderImage(provider, uris, request))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	protected Response getProviderImage(EndpointProvider provider, UriInfo uris, Request request) {
		
		try {
			Resource image = provider.getImage();
			if (image != null) {
				if (image.getModified() != null) {
					ResponseBuilder builder = request.evaluatePreconditions(new Date(image.getModified()));
					if (builder != null) {
						return builder.build();
					}
				}
				
				ResponseBuilder builder = Response.ok(new ResourceStreamingOutput(image)).type(image.getContentType());
				if (image.getModified() != null) {
					builder.lastModified(new Date(image.getModified()));
				}
				
				return builder.build();
			}
			
			return Response.noContent().build();
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);		}
	}
	
	@Override
	public EndpointProviderList getProviderChildren(String name, String filter, UriInfo uris) {
		
		try {
			return providers.getProvider(name)
					.map(provider -> getChildren(provider, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointProviderList getChildren(EndpointProvider provider, UriInfo uris) {
		
		if (provider instanceof HierarchicalEndpointProvider) {
			EndpointProviderList result = new EndpointProviderList();
			
			try {
				List<HierarchicalEndpointProvider> children = ((HierarchicalEndpointProvider) provider).getChildren();
				if (children != null) {
					
					List<EndpointProviderDetail> providers = new ArrayList<>(children.size());
					children.forEach(child -> providers.add(convert(child, uris, Option.SCHEMA)));
					
					result.setProviders(providers);
					result.setLinks(LinkBuilder.getLinks(provider, uris));
				}
				
				return result;
			}
			catch (WebApplicationException e) {
				throw e;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		return new EndpointProviderList();
	}
	

	@Override
	public Response uploadProviderDocument(Multipart multipart, UriInfo uris) {
		
		for (Part part : multipart) {
			if (part.getType() == Type.FILE) {
				try {
					EndpointProviderDetail result = parser.parse(part.getContent(), Collections.emptyMap())
							.map(provider -> persist(provider))
							.map(provider -> convert(provider, uris, Option.CHILDREN, Option.SCHEMA))
							.orElseThrow(() -> new WebApplicationException(Status.BAD_REQUEST));
					
					URI location = uris.getBaseUriBuilder().path(EndpointProviderService.class, "getProvider").build(result.getKey());
					return Response.created(location).entity(result).build();
				} 
				catch (WebApplicationException e) {
					throw e;
				}				
				catch (IOException e) {
					log.error(e.getLocalizedMessage(), e);
					throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
				} 
				catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
					throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
				}
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	@Override
	public Response importProviderDocument(ExternalReference reference, UriInfo uris) {
		
		if ((reference == null) || (reference.getLocation() == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			URL url = new URL(reference.getLocation());
			Resource resource = new DataSourceResource(new URLDataSource(url));
			
			EndpointProviderDetail result = parser.parse(resource, Collections.emptyMap())
					.map(provider -> persist(provider))
					.map(provider -> convert(provider, uris, Option.CHILDREN, Option.SCHEMA))
					.orElseThrow(() -> new WebApplicationException(Status.BAD_REQUEST));
			
			URI location = uris.getBaseUriBuilder().path(EndpointProviderService.class, "getProvider").build(result.getKey());
			return Response.created(location).entity(result).build();			
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (MalformedURLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

	}
	
	protected EndpointProvider persist(EndpointProvider provider) {
		
		try {
			String key = provider.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
			}
			
			EndpointProvider result = providerRepository.create(key);
			if (result instanceof MutableEndpointProvider) {
				
				MutableEndpointProvider mutable = (MutableEndpointProvider)result;
				mutable.setName(provider.getName());
				mutable.setPresentationName(provider.getPresentationName());
				mutable.setDescription(provider.getDescription());
				mutable.setImage(provider.getImage());
				mutable.setVersion(provider.getVersion());
			
				for (Endpoint endpoint : provider.getEndpoints(null)) {
					mutable.addEndpoint(persist(endpoint, persist(endpoint.getMetadata(), result)));
				}
				
				return result;
			}
			
			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointMetadata persist(EndpointMetadata metadata, EndpointProvider provider) {
		
		try {
			String key = metadata.getName();
			if (key == null) {
				key = UUID.randomUUID().toString();
			}
			
			EndpointMetadata result = metadataRepository.create(key);
			if (result instanceof MutableEndpointMetadata) {
				
				MutableEndpointMetadata mutable = (MutableEndpointMetadata)result;
				mutable.setContentType(metadata.getContentType());
				mutable.setDescription(metadata.getDescription());
				mutable.setEndpointSchema(metadata.getEndpointSchema());
				mutable.setHeaderSchema(metadata.getHeaderSchema());
				mutable.setKeySchema(metadata.getKeySchema());
				mutable.setName(metadata.getName());
				mutable.setPayloadSchema(metadata.getPayloadSchema());
				mutable.setProvider(provider);
				mutable.setRole(metadata.getRole());
				
				return result;
				
			}
			
			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Endpoint persist(Endpoint endpoint, EndpointMetadata metadata) {
		
		try {
			String key = endpoint.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
			}
			
			Endpoint result = endpointRepository.create(key);
			if (result instanceof MutableEndpoint) {
				
				MutableEndpoint mutable = (MutableEndpoint)result;
				mutable.setConfiguration(endpoint.getConfiguration());
				mutable.setCreated(Instant.now());
				mutable.setDescription(endpoint.getDescription());
				mutable.setMetadata(metadata);
				mutable.setModified(Instant.now());
				mutable.setName(endpoint.getName());
				mutable.setUri(endpoint.getUri());
				
				return result;
			}

			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Endpoint update(Endpoint endpoint) {
		
		try {
			String key = endpoint.getKey();
			if (key != null) {
				return endpointRepository.store(key, endpoint);
			}

			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}	
	
	protected void remove(Endpoint endpoint) {
		
		try {
			String key = endpoint.getKey();
			if (key != null) {
				endpointRepository.remove(key);
			}

			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}	

	public void setProviders(EndpointProviderLookup providers) {
		this.providers = providers;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

	public void setParser(EndpointProviderParser parser) {
		this.parser = parser;
	}

	public void setEndpointRepository(EndpointRepository endpointRepository) {
		this.endpointRepository = endpointRepository;
	}

	public void setProviderRepository(EndpointProviderRepository providerRepository) {
		this.providerRepository = providerRepository;
	}

	public void setMetadataRepository(EndpointMetadataRepository metadataRepository) {
		this.metadataRepository = metadataRepository;
	}

	@Override
	public HierarchicalEndpointList getProviderEndpoints(String key, String filter, Integer depth, UriInfo uris) {
			
		try {
			HierarchicalEndpointList result = new HierarchicalEndpointList();
			
			result.addAll(providers.getProvider(key)
				.map(p -> getProviderEndpoints(getFilter(filter), p, depth, uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND)));
			
			result.setLinks(LinkBuilder.getLinks(result, uris));
			return result;			
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

	}
	
	protected EndpointFilter getFilter(String filter) {
		return null;
	}
	
	protected HierarchicalEndpointList getProviderEndpoints(EndpointFilter filter, EndpointProvider provider, int depth, UriInfo uris) {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		if (provider == null) {
			return result;
		}
		
		try {
			Iterable<? extends Endpoint> Endpoints = provider.getEndpoints(filter);
			if (Endpoints != null) {
				
				result.setEndpoints(new ArrayList<>());
				
				for (Endpoint Endpoint : Endpoints) {
					EndpointDetail detail = converter.convert(Endpoint, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
					detail.setLinks(LinkBuilder.getLinks(detail, uris));
					
					if (detail.getProvider() != null) {
						detail.getProvider().setLinks(LinkBuilder.getLinks(provider, uris));
					}
					
					result.getEndpoints().add(detail);
				}
			}
			
			if ((depth-- > 0) && (provider instanceof HierarchicalEndpointProvider)) {
				
				List<HierarchicalEndpointProvider> children = ((HierarchicalEndpointProvider)provider).getChildren();
				if (children != null) {
					
					for (HierarchicalEndpointProvider child : children) {
						result.addChild(getProviderEndpoints(filter, child, depth, uris));
					}
				}
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return result;
	}	

	@Override
	public Response createEndpoint(String key, String type, EndpointDetail endpoint, UriInfo uris) {
		
		if ((endpoint == null) || (endpoint.getName() == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			return providers.getProvider(key)
					.map(provider -> createEndpoint(provider, endpoint, type, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Response createEndpoint(EndpointProvider provider, EndpointDetail endpoint, String type, UriInfo uris) {
		
		if (provider instanceof EndpointFactory) {
			
			EndpointFactory factory = (EndpointFactory)provider;

			try {
				
				if (factory.getEndpointTypes() == null) {
					throw new WebApplicationException(Status.BAD_REQUEST);
				}
				
				if ((type == null) && (endpoint.getMetadata() != null) && (endpoint.getMetadata().getName() != null)) {
					type = endpoint.getMetadata().getName();
				}
				
				if (type == null) {
					EndpointMetadata metadata = IterableUtils.first(factory.getEndpointTypes());
					return createEndpoint(provider, metadata, endpoint, uris);
				}
				
				for (EndpointMetadata metadata : factory.getEndpointTypes()) {
					
					if (StringUtils.equals(metadata.getName(), type)) {
						return createEndpoint(provider, metadata, endpoint, uris);
					}
				}
			}
			catch (WebApplicationException e) {
				throw e;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}	
	
	protected Response createEndpoint(EndpointProvider provider, EndpointMetadata metadata, EndpointDetail endpoint, UriInfo uris) throws Exception {
		
		if (provider instanceof EndpointFactory) {
			
			EndpointFactory factory = (EndpointFactory)provider;
			
			Map<String,Object> configuration = Optional.ofNullable(endpoint.getConfiguration()).orElse(new HashMap<>());
			if (endpoint.getName() != null) {
				configuration.put(CONNECTOR_NAME, endpoint.getName());
			}
			
			Endpoint created = factory.createEndpoint(metadata)
												.configuration(configuration)
												.build();
			
			if (created != null) {
				EndpointDetail result = converter.convert(created, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				return Response.ok(result).build();
			}
			
			return Response.noContent().build();
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}	

	@Override
	public EndpointDetail getEndpoint(String key, String endpoint, UriInfo uris) {
		
		try {
			return providers.getProvider(key)
					.map(p -> getEndpoint(p, endpoint, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		} 		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointDetail getEndpoint(EndpointProvider provider, String endpoint, UriInfo uris) {
		
		try {
			return provider.getByKey(endpoint)
				.map(ep -> convert(ep, uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointDetail convert(Endpoint endpoint, UriInfo uris) {
		EndpointDetail result = converter.convert(endpoint, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}

	@Override
	public EndpointDetail updateEndpoint(String key, String endpoint, EndpointDetail update, UriInfo uris) {
		
		try {
			providers.getProvider(key)
				.map(provider -> updateEndpoint(provider, endpoint, update))
				.map(ep -> update(ep))
				.map(ep -> convert(ep, uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}
	
	protected Endpoint updateEndpoint(EndpointProvider provider, String endpoint, EndpointDetail update) {
		
		try {
			return provider.getByKey(endpoint)
				.map(ep -> updateEndpoint(ep, update))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Endpoint updateEndpoint(Endpoint endpoint, EndpointDetail update) {
		
		if (endpoint instanceof MutableEndpoint) {
			MutableEndpoint mutable = (MutableEndpoint)endpoint;
			mutable.setName(update.getName());
			mutable.setUri(update.getUri());
			mutable.setModified(Instant.now());
			
			try {
				if (update.getConfiguration() != null) {
					for (String k : update.getConfiguration().keySet()) {
						mutable.getConfiguration().put(k, update.getConfiguration().get(k));
					}
				}
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		else {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		return endpoint;
	}

	@Override
	public void removeEndpoint(String key, String endpoint) {
		
		try {
			providers.getProvider(key)
				.ifPresent(p -> removeEndpoint(p, endpoint));
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	protected void removeEndpoint(EndpointProvider provider, String endpoint) {
		try {
			provider.getByKey(endpoint)
				.ifPresent(ep -> {
					if (ep instanceof Removable) {
						try {
							((Removable)ep).remove();
						} 
						catch (Exception e) {
							log.error(e.getLocalizedMessage(), e);
							throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
						}
					}
					else {
						throw new WebApplicationException(Status.BAD_REQUEST);
					}
				});
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

}
