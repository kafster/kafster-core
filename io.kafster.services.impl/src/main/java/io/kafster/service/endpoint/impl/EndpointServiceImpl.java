/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.endpoint.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.Removable;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.endpoint.EndpointRegistry.EndpointFilter;
import io.kafster.endpoint.HierarchicalEndpointProvider;
import io.kafster.endpoint.MutableEndpoint;
import io.kafster.model.EndpointDetail;
import io.kafster.model.HierarchicalEndpointList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.service.endpoint.EndpointService;
import io.kafster.service.util.LinkBuilder;

public class EndpointServiceImpl implements EndpointService {
	
	private static final Logger log = LoggerFactory.getLogger(EndpointServiceImpl.class);
	
	// TODO: Should be io.kafster.kafka.connect.ConnectConstants.CONNECTOR_NAME
	//
	private static final String CONNECTOR_NAME = "name";
	
	private EndpointProviderLookup providers;
	private ModelConverter converter;

	@Override
	public HierarchicalEndpointList getEndpoints(String filter, String provider, Integer depth, UriInfo uris) {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		result.setEndpoints(new ArrayList<>());
			
		try {
			EndpointFilter f = getFilter(filter);
			if (provider == null) {
				result.addAll(getProviderEndpoints(f, depth, uris));
			}
			else {
				result.addAll(providers.getProvider(provider)
					.map(p -> getProviderEndpoints(f, p, depth, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND)));
			}
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}
	
	protected EndpointFilter getFilter(String filter) {
		return null;
	}
	
	protected HierarchicalEndpointList getProviderEndpoints(EndpointFilter filter, int depth, UriInfo uris) {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		
		try {
			for (EndpointProvider provider : providers.getProviders()) {
				result.addChild(getProviderEndpoints(filter, provider, depth, uris));
			}	
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return result;
	}
	
	protected HierarchicalEndpointList getProviderEndpoints(EndpointFilter filter, EndpointProvider provider, int depth, UriInfo uris) {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		if (provider == null) {
			return result;
		}
		
		try {
			Iterable<? extends Endpoint> Endpoints = provider.getEndpoints(filter);
			if (Endpoints != null) {
				
				result.setEndpoints(new ArrayList<>());
				
				for (Endpoint Endpoint : Endpoints) {
					EndpointDetail detail = converter.convert(Endpoint, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
					detail.setLinks(LinkBuilder.getLinks(detail, uris));
					
					/*
					if (detail.getProvider() != null) {
						detail.getProvider().setLinks(LinkBuilder.getLinks(provider, uris));
					}
					*/
					
					result.getEndpoints().add(detail);
				}
			}
			
			if ((depth-- > 0) && (provider instanceof HierarchicalEndpointProvider)) {
				
				List<HierarchicalEndpointProvider> children = ((HierarchicalEndpointProvider)provider).getChildren();
				if (children != null) {
					
					for (HierarchicalEndpointProvider child : children) {
						result.addChild(getProviderEndpoints(filter, child, depth, uris));
					}
				}
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return result;
	}

	@Override
	public Response createEndpoint(EndpointDetail endpoint, UriInfo uris) {
		
		if ((endpoint == null) || (endpoint.getName() == null) || (endpoint.getProvider() == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			return providers.getProvider(endpoint.getProvider().getName())
					.map(provider -> createEndpoint(provider, endpoint, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Response createEndpoint(EndpointProvider provider, EndpointDetail endpoint, UriInfo uris) {
		
		if (provider instanceof EndpointFactory) {
			
			EndpointFactory factory = (EndpointFactory)provider;

			try {
				if (factory.getEndpointTypes() == null) {
					throw new WebApplicationException(Status.BAD_REQUEST);
				}
				
				if ((endpoint.getMetadata() == null) || (endpoint.getMetadata().getName() == null)) {
					EndpointMetadata metadata = IterableUtils.first(factory.getEndpointTypes());
					return create(provider, metadata, endpoint, uris);
				}
				
				for (EndpointMetadata metadata : factory.getEndpointTypes()) {
					
					if (StringUtils.equals(metadata.getName(), endpoint.getMetadata().getName())) {
						return create(provider, metadata, endpoint, uris);
					}
				}
			}
			catch (WebApplicationException e) {
				throw e;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	protected Response create(EndpointProvider provider, EndpointMetadata metadata, EndpointDetail endpoint, UriInfo uris) throws Exception {
		
		if (provider instanceof EndpointFactory) {
			
			EndpointFactory factory = (EndpointFactory)provider;
			
			Map<String,Object> configuration = Optional.ofNullable(endpoint.getConfiguration()).orElse(new HashMap<>());
			if (endpoint.getName() != null) {
				configuration.put(CONNECTOR_NAME, endpoint.getName());
			}
			
			Endpoint created = factory.createEndpoint(metadata)
												.configuration(configuration)
												.build();
			
			if (created != null) {
				EndpointDetail result = converter.convert(created, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				return Response.ok(result).build();
			}
			
			return Response.noContent().build();
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public EndpointDetail getEndpoint(String key, UriInfo uris) {
		
		try {
			for (EndpointProvider provider : providers.getProviders()) {
				Optional<Endpoint> Endpoint = provider.getByKey(key);
				if (Endpoint.isPresent()) {
					EndpointDetail result = converter.convert(Endpoint.get(), EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
					result.setLinks(LinkBuilder.getLinks(result, uris));
					
					return result;
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public EndpointDetail updateEndpoint(String key, EndpointDetail update, UriInfo uris) {
		
		try {
			for (EndpointProvider provider : providers.getProviders()) {
				Optional<Endpoint> Endpoint = provider.getByKey(key);
				if (Endpoint.isPresent()) {
					if (Endpoint.get() instanceof MutableEndpoint) {
						MutableEndpoint mutable = (MutableEndpoint)Endpoint.get();
						mutable.setName(update.getName());
						mutable.setUri(update.getUri());
						mutable.setModified(Instant.now());
						
						if (update.getConfiguration() != null) {
							for (String k : update.getConfiguration().keySet()) {
								mutable.getConfiguration().put(k, update.getConfiguration().get(k));
							}
						}
					}
					
					return getEndpoint(key, uris);
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public void removeEndpoint(String key) {
		
		try {
			for (EndpointProvider provider : providers.getProviders()) {
				Optional<Endpoint> Endpoint = provider.getByKey(key);
				if (Endpoint.isPresent()) {
					if (Endpoint.get() instanceof Removable) {
						((Removable)Endpoint.get()).remove();
					}
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}
	
	protected HierarchicalEndpointList processProvider(HierarchicalEndpointProvider provider, UriInfo uris) throws Exception {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		
		result.setName(provider.getName());
		result.setPresentationName(provider.getPresentationName());
		result.setDescription(provider.getDescription());
		
		Iterable<? extends Endpoint> Endpoints = provider.getEndpoints(null);
		if (Endpoints != null) {
			result.setEndpoints(new ArrayList<>());
			
			for (Endpoint Endpoint : Endpoints) {
				EndpointDetail detail = converter.convert(Endpoint, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
				detail.setLinks(LinkBuilder.getLinks(detail, provider, uris));
				
				result.getEndpoints().add(detail);
			}
		}
		
		result.addChild(processChildren(provider, uris));
		return result;
	}
	
	protected HierarchicalEndpointList processChildren(HierarchicalEndpointProvider provider, UriInfo uris) throws Exception {
		
		HierarchicalEndpointList result = new HierarchicalEndpointList();
		
		if (provider.getChildren() != null) {
			
			for (HierarchicalEndpointProvider child : provider.getChildren()) {
				
				HierarchicalEndpointList kids = new HierarchicalEndpointList();
				kids.addChild(processProvider(child, uris));
				
				result.addChild(kids);
			}
		}
		
		return result;
	}

	public void setProviders(EndpointProviderLookup providers) {
		this.providers = providers;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

}
