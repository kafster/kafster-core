/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.cluster.impl;

import javax.ws.rs.core.UriInfo;

import io.kafster.model.Cluster;
import io.kafster.model.ClusterList;
import io.kafster.service.cluster.ClusterService;

public class ClusterServiceImpl implements ClusterService {

	@Override
	public ClusterList getClusters(String filter, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cluster getCluster(String key, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

}
