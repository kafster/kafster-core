/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.model.impl;

import java.util.ArrayList;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.model.Model;
import io.kafster.model.ModelList;
import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;
import io.kafster.schema.json.model.JSONSchema;
import io.kafster.schema.json.model.JSONSchemaConverter;
import io.kafster.service.model.ModelService;
import io.kafster.service.util.LinkBuilder;

public class ModelServiceImpl implements ModelService {
	
	private static final Logger log = LoggerFactory.getLogger(ModelServiceImpl.class);
	
	private Schema schema;

	@Override
	public ModelList getModels(String filter, UriInfo uris) {
		
		ModelList result = new ModelList();
		result.setModels(new ArrayList<>());
		
		try {
			for (SchemaType type : schema.getTypes()) {
				result.getModels().add(getModel(type, uris));
			}
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}

	@Override
	public Model getModel(String name, UriInfo uris) {

		try {
			return schema.getType(QName.valueOf(name))
					.map(type -> getModel(type, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (IllegalArgumentException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected Model getModel(SchemaType type, UriInfo uris) {
		
		Model model = new Model();
		model.setName(type.getName().getLocalPart());
		model.setNamespace(type.getName().getNamespaceURI());
		model.setDescription(type.getDescription());
		model.setSchema(type);
		model.setLinks(LinkBuilder.getLinks(model, uris));
		
		return model;
	}

	@Override
	public Response getModelSchema(String name, String format, UriInfo uris) {
		
		try  {
			schema.getType(QName.valueOf(name))
				.map(type -> Response.ok(convert(type)).type(MediaType.APPLICATION_JSON).build())
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (IllegalArgumentException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}
	
	protected JSONSchema convert(SchemaType type) {
		try {
			return JSONSchemaConverter.convert(type).getModel();
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	public void setSchema(Schema schema) {
		this.schema = schema;
	}

}
