/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.workflow.impl;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.model.workflow.WorkflowEvent;
import io.kafster.model.workflow.WorkflowEventList;
import io.kafster.model.workflow.WorkflowResult;
import io.kafster.service.workflow.WorkflowService;

public class WorkflowServiceImpl implements WorkflowService {
	
	private static final Logger log = LoggerFactory.getLogger(WorkflowServiceImpl.class);
	
	@Override
	public WorkflowResult processEvent(String entity, String key, WorkflowEvent event, UriInfo uris) {
		
		if ((event == null) || (event.getType() == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}

		try {
			// TODO:
			
		}
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public WorkflowEventList getEventHistory(String entity, String key, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkflowResult getEventStatus(String entity, String key, String event, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkflowEventList getAvailableEntityEvents(String entity, String key, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

}
