/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.views.impl;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.DataStreamLookup;
import io.kafster.io.Resource;
import io.kafster.jaxrs.io.ResourceStreamingOutput;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.ViewConfiguration;
import io.kafster.model.ViewDefinition;
import io.kafster.model.ViewDefinitionList;
import io.kafster.model.ViewList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.service.datastream.DataStreamService;
import io.kafster.service.datastream.DataStreamServiceConstants;
import io.kafster.service.util.LinkBuilder;
import io.kafster.service.views.ViewService;
import io.kafster.service.views.conditioner.Conditioner;
import io.kafster.view.MutableView;
import io.kafster.view.View;
import io.kafster.view.ViewLookup;
import io.kafster.view.ViewMetadata;
import io.kafster.view.ViewMetadata.Parameter;
import io.kafster.view.ViewMetadataLookup;

public class ViewServiceImpl implements ViewService {
	
	private static final Logger log = LoggerFactory.getLogger(ViewServiceImpl.class);
	
	private ViewMetadataLookup metadata;
	private ViewLookup views;
	private DataStreamLookup streams;
	
	private ModelConverter converter;
	
	private Map<String,Conditioner> conditioners;

	@Override
	public ViewList getViews(boolean includeDefinitions, UriInfo uris) {
		
		try {
			ViewList result = new ViewList();
			
			Iterable<? extends View> list = views.getViews();
			if (list != null) {
				result.setViews(new ArrayList<>());
				
				for (View view : list) {
					ViewConfiguration config = converter.convert(view, ViewConfiguration.class);
					if (config != null) {
						config.setLinks(LinkBuilder.getLinks(config, uris));
						result.getViews().add(config);
					}
				}
			}
			
			if (includeDefinitions) {
				Iterable<? extends ViewMetadata> definitions = metadata.getMetadata();
				if (definitions != null) {
					result.setDefinitions(new ArrayList<>());
					
					for (ViewMetadata metadata : definitions) {
						ViewDefinition definition = converter.convert(metadata, ViewDefinition.class, Option.SCHEMA);
						definition.setLinks(LinkBuilder.getLinks(definition, metadata, uris));
						
						result.getDefinitions().add(definition);
					}
				}
			}
			
			result.setLinks(LinkBuilder.getLinks(result, uris));
			return result;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ViewConfiguration addView(ViewConfiguration configuration, UriInfo uris) {
		
		if (configuration == null) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
				
			View created = converter.convert(configuration, View.class);
			if (created != null) {
				ViewConfiguration result = converter.convert(created, ViewConfiguration.class, Option.CHILDREN, Option.SCHEMA);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				if (result.getDefinition() != null) {
					result.getDefinition().setLinks(LinkBuilder.getLinks(result.getDefinition(), created.getMetadata(), uris));
					result.getDefinition().setSpecifications(getSpecifications(created, uris));
				}
				
				return result;
			}
			
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ViewConfiguration getView(String key, UriInfo uris) {
		
		if (key == null) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			return views.getView(key)
					.map(view -> {
						
						ViewConfiguration result = converter.convert(view, ViewConfiguration.class, Option.CHILDREN, Option.SCHEMA);
						result.setLinks(LinkBuilder.getLinks(result, uris));
						
						if (result.getDefinition() != null) {
							result.getDefinition().setSpecifications(getSpecifications(view, uris));
							result.getDefinition().setLinks(LinkBuilder.getLinks(result.getDefinition(), view.getMetadata(), uris));
						}
						return result;
						
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected View getView(String key) {
		
		try {
			return views.getView(key)
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	public ViewDefinitionList getViewDefinitions(UriInfo uris) {
		
		try {
			ViewDefinitionList result = new ViewDefinitionList();
			
			Iterable<? extends ViewMetadata> list = metadata.getMetadata();
			if (list != null) {
				result.setDefinitions(new ArrayList<>());
				
				for (ViewMetadata meta : list) {
					ViewDefinition definition = converter.convert(meta, ViewDefinition.class, Option.SCHEMA, Option.CHILDREN);
					if (definition != null) {
						definition.setLinks(LinkBuilder.getLinks(definition, meta, uris));
						result.getDefinitions().add(definition);
					}
				}
			}
			
			return result;
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public Response uploadDefinition(Multipart definition, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ViewDefinition getViewDefinition(String definition, UriInfo uris) {
		
		try {
			return metadata.getMetadata(definition)
					.map(m -> convert(m, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}
	
	protected ViewDefinition convert(ViewMetadata metadata, UriInfo uris) {

		try {
			ViewDefinition def = converter.convert(metadata, ViewDefinition.class, Option.SCHEMA, Option.CHILDREN);
			if (def != null) {
				def.setLinks(LinkBuilder.getLinks(def, metadata, uris));
				return def;
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
	}

	@Override
	public Response getViewDefinitionImage(String definition, UriInfo uris, Request request) {
		
		try {
			return metadata.getMetadata(definition)
					.map(metadata -> getPreview(metadata, request))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}
	
	protected Response getPreview(ViewMetadata metadata, Request request) {
		
		try {
			if (metadata.getPreview() != null) {
				Resource resource = metadata.getPreview();
				
				if (resource.getModified() != null) {
					ResponseBuilder builder = request.evaluatePreconditions(new Date(resource.getModified()));
					if (builder != null) {
						return builder.build();
					}
				}
				
				ResponseBuilder builder = Response.ok(new ResourceStreamingOutput(resource), resource.getContentType());
				if (resource.getModified() != null)  {
					builder.lastModified(new Date(resource.getModified()));
				}
				
				return builder.build();
			}
			else {
				return Response.status(Status.NO_CONTENT).build();
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);			
		}
	}

	private Map<String,Object> getSpecifications(View view, UriInfo uris) {
		Map<String,Object> result = new HashMap<>();
		if ((view.getMetadata() != null) && (view.getMetadata().getSpecification() != null)) {
			view.getMetadata().getSpecification().forEach((k,v) -> result.put(k, conditionSpecification(k,v,view,uris)));
		}
		return result;
	}
	
	private Map<String,Object> getDefaults(View view) {
		Map<String,Object> result = new HashMap<>();
		
		List<Parameter> parameters = view.getMetadata().getParameters();
		if (parameters != null) {
			for (Parameter parameter : parameters) {
				/*
				SchemaType schema = parameter.getSchema();
				if ((schema != null) && (schema.getDefaultValue() != null)) {
					result.put(parameter.getName(), parameter.getDefaultValue());
				}
				*/
			}
		}
		return result;
	}
	
	private Object conditionSpecification(String type, Object specification, View view, UriInfo uris) {
		
		Conditioner conditioner = conditioners.get(type);
		if (conditioner != null) {
		
			Map<String,Object> properties = new HashMap<>();
			properties.putAll(getDefaults(view));
			properties.putAll(view.getConfiguration());
			properties.put(Conditioner.DATA_URL_PROPERTY, getDatastreamLocation(view, uris));
			
			return conditioner.condition(specification, properties);
		}
		
		return specification;
	}
	
	protected String getDatastreamLocation(View view, UriInfo uris) {
		
		UriBuilder builder = uris.getBaseUriBuilder().path(DataStreamService.class, "peek");
		
		if (view.getDataStream() != null) {
			
			for (String field : getFields(view)) {
				builder.queryParam(DataStreamServiceConstants.FIELD_QUERY_PARAM, field);
			}
			
			return relativize(builder.build(view.getDataStream().getKey()));
		}
		
		return relativize(builder.build(DataStreamServiceConstants.EMTPY_STREAM));
	}
	
	protected String relativize(URI uri) {
		String relative = uri.getPath();
		if (uri.getQuery() != null) {
			relative += "?" + uri.getQuery();
		}
		return relative;
	}
	
	protected Set<String> getFields(View view) {
		
		if ((view != null) && (view.getMetadata() != null) && (view.getConfiguration() != null)) {
			Set<String> result = new HashSet<>();
			
			List<Parameter> parameters = view.getMetadata().getParameters();
			if (parameters != null) {
				for (Parameter parameter : parameters) {
					if (parameter.getSource() == Parameter.ParameterSource.field) {
						Object value = view.getConfiguration().get(parameter.getName());
						if (value != null) {
							result.add(String.valueOf(value));
						}
					}
				}
			}
			
			return result;
		}
		
		return Collections.emptySet();
	}

	@Override
	public ViewConfiguration updateView(String view, ViewConfiguration configuration, UriInfo uris) {
		
		if ((view == null) || (configuration == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			
			return views.getView(view)
					.map(v -> updateViewConfiguration(v, configuration, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected ViewConfiguration updateViewConfiguration(View view, ViewConfiguration configuration, UriInfo uris) { 
		
		if (view instanceof MutableView) {
			
			MutableView mutable = (MutableView)view;
			
			if (!StringUtils.equals(configuration.getName(), view.getName())) {
				mutable.setName(configuration.getName());
				mutable.setModified(Instant.now());
			}
			
			if (!StringUtils.equals(configuration.getDescription(), view.getDescription())) {
				mutable.setDescription(configuration.getDescription());
				mutable.setModified(Instant.now());
			}
			
			if (configuration.getProperties() != null) {
				mutable.getConfiguration().putAll(validate(view, configuration.getProperties()));
				mutable.setModified(Instant.now());
			}
			
			ViewConfiguration result = converter.convert(view, ViewConfiguration.class, Option.CHILDREN, Option.SCHEMA);
			result.setLinks(LinkBuilder.getLinks(result, uris));
			
			if (result.getDefinition() != null) {
				result.getDefinition().setSpecifications(getSpecifications(view, uris));
				result.getDefinition().setLinks(LinkBuilder.getLinks(result.getDefinition(), view.getMetadata(), uris));
			}
			
			return result;
		}
		
		return null;
	}
	
	protected Map<String,Object> validate(View view, Map<String,Object> properties) {
		
		if ((view.getMetadata() != null) && (view.getMetadata().getParameters() != null)) {
			Map<String,Object> result = new HashMap<>();
			
			for (ViewMetadata.Parameter parameter : view.getMetadata().getParameters()) {
				Object value = properties.get(parameter.getName());
				if (value != null) {
					result.put(parameter.getName(), value);
				}
			}
			
			return result;
		}
		return properties;
	}

	@Override
	public DataStreamDetail getViewDataStream(String view, UriInfo uris) {
		try {
			return views.getView(view)
					.map(v -> getDataStream(v, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected DataStreamDetail getDataStream(View view, UriInfo uris) {
		
		DataStream stream = view.getDataStream();
		if (stream != null) {
			DataStreamDetail result = converter.convert(stream, DataStreamDetail.class);
			result.setLinks(LinkBuilder.getLinks(result, uris));
			return result;
		}
		
		return null;
	}
	
	@Override
	public DataStreamDetail setViewDataStream(String view, DataStreamDetail datasource, UriInfo uris) {
		
		try {
			
			DataStream stream = streams.getDataStream(datasource.getKey())
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
			
			return views.getView(view)
					.map(v -> {
						if (v instanceof MutableView) {
							((MutableView)v).setDataStream(stream);
							
							DataStreamDetail result = converter.convert(stream, DataStreamDetail.class);
							result.setLinks(LinkBuilder.getLinks(result, uris));
							return result;
						}

						throw new WebApplicationException(Status.BAD_REQUEST);
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));

		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	public void setStreams(DataStreamLookup streams) {
		this.streams = streams;
	}

	public void setConditioners(Map<String, Conditioner> conditioners) {
		this.conditioners = conditioners;
	}

	public void setMetadata(ViewMetadataLookup metadata) {
		this.metadata = metadata;
	}

	public void setViews(ViewLookup views) {
		this.views = views;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

}
