/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.views.conditioner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

public class JsonNodeConditioner implements Conditioner {

	@Override
	public Object condition(Object input, Map<String,Object> properties) {
		
		if (input instanceof ObjectNode) {
			ObjectNode result = ((ObjectNode)input).deepCopy();
			process(null, new Property(result), new StrSubstitutor(properties));
			return result;
		}
		
		return input;
	}
	
	protected void process(JsonNode parent, Property property, StrSubstitutor substitutor) {
		
		if ((property.node instanceof TextNode) && (parent instanceof ObjectNode)) {
			String value = substitutor.replace(property.node.asText());
			if (value != null) {
				String name = substitutor.replace(property.name);
				((ObjectNode)parent).put(name, value);
			}
		}
		else if (property.node instanceof ObjectNode) {
			
			if (parent instanceof ObjectNode) {
				String sub = substitutor.replace(property.name);
				if (!StringUtils.equals(sub, property.name)) {
					((ObjectNode) parent).remove(property.name);
					((ObjectNode) parent).set(sub, property.node);
				}
			}
			
			List<Property> properties = getProperties((ObjectNode)property.node);
			if (properties != null) {
			
				for (Property child : properties) {
					process(property.node, child, substitutor);
				}
			}
		}
		else if (property.node instanceof ArrayNode) {
			
			// TODO Handle array of strings
			
			ArrayNode array = (ArrayNode)property.node;
			for (JsonNode node : array) {
				process(array, new Property(node), substitutor);
			}
		}
	}
	
	protected List<Property> getProperties(ObjectNode obj) {
		List<Property> result = new ArrayList<>();
		
		Iterator<String> names = obj.fieldNames();
		while (names.hasNext()) {
			String name = names.next();
			result.add(new Property(name, obj.get(name)));
		}
		
		return result;
	}
	
	protected class Property {
		private String name;
		private JsonNode node;
		
		public Property(JsonNode node) {
			this(null, node);
		}
		
		public Property(String name, JsonNode node) {
			this.name = name;
			this.node = node;
		}		
		
	}

}
