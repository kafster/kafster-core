/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.views.conditioner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

public class MapConditioner implements Conditioner {
	
	@Override
	public Object condition(Object input, Map<String, Object> properties) {
		
		if (input instanceof Map) {
			Map<String,Object> result = new HashMap<>();
			process(result, new Property(input), new StrSubstitutor(properties));
			return result;
		}
		
		return input;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void process(Object parent, Property property, StrSubstitutor substitutor) {
		
		if (property.value instanceof String) {
			String value = substitutor.replace(property.value);
			if (value != null) {
				
				if (parent instanceof Map) {
					String name = substitutor.replace(property.name);
					((Map)parent).put(name, value);
				}
				
				if (parent instanceof Collection) {
					((Collection)parent).add(value);
				}
			}
		}
		else if (property.value instanceof Map) {
			
			if ((parent instanceof Map) && !StringUtils.isEmpty(property.name)) {
				String sub = substitutor.replace(property.name);
				if (!StringUtils.equals(sub, property.name)) {
					((Map)parent).remove(property.name);
					((Map)parent).put(sub, property.value);
				}
			}
			
			Map<String,Object> obj = (Map<String,Object>)property.value;
			
			// Collect teh properties and then process them to avoid a concurrent
			// modification exception
			//
			List<Property> properties = new ArrayList<>();
			for (String name : obj.keySet()) {
				properties.add(new Property(name, obj.get(name)));
			}
			
			for (Property child : properties) {
				process(obj, child, substitutor);
			}
		}
		else if (property.value instanceof Collection) {
			
			Collection coll = (Collection)property.value;
			for (Object value : coll) {
				process(coll, new Property(value), substitutor);
			}
		}
	}	
	
	protected class Property {
		private String name;
		private Object value;
		
		public Property(Object value) {
			this(null, value);
		}		
		
		public Property(String name, Object value) {
			this.name = name;
			this.value = value;
		}		
		
	}	
	
}
