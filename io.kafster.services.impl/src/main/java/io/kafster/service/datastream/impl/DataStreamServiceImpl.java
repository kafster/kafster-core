/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.datastream.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.mail.internet.ContentDisposition;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.DataStream.ReaderBuilder;
import io.kafster.datastream.DataStreamFactory;
import io.kafster.datastream.DataStreamLookup;
import io.kafster.io.util.BoundedInputStream;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.jaxrs.multipart.Part;
import io.kafster.mime.MimeTypeService;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.DataStreamList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.persistence.DataStreamRepository;
import io.kafster.service.datastream.DataStreamService;
import io.kafster.service.datastream.DataStreamServiceConstants;
import io.kafster.service.util.LinkBuilder;
import io.kafster.service.util.QueryUtil;
import io.kafster.subscription.SubscriptionLookup;

public class DataStreamServiceImpl implements DataStreamService {
	
	private static final Logger log = LoggerFactory.getLogger(DataStreamServiceImpl.class);
	
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String FILENAME_PARAMETER = "filename";
	
	private static final int DEFAULT_MAX_FILE_SIZE = 5000000;
	
	private SubscriptionLookup subscriptions;
	private DataStreamLookup streams;
	private DataStreamFactory factory;
	private DataStreamRepository repository;

	private MimeTypeService mimeTypes;
	private ModelConverter converter;
	
	private int maxFileSize = DEFAULT_MAX_FILE_SIZE;
	
	@Override
	public DataStreamDetail uploadDataStream(Multipart multipart, UriInfo uris) {
		
		try {
			for (Part part : multipart) {
				
				String disposition = part.getHeaders().getFirst(CONTENT_DISPOSITION);
				if ((part.getContent() != null) && (disposition != null)) {
	
					ContentDisposition header = new ContentDisposition(disposition);
					
					String filename = header.getParameter(FILENAME_PARAMETER);
					if (filename != null) {
						
						InputStream istrm = new BoundedInputStream(part.getContent().getInputStream(), maxFileSize, t -> uploadSizeExceeded());
						
						DataStream stream = factory.create()
							.name(FilenameUtils.getBaseName(filename))
							.contentType(getContentType(filename, part))
							.records(istrm)
							.build();
						
						if (stream != null) {
							repository.store(stream.getKey(), stream);
							
							DataStreamDetail result = converter.convert(stream, DataStreamDetail.class);
							result.setLinks(LinkBuilder.getLinks(result, uris));
							
							return result;
						}
						
					}
				}
			}
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} 
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	protected String getContentType(String filename, Part part) throws IOException, ParseException {
		String contentType = part.getHeaders().getFirst(CONTENT_TYPE);
		if (contentType != null) {
			ContentType header = new ContentType(contentType);
			return header.getBaseType();
		}
		return mimeTypes.getMimeType(filename);
	}
	
	protected void uploadSizeExceeded() {
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public DataStreamList getDataStreams(UriInfo uris) {
		
		try {
			DataStreamList result = new DataStreamList();
			result.setStreams(new ArrayList<>());
			result.setLinks(LinkBuilder.getLinks(result, uris));
			
			Iterable<? extends DataStream> it = streams.getDataStreams();
			if (it != null) {
				for (DataStream stream : it) {
					DataStreamDetail detail = converter.convert(stream, DataStreamDetail.class);
					detail.setLinks(LinkBuilder.getLinks(detail, uris));
					
					result.getStreams().add(detail);
				}
			}
			
			return result;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public DataStreamDetail getDataStream(String dataset, UriInfo uris) {

		try {
			return streams.getDataStream(dataset)
					.map(stream -> convert(stream, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected DataStreamDetail convert(DataStream stream, UriInfo uris) {
		
		try {
			DataStreamDetail detail = converter.convert(stream, DataStreamDetail.class);
			detail.setLinks(LinkBuilder.getLinks(detail, uris));
			
			return detail;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}			
	}

	@Override
	public void removeDataStream(String dataset, UriInfo uris) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Response peek(String dataset, UriInfo uris) {
		
		// Check for special streams.  JSON only for now.
		//
		if (StringUtils.equals(dataset, DataStreamServiceConstants.EMTPY_STREAM)) {
			return Response.ok("[]", MediaType.APPLICATION_JSON).build();
		}		
		
		try {
			return streams.getDataStream(dataset)
					.map(stream -> Response.ok(getStream(stream, uris), stream.getContentType()).build())
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected StreamingOutput getStream(DataStream stream, UriInfo uris) {
		
		ReaderBuilder builder = stream.getReader();
		
		Integer count = QueryUtil.getInt(DataStreamServiceConstants.COUNT_QUERY_PARAM, uris.getQueryParameters());
		if (count != null) {
			builder.notMoreThan(count);
		}
		
		Long offset = QueryUtil.getLong(DataStreamServiceConstants.OFFSET_QUERY_PARAM, uris.getQueryParameters());
		if (offset != null) {
			builder.notBefore(offset);
		}
		
		Long before = QueryUtil.getLong(DataStreamServiceConstants.BEFORE_QUERY_PARAM, uris.getQueryParameters());
		if (before != null) {
			builder.notEarlierThan(before);
		}
		
		Long after = QueryUtil.getLong(DataStreamServiceConstants.AFTER_QUERY_PARAM, uris.getQueryParameters());
		if (after != null) {
			builder.notLaterThan(after);
		}
		
		Collection<String> fields = QueryUtil.getCollection(DataStreamServiceConstants.FIELD_QUERY_PARAM, uris.getQueryParameters());
		if (fields != null) {
			builder.fields(fields);
		}
		
		try {
			DataStream.Reader reader = builder.build();
			if (reader != null) {
				return new StreamingOutput() {
	
					@Override
					public void write(OutputStream ostrm) throws IOException, WebApplicationException {
						reader.writeTo(ostrm);
					}
					
				};
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);		
	}

	public void setSubscriptions(SubscriptionLookup subscriptions) {
		this.subscriptions = subscriptions;
	}

	public void setStreams(DataStreamLookup streams) {
		this.streams = streams;
	}

	public void setFactory(DataStreamFactory factory) {
		this.factory = factory;
	}

	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

	public void setMaxUploadSize(int maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	public void setRepository(DataStreamRepository repository) {
		this.repository = repository;
	}

}
