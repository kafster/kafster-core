/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.subscription.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.Removable;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointRegistry;
import io.kafster.model.EndpointDetail;
import io.kafster.model.PipelineDetail;
import io.kafster.model.PipelineStageDetail;
import io.kafster.model.SubscriptionDetail;
import io.kafster.model.SubscriptionList;
import io.kafster.model.SubscriptionState;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.persistence.SubscriptionRepository;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;
import io.kafster.service.subscription.SubscriptionService;
import io.kafster.service.util.LinkBuilder;
import io.kafster.subscription.MutableSubscription;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionLookup;
import io.kafster.subscription.SubscriptionStatus.State;

public class SubscriptionServiceImpl implements SubscriptionService {
	
	private static final Logger log = LoggerFactory.getLogger(SubscriptionServiceImpl.class);
	
	private SubscriptionLookup lookup;
	private SubscriptionRepository repository;
	
	private PipelineLookup pipelines;
	
	private EndpointRegistry sources;
	private EndpointRegistry targets;
	
	private ModelConverter converter;

	@Override
	public SubscriptionList getSubscriptions(UriInfo uris) {
		
		SubscriptionList result = new SubscriptionList();
		result.setSubscriptions(new ArrayList<>());
		
		try {
			for (Subscription subscription : lookup.getSubscriptions()) {
				
				SubscriptionDetail info = converter.convert(subscription, SubscriptionDetail.class, Option.CHILDREN, Option.SCHEMA);
				if (info != null) {
					info.setLinks(LinkBuilder.getLinks(info, uris));
					result.getSubscriptions().add(info);
				}
				else {
					log.warn("Could not convert subscription [" + subscription.getName() + "]");
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}

	@Override
	public Response addSubscription(SubscriptionDetail subscription, UriInfo uris) {
		
		if ((subscription == null) || (subscription.getName() == null)) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			Subscription converted = converter.convert(subscription, Subscription.class);
			if (converted != null) {
				
				converted.getStatus().setState(State.CREATED);
				Subscription created = repository.store(UUID.randomUUID().toString(), converted);
				
				SubscriptionDetail result = converter.convert(created, SubscriptionDetail.class, Option.CHILDREN);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				UriBuilder builder = uris.getBaseUriBuilder().path(SubscriptionService.class, "getSubscription");
				URI location = builder.build(result.getKey());
				
				return Response.created(location).entity(result).build();
			}
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public SubscriptionDetail getSubscription(String key, UriInfo uris) {
		
		try {
			return lookup.getSubscription(key)
				.map(subscription -> {
					SubscriptionDetail result = converter.convert(subscription, SubscriptionDetail.class, Option.CHILDREN, Option.SCHEMA);
					result.setLinks(LinkBuilder.getLinks(result, uris));
					
					return result;
				})
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);			
		}
	}

	@Override
	public SubscriptionDetail updateSubscription(String key, SubscriptionDetail update, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
				.map(subscription -> updateSubscription(update, subscription, uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);			
		}
	}
	
	protected SubscriptionDetail updateSubscription(SubscriptionDetail from, Subscription to, UriInfo uris) {
		
		if (to instanceof MutableSubscription) {
			
			MutableSubscription subscription = (MutableSubscription)to;
			
			if (from.getName() != null) {
				subscription.setName(from.getName());
			}
			
			if (from.getDescription() != null) {
				subscription.setDescription(from.getDescription());
			}
			
			if (from.getConfiguration() != null) {
				subscription.getConfiguration().putAll(from.getConfiguration());
			}
			
			if (from.getOverrides() != null) {
				subscription.getOverrides().putAll(from.getOverrides());
			}
			
			try {
				Subscription updated = repository.store(to.getKey(), subscription);
				
				SubscriptionDetail result = converter.convert(updated, SubscriptionDetail.class);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				return result;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public void deleteSubscription(String key) {
		
		try {
			lookup.getSubscription(key)
				.map(subscription -> {
					if (subscription instanceof Removable) {
						try {
							((Removable)subscription).remove();
							return null;
						}
						catch (Exception e) {
							log.error(e.getLocalizedMessage(), e);
							throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
						}
					}
					throw new WebApplicationException(Status.BAD_REQUEST);
				})
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public SubscriptionState getSubscriptionState(String key, UriInfo uris) {
		
		// TODO:
		try {
			Optional<Subscription> subscription = lookup.getSubscription(key);
			subscription.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public PipelineDetail getSubscriptionPipeline(String key, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
				.map(subscription -> getPipeline(subscription, uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected PipelineDetail getPipeline(Subscription subscription, UriInfo uris) {
		
		Pipeline pipeline = subscription.getPipeline();
		if (pipeline != null) {
			PipelineDetail result = converter.convert(pipeline, PipelineDetail.class, Option.CHILDREN, Option.SCHEMA);
			if (result != null) {
				result.setLinks(LinkBuilder.getLinks(result, uris));
				return result;
			}
		}
		
		return null;
	}

	@Override
	public PipelineDetail setSubscriptionPipeline(String key, PipelineDetail pipeline, UriInfo uris) {
		
		if (pipeline.getKey() == null)  {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		
		try {
			return lookup.getSubscription(key)
					.map(subscription -> setSubscriptionPipeline(subscription, pipeline, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected PipelineDetail setSubscriptionPipeline(Subscription subscription, PipelineDetail pipeline, UriInfo uris) {
		
		try {
			return pipelines.getPipeline(pipeline.getKey())
					.map(p -> setSubscriptionPipeline(subscription, p, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected PipelineDetail setSubscriptionPipeline(Subscription subscription, Pipeline pipeline, UriInfo uris) {
		
		if (subscription instanceof MutableSubscription) {
			
			MutableSubscription mutable = (MutableSubscription)subscription;
			mutable.setPipeline(pipeline); 
			
			updateSubscriptionState(mutable);
			return getSubscriptionPipeline(subscription.getKey(), uris);					
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	protected void updateSubscriptionState(MutableSubscription subscription) {
		
		if (subscription.getStatus() == null) {
			return;
		}
		
		switch (subscription.getStatus().getState()) {
			case AVAILABLE:
			case QUEUED:
				if ((subscription.getSource() == null) || (subscription.getPipeline() == null) || (subscription.getTarget() == null)) {
					subscription.getStatus().setState(State.CREATED);
				}				
				break;
			case CREATED:
				if ((subscription.getSource() != null) && (subscription.getPipeline() != null) && (subscription.getTarget() != null)) {
					subscription.getStatus().setState(State.AVAILABLE);
				}
				break;
			case DISABLED:
				break;
			case ERROR:
				break;
			case NOT_RUNNING:
				break;
			case PENDING_SHUTDOWN:
				break;
			case REBALANCING:
				break;
			case RUNNING:
				break;
			default:
				break;
		}
	}

	@Override
	public PipelineStageDetail getSubscriptionPipelineStage(String key, String stage, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PipelineStageDetail updateSubscriptionPipelineStage(String key, String stage,	PipelineStageDetail update, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EndpointDetail getSubscriptionSource(String key, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
				.map(subscription -> convert(subscription.getSource(), uris))
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointDetail convert(Endpoint endpoint, UriInfo uris) {
		
		if (endpoint != null) {
			EndpointDetail result = converter.convert(endpoint, EndpointDetail.class, Option.CHILDREN, Option.SCHEMA);
			result.setLinks(LinkBuilder.getLinks(result, uris));
			return result;					
		}

		return null;
	}

	@Override
	public EndpointDetail setSubscriptionSource(String key, EndpointDetail source, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
					.map(subscription -> setSubscriptionSource(subscription, source, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointDetail setSubscriptionSource(Subscription subscription, EndpointDetail source, UriInfo uris) { 
		
		if (subscription instanceof MutableSubscription) {
			
			MutableSubscription mutable = (MutableSubscription)subscription;
			
			try {
				if (source.getUri() != null) {
					Optional<Endpoint> d = sources.getByUri(source.getUri(), Collections.emptyMap());
					if (d.isPresent()) {
						mutable.setSource(d.get()); 
						updateSubscriptionState(mutable);
						return getSubscriptionSource(subscription.getKey(), uris);
					}
				}
				
				if (source.getKey() != null) {
					Optional<Endpoint> d = sources.getByKey(source.getKey());
					if (d.isPresent()) {
						mutable.setSource(d.get()); 
						updateSubscriptionState(mutable);
						return getSubscriptionSource(subscription.getKey(), uris);
					}
				}
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
			
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public EndpointDetail getSubscriptionTarget(String key, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
					.map(subscription -> convert(subscription.getTarget(), uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public EndpointDetail setSubscriptionTarget(String key, EndpointDetail target, UriInfo uris) {
		try {
			return lookup.getSubscription(key)
					.map(subscription -> setSubscriptionTarget(subscription, target, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}			
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected EndpointDetail setSubscriptionTarget(Subscription subscription, EndpointDetail target, UriInfo uris) {
		
		if ((target.getKey() != null) && (subscription instanceof MutableSubscription)) {
			
			try {
				targets.getByKey(target.getKey())
					.map(destination -> {
						MutableSubscription mutable = (MutableSubscription)subscription;
						
						mutable.setTarget(destination); 
						updateSubscriptionState(mutable);
						
						return getSubscriptionTarget(subscription.getKey(), uris);
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
			}
			catch (WebApplicationException e) {
				throw e;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	@Override
	public SubscriptionState updateState(String key, SubscriptionState state, UriInfo uris) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLookup(SubscriptionLookup lookup) {
		this.lookup = lookup;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

	public void setPipelineLookup(PipelineLookup pipelineLookup) {
		this.pipelines = pipelineLookup;
	}
	
	public void setSources(EndpointRegistry sources) {
		this.sources = sources;
	}

	public void setTargets(EndpointRegistry targets) {
		this.targets = targets;
	}

	public void setRepository(SubscriptionRepository repository) {
		this.repository = repository;
	}

}
