/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.pipeline.impl;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.io.Resource;
import io.kafster.jaxrs.io.ResourceStreamingOutput;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.jaxrs.multipart.Part;
import io.kafster.model.Model;
import io.kafster.model.ModelList;
import io.kafster.model.PipelineDestinationDetail;
import io.kafster.model.PipelineDetail;
import io.kafster.model.PipelineList;
import io.kafster.model.PipelineStageDescriptor;
import io.kafster.model.PipelineStageDescriptorList;
import io.kafster.model.PipelineStageDetail;
import io.kafster.model.PipelineStageList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.persistence.PipelineRepository;
import io.kafster.pipeline.MutablePipeline;
import io.kafster.pipeline.MutablePipelineStage;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineLookup;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.pipeline.PipelineStageRegistry;
import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;
import io.kafster.service.pipeline.PipelineService;
import io.kafster.service.util.LinkBuilder;

public class PipelineServiceImpl implements PipelineService {
	
	private static final Logger log = LoggerFactory.getLogger(PipelineServiceImpl.class);
	
	private PipelineLookup lookup;
	private PipelineStageRegistry stages;
	
	private PipelineRepository repository;
	
	private ModelConverter converter;
	private ObjectMapper mapper;
	
	private Schema schema;
	
	@Override
	public PipelineList getPipelines(String query, UriInfo uris) {
		
		PipelineList result = new PipelineList();
		result.setPipelines(new ArrayList<>());
		result.setLinks(LinkBuilder.getLinks(result, uris));
		
		try {
			Iterable<? extends Pipeline> pipelines = lookup.getPipelines();
			if (pipelines != null) {
				
				for (Pipeline pipeline : pipelines) {
					PipelineDetail detail = converter.convert(pipeline, PipelineDetail.class);
					detail.setLinks(LinkBuilder.getLinks(detail, uris));
					
					result.getPipelines().add(detail);
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(e);
		}
		
		return result;
	}
	
	@Override
	public Response addPipeline(PipelineDetail pipeline, UriInfo uris) {
		
		try {
			Pipeline created = converter.convert(pipeline, Pipeline.class);
			if (created != null) {
				
				created = repository.store(UUID.randomUUID().toString(), created);
				
				PipelineDetail result = converter.convert(created, PipelineDetail.class);
				result.setLinks(LinkBuilder.getLinks(result, uris));
				
				URI location = uris.getRequestUriBuilder().path(PipelineService.class, "getPipeline").build(result.getKey());
				return Response.created(location).entity(result).build();
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(e);
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public Response uploadPipeline(Multipart upload, UriInfo uris) {
		
		if (upload == null) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
			
		try {
			for (Part part : upload) {
				Resource content = part.getContent();
				if (content != null) {
					InputStream istrm = content.getInputStream();
					if (istrm != null) {
						PipelineDetail uploaded = mapper.readValue(istrm, PipelineDetail.class);
						return addPipeline(uploaded, uris);
					}
				}
			}
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public PipelineDetail getPipeline(String key, UriInfo uris) {
		
		try {
			return lookup.getPipeline(key)
					.map(pipeline -> {
						PipelineDetail result = converter.convert(pipeline, PipelineDetail.class, Option.CHILDREN, Option.SCHEMA);
						
						// Add stage links
						//
						if (result.getStages() != null) {
							for (PipelineStageDetail stage : result.getStages()) {
								stage.setLinks(LinkBuilder.getLinks(pipeline, getStageByKey(stage.getKey(), pipeline), uris));
							}
						}
						
						result.setLinks(LinkBuilder.getLinks(result, uris));
						return result;
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected PipelineStage getStageByKey(String key, Pipeline pipeline) {
		
		if (pipeline.getStages() != null) {
			
			for (PipelineStage stage : pipeline.getStages()) {
				if (StringUtils.equals(key, stage.getKey())) {
					return stage;
				}
			}
		}
		
		return null;
	}

	@Override
	public PipelineDetail updatePipeline(String key, PipelineDetail update, UriInfo uris) {

		try {

			// It needs to be in our persistence
			//
			return repository.fetch(key)
					.map(pipeline -> updatePipeline(key, pipeline, update, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected PipelineDetail updatePipeline(String key, Pipeline pipeline, PipelineDetail update, UriInfo uris) {
		
		if (pipeline instanceof MutablePipeline) {

			MutablePipeline mutable = (MutablePipeline) pipeline;

			// Limit what can be updated
			//
			mutable.setName(update.getName());
			mutable.setDescription(update.getDescription());

			// TODO update stages

			try {
				repository.store(key, mutable);
				return getPipeline(key, uris);
			} 
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}

		throw new WebApplicationException(Status.BAD_REQUEST);
	}

	@Override
	public void deletePipeline(String key) {
		
		try {
			// It needs to be in our persistence
			//
			repository.fetch(key).orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
			repository.remove(key);
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}

	@Override
	public PipelineStageList getPipelineStages(String key, UriInfo uris) {
		
		try {
			return lookup.getPipeline(key)
				.map(pipeline -> {
					PipelineStageList result = new PipelineStageList();
					
					if (pipeline.getStages() != null) {
						result.setStages(new ArrayList<>());
						
						for (PipelineStage stage : pipeline.getStages()) {
							PipelineStageDetail detail = converter.convert(stage, PipelineStageDetail.class);
							detail.setLinks(LinkBuilder.getLinks(pipeline, stage, uris));
							
							result.getStages().add(detail);
						}
						
						result.setLinks(LinkBuilder.getLinks(pipeline, result, uris));
					}
					
					return result;
				})
				.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);			
		}
	}

	@Override
	public Response addPipelineStage(String key, PipelineStageDetail stage, UriInfo uris) {
		try {
			PipelineStageMetadata metadata = stages.getStage(stage.getProvider());
			if (metadata != null) {

				// The pipeline needs to be in our persistence
				//
				return repository.fetch(key)
						.map(pipeline -> addPipelineStage(pipeline, metadata, stage, uris))
						.map(pipeline -> asResponse(key, pipeline, uris))
						.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
			}
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		throw new WebApplicationException(Status.NOT_FOUND);
	}
	
	protected PipelineStageDetail addPipelineStage(Pipeline pipeline, PipelineStageMetadata metadata, PipelineStageDetail stage, UriInfo uris) {
		
		if (pipeline instanceof MutablePipeline) {
			MutablePipeline mutable = (MutablePipeline) pipeline;

			try {
				PipelineStageBuilder builder = mutable.createStage(metadata);
				builder.condition(stage.getCondition())
					.configuration(stage.getConfiguration());
				
				PipelineStage newStage = builder.build();
				if (newStage != null) {
					mutable.addStage(newStage);
					
					PipelineStageDetail result = converter.convert(newStage, PipelineStageDetail.class);
					result.setLinks(LinkBuilder.getLinks(mutable, newStage, uris));
	
					return result;
				}
			} 
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		throw new WebApplicationException(Status.BAD_REQUEST);
	}
	
	protected Response asResponse(String pipeline, PipelineStageDetail stage, UriInfo uris) {
		URI location = uris.getRequestUriBuilder().path(PipelineService.class, "getPipelineStage").build(pipeline, stage.getKey());
		return Response.created(location).entity(stage).build();
	}

	@Override
	public PipelineStageDetail getPipelineStage(String key, String stage, UriInfo uris) {
		
		try {
			return lookup.getPipeline(key)
					.map(pipeline -> getStage(pipeline, stage, uris))
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}
	
	protected PipelineStageDetail getStage(Pipeline pipeline, String stage, UriInfo uris) {
		
		if (pipeline.getStages() != null) {
			for (PipelineStage candidate : pipeline.getStages()) {
				if (StringUtils.equals(candidate.getKey(), stage)) {
					PipelineStageDetail result = converter.convert(candidate, PipelineStageDetail.class);
					if (result != null) {
						result.setLinks(LinkBuilder.getLinks(pipeline, candidate, uris));
						return result;
					}
				}
			}
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public PipelineStageDetail updatePipelineStage(String key, String stage, PipelineStageDetail update, UriInfo uris) {
		
		try {
			Optional<Pipeline> pipeline = repository.fetch(key);
			pipeline.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
			
			if (pipeline.get() instanceof MutablePipeline) {
				
				if (pipeline.get().getStages() != null) {
					
					for (PipelineStage candidate : pipeline.get().getStages()) {
						
						if (StringUtils.equals(candidate.getKey(), stage) && (candidate instanceof MutablePipelineStage)) {
							
							MutablePipelineStage mutable = (MutablePipelineStage)candidate;
							mutable.setCondition(update.getCondition());
							mutable.setConfiguration(update.getConfiguration());
							
							repository.store(key, pipeline.get());
							
							PipelineStageDetail result = converter.convert(candidate, PipelineStageDetail.class);
							result.setLinks(LinkBuilder.getLinks(pipeline.get(), candidate, uris));
							
							return result;
						}
					}
				}					
			}

			throw new WebApplicationException(Status.BAD_REQUEST);
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}

	@Override
	public void deletePipelineStage(String key, String stage) {
		
		try {
			Optional<Pipeline> pipeline = lookup.getPipeline(key);
			pipeline.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));

			if (pipeline.get() instanceof MutablePipeline) {
				
				MutablePipeline mutable = (MutablePipeline)pipeline.get();
				
				for (PipelineStage s : mutable.getStages()) {
					if (StringUtils.equals(s.getKey(), stage)) {
						mutable.removeStage(s);
						return;
					}
				}
				
				throw new WebApplicationException(Status.NOT_FOUND);
			}
			
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public PipelineStageDescriptorList getStageProviders(String key, UriInfo uris) {
		
		try {
			Iterable<PipelineStageMetadata> metadata = stages.getStages();
			if (metadata != null) {
				PipelineStageDescriptorList result = new PipelineStageDescriptorList();
				result.setProviders(new ArrayList<>());
				
				for (PipelineStageMetadata m : metadata) {
					result.getProviders().add(converter.convert(m, PipelineStageDescriptor.class, Option.CHILDREN, Option.SCHEMA));
				}
				
				result.setLinks(LinkBuilder.getLinks(key, result, uris));
				return result;
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return null;
	}

	@Override
	public PipelineDestinationDetail getPipelineInput(String key, UriInfo uris) {
		try {
			return lookup.getPipeline(key)
					.map(pipeline -> {
						
						if (pipeline.getSource() != null) {
							// TODO links
							PipelineDestinationDetail result = converter.convert(pipeline.getSource(), PipelineDestinationDetail.class);
							return result;
						}
						
						return null;
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public PipelineDestinationDetail getPipelineOutput(String key, UriInfo uris) {
		try {
			return lookup.getPipeline(key)
					.map(pipeline -> {
						if (pipeline.getTarget() != null) {
							// TODO links
							PipelineDestinationDetail result = converter.convert(pipeline.getTarget(), PipelineDestinationDetail.class);
							return result;
						}
						
						return null;
					})
					.orElseThrow(() -> new WebApplicationException(Status.NOT_FOUND));
		} 
		catch (WebApplicationException e) {
			throw e;
		}		
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public Response getStageProviderImage(String key, String provider, UriInfo uris, Request request) {
		
		try {
			Iterable<PipelineStageMetadata> metadata = stages.getStages();
			if (metadata != null) {
				for (PipelineStageMetadata m : metadata) {
					
					if (StringUtils.equals(provider, m.getName())) {
						
						Resource image = m.getImage();
						if (image != null) {
							if (image.getModified() != null) {
								ResponseBuilder builder = request.evaluatePreconditions(new Date(image.getModified()));
								if (builder != null) {
									return builder.build();
								}
							}
							
							ResponseBuilder builder = Response
									.ok(new ResourceStreamingOutput(image))
									.type(image.getContentType());
							
							if (image.getModified() != null) {
								builder.lastModified(new Date(image.getModified()));
							}
							
							return builder.build();
						}
						
						break;
					}
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		throw new WebApplicationException(Status.NOT_FOUND);
	}

	@Override
	public PipelineDestinationDetail updatePipelineInput(String key, PipelineDestinationDetail in, UriInfo uris) {

		if (in == null) {
			return null;
		}

		try {
			return lookup.getPipeline(key).map(pipeline -> {
				if (pipeline instanceof MutablePipeline) {
					((MutablePipeline) pipeline).setSource(converter.convert(in, PipelineDestination.class));
					return getPipelineInput(key, uris);
				}

				throw new WebApplicationException(Status.BAD_REQUEST);
				
			}).orElseThrow(() -> new WebApplicationException(Status.BAD_REQUEST));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public PipelineDestinationDetail updatePipelineOutput(String key, PipelineDestinationDetail out, UriInfo uris) {

		if (out == null) {
			return null;
		}

		try {
			return lookup.getPipeline(key).map(pipeline -> {
				if (pipeline instanceof MutablePipeline) {
					((MutablePipeline) pipeline).setTarget(converter.convert(out, PipelineDestination.class));
					return getPipelineInput(key, uris);
				}

				throw new WebApplicationException(Status.BAD_REQUEST);
			}).orElseThrow(() -> new WebApplicationException(Status.BAD_REQUEST));
		} 
		catch (WebApplicationException e) {
			throw e;
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	protected ModelList getModels(UriInfo uris) throws Exception {
		
		ModelList result = new ModelList();
		
		Iterable<? extends SchemaType> types = schema.getTypes();
		if (types != null) {
			result.setModels(new ArrayList<>());
			
			for (SchemaType type : types) {
				
				QName qname = type.getName();
				
				Model model = new Model();
				model.setName(qname.getLocalPart());
				model.setNamespace(qname.getNamespaceURI());
				model.setLinks(LinkBuilder.getLinks(model, uris));
				
				result.getModels().add(model);
			}
		}
		
		result.setLinks(LinkBuilder.getLinks(result, uris));
		return result;
	}

	@Override
	public ModelList getPipelineOutputModels(String key, UriInfo uris) {
		try {
			return getModels(uris);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	public ModelList getPipelineInputModels(String key, UriInfo uris) {
		try {
			return getModels(uris);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}	

	public void setLookup(PipelineLookup lookup) {
		this.lookup = lookup;
	}

	public void setStages(PipelineStageRegistry stages) {
		this.stages = stages;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setRepository(PipelineRepository repository) {
		this.repository = repository;
	}

	public void setSchema(Schema schema) {
		this.schema = schema;
	}

}
