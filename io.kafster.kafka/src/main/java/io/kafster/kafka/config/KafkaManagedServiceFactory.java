/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.config;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import io.kafster.kafka.KafkaProvider;
import io.kafster.util.DictionaryUtils;

public class KafkaManagedServiceFactory implements ManagedServiceFactory, BundleContextAware {
	
	public static final String PID = "kafster.kafka.provider";
	
	private boolean enabled = true;
	
	private BundleContext ctx;
	private Map<String, ServiceRegistration<KafkaProvider>> registrations = new ConcurrentHashMap<>();

	@Override
	public String getName() {
		return "Kafster.io Kafka Provider Factory";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		
		if (!enabled) {
			return;
		}
		
		if (properties != null) {
			deleted(pid);
			publish(pid, properties);
		}
	}

	@Override
	public void deleted(String pid) {
		
		if (!enabled) {
			return;
		}
		
		ServiceRegistration<KafkaProvider> registration = registrations.remove(pid);
		if (registration != null) {
			registration.unregister();
		}
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}
	
	private void publish(String pid, Dictionary<String, ?> properties) {
		KafkaProvider provider = new SimpleKafkaProvider(DictionaryUtils.asMap(properties));
		registrations.put(pid, ctx.registerService(KafkaProvider.class, provider, sanitize(properties)));
	}
	
	private Dictionary<String, ?> sanitize(Dictionary<String, ?> properties) {
		return DictionaryUtils.filter(properties, key -> (key.startsWith("ssl.") || key.startsWith("sasl.")));
	}

	public void setEnable(boolean enabled) {
		this.enabled = enabled;
	}

}
