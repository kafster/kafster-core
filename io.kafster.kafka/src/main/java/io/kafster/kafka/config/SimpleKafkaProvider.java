/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.config;

import java.util.Map;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.Producer;

import io.kafster.kafka.KafkaProvider;

public class SimpleKafkaProvider implements KafkaProvider {
	
	private Map<String,Object> properties;
	
	private AdminClient admin;
	private Producer<byte[], byte[]> producer;
	private Consumer<byte[], byte[]> consumer;
	
	public SimpleKafkaProvider(Map<String,Object> properties) {
		this.properties = properties;
	}

	@Override
	public synchronized Consumer<byte[], byte[]> getConsumer() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized Producer<byte[], byte[]> getProducer() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized AdminClient getAdmin() throws Exception {
		if (admin == null) {
			ClassLoader ccl = Thread.currentThread().getContextClassLoader();
			try {
				Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
				admin = AdminClient.create(properties);
			}
			finally {
				Thread.currentThread().setContextClassLoader(ccl);
			}
		}
		return admin;
	}
	
	public void close() {
		if (admin != null) {
			admin.close();
			admin = null;
		}
		
		if (producer != null) {
			producer.close();
			producer = null;
		}
		
		if (consumer != null) {
			consumer.close();
			consumer = null;
		}
	}
	

}
