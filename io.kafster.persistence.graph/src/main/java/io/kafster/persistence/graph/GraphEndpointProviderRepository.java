/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.persistence.EndpointProviderRepository;
import io.kafster.persistence.graph.model.EndpointProviderNode;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;

public class GraphEndpointProviderRepository implements EndpointProviderRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphEndpointProviderRepository.class); 
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;	

	@Override
	public Optional<EndpointProvider> fetch(String key) throws Exception {
		EndpointProviderNode result = lookup(key);
		return Optional.ofNullable((result == null) ? null : enrich(result));
	}
	
	private EndpointProviderNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT_PROVIDER)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(EndpointProviderNode.class, null);
	}
	
	private EndpointProvider enrich(EndpointProviderNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}

	@Override
	public EndpointProvider create(String key) throws Exception {
		EndpointProviderNode result = factory.create(EndpointProviderNode.class, Labels.ENDPOINT_PROVIDER);
		result.setKey(key);
		return result;
	}

	@Override
	public EndpointProvider store(String key, EndpointProvider value) throws Exception {
		return null;
	}

	@Override
	public void remove(String key) throws Exception {
		EndpointProviderNode node = lookup(key);
		if (node != null) {
			node.remove();
		}		
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends EndpointProvider> fetch(Predicate<EndpointProvider> predicate) throws Exception {
		
		List<? extends EndpointProviderNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT_PROVIDER))
				.toListExplicit(EndpointProviderNode.class);
		
		if (predicate == null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(node -> enrich(node))
				.filter(predicate)
				.collect(Collectors.toList());	
	}
	
	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}	

}
