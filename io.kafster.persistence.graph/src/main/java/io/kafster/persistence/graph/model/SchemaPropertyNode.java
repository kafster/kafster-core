/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.SchemaType;

public class SchemaPropertyNode extends SchemaModelNode implements MutableSchemaProperty {
	
	public static final String MAX_CARDINALITY_PROP = "max_cardinality";
	public static final String MIN_CARDINALITY_PROP = "min_cardinality";
	
	public interface RelationshipTypes {
	     String TYPE = "property_type";
	}
	
	@Override
	public SchemaType getType() {
		return traverse(g -> g.out(RelationshipTypes.TYPE)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public Long getMaxCardinality() {
		return getProperty(MAX_CARDINALITY_PROP);
	}

	@Override
	public Long getMinCardinality() {
		return getProperty(MIN_CARDINALITY_PROP);
	}

	@Override
	public void setMaxCardinality(Long maxCardinality) {
		setProperty(MAX_CARDINALITY_PROP, maxCardinality);
	}

	@Override
	public void setMinCardinality(Long minCardinality) {
		setProperty(MIN_CARDINALITY_PROP, minCardinality);
	}

	@Override
	public void setType(SchemaType type) {
		if (type instanceof SchemaTypeNode) {
			linkOut((SchemaTypeNode)type, RelationshipTypes.TYPE);
		}
	}

}
