/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.persistence.PipelineRepository;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;
import io.kafster.persistence.graph.model.PipelineDestinationNode;
import io.kafster.persistence.graph.model.PipelineNode;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;

public class GraphPipelineRepository implements PipelineRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphPipelineRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;

	@Override
	public Optional<Pipeline> fetch(String key) throws Exception {
		PipelineNode result = lookup(key);
		return Optional.ofNullable(result == null ? null : enrich(result));
	}
	
	protected PipelineNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.PIPELNE)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(PipelineNode.class, null);
	}
	
	protected PipelineNode condition(PipelineNode node) {
		return node;
	}

	@Override
	public Pipeline create(String key) throws Exception {
		PipelineNode result = factory.create(PipelineNode.class, Labels.PIPELNE);
		result.setCreated(Instant.now());
		result.setKey(key);
		return result;
	}

	@Override
	public void remove(String key) throws Exception {
		PipelineNode node = lookup(key);
		if (node != null) {
			node.remove();
		}		
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key).isPresent();
	}

	@Override
	public Iterable<? extends Pipeline> fetch(Predicate<Pipeline> predicate) throws Exception {
		
		List<? extends PipelineNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.PIPELNE))
				.toListExplicit(PipelineNode.class);
		
		if (predicate != null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(destination -> enrich(destination))
				.filter(predicate)
				.collect(Collectors.toList());
	}
	
	private Pipeline enrich(PipelineNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}

	@Override
	public Pipeline store(String key, Pipeline pipeline) throws Exception {
		
		if (pipeline instanceof PipelineNode) {
			return pipeline;
		}
		
		PipelineNode node = lookup(key);
		if (node == null) {
			node = (PipelineNode)create(key);
		}
		
		node.setKey(key);
		node.setName(pipeline.getName());
		node.setCreated(pipeline.getCreated());
		node.setDescription(pipeline.getDescription());
		node.setModified(pipeline.getModified());
		node.setDescription(pipeline.getDescription());
		node.setVersion(pipeline.getVersion());
		
		PipelineDestination source = pipeline.getSource();
		if (source != null) {
			if (source instanceof PipelineDestinationNode) {
				node.setSource(source);
			}
			else {
				PipelineDestinationNode sourceNode = node.addSource();
				sourceNode.setAs(source.getAs());
				sourceNode.setType(source.getType());
			}
		}
		
		PipelineDestination target = pipeline.getTarget();
		if (target != null) {
			if (target instanceof PipelineDestinationNode) {
				node.setTarget(target);
			}
			else {
				PipelineDestinationNode targetNode = node.addTarget();
				targetNode.setAs(target.getAs());
				targetNode.setType(target.getType());
			}
		}
		
		Iterable<? extends PipelineStage> stages = pipeline.getStages();
		if (stages != null) {
			for (PipelineStage stage : stages) {
				node.addStage(stage);
			}
		}
		
		return enrich(node);
	}
	
	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}

}
