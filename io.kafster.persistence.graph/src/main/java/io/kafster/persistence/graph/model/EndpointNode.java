/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.time.Instant;
import java.util.Map;

import io.kafster.Removable;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.MutableEndpoint;
import io.kafster.persistence.graph.annotations.GraphVertex;

@GraphVertex(label=Labels.ENDPOINT)
public class EndpointNode extends KeyedNode implements MutableEndpoint, Removable {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String URI_PROP = "uri";
	public static final String MODIFIED_PROP = "modified";
	public static final String CREATED_PROP = "created";
	public static final String PROVIDER_PROP = "provider";	
	public static final String PROVIDER_ENDPOINT_PROP = "endpoint";	
	public static final String REFERENCE_PROP = "ref";	
	
	public interface RelationshipTypes {
		String CONFIGURATION = "endpoint_configuration";
		String METADATA = "endpoint_metdata";
	}		

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);
	}	

	@Override
	public String getUri() {
		return getProperty(URI_PROP);
	}

	@Override
	public void setUri(String uri) {
		setProperty(URI_PROP, uri);
	}	

	@Override
	public Instant getModified() {
		Long modified = getProperty(MODIFIED_PROP);
		if (modified != null) {
			return Instant.ofEpochMilli(modified);
		}
		return null;
	}
	
	@Override
	public void setModified(Instant modified) {
		setProperty(MODIFIED_PROP, Long.valueOf(modified.toEpochMilli()));
	}	

	@Override
	public Instant getCreated() {
		Long modified = getProperty(CREATED_PROP);
		if (modified != null) {
			return Instant.ofEpochMilli(modified);
		}
		return null;
	}
	
	@Override
	public void setCreated(Instant created) {
		setProperty(CREATED_PROP, Long.valueOf(created.toEpochMilli()));
	}

	@Override
	public EndpointMetadata getMetadata() {
		return  traverse(g -> g.out(RelationshipTypes.METADATA)).nextOrDefaultExplicit(EndpointMetadataNode.class, null);
	}

	@Override
	public Map<String, Object> getConfiguration() throws Exception {
		PropertiesNode result = traverse(g -> g.out(RelationshipTypes.CONFIGURATION)).nextOrDefaultExplicit(PropertiesNode.class, null);
		if (result == null) {
			result = createToRelationship(PropertiesNode.class, RelationshipTypes.CONFIGURATION);
		}
		return result;
	}
	
	@Override
	public void setConfiguration(Map<String, Object> configuration) throws Exception {
		Map<String, Object> current = getConfiguration();
		current.clear();
		current.putAll(configuration);
	}	

	@Override
	public void setMetadata(EndpointMetadata metadata) {
		
		EndpointMetadataNode current = traverse(g -> g.out(RelationshipTypes.METADATA)).nextOrDefaultExplicit(EndpointMetadataNode.class, null);
		if (current != null) {
			unlinkOut(current, RelationshipTypes.METADATA);
		}
		
		if (metadata instanceof EndpointMetadataNode) {
			linkOut((EndpointMetadataNode)metadata, RelationshipTypes.METADATA);
		}
	}
	
	public void setProvider(String provider) {
		setProperty(PROVIDER_PROP, provider);
	}
	
	public String getProvider() {
		return getProperty(PROVIDER_PROP);
	}
	
	public void setEndpoint(String endpoint) {
		setProperty(PROVIDER_ENDPOINT_PROP, endpoint);
	}
	
	public String getEndpoint() {
		return getProperty(PROVIDER_ENDPOINT_PROP);
	}
	
	public Boolean getReference() {
		return getProperty(REFERENCE_PROP);
	}
	
	public void setReference(Boolean ref) {
		setProperty(REFERENCE_PROP, ref);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);
	}

	@Override
	public void setConfiguration(String name, String value) throws Exception {
		getConfiguration().put(name, value);
	}

}
