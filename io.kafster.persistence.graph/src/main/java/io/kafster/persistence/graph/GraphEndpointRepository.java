/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.endpoint.Endpoint;
import io.kafster.persistence.EndpointRepository;
import io.kafster.persistence.graph.model.EndpointNode;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;

public class GraphEndpointRepository implements EndpointRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphEndpointRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;

	@Override
	public Optional<Endpoint> fetch(String key) throws Exception {
		
		EndpointNode result = lookup(key);		
		return Optional.ofNullable(result == null ? null : enrich(result));
	}
	
	private EndpointNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(EndpointNode.class, null);
	}

	@Override
	public Endpoint create(String key) throws Exception {
		EndpointNode result = factory.create(EndpointNode.class, Labels.ENDPOINT);
		result.setCreated(Instant.now());
		result.setKey(key);
		return result;
	}

	@Override
	public void remove(String key) throws Exception {
		EndpointNode node = lookup(key);
		if (node != null) {
			node.remove();
		}
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends Endpoint> fetch(Predicate<Endpoint> predicate) throws Exception {
		
		List<? extends EndpointNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT))
				.toListExplicit(EndpointNode.class);
		
		if (predicate == null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(node -> enrich(node))
				.filter(predicate)
				.collect(Collectors.toList());		
	}
	
	private Endpoint enrich(EndpointNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}	
	
	@Override
	public List<? extends Endpoint> findByProvider(String provider) throws Exception {
		
		List<? extends EndpointNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT)
				.has(EndpointNode.PROVIDER_PROP, provider))
				.toListExplicit(EndpointNode.class);
		
		return result.stream()
				.map(node -> enrich(node))
				.collect(Collectors.toList());		
	}

	@Override
	public Endpoint store(String key, Endpoint value) throws Exception {
		return null;
	}

	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}
	

}
