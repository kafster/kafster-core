/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.util.Map;

import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.pipeline.MutablePipelineStage;
import io.kafster.pipeline.PipelineStageMetadata;

@GraphVertex(label=Labels.PIPELINE_STAGE)
public class PipelineStageNode extends KeyedNode implements MutablePipelineStage {
	
	public static final String CONDITION_PROP = "condition";
	public static final String PROVIDER_PROP = "provider";
	
	public interface RelationshipTypes {
		String CONFIGURATION = "configuration";
	}
	
	private PipelineStageMetadata metadata;

	@Override
	public String getCondition() {
		return getProperty(CONDITION_PROP);
	}
	
	@Override
	public void setCondition(String expression) {
		setProperty(CONDITION_PROP, expression);
	}	
	
	@Override
	public Map<String, Object> getConfiguration() {
		PropertiesNode result = traverse(t -> t.out(RelationshipTypes.CONFIGURATION)).nextOrDefaultExplicit(PropertiesNode.class, null);
		if (result == null) {
			result = createToRelationship(PropertiesNode.class, RelationshipTypes.CONFIGURATION);
		}
		return result;
	}
	
	@Override
	public void setConfiguration(Map<String, Object> configuration) {
		Map<String,Object> current = getConfiguration();
		current.clear();
		current.putAll(configuration);
	}		

	@Override
	public PipelineStageMetadata getMetadata() {
		return metadata;
	}

	@Override
	public void setMetadata(PipelineStageMetadata metadata) {
		this.metadata = metadata;
	}
	
	public void setProvider(String provider) {
		setProperty(PROVIDER_PROP, provider);
	}
	
	public String getProvider() {
		return getProperty(PROVIDER_PROP);
	}
	

}
