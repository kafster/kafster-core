/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.datastream.DataStream;
import io.kafster.persistence.DataStreamRepository;
import io.kafster.persistence.graph.model.DataStreamNode;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;

public class GraphDataStreamRepository implements DataStreamRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphDataStreamRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;	

	@Override
	public Optional<DataStream> fetch(String key) throws Exception {
		DataStreamNode result = lookup(key);
		return result == null ? Optional.empty() : Optional.of(enrich(result));
	}
	
	private DataStreamNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.DATASTREAM)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(DataStreamNode.class, null);
	}
	
	private DataStream enrich(DataStreamNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}	

	@Override
	public DataStream create(String key) throws Exception {
		DataStreamNode result = factory.create(DataStreamNode.class, Labels.DATASTREAM);
		result.setKey(key);
		return result;
	}

	@Override
	public DataStream store(String key, DataStream value) throws Exception {
		
		if (value instanceof DataStreamNode) {
			return value;
		}
		return null;
	}

	@Override
	public void remove(String key) throws Exception {
		DataStreamNode node = lookup(key);
		if (node != null) {
			node.remove();
		}		
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends DataStream> fetch(Predicate<DataStream> predicate) throws Exception {
		
		List<? extends DataStreamNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.DATASTREAM))
				.toListExplicit(DataStreamNode.class);
		
		if (predicate == null) {
			predicate = (v) -> true;
		}
		
		return IterableUtils.transformedIterable(result, node -> enrich(node));
	}

	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}

}
