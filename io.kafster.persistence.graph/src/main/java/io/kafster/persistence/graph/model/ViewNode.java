/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.time.Instant;
import java.util.Map;

import io.kafster.datastream.DataStream;
import io.kafster.view.MutableView;
import io.kafster.view.ViewMetadata;

public class ViewNode extends KeyedNode implements MutableView {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String MODIFIED_PROP = "modified";
	public static final String CREATED_PROP = "created";	
	public static final String METADATA_PROP = "metadata";
	
	private ViewMetadata metadata;
	
	public interface RelationshipTypes {
		String CONFIGURATION = "configuration";
		String DATASTREAM = "datastream";
	}	

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public Instant getCreated() {
		Long created = getProperty(CREATED_PROP);
		if (created != null) {
			return Instant.ofEpochMilli(created);
		}
		return null;
	}

	@Override
	public Instant getModified() {
		Long created = getProperty(MODIFIED_PROP);
		if (created != null) {
			return Instant.ofEpochMilli(created);
		}
		return null;
	}

	@Override
	public ViewMetadata getMetadata() {
		return metadata;
	}

	@Override
	public Map<String, Object> getConfiguration() {
		PropertiesNode result = traverse(g -> g.out(RelationshipTypes.CONFIGURATION)).nextOrDefaultExplicit(PropertiesNode.class, null);
		if (result == null) {
			result = createToRelationship(PropertiesNode.class, RelationshipTypes.CONFIGURATION);
		}
		return result;
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);		
	}

	@Override
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);		
	}

	@Override
	public void setCreated(Instant timestamp) {
		setProperty(CREATED_PROP, Long.valueOf(timestamp.toEpochMilli()));		
	}

	@Override
	public void setModified(Instant timestamp) {
		setProperty(MODIFIED_PROP, Long.valueOf(timestamp.toEpochMilli()));		
	}

	@Override
	public void setMetadata(ViewMetadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public void setConfiguration(Map<String, Object> configuration) {
		Map<String, Object> current = getConfiguration();
		current.clear();
		current.putAll(configuration);		
	}
	
	public void setMetadataRef(String ref) {
		setProperty(METADATA_PROP, ref);
	}	
	
	public String getMetadataRef() {
		return getProperty(METADATA_PROP);		
	}

	@Override
	public DataStream getDataStream() {
		return traverse(g -> g.out(RelationshipTypes.DATASTREAM)).nextOrDefaultExplicit(DataStreamNode.class, null);
	}

	@Override
	public void setDataStream(DataStream stream) {
		
		DataStream current = getDataStream();
		if (current != null) {
			unlinkOut((DataStreamNode)current, RelationshipTypes.DATASTREAM);
		}
		
		if (stream instanceof DataStreamNode) {
			linkOut((DataStreamNode)stream, RelationshipTypes.DATASTREAM);
		}
		else {
			// TODO
		}
	}

}
