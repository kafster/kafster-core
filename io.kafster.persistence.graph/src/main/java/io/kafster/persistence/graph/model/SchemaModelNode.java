/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import javax.xml.namespace.QName;

import io.kafster.schema.MutableSchemaModel;

public class SchemaModelNode extends KeyedNode implements MutableSchemaModel {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String TITLE_PROP = "title";

	@Override
	public QName getName() {
		String qname = getProperty(NAME_PROP);
		if (qname != null) {
			return QName.valueOf(qname);
		}
		return null;
	}

	@Override
	public String getTitle() {
		return getProperty(TITLE_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public void setName(QName name) {
		if (name != null) {
			setProperty(NAME_PROP, name.toString());
		}
	}

	@Override
	public void setDescription(String description) {
		if (description != null) {
			setProperty(DESCRIPTION_PROP, description);
		}
	}

	@Override
	public void setTitle(String title) {
		if (title != null) {
			setProperty(TITLE_PROP, title);
		}
	}

}
