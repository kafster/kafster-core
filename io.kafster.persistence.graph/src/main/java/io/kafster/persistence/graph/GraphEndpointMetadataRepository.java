/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.endpoint.EndpointMetadata;
import io.kafster.persistence.EndpointMetadataRepository;
import io.kafster.persistence.graph.model.EndpointMetadataNode;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;

public class GraphEndpointMetadataRepository  implements EndpointMetadataRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphEndpointMetadataRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;	

	@Override
	public Optional<EndpointMetadata> fetch(String key) throws Exception {
		EndpointMetadataNode result = lookup(key);
		return Optional.ofNullable((result == null) ? null : enrich(result));
	}

	@Override
	public EndpointMetadata create(String key) throws Exception {
		EndpointMetadataNode result = factory.create(EndpointMetadataNode.class, Labels.ENDPOINT_METADATA);
		result.setKey(key);
		return result;
	}

	@Override
	public EndpointMetadata store(String key, EndpointMetadata value) throws Exception {
		return null;
	}

	@Override
	public void remove(String key) throws Exception {
		EndpointMetadataNode node = lookup(key);
		if (node != null) {
			node.remove();
		}		
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends EndpointMetadata> fetch(Predicate<EndpointMetadata> predicate) throws Exception {
		
		List<? extends EndpointMetadataNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT_METADATA))
				.toListExplicit(EndpointMetadataNode.class);
		
		if (predicate == null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(node -> enrich(node))
				.filter(predicate)
				.collect(Collectors.toList());	
	}
	
	private EndpointMetadataNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT_METADATA)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(EndpointMetadataNode.class, null);
	}	
	
	private EndpointMetadata enrich(EndpointMetadataNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}
	
	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}

}
