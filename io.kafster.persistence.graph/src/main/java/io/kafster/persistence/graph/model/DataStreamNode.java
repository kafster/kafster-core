/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import io.kafster.datastream.DataStream;

public class DataStreamNode extends KeyedNode implements DataStream {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String CONTENT_TYPE_PROP = "contentType";
	public static final String REF_PROP = "ref";
	
	private ReaderBuilder reader;

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}
	
	public void setName(String name) {
		setProperty(NAME_PROP, name);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}
	
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);
	}
	
	public String getRef() {
		return getProperty(REF_PROP);
	}
	
	public void setRef(String ref) {
		setProperty(REF_PROP, ref);
	}

	@Override
	public ReaderBuilder getReader() {
		return reader;
	}

	public void setReader(ReaderBuilder reader) {
		this.reader = reader;
	}

	@Override
	public String getContentType() {
		return getProperty(CONTENT_TYPE_PROP);
	}
	
	public void setContentType(String contentType) {
		setProperty(CONTENT_TYPE_PROP, contentType);
	}

}
