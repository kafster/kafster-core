/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.persistence.SubscriptionRepository;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;
import io.kafster.persistence.graph.model.SubscriptionNode;
import io.kafster.persistence.graph.model.SubscriptionStatusNode;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionStatus.State;

public class GraphSubscriptionRepository implements SubscriptionRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphSubscriptionRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;

	@Override
	public Optional<Subscription> fetch(String key) throws Exception {
		SubscriptionNode result = lookup(key);
		return Optional.ofNullable(result == null ? null : enrich(result));
	}
	
	protected SubscriptionNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.SUBSCRIPTION)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(SubscriptionNode.class, null);
	}

	@Override
	public Subscription create(String key) throws Exception {
		SubscriptionNode result = factory.create(SubscriptionNode.class, Labels.SUBSCRIPTION);
		result.setCreated(Instant.now());
		result.setModified(Instant.now());
		result.setKey(key);
		return result;
	}

	@Override
	public void remove(String key) throws Exception {
		SubscriptionNode node = lookup(key);
		if (node != null) {
			node.remove();
		}		
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends Subscription> fetch(Predicate<Subscription> predicate) throws Exception {
		
		List<? extends SubscriptionNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.SUBSCRIPTION))
				.toListExplicit(SubscriptionNode.class);
		
		if (predicate == null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(subscription -> enrich(subscription))
				.filter(predicate)
				.collect(Collectors.toList());
	}

	@Override
	public List<? extends Subscription> getByState(State... states) {
		
		if (states == null) {
			return null;
		}
		
		List<String> values = Arrays.stream(states).map(s -> s.name()).collect(Collectors.toList());
				
		// Administrative API.  Tenant is not considered.
		//
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.SUBSCRIPTION)
				.out(SubscriptionNode.RelationshipTypes.STATUS)
				.has(SubscriptionStatusNode.STATE_PROP, P.within(values)))
				.toListExplicit(SubscriptionNode.class);
	}
	
	private Subscription enrich(SubscriptionNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}	

	@Override
	public Subscription store(String key, Subscription value) throws Exception {
		
		if (value instanceof SubscriptionNode) {
			return value;
		}
		
		SubscriptionNode node = lookup(key);
		if (node == null) {
			node = (SubscriptionNode)create(key);
		}
		
		node.setName(value.getName());
		node.setDescription(value.getDescription());
		node.setSource(value.getSource());
		node.setPipeline(value.getPipeline());
		node.setTarget(value.getTarget());
		
		if (value.getConfiguration() != null) {
			node.getConfiguration().putAll(value.getConfiguration());
		}
		
		return enrich(node);
	}

	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}

}
