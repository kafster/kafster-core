/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.tx;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.springframework.beans.factory.FactoryBean;

import io.kafster.persistence.transaction.TransactionContext;

public class TransactionContextFactoryBean implements FactoryBean<TransactionContext> {
	
	private TransactionContext context;
	private Graph graph;

	@Override
	public TransactionContext getObject() throws Exception {
		if (context == null) {
			if ((graph != null) && graph.features().graph().supportsTransactions()) {
				context = new GraphTransactionContext(graph);
			}
		}
		return context;
	}

	@Override
	public Class<?> getObjectType() {
		return TransactionContext.class;
	}

}
