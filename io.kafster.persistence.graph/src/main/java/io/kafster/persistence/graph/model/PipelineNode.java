/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.pipeline.MutablePipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;

@GraphVertex(label=Labels.PIPELNE)
public class PipelineNode extends KeyedNode implements MutablePipeline {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String MODIFIED_PROP = "modified";
	public static final String CREATED_PROP = "created";
	public static final String VERSION_PROP = "version";
	public static final String REFERENCE_PROP = "ref";
	
	public interface RelationshipTypes {
	     String STAGE = "stage";
	     String SOURCE = "source";
	     String TARGET = "target";
	}	

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public String getVersion() {
		return getProperty(VERSION_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public Instant getCreated() {
		Long value = getProperty(CREATED_PROP);
		if (value != null) {
			return Instant.ofEpochMilli(value);
		}
		return null;
	}

	@Override
	public Instant getModified() {
		Long value = getProperty(MODIFIED_PROP);
		if (value != null) {
			return Instant.ofEpochMilli(value);
		}
		return null;
	}

	@Override
	public PipelineDestination getSource() {
		return traverse(v -> v.out(RelationshipTypes.SOURCE)).nextOrDefaultExplicit(PipelineDestinationNode.class, null);
	}

	@Override
	public PipelineDestination getTarget() {
		return traverse(v -> v.out(RelationshipTypes.TARGET)).nextOrDefaultExplicit(PipelineDestinationNode.class, null);
	}

	@Override
	public Iterable<? extends PipelineStage> getStages() {
		return traverse(v -> v.out(RelationshipTypes.STAGE)).toListExplicit(PipelineStageNode.class);
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);
	}

	@Override
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);
	}

	@Override
	public void setVersion(String version) {
		setProperty(VERSION_PROP, version);
	}

	@Override
	public void setModified(Instant modified) {
		setProperty(MODIFIED_PROP, modified.toEpochMilli());
	}

	@Override
	public void setCreated(Instant created) {
		setProperty(CREATED_PROP, created.toEpochMilli());
	}

	@Override
	public void setSource(PipelineDestination source) {
		if (source instanceof PipelineDestinationNode) {
			linkOut((PipelineDestinationNode)source, RelationshipTypes.SOURCE);
		}
	}
	
	public PipelineDestinationNode addSource() {
		PipelineDestinationNode result = (PipelineDestinationNode)getSource();
		if (result == null) {
			result = createToRelationship(PipelineDestinationNode.class, RelationshipTypes.SOURCE);
		}
		return result;
	}

	@Override
	public void setTarget(PipelineDestination target) {
		if (target instanceof PipelineDestinationNode) {
			linkOut((PipelineDestinationNode)target, RelationshipTypes.TARGET);
		}		
	}
	
	public PipelineDestinationNode addTarget() {
		PipelineDestinationNode result = (PipelineDestinationNode)getTarget();
		if (result == null) {
			result = createToRelationship(PipelineDestinationNode.class, RelationshipTypes.TARGET);
		}
		return result;
	}	

	@Override
	public PipelineStageBuilder createStage(PipelineStageMetadata metadata) throws Exception {
		
		return new PipelineStageBuilder() {
			
			private String condition;
			private Map<String, Object> configuration;

			@Override
			public PipelineStageBuilder condition(String expression) {
				this.condition = expression;
				return this;
			}

			@Override
			public PipelineStageBuilder key(String key) {
				return this;
			}

			@Override
			public PipelineStageBuilder configuration(Map<String, Object> props) {
				this.configuration = props;
				return this;
			}

			@Override
			public PipelineStage build() {
				
				PipelineStageNode node = createVertex(PipelineStageNode.class);
				node.setKey(UUID.randomUUID().toString());
				
				if (metadata != null) {
					node.setProvider(metadata.getName());
					node.setCondition(condition);
					node.setConfiguration(configuration);
				}
				return node;
			}
			
		};
	}
	
	@Override
	public PipelineStageNode addStage(PipelineStage stage) {
		
		if (stage instanceof PipelineStageNode) {
			linkOut((PipelineStageNode)stage, RelationshipTypes.STAGE);
			return (PipelineStageNode)stage;
		}
		
		PipelineStageNode node = createToRelationship(PipelineStageNode.class, RelationshipTypes.STAGE);
		
		String key = stage.getKey();
		if (key == null) {
			key = UUID.randomUUID().toString();
		}
		
		node.setKey(key);
		
		if (stage.getCondition() != null) {
			node.setCondition(stage.getCondition());
		}
		
		if (stage.getConfiguration() != null) {
			node.setConfiguration(stage.getConfiguration());
		}
		
		PipelineStageMetadata metadata = stage.getMetadata();
		if (metadata != null) {
			node.setProvider(metadata.getName());
		}
		
		return node;
	}

	@Override
	public boolean removeStage(PipelineStage stage) throws Exception {
		
		if (stage instanceof PipelineStageNode) {
			PipelineStageNode node = (PipelineStageNode)stage;
			
			unlinkOut(node, RelationshipTypes.STAGE);
			node.remove();
			
			return true;
		}
		return false;
	}
	
	public Boolean getReference() {
		return getProperty(REFERENCE_PROP);
	}
	
	public void setReference(Boolean ref) {
		setProperty(REFERENCE_PROP, ref);
	}

}
