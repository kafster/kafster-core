/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import org.apache.tinkerpop.gremlin.structure.T;

import com.syncleus.ferma.AbstractVertexFrame;
import com.syncleus.ferma.DefaultClassInitializer;

import io.kafster.persistence.graph.annotations.GraphVertex;

public class KeyedNode extends AbstractVertexFrame {

	public static final String KEY_PROP = "key";
	
	public void setKey(String key) {
		setProperty(KEY_PROP, key);
	}
	
	public String getKey() {
		return getProperty(KEY_PROP);
	}
	
	protected <V extends AbstractVertexFrame> V createVertex(Class<V> type) {

		V result = null;
		
		GraphVertex annotation = type.getAnnotation(GraphVertex.class);
		if (annotation != null) {	
			String label = annotation.label();
			result = getGraph().addFramedVertex(new DefaultClassInitializer<>(type), T.label, label);
		}
		else {
			result = getGraph().addFramedVertex(type);
		}

		return result;
	}	
	
	protected <V extends AbstractVertexFrame> V createToRelationship(Class<V> type, String relationship) {

		V result = createVertex(type);
		addFramedEdge(relationship, result);
		return result;
	}	
}
