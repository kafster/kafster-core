/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.time.Instant;
import java.util.Map;

import javax.security.auth.Subject;

import io.kafster.endpoint.Endpoint;
import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.pipeline.Pipeline;
import io.kafster.subscription.MutableSubscription;
import io.kafster.subscription.SubscriptionStatus;

@GraphVertex(label=Labels.SUBSCRIPTION)
public class SubscriptionNode extends KeyedNode implements MutableSubscription {

	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String MODIFIED_PROP = "modified";
	public static final String CREATED_PROP = "created";
	
	public interface RelationshipTypes {
	     String SOURCE = "source";
	     String TARGET = "target";
	     String PIPELINE = "pipeline";
	     String CONFIGURATION = "configuration";
	     String OVERRIDES = "overrides";
	     String STATUS = "status";
	}	

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}
	
	@Override
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);
	}	

	@Override
	public Subject getOwner() {
		return null;
	}

	@Override
	public Instant getModified() {
		Long modified = getProperty(MODIFIED_PROP);
		if (modified != null) {
			return Instant.ofEpochMilli(modified);
		}
		return null;
	}

	@Override
	public Instant getCreated() {
		Long created = getProperty(CREATED_PROP);
		if (created != null) {
			return Instant.ofEpochMilli(created);
		}
		return null;
	}

	@Override
	public Map<String, Object> getConfiguration() {
		PropertiesNode result = traverse(g -> g.out(RelationshipTypes.CONFIGURATION)).nextOrDefaultExplicit(PropertiesNode.class, null);
		if (result == null) {
			result = createToRelationship(PropertiesNode.class, RelationshipTypes.CONFIGURATION);
		}
		return result;
	}

	@Override
	public Endpoint getSource() {
		return getSourceNode();
	}
	
	public EndpointNode getSourceNode() {
		return traverse(g -> g.out(RelationshipTypes.SOURCE)).nextOrDefaultExplicit(EndpointNode.class, null);
	}

	@Override
	public Endpoint getTarget() {
		return getTargetNode();
	}
	
	public EndpointNode getTargetNode() {
		return traverse(g -> g.out(RelationshipTypes.TARGET)).nextOrDefaultExplicit(EndpointNode.class, null);
	}

	@Override
	public Pipeline getPipeline() {
		return getPipelineNode();
	}
	
	public PipelineNode getPipelineNode() {
		return traverse(g -> g.out(RelationshipTypes.PIPELINE)).nextOrDefaultExplicit(PipelineNode.class, null);
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);
	}

	@Override
	public void setOwner(Subject owner) {
		// TODO
	}

	@Override
	public void setModified(Instant timestamp) {
		setProperty(MODIFIED_PROP, Long.valueOf(timestamp.toEpochMilli()));
	}

	@Override
	public void setCreated(Instant timestamp) {
		setProperty(CREATED_PROP, Long.valueOf(timestamp.toEpochMilli()));
	}
	
	protected EndpointNode findEndpoint(String key) {
		return traverse(g -> g.V()
				.hasLabel(Labels.ENDPOINT)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(EndpointNode.class, null);
	}

	@Override
	public void setSource(Endpoint source) {
		
		Endpoint current = getSource();
		if (current != null) {
			unlinkOut((EndpointNode)current, RelationshipTypes.SOURCE);
		}
		
		if (source == null) {
			return;
		}
		
		if (source instanceof EndpointNode) {
			linkOut((EndpointNode)source, RelationshipTypes.SOURCE);
		}
		else {
			EndpointNode node = findEndpoint(source.getKey());
			if (node != null) {
				linkOut(node, RelationshipTypes.SOURCE);
			}
			else {
				// Reference by key
				//
				node = createToRelationship(EndpointNode.class, RelationshipTypes.SOURCE);
				node.setKey(source.getKey());
				node.setReference(true);
			}
		}
	}

	@Override
	public void setTarget(Endpoint target) {
		
		Endpoint current = getTarget();
		if (current != null) {
			unlinkOut((EndpointNode)current, RelationshipTypes.TARGET);
		}
		
		if (target == null) {
			return;
		}
		
		if (target instanceof EndpointNode) {
			linkOut((EndpointNode)target, RelationshipTypes.TARGET);
		}	
		else {
			EndpointNode node = findEndpoint(target.getKey());
			if (node != null) {
				linkOut(node, RelationshipTypes.TARGET);
			}
			else {
				// Reference by key
				//
				node = createToRelationship(EndpointNode.class, RelationshipTypes.TARGET);
				node.setKey(target.getKey());
				node.setReference(true);
			}
		}
	}

	@Override
	public void setPipeline(Pipeline pipeline) {
		
		PipelineNode current = getPipelineNode();
		if (current != null) {
			unlinkOut(current, RelationshipTypes.PIPELINE);
		}
		
		if (pipeline == null) {
			return;
		}
		
		if (pipeline instanceof PipelineNode) {
			linkOut((PipelineNode)pipeline, RelationshipTypes.PIPELINE);
		}
		else {
			// Reference by key
			//
			PipelineNode node = createToRelationship(PipelineNode.class, RelationshipTypes.PIPELINE);
			node.setKey(pipeline.getKey());
			node.setReference(true);
		}
	}

	@Override
	public Map<String, Object> getOverrides() {
		PropertiesNode result = traverse(g -> g.out(RelationshipTypes.OVERRIDES)).nextOrDefaultExplicit(PropertiesNode.class, null);
		if (result == null) {
			result = createToRelationship(PropertiesNode.class, RelationshipTypes.OVERRIDES);
		}
		return result;
	}

	@Override
	public SubscriptionStatus getStatus() {
		SubscriptionStatusNode result = traverse(g -> g.out(RelationshipTypes.STATUS)).nextOrDefaultExplicit(SubscriptionStatusNode.class, null);
		if (result == null) {
			result = createToRelationship(SubscriptionStatusNode.class, RelationshipTypes.STATUS);
		}
		return result;
	}

	@Override
	public void setStatus(SubscriptionStatus status) {
	}

}
