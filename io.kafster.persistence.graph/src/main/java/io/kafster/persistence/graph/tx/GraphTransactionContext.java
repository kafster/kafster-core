/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.tx;

import org.apache.tinkerpop.gremlin.structure.Graph;

import io.kafster.persistence.transaction.TransactionContext;

public class GraphTransactionContext implements TransactionContext {
	
	private Graph graph;
	
	public GraphTransactionContext(Graph graph) {
		this.graph = graph;
	}

	@Override
	public Transaction getTransaction() {
		return new GraphTransaction();
	}
	
	protected class GraphTransaction implements Transaction {

		@Override
		public boolean isOpen() {
			return graph.tx().isOpen();
		}

		@Override
		public void open() {
			graph.tx().open();
		}

		@Override
		public void commit() {
			graph.tx().commit();
		}

		@Override
		public void rollback() {
			graph.tx().rollback();
		}

		@Override
		public void close() {
			graph.tx().close();
		}
		
	}

}
