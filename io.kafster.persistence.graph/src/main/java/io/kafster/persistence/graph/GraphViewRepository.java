/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.syncleus.ferma.FramedGraph;

import io.kafster.persistence.ViewRepository;
import io.kafster.persistence.graph.model.KeyedNode;
import io.kafster.persistence.graph.model.Labels;
import io.kafster.persistence.graph.model.ViewNode;
import io.kafster.view.View;

public class GraphViewRepository implements ViewRepository {
	
	private static final Logger log = LoggerFactory.getLogger(GraphViewRepository.class);
	
	private FramedGraph graph;
	private VertexFactory factory;
	
	private NodeEnricher enricher;

	@Override
	public Optional<View> fetch(String key) throws Exception {
		ViewNode result = lookup(key);
		return Optional.ofNullable(result == null ? null : enrich(result));
	}
	
	protected ViewNode lookup(String key) {
		return graph.traverse(g -> g.V()
				.hasLabel(Labels.VIEW)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(ViewNode.class, null);
	}

	@Override
	public View create(String key) throws Exception {
		ViewNode result = factory.create(ViewNode.class, Labels.VIEW);
		result.setCreated(Instant.now());
		result.setModified(Instant.now());
		result.setKey(key);
		return result;
	}

	@Override
	public View store(String key, View value) throws Exception {
		
		if (value instanceof ViewNode) {
			return value;
		}
		
		ViewNode node = lookup(key);
		if (node == null) {
			node = (ViewNode)create(key);
			if (value.getCreated() == null) {
				node.setCreated(Instant.now());
			}
		}
		
		node.setKey(key);
		
		if (value.getCreated() != null) {
			node.setCreated(value.getCreated());
		}
		
		if (value.getModified() != null) {
			node.setModified(value.getModified());
		}
		
		if (value.getName() != null) {
			node.setName(value.getName());
		}
		
		if (value.getDescription() != null) {
			node.setDescription(value.getDescription());
		}
		
		if (value.getMetadata() != null) {
			node.setMetadataRef(value.getMetadata().getKey());
			node.setMetadata(value.getMetadata());
		}
		
		if (value.getConfiguration() != null) {
			node.setConfiguration(value.getConfiguration());
		}
		
		return node;
	}

	@Override
	public void remove(String key) throws Exception {
		
		ViewNode node = lookup(key);
		if (node != null) {
			node.remove();
		}
	}

	@Override
	public boolean exists(String key) throws Exception {
		return fetch(key) != null;
	}

	@Override
	public Iterable<? extends View> fetch(Predicate<View> predicate) throws Exception {
		
		List<? extends ViewNode> result = graph.traverse(g -> g.V()
				.hasLabel(Labels.VIEW))
				.toListExplicit(ViewNode.class);
		
		if (predicate != null) {
			predicate = (v) -> true;
		}
		
		return result.stream()
				.map(destination -> enrich(destination))
				.filter(predicate)
				.collect(Collectors.toList());
	}
	
	private View enrich(ViewNode node) {
		try {
			return enricher.enrich(node);
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return node;
	}

	public void setGraph(FramedGraph graph) {
		this.graph = graph;
	}

	public void setFactory(VertexFactory factory) {
		this.factory = factory;
	}

	public void setEnricher(NodeEnricher enricher) {
		this.enricher = enricher;
	}	

}
