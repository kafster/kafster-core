/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import io.kafster.Removable;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.MutableEndpointProvider;
import io.kafster.io.Resource;
import io.kafster.persistence.graph.annotations.GraphVertex;

@GraphVertex(label=Labels.ENDPOINT_PROVIDER)
public class EndpointProviderNode extends KeyedNode implements MutableEndpointProvider, Removable {
	
	public static final String NAME_PROP = "name";
	public static final String PRESENTATION_NAME_PROP = "presentation_name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String MODIFIED_PROP = "modified";
	public static final String CREATED_PROP = "created";
	public static final String VERSION_PROP = "version";
	public static final String REFERENCE_PROP = "ref";
	
	public interface RelationshipTypes {
	     String ENDPOINT = "endpoint";
	}

	@Override
	public Optional<Endpoint> getByKey(String key) throws Exception {
		return Optional.ofNullable(traverse(v -> v.out(RelationshipTypes.ENDPOINT)
				.has(KeyedNode.KEY_PROP, key))
				.nextOrDefaultExplicit(EndpointNode.class, null));
	}

	@Override
	public Optional<Endpoint> getByUri(String uri, Map<String, Object> props) throws Exception {
		return Optional.ofNullable(traverse(v -> v.out(RelationshipTypes.ENDPOINT)
				.has(EndpointNode.URI_PROP, uri))
				.nextOrDefaultExplicit(EndpointNode.class, null));
	}

	@Override
	public Iterable<? extends Endpoint> getEndpoints(EndpointFilter filter) throws Exception {
		return traverse(v -> v.out(RelationshipTypes.ENDPOINT)).toListExplicit(EndpointNode.class);
	}

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public String getPresentationName() {
		return getProperty(PRESENTATION_NAME_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public String getVersion() {
		return getProperty(VERSION_PROP);
	}

	@Override
	public Resource getImage() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addEndpoint(Endpoint endpoint) throws Exception {
		
		if (endpoint instanceof EndpointNode) {
			
			if (getByKey(endpoint.getKey()).isPresent()) {
				return;
			}
			
			linkOut((EndpointNode)endpoint, RelationshipTypes.ENDPOINT);
		}
		else {
			EndpointNode node = createToRelationship(EndpointNode.class, RelationshipTypes.ENDPOINT);
			node.setReference(true);
			node.setKey(endpoint.getKey());
		}
	}

	@Override
	public void setDescription(String description) {
		setProperty(DESCRIPTION_PROP, description);
	}

	@Override
	public void setImage(Resource image) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);		
	}

	@Override
	public void setPresentationName(String name) {
		setProperty(PRESENTATION_NAME_PROP, name);		
	}

	@Override
	public void setVersion(String version) {
		setProperty(VERSION_PROP, version);		
	}
	
	public void setReference(Boolean ref) {
		setProperty(REFERENCE_PROP, ref);
	}
	
	public Boolean isReference() {
		return Optional.ofNullable((Boolean)getProperty(REFERENCE_PROP)).orElse(Boolean.FALSE); 
	}

}
