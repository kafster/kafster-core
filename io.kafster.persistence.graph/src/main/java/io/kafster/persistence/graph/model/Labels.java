/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

public interface Labels {

	String PIPELNE = "pipeline";
	String SUBSCRIPTION = "subscription";
	String ENDPOINT = "endpoint";
	String ENDPOINT_METADATA = "endpoint_metadata";
	String ENDPOINT_PROVIDER = "endpoint_provider";
	String VIEW = "view";
	String PIPELINE_STAGE = "stage";
	String PROPERTIES = "properties";
	String PIPELINE_DESTINATION = "pipeline_destination";
	String DATASTREAM = "datastream";
}
