/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.tx;

import java.util.Hashtable;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import io.kafster.persistence.transaction.TransactionContext;

public class TransactionContextPublisher implements BundleContextAware {

	private Graph graph;
	private BundleContext ctx;
	
	private ServiceRegistration<TransactionContext> registration;
	
	public void init() {
		if ((graph != null) && graph.features().graph().supportsTransactions()) {
			TransactionContext context = new GraphTransactionContext(graph);
			registration = ctx.registerService(TransactionContext.class, context, new Hashtable<>());
		}
	}
	
	public void shutdown() {
		if (registration != null) {
			try {
				registration.unregister();
			}
			finally {
				registration = null;
			}
		}
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}
}
