/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import com.syncleus.ferma.AbstractVertexFrame;

import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.schema.SchemaType;

@GraphVertex(label=Labels.PIPELINE_DESTINATION)
public class PipelineDestinationNode extends AbstractVertexFrame implements PipelineDestination {
	
	public static final String AS_PROP = "as";
	public static final String DESCRIPTION_PROP = "description";

	@Override
	public String getAs() {
		return getProperty(AS_PROP);
	}

	@Override
	public void setAs(String as) {
		setProperty(AS_PROP, as);
	}

	@Override
	public SchemaType getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setType(SchemaType type) {
		// TODO Auto-generated method stub
		
	}

}
