/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import io.kafster.Removable;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.MutableEndpointMetadata;
import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.schema.SchemaType;

@GraphVertex(label=Labels.ENDPOINT_METADATA)
public class EndpointMetadataNode extends KeyedNode implements MutableEndpointMetadata, Removable {
	
	public static final String NAME_PROP = "name";
	public static final String DESCRIPTION_PROP = "description";
	public static final String ROLE_PROP = "role";
	public static final String CONTENT_TYPE_PROP = "content_type";
	public static final String URI_TEMPLATE_PROP = "uri_template";
	
	private interface RelationshipTypes {
	     String PROVIDER = "metadata_provider";
	     String ENDPOINT_SCHEMA = "metadata_endpoint_schema";
	     String HEADER_SCHEMA = "metadata_header_schema";
	     String KEY_SCHEMA = "metadata_key_schema";
	     String PAYLOAD_SCHEMA = "metadata_payload_schema";
	}

	@Override
	public Role getRole() {
		String result = getProperty(ROLE_PROP);
		if (result != null) {
			return Role.valueOf(result);
		}
		return null;
	}

	@Override
	public String getName() {
		return getProperty(NAME_PROP);
	}

	@Override
	public String getDescription() {
		return getProperty(DESCRIPTION_PROP);
	}

	@Override
	public String getContentType() {
		return getProperty(CONTENT_TYPE_PROP);
	}

	@Override
	public SchemaType getEndpointSchema() {
		return traverse(g -> g.out(RelationshipTypes.ENDPOINT_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public SchemaType getPayloadSchema() {
		return traverse(g -> g.out(RelationshipTypes.PAYLOAD_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public SchemaType getHeaderSchema() {
		return traverse(g -> g.out(RelationshipTypes.HEADER_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public SchemaType getKeySchema() {
		return traverse(g -> g.out(RelationshipTypes.KEY_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public EndpointProvider getProvider() {
		return getProviderNode();
	}
	
	public EndpointProviderNode getProviderNode() {
		return traverse(g -> g.out(RelationshipTypes.PROVIDER)).nextOrDefaultExplicit(EndpointProviderNode.class, null);
	}

	@Override
	public void setContentType(String contentType) {
		setProperty(CONTENT_TYPE_PROP, contentType);
	}

	@Override
	public void setDescription(String descrption) {
		setProperty(DESCRIPTION_PROP, descrption);
	}

	@Override
	public void setEndpointSchema(SchemaType schema) {
		
		SchemaTypeNode current = traverse(g -> g.out(RelationshipTypes.ENDPOINT_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
		if (current != null) {
			unlinkOut(current, RelationshipTypes.ENDPOINT_SCHEMA);
		}
		
		if (schema instanceof SchemaTypeNode) {
			linkOut((SchemaTypeNode)schema, RelationshipTypes.ENDPOINT_SCHEMA);
		}		
	}

	@Override
	public void setHeaderSchema(SchemaType schema) {
		
		SchemaTypeNode current = traverse(g -> g.out(RelationshipTypes.HEADER_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
		if (current != null) {
			unlinkOut(current, RelationshipTypes.HEADER_SCHEMA);
		}
		
		if (schema instanceof SchemaTypeNode) {
			linkOut((SchemaTypeNode)schema, RelationshipTypes.HEADER_SCHEMA);
		}			
	}

	@Override
	public void setKeySchema(SchemaType schema) {

		SchemaTypeNode current = traverse(g -> g.out(RelationshipTypes.KEY_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
		if (current != null) {
			unlinkOut(current, RelationshipTypes.KEY_SCHEMA);
		}
		
		if (schema instanceof SchemaTypeNode) {
			linkOut((SchemaTypeNode)schema, RelationshipTypes.KEY_SCHEMA);
		}	
	}

	@Override
	public void setName(String name) {
		setProperty(NAME_PROP, name);
	}

	@Override
	public void setPayloadSchema(SchemaType schema) {
		
		SchemaTypeNode current = traverse(g -> g.out(RelationshipTypes.PAYLOAD_SCHEMA)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
		if (current != null) {
			unlinkOut(current, RelationshipTypes.PAYLOAD_SCHEMA);
		}
		
		if (schema instanceof SchemaTypeNode) {
			linkOut((SchemaTypeNode)schema, RelationshipTypes.PAYLOAD_SCHEMA);
		}			
	}

	@Override
	public void setProvider(EndpointProvider provider) {
		
		EndpointProviderNode current = getProviderNode();
		if (current != null) {
			unlinkOut(current, RelationshipTypes.PROVIDER);
		}
		
		if (provider == null) {
			return;
		}
		
		if (provider instanceof EndpointProviderNode) {
			linkOut((EndpointProviderNode)provider, RelationshipTypes.PROVIDER);
		}
		else {
			// Reference by key
			//
			EndpointProviderNode node = createToRelationship(EndpointProviderNode.class, RelationshipTypes.PROVIDER);
			node.setKey(provider.getKey());
			node.setReference(true);
		}		
		
	}

	@Override
	public void setRole(Role role) {
		setProperty(ROLE_PROP, role.name());
	}

	@Override
	public String getUriTemplate() throws Exception {
		return getProperty(URI_TEMPLATE_PROP);
	}

	@Override
	public void setUriTemplate(String template) {
		setProperty(URI_TEMPLATE_PROP, template);
	}

}
