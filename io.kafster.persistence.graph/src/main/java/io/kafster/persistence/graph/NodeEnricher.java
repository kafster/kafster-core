/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph;

import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.beans.JacksonResourceReaderBuilder;
import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointRegistry;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.io.ResourceLookup;
import io.kafster.persistence.graph.model.DataStreamNode;
import io.kafster.persistence.graph.model.EndpointMetadataNode;
import io.kafster.persistence.graph.model.EndpointNode;
import io.kafster.persistence.graph.model.EndpointProviderNode;
import io.kafster.persistence.graph.model.PipelineNode;
import io.kafster.persistence.graph.model.PipelineStageNode;
import io.kafster.persistence.graph.model.SubscriptionNode;
import io.kafster.persistence.graph.model.ViewNode;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageRegistry;
import io.kafster.subscription.MutableSubscription;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.beans.DelegatingMutableSubscription;
import io.kafster.view.View;
import io.kafster.view.ViewMetadataLookup;

public class NodeEnricher {
	
	private static final Logger log = LoggerFactory.getLogger(NodeEnricher.class);
	
	private PipelineStageRegistry stages;
	private EndpointProviderLookup providers;
	private EndpointRegistry endpoints;
	private PipelineLookup pipelines;
	private ViewMetadataLookup views;
	private ResourceLookup resources;
	
	private ObjectMapper mapper;

	public Endpoint enrich(EndpointNode endpoint) throws Exception {
		
		if (endpoint == null) {
			return null;
		}
		
		Boolean isRef = Optional.ofNullable(endpoint.getReference()).orElse(false);
		if (isRef) {
			return endpoints.getByKey(endpoint.getKey()).orElse(null);
		}
		
		return endpoint;
	}
	
	public EndpointProvider enrich(EndpointProviderNode provider) throws Exception {
		
		if ((provider != null) && provider.isReference()) {
			return providers.getProvider(provider.getKey()).orElse(null);
		}
		
		return provider;
	}	
	
	public EndpointMetadata enrich(EndpointMetadataNode metadata) throws Exception {
		
		if (metadata != null) {

		}
		
		return metadata;
	}	
	
	protected void setDestinationMetadata(EndpointProvider provider, EndpointNode endpoint) {
		
		try {
			if ((provider instanceof EndpointFactory) && (((EndpointFactory)provider).getEndpointTypes() != null)) {
				for (EndpointMetadata metadata : ((EndpointFactory)provider).getEndpointTypes()) {
					if (StringUtils.equals(metadata.getName(), endpoint.getEndpoint())) {
						endpoint.setMetadata(metadata);
						break;
					}
				}
			}
			else {
				// Must be a fixed destination
				//
				Endpoint fixed = provider.getByKey(endpoint.getKey())
						.orElse(provider.getByUri(endpoint.getUri(), Collections.emptyMap())
								.orElse(null));
				
				if ((fixed != null) && (fixed.getMetadata() != null)) {
					endpoint.setMetadata(fixed.getMetadata());
				}
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	public Pipeline enrich(PipelineNode pipeline) throws Exception {
		
		if (pipeline == null) {
			return null;
		}
		
		Boolean isRef = Optional.ofNullable(pipeline.getReference()).orElse(false);
		if (isRef) {
			return pipelines.getPipeline(pipeline.getKey()).orElse(null);
		}
		
		if (pipeline.getStages() != null) {
			for (PipelineStage s : pipeline.getStages()) {
				PipelineStageNode node = (PipelineStageNode)s;
				if (node.getProvider() != null) {
					node.setMetadata(stages.getStage(node.getProvider())); 
				}
			}
		}
		
		return pipeline;
	}
	
	public Subscription enrich(SubscriptionNode subscription) throws Exception {
		
		Endpoint source = enrich(subscription.getSourceNode());
		Endpoint target = enrich(subscription.getTargetNode());
		Pipeline pipeline = enrich(subscription.getPipelineNode());
		
		MutableSubscription result = new DelegatingMutableSubscription(subscription);
		result.setSource(source);
		result.setTarget(target);
		result.setPipeline(pipeline);
		
		return result;
	}
	
	public View enrich(ViewNode view) throws Exception {
		
		if (view == null) {
			return null;
		}
		
		String key = view.getMetadataRef();
		if (key != null) {
			views.getMetadata(key).ifPresent(m -> view.setMetadata(m));
		}
		
		return view;
	}
	
	public DataStream enrich(DataStreamNode stream) throws Exception {
		 
		if (stream != null) {
			if ((stream.getRef() != null) && (resources != null)) {
				resources.lookup(stream.getRef())
					.ifPresent(resource -> stream.setReader(new JacksonResourceReaderBuilder(resource, mapper)));
			}
		}
		
		return stream;
	}

	public void setStages(PipelineStageRegistry stages) {
		this.stages = stages;
	}

	public void setProviders(EndpointProviderLookup providers) {
		this.providers = providers;
	}

	public void setPipelines(PipelineLookup pipelines) {
		this.pipelines = pipelines;
	}

	public void setEndpoints(EndpointRegistry endpoints) {
		this.endpoints = endpoints;
	}

	public void setViews(ViewMetadataLookup views) {
		this.views = views;
	}

	public void setResources(ResourceLookup resources) {
		this.resources = resources;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}
}
