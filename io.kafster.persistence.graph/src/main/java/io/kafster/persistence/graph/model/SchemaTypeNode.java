/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;

import io.kafster.schema.Dependency;
import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;

public class SchemaTypeNode extends SchemaModelNode implements MutableSchemaType {
	
	public static final String IS_COLLECTION_PROP = "is_collection";
	public static final String IS_PRIMITIVE_PROP = "is_primtive";
	public static final String IS_ANY_PROP = "is_any";
	public static final String IS_REF_PROP = "is_ref";
	public static final String IS_ENUM_PROP = "is_enum";
	
	public static final String REFERENCE_PROP = "reference";
	public static final String MAX_INCLUSIVE_PROP = "imax";
	public static final String MIN_INCLUSIVE_PROP = "imin";
	public static final String MAX_EXCLUSIVE_PROP = "emax";
	public static final String MIN_EXCLUSIVE_PROP = "emin";	
	public static final String MULTIPLE_OF_PROP = "multiple_of";
	public static final String MIN_LENGTH_PROP = "min_length";
	public static final String MAX_LENGTH_PROP = "max_length";
	public static final String FORMAT_PROP = "format";
	public static final String PATTERN_PROP = "pattern";
	
	public interface RelationshipTypes {
	     String COLLECTION = "collection";
	     String PROPERTY = "property";
	}	

	@Override
	public boolean isCollection() {
		return Optional.ofNullable((Boolean)getProperty(IS_COLLECTION_PROP)).orElse(Boolean.FALSE);
	}

	@Override
	public boolean isPrimitive() {
		return Optional.ofNullable((Boolean)getProperty(IS_PRIMITIVE_PROP)).orElse(Boolean.FALSE);
	}

	@Override
	public boolean isAny() {
		return Optional.ofNullable((Boolean)getProperty(IS_ANY_PROP)).orElse(Boolean.FALSE);
	}

	@Override
	public boolean isRef() {
		return Optional.ofNullable((Boolean)getProperty(IS_REF_PROP)).orElse(Boolean.FALSE);
	}

	@Override
	public boolean isEnum() {
		return Optional.ofNullable((Boolean)getProperty(IS_ENUM_PROP)).orElse(Boolean.FALSE);
	}

	@Override
	public Object[] getEnumValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SchemaType getCollectionType() {
		 return traverse(g -> g.out(RelationshipTypes.COLLECTION)).nextOrDefaultExplicit(SchemaTypeNode.class, null);
	}

	@Override
	public String getReference() {
		return getProperty(REFERENCE_PROP);
	}

	@Override
	public Number getMaxInclusive() {
		return getProperty(MAX_INCLUSIVE_PROP);
	}

	@Override
	public Number getMaxExclusive() {
		return getProperty(MAX_EXCLUSIVE_PROP);
	}

	@Override
	public Number getMinInclusive() {
		return getProperty(MIN_INCLUSIVE_PROP);
	}

	@Override
	public Number getMinExclusive() {
		return getProperty(MIN_EXCLUSIVE_PROP);
	}

	@Override
	public Number getMultipleOf() {
		return getProperty(MULTIPLE_OF_PROP);
	}

	@Override
	public Integer getMinLength() {
		return getProperty(MIN_LENGTH_PROP);
	}

	@Override
	public Integer getMaxLength() {
		return getProperty(MAX_LENGTH_PROP);
	}

	@Override
	public String getPattern() {
		return getProperty(PATTERN_PROP);
	}

	@Override
	public String getFormat() {
		return getProperty(FORMAT_PROP);
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		Map<String, SchemaProperty> result = new HashMap<>();
		traverse(g -> g.out(RelationshipTypes.PROPERTY))
			.frame(SchemaPropertyNode.class)
			.forEachRemaining(p -> result.put(p.getName().getLocalPart(), p));
		
		return result;
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDependencies(Iterable<Dependency> dependencies) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnumValues(Object[] values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFormat(String format) {
		setProperty(FORMAT_PROP, format);
	}

	@Override
	public void setMaxExclusive(Number maxExclusive) {
		setProperty(MAX_EXCLUSIVE_PROP, maxExclusive);		
	}

	@Override
	public void setMaxInclusive(Number maxInclusive) {
		setProperty(MAX_INCLUSIVE_PROP, maxInclusive);
	}

	@Override
	public void setMaxLength(Integer length) {
		setProperty(MAX_LENGTH_PROP, length);		
	}

	@Override
	public void setMinExclusive(Number minExclusive) {
		setProperty(MIN_EXCLUSIVE_PROP, minExclusive);		
	}

	@Override
	public void setMinInclusive(Number minInclusive) {
		setProperty(MIN_INCLUSIVE_PROP, minInclusive);		
	}

	@Override
	public void setMultipleOf(Number multipleOf) {
		setProperty(MULTIPLE_OF_PROP, multipleOf);		
	}

	@Override
	public void setMinLength(Integer minLength) {
		setProperty(MIN_LENGTH_PROP, minLength);		
	}

	@Override
	public void setPattern(String pattern) {
		setProperty(PATTERN_PROP, pattern);		
	}

	@Override
	public void setReference(String ref) {
		setProperty(REFERENCE_PROP, ref);	
	}

	@Override
	public void setAny(boolean isAny) {
		setProperty(IS_ANY_PROP, isAny);			
	}

	@Override
	public void setCollection(boolean isCollection) {
		setProperty(IS_COLLECTION_PROP, isCollection);			
	}

	@Override
	public void setEnum(boolean isEnum) {
		setProperty(IS_ENUM_PROP, isEnum);			
	}

	@Override
	public void setPrimitive(boolean isPrimitive) {
		setProperty(IS_PRIMITIVE_PROP, isPrimitive);			
	}

	@Override
	public void setRef(boolean isRef) {
		setProperty(IS_REF_PROP, isRef);	
	}

	@Override
	public MutableSchemaProperty addProperty(String name) {
		SchemaPropertyNode property = createToRelationship(SchemaPropertyNode.class, RelationshipTypes.PROPERTY);
		property.setName(new QName(name));
		
		return property;
	}

}
