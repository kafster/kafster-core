/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.beans;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.springframework.beans.factory.FactoryBean;

import com.syncleus.ferma.DelegatingFramedGraph;
import com.syncleus.ferma.FramedGraph;

public class FramedGraphFactoryBean implements FactoryBean<FramedGraph> {

	private Graph graph;
	private FramedGraph framed;
	
	private boolean supportAnnotations = false;
	private boolean typeResolution = true;
	
	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	@Override
	public FramedGraph getObject() throws Exception {
		if (framed == null) {
			framed = new DelegatingFramedGraph<Graph>(graph, typeResolution, supportAnnotations);
		}
		return framed;
	}

	@Override
	public Class<?> getObjectType() {
		return FramedGraph.class;
	}
	
	public void setSupportAnnotations(boolean supportAnnotations) {
		this.supportAnnotations = supportAnnotations;
	}

	public void setTypeResolution(boolean typeResolution) {
		this.typeResolution = typeResolution;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}	

}
