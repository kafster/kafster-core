/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.collection;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.VertexProperty;

public class VertexMap<V> implements Map<String, V> {
	
	private Vertex vertex;
	
	public VertexMap(Vertex v) {
		this.vertex = v;
	}
	
	protected Vertex getVertex() {
		return vertex;
	}

	@Override
	public int size() {
		return vertex.keys().size();
	}

	@Override
	public boolean isEmpty() {
		return vertex.keys().isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return vertex.keys().contains(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return values().contains(value);
	}

	@Override
	public V get(Object key) {
		VertexProperty<V> property = vertex.property(String.valueOf(key));
		if ((property != null) && property.isPresent()) {
			return property.value();
		}
		return null;		
	}

	@Override
	public V put(String key, V value) {
		VertexProperty<V> property = vertex.property(key);
		if ((property == null) || !property.isPresent()) {
			vertex.property(key, value);
			return null;
		}
		V result = property.value();
		vertex.property(key, value);
		return result;
	}

	@Override
	public V remove(Object key) {
		VertexProperty<V> property = vertex.property(String.valueOf(key));
		if ((property != null) && property.isPresent()) {
			V result = property.value();
			property.remove();
			return result;
		}
		return null;
	}

	@Override
	public void putAll(Map<? extends String, ? extends V> m) {
		for (String key : m.keySet()) {
			put(key, m.get(key));
		}
	}

	@Override
	public void clear() {
		vertex.properties().forEachRemaining(v -> v.remove());
	}

	@Override
	public Set<String> keySet() {
		return vertex.keys();
	}

	@Override
	public Collection<V> values() {
		return IteratorUtils.toList(vertex.values());
	}

	@Override
	public Set<Map.Entry<String, V>> entrySet() {
		Set<Map.Entry<String, V>> result = new HashSet<Map.Entry<String, V>>();
		
		for (String key : vertex.keys()) {
			result.add(new SimpleImmutableEntry<String,V>(key, vertex.value(key)));
		}
		
		return result;
	}


}
