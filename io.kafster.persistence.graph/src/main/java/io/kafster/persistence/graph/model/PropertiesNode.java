/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.graph.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.tinkerpop.gremlin.structure.VertexProperty;

import com.syncleus.ferma.AbstractVertexFrame;
import com.syncleus.ferma.typeresolvers.PolymorphicTypeResolver;

import io.kafster.persistence.graph.annotations.GraphVertex;
import io.kafster.persistence.graph.collection.VertexMap;

@GraphVertex(label=Labels.PROPERTIES)
public class PropertiesNode extends AbstractVertexFrame implements Map<String, Object> {
	
	private static final String TYPE = "type";
	
	private Map<String,Object> delegate;
	
	private static Set<String> RESERVED = new HashSet<>();
	static {
		RESERVED.add(PolymorphicTypeResolver.TYPE_RESOLUTION_KEY);
	};
	
	protected Map<String, Object> getDelegate() {
		if (delegate == null) {
			delegate = new VertexMap<Object>(getElement());
		}
		return delegate;
	}

	public void put(String key, Object value, String type) {
		put(key, value);
		if (type != null) {
			VertexProperty<?> prop = getElement().property(key);
			prop.property(TYPE, type);
		}
	}
	
	public Object getType(String key) {
		VertexProperty<?> prop = getElement().property(key);
		if ((prop != null) && !prop.isPresent()) {
			return prop.property(TYPE);
		}
		return null;
	}
	
	public void destroy() {
		remove();
	}

	@Override
	public int size() {
		return getDelegate().size();
	}

	@Override
	public boolean isEmpty() {
		return getDelegate().isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return getDelegate().containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return getDelegate().containsValue(value);
	}

	@Override
	public Object get(Object key) {
		return getDelegate().get(key);
	}
	
	@Override
	public Object put(String key, Object value) {
		return getDelegate().put(key, value);
	}

	@Override
	public Object remove(Object key) {
		return getDelegate().remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		getDelegate().putAll(m);
	}

	@Override
	public void clear() {
		getDelegate().clear();
	}

	@Override
	public Set<String> keySet() {
		Set<String> result = new HashSet<>(getDelegate().keySet());
		result.removeAll(RESERVED);
		return result;
	}

	@Override
	public Collection<Object> values() {
		return getDelegate().values();
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		Set<Entry<String, Object>> result = new HashSet<>(getDelegate().entrySet());
		result.removeIf(entry -> RESERVED.contains(entry.getKey()));
		return result;
	}

}

