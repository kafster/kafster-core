/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.k8s;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.kafster.k8s.client.ClientSupplier;
import io.kafster.k8s.client.local.LocalClientSupplier;
import io.kafster.k8s.model.LabelIn;
import io.kafster.model.converter.ModelConverter;
import io.kafster.subscription.deploy.SubscriptionListener;

public class SubscriptionDeployerFactory implements ManagedServiceFactory {
	
	public static final String CLIENT_PROP = "client";
	public static final String LABELS_PROP = "labels";
	public static final String LABEL_IN_PROP = "labelIn";
	public static final String NAME_PROP = "name";
	public static final String NAMESPACE_PROP = "namespace";
	public static final String RESYNC_INTERVAL_PROP = "resyncInterval";
	
	private String defaultClient = LocalClientSupplier.NAME;
	
	private Map<String,SubscriptionDeployer> deployers = new ConcurrentHashMap<>();
	private Map<String,ClientSupplier> clients = new ConcurrentHashMap<>();
	
	private ModelConverter converter;
	private SubscriptionListener listener;

	@Override
	public String getName() {
		return "Subscription Deployer Factory";
	}

	@Override
	public synchronized void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		
		deleted(pid);
		
		SubscriptionDeployer deployer = new SubscriptionDeployer();
		deployer.setConverter(converter);
		deployer.setClient(selectClient(pid, properties));
		deployer.setLabelIn(getLabelIn(pid, properties));
		deployer.setLabels(getLabels(properties));
		deployer.setNamespace(getNamespace(properties));
		deployer.setResyncInterval(getResyncInterval(properties));
		deployer.setListener(listener);
		
		deployer.start();
		deployers.put(pid, deployer);
	}
	
	protected KubernetesClient selectClient(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		
		String client = (String) properties.get(CLIENT_PROP);
		if (client == null) {
			client = defaultClient;
		}
		
		ClientSupplier supplier = clients.get(client);
		if (supplier == null) {
			throw new ConfigurationException(pid, "Could not find a client with ID [" + client + "]");
		}
		
		return supplier.getClient();
	}
	
	protected LabelIn getLabelIn(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		
		String labelIn = (String) properties.get(LABEL_IN_PROP);
		if (labelIn != null) {
			String label = StringUtils.substringBefore(labelIn, '=');
			if (label != null) {
				String[] values = StringUtils.split(StringUtils.substringAfter(labelIn, '='), ',');
				if ((values != null) && (values.length > 0)) {
					return new LabelIn(label, values);
				}
			}
			
			throw new ConfigurationException(pid, "Invalid labelIn value.");
		}
		
		return null;
	}
	
	protected Map<String,String> getLabels(Dictionary<String, ?> properties) {
		
		String labels = (String) properties.get(LABELS_PROP);
		if (labels != null) {
			Map<String,String> result = new HashMap<>();
			
			String[] nvps = StringUtils.split(labels, ',');
			if ((nvps != null) && (nvps.length > 0)) {
				for (String nvp : nvps) {
					String name = StringUtils.substringBefore(nvp, '=');
					String value = StringUtils.substringAfter(nvp, '=');
					
					if ((name != null) && (value != null)) {
						result.put(name, value);
					}
				}
			}
			
			return result;
		}
		
		return null;
	}
	
	protected Long getResyncInterval(Dictionary<String, ?> properties) {
		
		String interval = (String) properties.get(RESYNC_INTERVAL_PROP);
		if (interval != null) {
			return Long.parseLong(interval);
		}
		
		return null;
	}
	
	protected String getNamespace(Dictionary<String, ?> properties) {
		return (String) properties.get(NAMESPACE_PROP);
	}

	@Override
	public synchronized void deleted(String pid) {
		SubscriptionDeployer deployer = deployers.remove(pid);
		if (deployer != null) {
			deployer.stop();
		}
	}
	
	public void setListener(SubscriptionListener listener) {
		this.listener = listener;
	}	

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}
	
	public void clientAdded(ClientSupplier client, Map<String,Object> props) {
		
		if (client == null) {
			return;
		}
		
		String name = (String) props.get(NAME_PROP);
		if (name != null) {
			clients.put(name, client);
		}
	}
	
	public void clientRemoved(ClientSupplier client, Map<String,Object> props) {
		
		if (client == null) {
			return;
		}
		
		String name = (String) props.get(NAME_PROP);
		if (name != null) {
			clients.remove(name);
		}
	}

	public void setDefaultClient(String defaultClient) {
		this.defaultClient = defaultClient;
	}

}
