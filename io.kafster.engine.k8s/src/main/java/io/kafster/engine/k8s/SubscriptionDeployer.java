/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.k8s;

import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.kafster.k8s.model.LabelIn;
import io.kafster.k8s.model.crd.SubscriptionCRD;
import io.kafster.k8s.model.crd.SubscriptionCRDList;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.deploy.SubscriptionListener;

public class SubscriptionDeployer implements ResourceEventHandler<SubscriptionCRD> {
	
	private static final Logger log = LoggerFactory.getLogger(SubscriptionDeployer.class);
	
	private SharedIndexInformer<SubscriptionCRD> informer;
	
	private String name;
	private Map<String,String> labels = Collections.emptyMap();
	private LabelIn labelIn;
	
	private String namespace;
	private long resyncInterval = 10 * 60 * 1000;	

	private SubscriptionListener listener;
	private ModelConverter converter;
	
	private KubernetesClient client;
	
	public void start() {
		
		if (informer == null) {
			
			String ns = (namespace == null) ? client.getNamespace() : namespace;
			if (ns == null) {
				ns = "default";
			}
			
			if (name != null) {
				informer = client
						.resources(SubscriptionCRD.class, SubscriptionCRDList.class)
						.inNamespace(ns)
						.withName(name)
						.inform(this, resyncInterval);
			}
			else if (labelIn != null) {
				informer = client
						.resources(SubscriptionCRD.class, SubscriptionCRDList.class)
						.inNamespace(ns)
						.withLabelIn(labelIn.getName(), labelIn.getValues())
						.inform(this, resyncInterval);				
			}
			else if (!labels.isEmpty()){
				informer = client
						.resources(SubscriptionCRD.class, SubscriptionCRDList.class)
						.inNamespace(ns)
						.withLabels(labels)
						.inform(this, resyncInterval);
			}
			else {
				informer = client
						.resources(SubscriptionCRD.class, SubscriptionCRDList.class)
						.inNamespace(ns)
						.inform(this, resyncInterval);
			}
		}		
	}
	
	public void stop() {
		
		if (informer != null) {
			try {
				informer.stop();
			}
			finally {
				informer = null;
			}
		}		
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}

	public void setLabelIn(LabelIn labelIn) {
		this.labelIn = labelIn;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setResyncInterval(Long resyncInterval) {
		if (resyncInterval != null) {
			this.resyncInterval = resyncInterval;
		}
	}

	public void setListener(SubscriptionListener listener) {
		this.listener = listener;
	}

	public void setClient(KubernetesClient client) {
		this.client = client;
	}
	
	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}
	
	protected Subscription getSubscription(SubscriptionCRD crd) {
		return converter.convert(crd, Subscription.class, Option.CHILDREN, Option.SCHEMA);
	}

	@Override
	public void onAdd(SubscriptionCRD subscription) {
		if (listener != null) {
			try {
				listener.subscriptionAdded(getSubscription(subscription));
			} 
			catch (Exception e) {
				log.error("Could not add subscription [" + subscription.getSpec() + "]", e);
			}
		}
	}

	@Override
	public void onDelete(SubscriptionCRD subscription, boolean deletedFinalStateUnknown) {
		if (listener != null) {
			try {
				listener.subscriptionRemoved(getSubscription(subscription));
			} 
			catch (Exception e) {
				log.error("Could not remove subscription [" + subscription.getSpec() + "]", e);
			}
		}		
	}

	@Override
	public void onUpdate(SubscriptionCRD old, SubscriptionCRD updated) {
		if (listener != null) {
			try {
				listener.subscriptionUpdated(getSubscription(updated));
			} 
			catch (Exception e) {
				log.error("Could not update subscription [" + updated.getSpec() + "]", e);
			}
		}		
	}
}
