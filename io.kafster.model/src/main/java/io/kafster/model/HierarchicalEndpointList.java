/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="HierarchicalEndpoints")
@XmlType(name="HierarchicalEndpoints")
public class HierarchicalEndpointList {
	
	private String name;
	private String presentationName;
	private String description;
	
	private List<EndpointDetail> endpoints;
	private List<HierarchicalEndpointList> children;
	private HierarchicalEndpointList parent;
	private List<Link> links;
	
	public List<Link> getLinks() {
		return links;
	}
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	public List<EndpointDetail> getEndpoints() {
		return endpoints;
	}
	
	public void setEndpoints(List<EndpointDetail> endpoints) {
		this.endpoints = endpoints;
	}
	
	public void addEndpoint(EndpointDetail endpoint) {
		if (endpoints == null) {
			endpoints = new ArrayList<>();
		}
		endpoints.add(endpoint);
	}
	
	public List<HierarchicalEndpointList> getChildren() {
		return children;
	}
	
	public void setChildren(List<HierarchicalEndpointList> children) {
		this.children = children;
	}
	
	public void addChild(HierarchicalEndpointList child) {
		if (children == null) {
			children = new ArrayList<>();
		}
		children.add(child);
	}
	
	public void addAll(HierarchicalEndpointList from) {
		if (from != null) {
			if (from.getEndpoints() != null) {
				from.getEndpoints().forEach(endpoint -> addEndpoint(endpoint));
			}
			if (from.getChildren() != null) {
				from.getChildren().forEach(child -> addChild(child));
			}
		}
	}
	
	public HierarchicalEndpointList getParent() {
		return parent;
	}
	public void setParent(HierarchicalEndpointList parent) {
		this.parent = parent;
	}
	public String getPresentationName() {
		return presentationName;
	}
	public void setPresentationName(String presentationName) {
		this.presentationName = presentationName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
