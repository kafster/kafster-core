/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.workflow;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import io.kafster.model.Link;

@XmlRootElement
public class WorkflowEventList {

	private List<WorkflowEvent> events;
	private List<Link> links;
	
	public List<WorkflowEvent> getEvents() {
		return events;
	}
	public void setEvents(List<WorkflowEvent> events) {
		this.events = events;
	}
	public List<Link> getLinks() {
		return links;
	}
	public void setLinks(List<Link> links) {
		this.links = links;
	}
}
