/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.kafster.schema.SchemaType;
import io.kafster.schema.json.jackson.JSONSchemaDeserializer;
import io.kafster.schema.json.jackson.JSONSchemaSerializer;
import io.kafster.view.ViewMetadata.Parameter;

@XmlRootElement(name="ViewParameter")
@XmlType(name="ViewParameter")
public class ViewParameter implements Parameter {
	
	private String name;
	private String description;
	private ParameterType type;
	private ParameterSource source;
	
	@JsonSerialize(using = JSONSchemaSerializer.class)
	@JsonDeserialize(using = JSONSchemaDeserializer.class)
	private SchemaType schema;
	
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public ParameterType getType() {
		return type;
	}

	public void setType(ParameterType type) {
		this.type = type;
	}

	@Override
	public ParameterSource getSource() {
		return source;
	}

	public void setSource(ParameterSource source) {
		this.source = source;
	}

	@Override
	public SchemaType getSchema() {
		return schema;
	}

	public void setSchema(SchemaType schema) {
		this.schema = schema;
	}
}
