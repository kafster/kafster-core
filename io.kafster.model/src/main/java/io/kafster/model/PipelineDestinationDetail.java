/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.List;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="PipelineDestination")
public class PipelineDestinationDetail {

	private Model type;
	private String as;
	
	private List<Link> links;
	
	public PipelineDestinationDetail() {
	}

	public PipelineDestinationDetail(String as) {
		this.as = as;
	}
	
	public PipelineDestinationDetail(String as, Model type) {
		this.as = as;
		this.type = type;
	}
	
	public Model getType() {
		return type;
	}
	
	public void setType(Model type) {
		this.type = type;
	}
	
	public String getAs() {
		return as;
	}
	
	public void setAs(String as) {
		this.as = as;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}


}
