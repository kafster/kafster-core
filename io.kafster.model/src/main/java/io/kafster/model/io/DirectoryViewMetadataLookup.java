/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import io.kafster.io.Resource;
import io.kafster.io.util.FileResource;
import io.kafster.mime.MimeTypeService;

public class DirectoryViewMetadataLookup extends BaseViewMetadataLookup {
	
	private static final String SUFFIX = "json";
	
	private static final int CHECK_INTERVAL = 5000;
	
	private File directory;
	private boolean create = true;
	
	private long lastCheck = 0;
	private long lastModified = 0;
	
	private MimeTypeService mimeTypes;
	
	public void init() {
		if (!directory.exists() && create) {
			directory.mkdirs();
		}
	}
	
	@Override
	protected Iterable<Resource> getResources() {
		
		if ((directory != null) && directory.exists() && directory.canRead()) {
			File[] files = directory.listFiles(f -> StringUtils.equals(SUFFIX, FilenameUtils.getExtension(f.getName())));
			if (files != null) {
				List<Resource> result = new ArrayList<>();
				for (File file : files) {
					result.add(new FileResource(file, mimeTypes.getMimeType(file.getName())));
				}
				return result;
			}
		}
		return null;
	}

	@Override
	protected Resource getResource(String location) {
		if (directory.exists() && directory.canRead()) {
			File file = new File(directory, location);
			if (file.exists() && file.canRead()) {
				return new FileResource(file, mimeTypes.getMimeType(file.getName()));
			}
		}
		return null;
	}

	@Override
	protected long getLastModified() {
		
		if ((System.currentTimeMillis() - lastCheck) > CHECK_INTERVAL) {
			
			lastCheck = System.currentTimeMillis();
			
			File[] files = directory.listFiles(f -> StringUtils.equals(SUFFIX, FilenameUtils.getExtension(f.getName())));
			if (files != null) {
				for (File file : files) {
					if (file.lastModified() > lastModified) {
						lastModified = file.lastModified();
					}
				}
			}
		}
		
		return lastModified;
	}
	
	public void setLocation(String location) {
		this.directory = new File(location);
	}

	public void setCreate(boolean create) {
		this.create = create;
	}

	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}

}
