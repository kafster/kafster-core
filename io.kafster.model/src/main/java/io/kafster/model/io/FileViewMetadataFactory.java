/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.mime.MimeTypeService;
import io.kafster.model.converter.ModelConverter;
import io.kafster.view.ViewMetadataLookup;

public class FileViewMetadataFactory implements ManagedServiceFactory, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(FileViewMetadataFactory.class);
	
	public static final String PID = "io.kafster.view.fs";
	
	public static final String LOCATION_PROP = "location";
	public static final String CREATE_PROP = "create";
	
	private ObjectMapper mapper;
	private ModelConverter converter;
	private MimeTypeService mimeTypes;
	
	private Map<String,ServiceRegistration<ViewMetadataLookup>> lookups = new ConcurrentHashMap<>();
	
	private boolean enabled = true;
	private BundleContext ctx;

	@Override
	public String getName() {
		return "Kafster View Metadata Factory";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) {

		if (enabled) {
			String location = (String)properties.get(LOCATION_PROP);
			if (location != null) {
				DirectoryViewMetadataLookup lookup = new DirectoryViewMetadataLookup();
				lookup.setConverter(converter);
				lookup.setLocation(location);
				lookup.setMapper(mapper);
				lookup.setMimeTypes(mimeTypes);
				
				String create = (String)properties.get(CREATE_PROP);
				lookup.setCreate((create == null) || Boolean.parseBoolean(create));
				
				lookup.init();
				
				lookups.put(pid, ctx.registerService(ViewMetadataLookup.class, lookup, properties));
			}
			else {
				log.warn("Invalid configuration [" + pid + "]. A location is required");
			}
		}
	}

	@Override
	public void deleted(String pid) {
		ServiceRegistration<ViewMetadataLookup> registration = lookups.remove(pid);
		if (registration != null) {
			registration.unregister();
		}
	}

	public void setEnable(boolean enable) {
		this.enabled = enable;
	}
	
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}
	
	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}	

}
