/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.net.URL;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.FactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.model.PipelineDetail;
import io.kafster.model.converter.ModelConverter;
import io.kafster.pipeline.Pipeline;

public class JSONPipelineFactoryBean implements FactoryBean<Pipeline>, BundleContextAware {
	
	private String location;
	private ObjectMapper mapper;
	private ModelConverter converter;
	
	private Pipeline pipeline;
	
	private BundleContext ctx;

	@Override
	public Pipeline getObject() throws Exception {
		if (pipeline == null ) {
			URL url = ctx.getBundle().getEntry(location);
			if (url != null) {
				PipelineDetail info = mapper.readValue(url.openStream(), PipelineDetail.class);
				if (info != null) {
					pipeline = converter.convert(info, Pipeline.class);
				}
			}
		}
		return pipeline;
	}

	@Override
	public Class<?> getObjectType() {
		return Pipeline.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

}
