/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.model.PipelineDetail;
import io.kafster.model.converter.ModelConverter;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;
import io.kafster.pipeline.beans.DelegatingPipeline;

public class BundlePipelineLookup implements PipelineLookup, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(BundlePipelineLookup.class);
	
	private BundleContext ctx;
	private String location;
	private ObjectMapper mapper;
	private ModelConverter converter;
	
	private boolean lookupName = false;
	private boolean recurse = true;
	
	private Map<String,Pipeline> pipelines;
	
	@Override
	public Optional<Pipeline> getPipeline(String query) throws Exception {
		
		Map<String,Pipeline> pipelines = loadPipelines();
		if (pipelines.containsKey(query)) {
			return Optional.ofNullable(pipelines.get(query));
		}
		
		// Try looking up by name
		//
		if (lookupName) {
			for (Pipeline pipeline : pipelines.values()) {
				if (StringUtils.equals(pipeline.getName(), query)) {
					return Optional.of(pipeline);
				}
			}
		}
		
		return Optional.empty();
	}

	@Override
	public Iterable<Pipeline> getPipelines() throws Exception {
		return loadPipelines().values();
	}
	
	private synchronized Map<String,Pipeline> loadPipelines() throws Exception  {
		
		if (pipelines == null) {
			
			pipelines = new HashMap<>();
			if (location != null) {
				
				Enumeration<URL> resources = ctx.getBundle().findEntries(location, "*.json", recurse);
				if (resources != null) {
					
					while (resources.hasMoreElements()) {
						
						URL url = resources.nextElement();
						
						try (InputStream istrm = url.openStream()) {
							
							PipelineDetail info = mapper.readValue(istrm, PipelineDetail.class);
							if (info != null) {
								
								info.setCreated(new Date(ctx.getBundle().getLastModified()));
								info.setModified(new Date(ctx.getBundle().getLastModified()));
								
								if (info.getVersion() == null) {
									info.setVersion(ctx.getBundle().getVersion().toString());
								}
								
								// Most metadata, including pipeline stages, schemas etc., will be handled
								// by the converter.
								//
								Pipeline pipeline = converter.convert(info, Pipeline.class);
								if (pipeline != null) {
									// Wrap in a delegate so it is not mutable
									//
									pipelines.put(pipeline.getKey(), new DelegatingPipeline(pipeline));
								}
								else {
									log.warn("Could not create pipeline for [" + url + "");
								}
							}
						}
						catch (Exception e) {
							log.error("Could not create Pipeline for [" + url + "]", e);
						}
					}
				}
			}
		}	
		return pipelines;
	}
	
	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setLookupName(boolean lookupName) {
		this.lookupName = lookupName;
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

}
