/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.activation.URLDataSource;

import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.io.Resource;
import io.kafster.io.util.DataSourceResource;
import io.kafster.mime.MimeTypeService;

public class BundleViewMetadataLookup extends BaseViewMetadataLookup implements BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(BundleViewMetadataLookup.class);
	
	private String location;
	private boolean recurse = false;
	private boolean enabled = true;
	
	private BundleContext ctx;
	private MimeTypeService mimeTypes;
	
	@Override
	protected Iterable<Resource> getResources() {
		
		if (enabled) {
			Enumeration<URL> resources = ctx.getBundle().findEntries(location, "*.json", recurse);
			if (resources != null) {
				List<Resource> result = new ArrayList<>();
				
				while (resources.hasMoreElements()) {
					result.add(getResource(resources.nextElement()));
				}
				
				return result;
			}
		}
		
		return null;
	}

	@Override
	protected Resource getResource(String location) {
		URL resource = ctx.getBundle().getResource(location);
		if (resource == null) {
			resource = ctx.getBundle().getResource(this.location + "/" + location);
		}
		return getResource(resource);
	}
	
	protected Resource getResource(URL url) {
		
		if (enabled && (url != null)) {

			try {
				DataSourceResource result = new DataSourceResource(new URLDataSource(url));
				result.setModified(ctx.getBundle().getLastModified());
				result.setUri(url.toURI());
				result.setContentType(mimeTypes.getMimeType(url.getFile()));
				return result;
			} 
			catch (URISyntaxException e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		
		return null;
	}	

	@Override
	protected long getLastModified() {
		return ctx.getBundle().getLastModified();
	}
	
	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}	

	public void setEnable(boolean enable) {
		this.enabled = enable;
	}

	public void setMimeTypes(MimeTypeService mimeTypes) {
		this.mimeTypes = mimeTypes;
	}

}
