/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.io.Resource;
import io.kafster.model.Link;
import io.kafster.model.ViewDefinition;
import io.kafster.model.converter.ModelConverter;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.view.MutableViewMetadata;
import io.kafster.view.ViewMetadata;
import io.kafster.view.ViewMetadataLookup;

public abstract class BaseViewMetadataLookup implements ViewMetadataLookup {
	
	private static final Logger log = LoggerFactory.getLogger(BaseViewMetadataLookup.class);
	
	private ObjectMapper mapper;
	private ModelConverter converter;
	
	private Map<String,ViewMetadata> views;
	private long lastModified = 0;

	@Override
	public Iterable<? extends ViewMetadata> getMetadata() throws Exception {
		return getViews().values();
	}

	@Override
	public Optional<ViewMetadata> getMetadata(String key) throws Exception {
		return Optional.ofNullable(getViews().get(key));
	}
	
	private synchronized Map<String,ViewMetadata> getViews() throws Exception  {
		
		long modified = getLastModified();
				
		if ((views == null) || (lastModified < modified)) {
			
			views = new HashMap<>();
			
			Iterable<Resource> resources = getResources();
			if (resources != null) {

				for (Resource resource : resources) {
					
					try {
						ViewDefinition def = mapper.readValue(resource.getInputStream(), ViewDefinition.class);
						if (def != null) {
							
							ViewMetadata metadata = converter.convert(def, ViewMetadata.class, Option.CHILDREN, Option.SCHEMA);
							if ((metadata != null) && (metadata.getKey() != null)) {
								
								if (metadata instanceof MutableViewMetadata) {
									((MutableViewMetadata)metadata).setPreview(loadPreview(def));
								}
								
								views.put(metadata.getKey(), metadata);
							}
						}
					}
					catch (Exception e) {
						log.error("Could not load [" + resource + "]", e);
					}
				}
			}
			
			lastModified = modified;
		}	
		return views;
	}
	
	protected Resource loadPreview(ViewDefinition definition) throws URISyntaxException {
		
		// Links are ignored in the conversion, but a "preview" link is allowed
		// for in-bundle view definitions.
		//
		if (definition.getLinks() != null) {
			for (Link link : definition.getLinks()) {
				if (StringUtils.equals(link.getRel(), "preview")) {
					String href = link.getHref();
					if (href != null) {
						return getResource(href);
					}
				}
			}
		}	
		return null;
	}	
	
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}
	
	protected abstract Iterable<Resource> getResources();
	protected abstract Resource getResource(String location);
	protected abstract long getLastModified();

}
