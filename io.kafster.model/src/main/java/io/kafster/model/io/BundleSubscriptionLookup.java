/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.io;

import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.model.SubscriptionDetail;
import io.kafster.model.SubscriptionState;
import io.kafster.model.converter.ModelConverter;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionLookup;
import io.kafster.subscription.SubscriptionStatus.State;

public class BundleSubscriptionLookup implements SubscriptionLookup, BundleContextAware {

	private static final Logger log = LoggerFactory.getLogger(BundleSubscriptionLookup.class);
	
	private BundleContext ctx;
	private String location;
	private ObjectMapper mapper;
	
	private boolean lookupName = false;
	private boolean recurse = true;
	private boolean enabled = true;
	
	private ModelConverter converter;
	
	private Map<String,Subscription> subscriptions;
	
	@Override
	public Iterable<Subscription> getSubscriptions() throws Exception {
		
		if (enabled) {
			return loadSubscriptions().values();
		}
		
		return Collections.emptyList();
	}	

	@Override
	public Optional<Subscription> getSubscription(String query) throws Exception {
		
		if (enabled) {
			Map<String,Subscription> subscriptions = loadSubscriptions();
			if (subscriptions.containsKey(query)) {
				return Optional.ofNullable(subscriptions.get(query));
			}
			
			// Try looking up by name
			//
			if (lookupName) {
				for (Subscription subscription : subscriptions.values()) {
					if (StringUtils.equals(subscription.getName(), query)) {
						return Optional.of(subscription);
					}
				}
			}
		}
		
		return Optional.empty();
	}

	private synchronized Map<String,Subscription> loadSubscriptions() throws Exception  {
		
		if (subscriptions == null) {
			subscriptions = new HashMap<>();
			
			if (location != null) {
				
				Enumeration<URL> resources = ctx.getBundle().findEntries(location, "*.json", recurse);
				if (resources != null) {
					while (resources.hasMoreElements()) {
						
						URL url = resources.nextElement();
						
						try (InputStream istrm = url.openStream()) {
							
							SubscriptionDetail detail = mapper.readValue(istrm, SubscriptionDetail.class);
							if (detail != null) {
								detail.setCreated(new Date(ctx.getBundle().getLastModified()));
								detail.setModified(new Date(ctx.getBundle().getLastModified()));
								detail.setStatus(new SubscriptionState(State.AVAILABLE));
								
								if (detail.getVersion() == null) {
									detail.setVersion(ctx.getBundle().getVersion().toString());
								}
								
								Subscription subscription = converter.convert(detail, Subscription.class);
								if (subscription != null) {
									subscriptions.put(subscription.getKey(), subscription);
								}
							}
						}
						catch (Exception e) {
							log.error(e.getLocalizedMessage(), e);
						}
					}
				}
			}
		}	
		
		return subscriptions;
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setLookupName(boolean lookupName) {
		this.lookupName = lookupName;
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}	
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setConverter(ModelConverter converter) {
		this.converter = converter;
	}

}
