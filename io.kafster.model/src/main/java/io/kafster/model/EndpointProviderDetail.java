/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="EndpointProvider")
@XmlType(name="EndpointProvider")
public class EndpointProviderDetail {
	
	private String key;
	private String name;
	private String presentationName;
	private String description;
	private List<EndpointMetadataDetail> metadata;
	
	private EndpointProviderDetail parent;
	private List<EndpointProviderDetail> children;
	
	private List<EndpointDetail> endpoints;
	
	private List<Link> links;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<Link> getLinks() {
		return links;
	}
	
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	public String getPresentationName() {
		return presentationName;
	}
	
	public void setPresentationName(String presentationName) {
		this.presentationName = presentationName;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public List<EndpointMetadataDetail> getMetadata() {
		return metadata;
	}
	
	public void setMetadata(List<EndpointMetadataDetail> metadata) {
		this.metadata = metadata;
	}

	public List<EndpointProviderDetail> getChildren() {
		return children;
	}

	public void setChildren(List<EndpointProviderDetail> children) {
		this.children = children;
	}

	public EndpointProviderDetail getParent() {
		return parent;
	}

	public void setParent(EndpointProviderDetail parent) {
		this.parent = parent;
	}

	public List<EndpointDetail> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<EndpointDetail> endpoints) {
		this.endpoints = endpoints;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
