/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Pipeline")
@XmlType(name="Pipeline")
public class PipelineDetail {

	private String key;
	private String name; 
	private String description; 
	private String version;
	
	private Date created;
	private Date modified;
	
	private List<PipelineStageDetail> stages;
	private PipelineDestinationDetail source;
	private PipelineDestinationDetail target;
	
	private List<Link> links;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public List<PipelineStageDetail> getStages() {
		return stages;
	}

	public void setStages(List<PipelineStageDetail> stages) {
		this.stages = stages;
	}

	public PipelineDestinationDetail getSource() {
		return source;
	}

	public void setSource(PipelineDestinationDetail source) {
		this.source = source;
	}

	public PipelineDestinationDetail getTarget() {
		return target;
	}

	public void setTarget(PipelineDestinationDetail target) {
		this.target = target;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}
}
