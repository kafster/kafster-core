/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaType;
import io.kafster.schema.json.jackson.JSONSchemaDeserializer;
import io.kafster.schema.json.jackson.JSONSchemaSerializer;

@XmlRootElement(name="Model")
@XmlType(name="Model")
public class Model {

	private String description;
	private Date created;
	private Date modified;
	
	private List<Link> links;
	
	@JsonSerialize(using = JSONSchemaSerializer.class)
	@JsonDeserialize(using = JSONSchemaDeserializer.class)
	private SchemaType schema;

	public String getName() {
		if (schema == null) {
			return null;
		}
		return schema.getName() == null ? null : schema.getName().getLocalPart();
	}

	public void setName(String name) {
		if (schema instanceof MutableSchemaType) {
			QName qname = schema.getName();
			if (qname == null) {
				((MutableSchemaType)schema).setName(new QName(name));
			}
			else {
				((MutableSchemaType)schema).setName(new QName(qname.getNamespaceURI(), name));
			}
		}
	}

	public String getNamespace() {
		if (schema == null) {
			return null;
		}
		return schema.getName() == null ? null : schema.getName().getNamespaceURI();
	}

	public void setNamespace(String namespace) {
		if (schema instanceof MutableSchemaType) {
			QName qname = schema.getName();
			if (qname == null) {
				((MutableSchemaType)schema).setName(new QName(namespace, ""));
			}
			else {
				((MutableSchemaType)schema).setName(new QName(namespace, qname.getLocalPart()));
			}
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public SchemaType getSchema() {
		return schema;
	}

	public void setSchema(SchemaType schema) {
		this.schema = schema;
	}

	
}
