/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="EndpointMetadata")
@XmlType(name="EndpointMetadata")
public class EndpointMetadataDetail {

	private String name;
	private String description;
	private String role;
	
	private String contentType;
	
	private Model endpointSchema;
	private Model contentSchema;
	
	public Model getEndpointModel() {
		return endpointSchema;
	}
	
	public void setEndpointModel(Model schema) {
		this.endpointSchema = schema;
	}	
	
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public Model getContentModel() {
		return contentSchema;
	}
	
	public void setContentModel(Model contentSchema) {
		this.contentSchema = contentSchema;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}	
}
