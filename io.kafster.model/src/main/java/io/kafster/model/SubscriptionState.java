/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import io.kafster.subscription.SubscriptionStatus.State;

@XmlRootElement(name="SubscriptionState")
public class SubscriptionState {

	private String key;
	private State state;
	private String statusDetail;
	
	private List<Link> links;
	
	public SubscriptionState() {
	}

	public SubscriptionState(State state) {
		this.state = state;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getStatusDetail() {
		return statusDetail;
	}
	
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}
	
	public List<Link> getLinks() {
		return links;
	}
	
	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}	
}
