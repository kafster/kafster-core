/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Endpoints")
@XmlType(name="EndpointList")
public class EndpointList {

	private List<EndpointDetail> endpoints;
	private List<Link> links;
	
	public List<EndpointDetail> getEndpoints() {
		return endpoints;
	}
	
	public void setEndpoints(List<EndpointDetail> endpoints) {
		this.endpoints = endpoints;
	}
	
	public void addEndpoint(EndpointDetail endpoint) {
		if (endpoints == null) {
			endpoints = new ArrayList<>();
		}
		endpoints.add(endpoint);
	}
	
	public void addAll(EndpointList from) {
		if ((from != null) && (from.getEndpoints() != null)) {
			from.getEndpoints().forEach(endpoint -> addEndpoint(endpoint));
		}
	}
	
	public List<Link> getLinks() {
		return links;
	}
	
	public void setLinks(List<Link> links) {
		this.links = links;
	}	
}
