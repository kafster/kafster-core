/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.beans;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter;

public class SimpleModelConverter implements ModelConverter {
	
	private static final Logger log = LoggerFactory.getLogger(SimpleModelConverter.class);
	
	private ConverterFactory factory;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> O convert(I in, Class<O> type, Option... options) {
		
		Set<Option> opts = new HashSet<>(Arrays.asList(options));
		
		Converter<I, O> converter = (Converter<I, O>) factory.createConverter(in.getClass(), type, opts).orElse(null);
		if (converter != null) {
			return converter.convert(in, factory);
		}
		else {
			log.warn("No converter found for [" + in.getClass() + "] -> [" + type + "]");
		}
		
		return null;
	}

	public void setFactory(ConverterFactory factory) {
		this.factory = factory;
	}
	

}

