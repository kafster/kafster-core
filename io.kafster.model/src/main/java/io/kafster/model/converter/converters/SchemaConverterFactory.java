/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Optional;
import java.util.Set;

import javax.xml.namespace.QName;

import io.kafster.model.Model;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;

public class SchemaConverterFactory implements ConverterFactory {
	
	private Schema schema;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (SchemaType.class.isAssignableFrom(from) && Model.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new TypeToModel());
		}
		
		if (SchemaType.class.isAssignableFrom(to) && Model.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new ModelToType());
		}
		
		return Optional.empty();
	}

	protected class TypeToModel implements Converter<SchemaType,Model> {
		
		@Override
		public Model convert(SchemaType type, ConverterFactory factory) {
			
			if (type == null) {
				return null;
			}
			
			Model result = new Model();
			result.setDescription(type.getDescription());
			
			if (type.getName() != null) {
				result.setName(type.getName().getLocalPart());
				result.setNamespace(type.getName().getNamespaceURI());
			}
			
			if (type != null) {
				result.setSchema(type);
			}
			
			return result;
		}
		
	}
	
	protected class ModelToType implements Converter<Model,SchemaType> {

		@Override
		public SchemaType convert(Model model, ConverterFactory factory) {
			
			if (model != null) {
				
				SchemaType result = model.getSchema();
				if (result == null) {
					String name = model.getName();
					if (name != null) {
						
						try {
							result = schema.getType(new QName(model.getNamespace(), model.getName())).orElse(null);
						}
						catch (Exception e) {
							throw new RuntimeException(e);
						}
					}
				}
				
				return result;
			}
			
			return null;
		}
	}
	
	public void setSchema(Schema schema) {
		this.schema = schema;
	}

}
