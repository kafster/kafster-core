/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Optional;
import java.util.Set;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.beans.SimpleDataStream;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;

public class DataStreamConverterFactory implements ConverterFactory {

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (DataStream.class.isAssignableFrom(from) && DataStreamDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new StreamToDetail());
		}
		
		if (DataStream.class.isAssignableFrom(to) && DataStreamDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToStream());
		}
		
		return Optional.empty();
	}
	
	protected class StreamToDetail implements Converter<DataStream,DataStreamDetail> {

		@Override
		public DataStreamDetail convert(DataStream stream, ConverterFactory factory) {
			DataStreamDetail result = new DataStreamDetail();
			result.setKey(stream.getKey());
			result.setName(stream.getName());
			result.setDescription(stream.getDescription());
			result.setContentType(stream.getContentType());
			
			return result;
		}
	}
	
	protected class DetailToStream implements Converter<DataStreamDetail,DataStream> {

		@Override
		public DataStream convert(DataStreamDetail stream, ConverterFactory factory) {
			SimpleDataStream result = new SimpleDataStream();
			result.setKey(stream.getKey());
			result.setName(stream.getName());
			result.setDescription(stream.getDescription());
			result.setContentType(stream.getContentType());
			
			return result;
		}
		
	}

}
