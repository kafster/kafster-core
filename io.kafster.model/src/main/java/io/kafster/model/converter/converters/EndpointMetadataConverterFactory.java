/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Optional;
import java.util.Set;

import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.Endpoint.Role;
import io.kafster.endpoint.beans.SimpleEndpointMetadata;
import io.kafster.model.EndpointMetadataDetail;
import io.kafster.model.Model;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.schema.SchemaType;

public class EndpointMetadataConverterFactory implements ConverterFactory {

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (EndpointMetadata.class.isAssignableFrom(from) && EndpointMetadataDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new MetadataToDetail(options));
		}
		
		if (EndpointMetadata.class.isAssignableFrom(to) && EndpointMetadataDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToMetadata(options));
		}
		
		return Optional.empty();
	}
	
	protected class MetadataToDetail implements Converter<EndpointMetadata,EndpointMetadataDetail> {
		
		private Set<Option> options;
		
		public MetadataToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public EndpointMetadataDetail convert(EndpointMetadata metadata, ConverterFactory factory) {
			
			if (metadata == null) {
				return null;
			}
			
			try {
				EndpointMetadataDetail result = new EndpointMetadataDetail();
				result.setContentType(metadata.getContentType());
				result.setName(metadata.getName());
				result.setDescription(metadata.getDescription());
				
				if (options.contains(Option.SCHEMA)) {
					result.setEndpointModel(convert(metadata.getEndpointSchema(), factory));
					result.setContentModel(convert(metadata.getPayloadSchema(), factory));
				}
				
				if (metadata.getRole() != null) {
					result.setRole(metadata.getRole().name());
				}
				
				return result;
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		protected Model convert(SchemaType type, ConverterFactory factory) {
			return factory.createConverter(SchemaType.class, Model.class, options)
					.map(converter -> converter.convert(type, factory))
					.orElse(null);
		}
		
	}
	
	protected class DetailToMetadata implements Converter<EndpointMetadataDetail,EndpointMetadata> {
		
		private Set<Option> options;
		
		public DetailToMetadata(Set<Option> options) {
			this.options = options;
		}

		@Override
		public EndpointMetadata convert(EndpointMetadataDetail metadata, ConverterFactory factory) {
			
			if (metadata == null) {
				return null;
			}
			
			SimpleEndpointMetadata result = new SimpleEndpointMetadata();
			result.setContentType(metadata.getContentType());
			result.setName(metadata.getName());
			result.setRole(Role.valueOf(metadata.getRole()));
			result.setEndpointSchema(convert(metadata.getEndpointModel(), factory));
			result.setPayloadSchema(convert(metadata.getContentModel(), factory));
			
			return result;
		}
		
		protected SchemaType convert(Model model, ConverterFactory factory) {
			return factory.createConverter(Model.class, SchemaType.class, options)
					.map(converter -> converter.convert(model, factory))
					.orElse(null);
		}
		
	}

}
