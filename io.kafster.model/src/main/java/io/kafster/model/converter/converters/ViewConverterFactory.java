/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.datastream.DataStream;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.ViewConfiguration;
import io.kafster.model.ViewDefinition;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.view.View;
import io.kafster.view.ViewFactory;
import io.kafster.view.ViewFactory.ViewBuilder;
import io.kafster.view.ViewMetadata;
import io.kafster.view.ViewMetadataLookup;

public class ViewConverterFactory implements ConverterFactory {
	
	private static final Logger log = LoggerFactory.getLogger(ViewConverterFactory.class);
	
	private ViewMetadataLookup metadata;
	private ViewFactory factory;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (View.class.isAssignableFrom(from) && ViewConfiguration.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new ViewToConfiguration(options));
		}
		
		if (View.class.isAssignableFrom(to) && ViewConfiguration.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new ConfigurationToView(options));
		}
		
		return Optional.empty();
	}
	
	protected class ViewToConfiguration implements Converter<View,ViewConfiguration> {
		
		private Set<Option> options;
		
		public ViewToConfiguration(Set<Option> options) {
			this.options = options;
		}

		@Override
		public ViewConfiguration convert(View view, ConverterFactory factory) {
			
			ViewConfiguration result = new ViewConfiguration();
			result.setKey(view.getKey());
			result.setName(view.getName());
			result.setDescription(view.getDescription());
			result.setProperties(view.getConfiguration());
			
			if (view.getCreated() != null) {
				result.setCreated(view.getCreated().toEpochMilli());
			}
			
			if (view.getModified() != null) {
				result.setModified(view.getModified().toEpochMilli());
			}
			
			if (view.getMetadata() != null) {
				result.setDefinition(convert(view.getMetadata(), factory));
			}
			
			if (view.getDataStream() != null) {
				result.setDataStream(convert(view.getDataStream(), factory));
			}
			
			return result;
		}
		
		protected ViewDefinition convert(ViewMetadata metadata, ConverterFactory factory) {
			return factory.createConverter(ViewMetadata.class, ViewDefinition.class, options)
					.map(converter -> converter.convert(metadata, factory))
					.orElse(null);
		}
		
		protected DataStreamDetail convert(DataStream stream, ConverterFactory factory) {
			return factory.createConverter(DataStream.class, DataStreamDetail.class, options)
					.map(converter -> converter.convert(stream, factory))
					.orElse(null);
		}
		
	}
	
	protected class ConfigurationToView implements Converter<ViewConfiguration,View> {
		
		private Set<Option> options;
		
		public ConfigurationToView(Set<Option> options) {
			this.options = options;
		}

		@Override
		public View convert(ViewConfiguration configuration, ConverterFactory converters) {
			
			if ((configuration == null) || (configuration.getDefinition() == null)) {
				return null;
			}
			
			try {
				return metadata.getMetadata(configuration.getDefinition().getKey())
						.map(m -> create(configuration, m))
						.orElse(null);
			} 
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
		}
		
		protected View create(ViewConfiguration configuration, ViewMetadata metadata) {
			
			ViewBuilder builder = factory.createView();
			builder.created(Instant.now())
					.modified(Instant.now())
					.metadata(metadata);
			
			String key = configuration.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
			}
			
			builder.key(key);
			
			if (configuration.getName() != null) {
				builder.name(configuration.getName());
			}
			
			if (configuration.getDescription() != null) {
				builder.description(configuration.getDescription());
			}
			
			if (configuration.getProperties() != null) {
				builder.configuration(configuration.getProperties());
			}
			
			try {
				return builder.build();
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
		}
		
	}

}
