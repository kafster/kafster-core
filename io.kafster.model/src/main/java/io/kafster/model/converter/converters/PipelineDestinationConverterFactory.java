/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Optional;
import java.util.Set;

import io.kafster.model.Model;
import io.kafster.model.PipelineDestinationDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.beans.SimplePipelineDestination;
import io.kafster.schema.SchemaType;

public class PipelineDestinationConverterFactory implements ConverterFactory {

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (PipelineDestination.class.isAssignableFrom(from) && PipelineDestinationDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new DestinationToDetail(options));
		}
		
		if (PipelineDestination.class.isAssignableFrom(to) && PipelineDestinationDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToDestination(options));
		}
		
		return Optional.empty();
	}
	
	protected class DestinationToDetail implements Converter<PipelineDestination,PipelineDestinationDetail> {
		
		private Set<Option> options;
		
		public DestinationToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public PipelineDestinationDetail convert(PipelineDestination destination, ConverterFactory factory) {
			if (destination != null) {
				PipelineDestinationDetail result = new PipelineDestinationDetail();
				result.setAs(destination.getAs());
				result.setType(convert(destination.getType(), factory));
				return result;
			}
			return null;
		}
		
		protected Model convert(SchemaType type, ConverterFactory factory) {
			return factory.createConverter(SchemaType.class, Model.class, options)
					.map(converter -> converter.convert(type, factory))
					.orElse(null);
		}
		
	}
	
	protected class DetailToDestination implements Converter<PipelineDestinationDetail,PipelineDestination> {
		
		private Set<Option> options;
		
		public DetailToDestination(Set<Option> options) {
			this.options = options;
		}

		@Override
		public PipelineDestination convert(PipelineDestinationDetail destination, ConverterFactory factory) {
			if (destination != null) {
				SimplePipelineDestination result = new SimplePipelineDestination();
				result.setAs(destination.getAs());
				result.setType(convert(destination.getType(), factory));
				return result;
			}
			return null;
		}
		
		protected SchemaType convert(Model model, ConverterFactory factory) {
			return factory.createConverter(Model.class, SchemaType.class, options)
					.map(converter -> converter.convert(model, factory))
					.orElse(null);
		}
		
	}

}
