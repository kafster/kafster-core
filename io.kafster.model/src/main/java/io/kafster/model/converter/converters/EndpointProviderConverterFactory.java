/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.HierarchicalEndpointProvider;
import io.kafster.endpoint.beans.SimpleHierarchicalEndpointProvider;
import io.kafster.model.EndpointDetail;
import io.kafster.model.EndpointMetadataDetail;
import io.kafster.model.EndpointProviderDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;

public class EndpointProviderConverterFactory implements ConverterFactory {

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (EndpointProvider.class.isAssignableFrom(from) && EndpointProviderDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new ProviderToDetail(options));
		}
		
		if (EndpointProvider.class.isAssignableFrom(to) && EndpointProviderDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToProvider(options));
		}
		
		return Optional.empty();
	}
	
	protected class ProviderToDetail implements Converter<EndpointProvider,EndpointProviderDetail> {
		
		private Set<Option> options;
		
		public ProviderToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public EndpointProviderDetail convert(EndpointProvider provider, ConverterFactory factory) {
			
			if (provider == null) {
				return null;
			}
			
			try {
				EndpointProviderDetail result = new EndpointProviderDetail();
				result.setKey(provider.getKey());
				result.setName(provider.getName());
				result.setDescription(provider.getDescription());
				result.setPresentationName(provider.getPresentationName());
				
				if (provider instanceof EndpointFactory) {
					
					Set<EndpointMetadata> types = ((EndpointFactory)provider).getEndpointTypes();
					if ((types != null) && !types.isEmpty()) {
						factory.createConverter(EndpointMetadata.class, EndpointMetadataDetail.class, options)
							.ifPresent(converter -> {
								result.setMetadata(new ArrayList<>());
								types.forEach(t -> result.getMetadata().add(converter.convert(t, factory)));
							});
					}
				}
				
				if (provider instanceof HierarchicalEndpointProvider) {
					
					HierarchicalEndpointProvider hierarchy = (HierarchicalEndpointProvider)provider;
					if (hierarchy.getParent() != null) {
						result.setParent(convert(hierarchy.getParent(), factory));
					}
					
					if (options.contains(Option.CHILDREN)) {
						List<HierarchicalEndpointProvider> children = hierarchy.getChildren();
						if ((children != null) && !children.isEmpty()) {
							
							// Just one level deep
							//
							result.setChildren(new ArrayList<>());
							children.forEach(child -> {
								result.getChildren().add(convert(child, factory));
							});
						}
					}
				}
				
				if (options.contains(Option.CHILDREN)) {
					Iterable<? extends Endpoint> endpoints = provider.getEndpoints(null);
					if (endpoints != null) {
						result.setEndpoints(new ArrayList<>());
						
						factory.createConverter(Endpoint.class, EndpointDetail.class, Collections.emptySet())
							.ifPresent(converter -> {
								endpoints.forEach(endpoint -> result.getEndpoints().add(converter.convert(endpoint, factory)));
							});
					}
				}
				
				return result;
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
	protected class DetailToProvider implements Converter<EndpointProviderDetail,EndpointProvider> {
		
		private Set<Option> options;
		
		public DetailToProvider(Set<Option> options) {
			this.options = options;
		}

		@Override
		public EndpointProvider convert(EndpointProviderDetail provider, ConverterFactory factory) {
			
			if (provider == null) {
				return null;
			}
			
			SimpleHierarchicalEndpointProvider result = new SimpleHierarchicalEndpointProvider();
			result.setKey(provider.getKey());
			result.setName(provider.getName());
			result.setDescription(provider.getDescription());
			result.setPresentationName(provider.getPresentationName());
			
			List<EndpointMetadataDetail> metadata = provider.getMetadata();
			if ((metadata != null) && !metadata.isEmpty()) {
				factory.createConverter(EndpointMetadataDetail.class, EndpointMetadata.class, options)
					.ifPresent(converter -> {
						Set<EndpointMetadata> types = new HashSet<>();
						metadata.forEach(m -> types.add(converter.convert(m, factory)));
						result.setEndpointTypes(types);
					});
			}
			
			if (provider.getParent() != null) {
				result.setParent((HierarchicalEndpointProvider)convert(provider.getParent(), factory));
			}
			
			List<EndpointProviderDetail> children = provider.getChildren();
			if ((children != null) && !children.isEmpty()) {
				List<HierarchicalEndpointProvider> kids = new ArrayList<>();
				children.forEach(child -> {
					kids.add((HierarchicalEndpointProvider)convert(child, factory));
				});
				result.setChildren(kids);
			}
			
			return result;
		}
		
	}	

}
