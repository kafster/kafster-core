/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointFactory;
import io.kafster.endpoint.EndpointMetadata;
import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.endpoint.EndpointRegistry;
import io.kafster.endpoint.beans.SimpleEndpoint;
import io.kafster.model.EndpointDetail;
import io.kafster.model.EndpointMetadataDetail;
import io.kafster.model.EndpointProviderDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;

public class EndpointConverterFactory implements ConverterFactory {
	
	private static final Logger log = LoggerFactory.getLogger(EndpointConverterFactory.class);
	
	private EndpointRegistry sources;
	private EndpointRegistry targets;
	private EndpointProviderLookup providers;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (Endpoint.class.isAssignableFrom(from) && EndpointDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new EndpointToDetail(options));
		}
		
		if (Endpoint.class.isAssignableFrom(to) && EndpointDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToDestination(options));
		}
		
		return Optional.empty();
	}
	
	protected Endpoint getEndpoint(EndpointRegistry lookup, String uri) throws Exception {
		return lookup.getByUri(uri, Collections.emptyMap()).orElse(null);
	}
	
	protected class EndpointToDetail implements Converter<Endpoint,EndpointDetail> {
		
		private Set<Option> options;
		
		public EndpointToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public EndpointDetail convert(Endpoint endpoint, ConverterFactory factory) {
			
			if (endpoint == null) {
				return null;
			}
			
			try {
				EndpointDetail result = new EndpointDetail();
				result.setConfiguration(endpoint.getConfiguration());
				result.setKey(endpoint.getKey());
				result.setName(endpoint.getName());
				result.setUri(endpoint.getUri());
				
				if (endpoint.getCreated() != null) {
					result.setCreated(Date.from(endpoint.getCreated()));
				}
				
				if (endpoint.getModified() != null) {
					result.setModified(Date.from(endpoint.getModified()));
				}
				
				EndpointMetadata metadata = endpoint.getMetadata();
				if (metadata == null) {
					if (endpoint.getUri() != null) {
						Endpoint candidate = getEndpoint(sources, endpoint.getUri());
						
						if (candidate == null) {
							candidate = getEndpoint(targets, endpoint.getUri());
						}
						
						if (candidate != null) {
							metadata = candidate.getMetadata();
						}
					}
				}
				
				if (metadata != null) {
					result.setMetadata(convert(endpoint.getMetadata(), factory));
					
					EndpointProvider provider = metadata.getProvider();
					if (provider != null) {
						EndpointProviderDetail detail = new EndpointProviderDetail();
						detail.setDescription(provider.getDescription());
						detail.setName(provider.getName());
						detail.setPresentationName(provider.getPresentationName());
						result.setProvider(detail);
					}
				}
				
				return result;
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
		}
		
		public EndpointMetadataDetail convert(EndpointMetadata metadata, ConverterFactory factory) {
			return factory.createConverter(EndpointMetadata.class, EndpointMetadataDetail.class, options)
						.map(converter -> converter.convert(metadata, factory))
						.orElse(null);
		}
		
	}
	
	protected class DetailToDestination implements Converter<EndpointDetail,Endpoint> {
		
		private Set<Option> options;
		
		public DetailToDestination(Set<Option> options) {
			this.options = options;
		}

		@Override
		public Endpoint convert(EndpointDetail endpoint, ConverterFactory factory) {
			
			if (endpoint == null) {
				return null;
			}
			
			SimpleEndpoint result = new SimpleEndpoint();
			result.setConfiguration(endpoint.getConfiguration());
			result.setKey(endpoint.getKey());
			result.setName(endpoint.getName());
			result.setMetadata(getMetadata(endpoint, factory));
			result.setUri(endpoint.getUri());
			
			if (endpoint.getModified() != null) {
				result.setModified(endpoint.getModified().toInstant());
			}
			
			if (endpoint.getCreated() != null)  {
				result.setCreated(endpoint.getCreated().toInstant());
			}
			
			return result;
		}
		
		protected EndpointMetadata getMetadata(EndpointDetail endpoint, ConverterFactory factory) {
			
			if (endpoint == null) {
				return null;
			}
			
			if (providers != null) {
				
				if ((endpoint.getProvider() != null) && (endpoint.getProvider().getName() != null)) {
					
					try {
						return providers.getProvider(endpoint.getProvider().getName())
							.map(provider -> getMetadata(provider, endpoint))
							.orElse(null);
					}
					catch (Exception e) {
						log.error(e.getLocalizedMessage(), e);
						throw new RuntimeException(e);
					}
				}
			}
			
			if (endpoint.getMetadata() != null) {
				return factory.createConverter(EndpointMetadataDetail.class, EndpointMetadata.class, options)
					.map(converter -> converter.convert(endpoint.getMetadata(), factory))
					.orElse(null);
			}
			
			return null;
		}
		
		protected EndpointMetadata getMetadata(EndpointProvider provider, EndpointDetail endpoint) {
			
			try {
				if (provider instanceof EndpointFactory) {
					
					EndpointFactory factory = (EndpointFactory)provider;
					
					Set<EndpointMetadata> candidates = factory.getEndpointTypes();
					if ((endpoint.getMetadata() != null) && (endpoint.getMetadata().getName() != null)) {
						for (EndpointMetadata candidate : candidates) {
							if (StringUtils.equals(endpoint.getMetadata().getName(), candidate.getName())) {
								return candidate;
							}
						}
					}
				}
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
			
			return null;
		}
		
	}

	public void setSources(EndpointRegistry sources) {
		this.sources = sources;
	}

	public void setTargets(EndpointRegistry targets) {
		this.targets = targets;
	}

	public void setProviders(EndpointProviderLookup providers) {
		this.providers = providers;
	}

}
