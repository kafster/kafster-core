/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import io.kafster.model.ViewDefinition;
import io.kafster.model.ViewParameter;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.view.ViewMetadata;
import io.kafster.view.ViewMetadata.Parameter;
import io.kafster.view.ViewMetadataLookup;
import io.kafster.view.beans.SimpleViewMetadata;

public class ViewMetadataConverterFactory implements ConverterFactory {
	
	private ViewMetadataLookup lookup;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (ViewMetadata.class.isAssignableFrom(from) && ViewDefinition.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new MetadataToDefinition(options));
		}
		
		if (ViewMetadata.class.isAssignableFrom(to) && ViewDefinition.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DefinitionToMetadata());
		}
		
		return Optional.empty();
	}
	
	protected class MetadataToDefinition implements Converter<ViewMetadata,ViewDefinition> {
		
		private Set<Option> options;
		
		public MetadataToDefinition(Set<Option> options) {
			this.options = options;
		}

		@Override
		public ViewDefinition convert(ViewMetadata metadata, ConverterFactory factory) {
			
			ViewDefinition result = new ViewDefinition();
			result.setAccepts(metadata.getAccepts());
			result.setDescription(metadata.getDescription());
			result.setKey(metadata.getKey());
			result.setName(metadata.getName());
			
			if (options.contains(Option.SCHEMA)) {
				result.setParameters(getParameters(metadata));
			}
			
			if (options.contains(Option.CHILDREN)) {
				result.setSpecifications(metadata.getSpecification());
			}
			return result;
		}
		
		protected List<ViewParameter> getParameters(ViewMetadata metadata) {
			List<Parameter> parameters = metadata.getParameters();
			if (parameters != null) {
				List<ViewParameter> result = new ArrayList<>();
				parameters.forEach(p -> result.add(convert(p)));
				return result;
			}
			return null;
		}
		
		protected ViewParameter convert(Parameter param) {
			ViewParameter result = new ViewParameter();
			result.setName(param.getName());
			result.setSource(param.getSource());
			result.setType(param.getType());
			result.setSchema(param.getSchema());
			return result;
		}
		
	}
	
	protected class DefinitionToMetadata implements Converter<ViewDefinition,ViewMetadata> {
		
		@Override
		public ViewMetadata convert(ViewDefinition definition, ConverterFactory factory) {
			
			SimpleViewMetadata result = new SimpleViewMetadata();
			result.setAccepts(definition.getAccepts());
			result.setDescription(definition.getDescription());
			result.setKey(definition.getKey());
			result.setName(definition.getName());
			result.setSpecification(definition.getSpecifications());
			
			if (definition.getParameters() != null) {
				List<Parameter> parameters = new ArrayList<>();
				definition.getParameters().forEach(p -> parameters.add(p));
				result.setParameters(parameters);
			}
			
			return result;
		}
		
	}
	
	public void setLookup(ViewMetadataLookup lookup) {
		this.lookup = lookup;
	}

}
