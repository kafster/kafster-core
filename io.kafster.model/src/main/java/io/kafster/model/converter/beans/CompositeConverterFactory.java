/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.beans;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;

public class CompositeConverterFactory implements ConverterFactory {
	
	private List<ConverterFactory> factories = new CopyOnWriteArrayList<>();

	@Override
	public <I,O> Optional<Converter<I,O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		for (ConverterFactory factory : factories) {
			Optional<Converter<I,O>> result = factory.createConverter(from, to, options);
			if (result.isPresent()) {
				return result;
			}
		}
		
		return Optional.empty();
	}

	public void setFactories(List<ConverterFactory> factories) {
		this.factories.addAll(factories);
	}
	
	public void factoryAdded(ConverterFactory factory, Map<String,Object> props) {
		
		if (factory == null) {
			return;
		}
		
		factories.add(factory);
	}
	
	public void factoryRemoved(ConverterFactory factory, Map<String,Object> props) {
		
		if (factory == null) {
			return;
		}
		
		factories.remove(factory);
	}
	
}
