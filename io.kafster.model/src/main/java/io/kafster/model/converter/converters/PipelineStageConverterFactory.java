/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.model.Model;
import io.kafster.model.PipelineStageDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.pipeline.PipelineStageRegistry;
import io.kafster.pipeline.beans.SimplePipelineStage;
import io.kafster.schema.SchemaType;

public class PipelineStageConverterFactory implements ConverterFactory {
	
	private static final Logger log = LoggerFactory.getLogger(PipelineStageConverterFactory.class);
	
	private PipelineStageRegistry registry;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (PipelineStage.class.isAssignableFrom(from) && PipelineStageDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new StageToDetail(options));
		}
		
		if (PipelineStage.class.isAssignableFrom(to) && PipelineStageDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToStage(options));
		}
		
		return Optional.empty();
	}
	
	protected class StageToDetail implements Converter<PipelineStage,PipelineStageDetail> {
		
		private Set<Option> options;
		
		public StageToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public PipelineStageDetail convert(PipelineStage stage, ConverterFactory factory) {
			
			if (stage == null) {
				return null;
			}
			
			PipelineStageDetail result = new PipelineStageDetail();
			result.setCondition(stage.getCondition());
			result.setConfiguration(stage.getConfiguration());
			result.setKey(stage.getKey());
			
			if (stage.getMetadata() != null) {
				
				try {
					result.setProvider(stage.getMetadata().getName());
					result.setPresentationName(stage.getMetadata().getPresentationName());
					result.setDescription(stage.getMetadata().getDescription());
					result.setModel(convert(stage.getMetadata().getSchema(), factory));
				}
				catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
					throw new RuntimeException(e);
				}
			}
			
			return result;
		}
		
		protected Model convert(SchemaType type, ConverterFactory factory) {
			return factory.createConverter(SchemaType.class, Model.class, options)
					.map(converter -> converter.convert(type, factory))
					.orElse(null);
		}
		
	}
	
	protected class DetailToStage implements Converter<PipelineStageDetail,PipelineStage> {
		
		private Set<Option> options;
		
		public DetailToStage(Set<Option> options) {
			this.options = options;
		}

		@Override
		public PipelineStage convert(PipelineStageDetail stage, ConverterFactory factory) {
			
			if (stage == null) {
				return null;
			}
			
			SimplePipelineStage result = new SimplePipelineStage();
			result.setCondition(stage.getCondition());
			result.setConfiguration(stage.getConfiguration());
			result.setKey(stage.getKey());
			result.setMetadata(getStageMetadata(stage.getProvider()));
			return result;
		}
		
		protected PipelineStageMetadata getStageMetadata(String provider) {
			
			if (provider == null) {
				return null;
			}
			
			try {
				return registry.getStage(provider);
			} 
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				throw new RuntimeException(e);
			}
		}
		
	}
	
	public void setRegistry(PipelineStageRegistry registry) {
		this.registry = registry;
	}

}
