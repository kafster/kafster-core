/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import io.kafster.endpoint.Endpoint;
import io.kafster.model.EndpointDetail;
import io.kafster.model.PipelineDetail;
import io.kafster.model.SubscriptionDetail;
import io.kafster.model.SubscriptionState;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineLookup;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.beans.SimpleSubscription;
import io.kafster.subscription.beans.SimpleSubscriptionStatus;

public class SubscriptionConverterFactory implements ConverterFactory {
	
	private PipelineLookup pipelines;

	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (Subscription.class.isAssignableFrom(from) && SubscriptionDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new SubscriptionToDetail(options));
		}
		
		if (Subscription.class.isAssignableFrom(to) && SubscriptionDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToSubscription(options));
		}
		
		return Optional.empty();
	}
	
	protected class SubscriptionToDetail implements Converter<Subscription,SubscriptionDetail> {
		
		private Set<Option> options;
		
		public SubscriptionToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public SubscriptionDetail convert(Subscription subscription, ConverterFactory factory) {
			
			if (subscription == null) {
				return null;
			}
			
			SubscriptionDetail result = new SubscriptionDetail();
			result.setKey(subscription.getKey());
			result.setName(subscription.getName());
			result.setCreated(Date.from(subscription.getCreated()));
			result.setModified(Date.from(subscription.getModified()));
			result.setConfiguration(subscription.getConfiguration());
			result.setDescription(subscription.getDescription());
			
			if (subscription.getStatus() != null) {
				SubscriptionState state = new SubscriptionState();
				state.setState(subscription.getStatus().getState());
				state.setStatusDetail(subscription.getStatus().getStateDetail());
				
				result.setStatus(state);
			}
			
			if (options.contains(Option.CHILDREN)) {
				result.setSource(convert(subscription.getSource(), factory));
				result.setPipeline(convert(subscription.getPipeline(), factory));
				result.setTarget(convert(subscription.getTarget(), factory));
			}
			
			return result;
		}
		
		protected PipelineDetail convert(Pipeline pipeline, ConverterFactory factory) {
			return factory.createConverter(Pipeline.class, PipelineDetail.class, options)
					.map(converter -> converter.convert(pipeline, factory))
					.orElse(null);
		}
		
		protected EndpointDetail convert(Endpoint destination, ConverterFactory factory) {
			return factory.createConverter(Endpoint.class, EndpointDetail.class, options)
					.map(converter -> converter.convert(destination, factory))
					.orElse(null);
		}
		
	}
	
	protected class DetailToSubscription implements Converter<SubscriptionDetail,Subscription> {
		
		private Set<Option> options;
		
		public DetailToSubscription(Set<Option> options) {
			this.options = options;
		}

		@Override
		public Subscription convert(SubscriptionDetail subscription, ConverterFactory factory) {
			
			if (subscription == null) {
				return null;
			}
			
			SimpleSubscription result = new SimpleSubscription();
			result.setConfiguration(subscription.getConfiguration());
			result.setDescription(subscription.getDescription());
			result.setKey(subscription.getKey());
			result.setName(subscription.getName());
			result.setSource(convert(subscription.getSource(), factory));
			result.setTarget(convert(subscription.getTarget(), factory));
			result.setStatus(new SimpleSubscriptionStatus());
			
			if (subscription.getPipeline() != null) {
				
				String key = subscription.getPipeline().getKey();
				if ((pipelines != null) && (key != null)) {
					
					try {
						pipelines.getPipeline(key).ifPresent(pipeline -> result.setPipeline(pipeline));
					}
					catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
				else {
					result.setPipeline(convert(subscription.getPipeline(), factory));
				}
			}
			
			if (subscription.getModified() != null) {
				result.setModified(subscription.getModified().toInstant());
			}
			
			if (subscription.getCreated() != null) {
				result.setCreated(subscription.getCreated().toInstant());
			}
			
			return result;
		}
		
		protected Pipeline convert(PipelineDetail pipeline, ConverterFactory factory) {
			return factory.createConverter(PipelineDetail.class, Pipeline.class, options)
				.map(converter -> converter.convert(pipeline, factory))
				.orElse(null);
		}
		
		protected Endpoint convert(EndpointDetail destination, ConverterFactory factory) {
			
			if (destination == null) {
				return null;
			}
			
			return factory.createConverter(EndpointDetail.class, Endpoint.class, options)
				.map(converter -> converter.convert(destination, factory))
				.orElse(null);
		}
		
	}
	
	public void setPipelines(PipelineLookup pipelines) {
		this.pipelines = pipelines;
	}

}
