/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model.converter.converters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.model.PipelineDestinationDetail;
import io.kafster.model.PipelineDetail;
import io.kafster.model.PipelineStageDetail;
import io.kafster.model.converter.Converter;
import io.kafster.model.converter.ConverterFactory;
import io.kafster.model.converter.ModelConverter.Option;
import io.kafster.pipeline.MutablePipeline;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineDestination;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.beans.SimplePipeline;

public class PipelineConverterFactory implements ConverterFactory {
	
	private static final Logger log = LoggerFactory.getLogger(PipelineConverterFactory.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public <I, O> Optional<Converter<I, O>> createConverter(Class<I> from, Class<O> to, Set<Option> options) {
		
		if (Pipeline.class.isAssignableFrom(from) && PipelineDetail.class.isAssignableFrom(to)) {
			return Optional.of((Converter<I, O>)new PipelineToDetail(options));
		}
		
		if (Pipeline.class.isAssignableFrom(to) && PipelineDetail.class.isAssignableFrom(from)) {
			return Optional.of((Converter<I, O>)new DetailToPipeline(options));
		}
		
		return Optional.empty();
	}
	
	protected PipelineDetail createDetail() {
		return new PipelineDetail();
	}
	
	protected MutablePipeline createPipeline() {
		return new SimplePipeline();
	}
	
	protected class PipelineToDetail implements Converter<Pipeline,PipelineDetail> {
		
		private Set<Option> options;
		
		public PipelineToDetail(Set<Option> options) {
			this.options = options;
		}

		@Override
		public PipelineDetail convert(Pipeline pipeline, ConverterFactory factory) {
			
			if (pipeline == null) {
				return null;
			}
			
			PipelineDetail result = createDetail();
			result.setDescription(pipeline.getDescription());
			result.setKey(pipeline.getKey());
			result.setName(pipeline.getName());
			result.setVersion(pipeline.getVersion());
			
			if (pipeline.getCreated() != null) {
				result.setCreated(Date.from(pipeline.getCreated()));
			}
			
			if (pipeline.getModified() != null) {
				result.setModified(Date.from(pipeline.getModified()));
			}		
			
			if (options.contains(Option.CHILDREN)) {
				result.setSource(convert(pipeline.getSource(), factory));
				result.setStages(getStages(pipeline.getStages(), factory));
				result.setTarget(convert(pipeline.getTarget(), factory));			
			}
			
			return result;
		}
		
		protected PipelineDestinationDetail convert(PipelineDestination destination, ConverterFactory factory) {
			if (destination == null) {
				return null;
			}
			
			return factory.createConverter(PipelineDestination.class, PipelineDestinationDetail.class, options)
					.map(converter -> converter.convert(destination, factory))
					.orElse(null);
		}
		
		protected PipelineStageDetail convert(PipelineStage stage, ConverterFactory factory) {
			
			if (stage == null) {
				return null;
			}
			
			return factory.createConverter(PipelineStage.class, PipelineStageDetail.class, options)
				.map(converter -> converter.convert(stage, factory))
				.orElse(null);
		}
		
		protected List<PipelineStageDetail> getStages(Iterable<? extends PipelineStage> stages, ConverterFactory factory) {
			
			if (stages == null) {
				return null;
			}
			
			List<PipelineStageDetail> result = new ArrayList<>();
			for (PipelineStage stage : stages) {
				result.add(convert(stage, factory));
			}
			
			return result;
		}
	}
	
	protected class DetailToPipeline implements Converter<PipelineDetail,Pipeline> {
		
		private Set<Option> options;
		
		public DetailToPipeline(Set<Option> options) {
			this.options = options;
		}

		@Override
		public Pipeline convert(PipelineDetail pipeline, ConverterFactory factory) {
			
			if (pipeline == null) {
				return null;
			}
			
			MutablePipeline result = createPipeline();
			result.setDescription(pipeline.getDescription());
			result.setKey(pipeline.getKey());
			result.setName(pipeline.getName());
			result.setVersion(pipeline.getVersion());
			
			if (pipeline.getCreated() != null) {
				result.setCreated(pipeline.getCreated().toInstant());
			}
			
			if (pipeline.getModified() != null) {
				result.setModified(pipeline.getModified().toInstant());
			}
			
			if (options.contains(Option.CHILDREN)) {
				result.setSource(convert(pipeline.getSource(), factory));
				result.setTarget(convert(pipeline.getTarget(), factory));
				
				List<PipelineStage> stages = getStages(pipeline.getStages(), factory);
				if (stages != null) {
					for (PipelineStage stage : stages) {
						try {
							result.addStage(stage);
						} 
						catch (Exception e) {
							log.error(e.getLocalizedMessage(), e);
							throw new RuntimeException(e);
						}
					}
				}
			}
			
			return result;
		}
		
		protected PipelineDestination convert(PipelineDestinationDetail destination, ConverterFactory factory) {
			
			if (destination == null) {
				return null;
			}
			
			return factory.createConverter(PipelineDestinationDetail.class, PipelineDestination.class, options)
				.map(converter -> converter.convert(destination, factory))
				.orElse(null);
		}
		
		protected PipelineStage convert(PipelineStageDetail stage, ConverterFactory factory) {
			
			if (stage == null) {
				return null;
			}
			
			return factory.createConverter(PipelineStageDetail.class, PipelineStage.class, options)
				.map(converter -> converter.convert(stage, factory))
				.orElse(null);
		}
		
		protected List<PipelineStage> getStages(List<PipelineStageDetail> stages, ConverterFactory factory) {
			
			if (stages == null) {
				return null;
			}
			
			List<PipelineStage> result = new ArrayList<>();
			for (PipelineStageDetail stage : stages) {
				result.add(convert(stage, factory));
			}
			
			return result;
		}
	}
}
