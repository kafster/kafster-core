/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="ViewDefinition")
@XmlType(name="ViewDefinition")
public class ViewDefinition {

	private String name;
	private String key;
	private String description;
	
	private Map<String,Object> specifications;
	private List<ViewParameter> parameters;
	private List<String> accepts;
	
	// Should include a "preview" link
	//
	private List<Link> links;
	
	public Map<String,Object> getSpecifications() {
		return specifications;
	}
	
	public void setSpecifications(Map<String,Object> specifications) {
		this.specifications = specifications;
	}

	public List<Link> getLinks() {
		return links;
	}
	
	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ViewParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<ViewParameter> parameters) {
		this.parameters = parameters;
	}

	public List<String> getAccepts() {
		return accepts;
	}

	public void setAccepts(List<String> accepts) {
		this.accepts = accepts;
	}
}
