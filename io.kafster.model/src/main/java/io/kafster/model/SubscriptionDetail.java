/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Subscription")
@XmlType(name="SubscriptionType")
public class SubscriptionDetail {

	private String key;
	private String name;
	private String description;
	private String version;
	private Date created;
	private Date modified;

	private EndpointDetail source;
	private EndpointDetail target;
	private PipelineDetail pipeline;
	
	private Map<String,Object> configuration;
	private Map<String,Object> overrides;
	
	private SubscriptionState status;
	
	private List<Link> links;

	public PipelineDetail getPipeline() {
		return pipeline;
	}
	
	public void setPipeline(PipelineDetail pipeline) {
		this.pipeline = pipeline;
	}

	public List<Link> getLinks() {
		return links;
	}
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	public EndpointDetail getSource() {
		return source;
	}

	public void setSource(EndpointDetail source) {
		this.source = source;
	}

	public EndpointDetail getTarget() {
		return target;
	}

	public void setTarget(EndpointDetail target) {
		this.target = target;
	}

	public Map<String, Object> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, Object> configuration) {
		this.configuration = configuration;
	}

	public Map<String, Object> getOverrides() {
		return overrides;
	}

	public void setOverrides(Map<String, Object> overrides) {
		this.overrides = overrides;
	}

	public SubscriptionState getStatus() {
		return status;
	}

	public void setStatus(SubscriptionState status) {
		this.status = status;
	}	
}
