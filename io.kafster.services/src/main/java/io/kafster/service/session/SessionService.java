/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.session;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.model.Credentials;
import io.kafster.model.Dashboard;
import io.kafster.model.Session;

@Path("")
public interface SessionService {

	@GET @Path("session")
	@Produces({MediaType.APPLICATION_JSON})
	Session getActiveSession(@Context UriInfo uris);	
	
	@POST @Path("session")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Session login(Credentials credentials, @Context UriInfo uris);	
	
	@POST @Path("session") 
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	Session loginForm(@FormParam("username") String user, @FormParam("password") String password, @Context UriInfo uris);	

	@DELETE @Path("session")
	@Produces({MediaType.APPLICATION_JSON})
	Session logout(@PathParam("key") String key, @Context UriInfo uris);	
	
	@PUT @Path("session/dashboard")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Response setHomeDashboard(Dashboard dashboard, @Context UriInfo uris);	
	
	@DELETE @Path("session/dashboard")
	@Produces({MediaType.APPLICATION_JSON})
	Response unsetHomeDashboard(@Context UriInfo uris);	
}
