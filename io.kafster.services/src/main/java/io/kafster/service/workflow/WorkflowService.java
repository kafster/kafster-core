/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.workflow;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import io.kafster.model.workflow.WorkflowEvent;
import io.kafster.model.workflow.WorkflowEventList;
import io.kafster.model.workflow.WorkflowResult;

@Path("workflow")
public interface WorkflowService {
	
	@GET @Path("{entity}/{key}")
	@Produces({MediaType.APPLICATION_JSON})
	WorkflowEventList getAvailableEntityEvents(@PathParam("entity") String entity, @PathParam("key") String key, @Context UriInfo uris);

	@POST @Path("{entity}/{key}/events")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	WorkflowResult processEvent(
			@PathParam("entity") String entity, 
			@PathParam("key") String key, 
			WorkflowEvent event, 
			@Context UriInfo uris);	
	
	@GET @Path("{entity}/{key}/events/{event}")
	@Produces({MediaType.APPLICATION_JSON})
	WorkflowResult getEventStatus(
			@PathParam("entity") String entity, 
			@PathParam("key") String key, 
			@PathParam("event") String event, 
			@Context UriInfo uris);		
	
	@GET @Path("{entity}/{key}/events")
	@Produces({MediaType.APPLICATION_JSON})
	WorkflowEventList getEventHistory(@PathParam("entity") String entity, @PathParam("key") String key, @Context UriInfo uris);	
}
