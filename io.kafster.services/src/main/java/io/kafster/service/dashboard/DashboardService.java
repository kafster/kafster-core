/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.dashboard;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.Dashboard;
import io.kafster.model.DashboardList;
import io.kafster.model.ViewConfiguration;
import io.kafster.model.ViewList;

@Path("")
public interface DashboardService {

	@GET @Path("dashboards")
	@Produces({MediaType.APPLICATION_JSON})
	DashboardList getDashboards(@Context UriInfo uris);
	
	@POST @Path("dashboards")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Response addDashboard(Dashboard dashboard, @Context UriInfo uris);	
	
	@POST @Path("dashboards")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	Response uploadDashboard(Multipart multipart, @Context UriInfo uris);	
	
	@GET @Path("dashboards/{key}")
	@Produces({MediaType.APPLICATION_JSON})
	Dashboard getDashboard(@PathParam("key") String key, @Context UriInfo uris);
	
	@GET @Path("dashboards/{key}/views")
	@Produces({MediaType.APPLICATION_JSON})
	ViewList getDashboardViews(@PathParam("key") String key, @Context UriInfo uris);	
	
	@POST @Path("dashboards/{key}/views")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Response addDashboardView(@PathParam("key") String key, ViewConfiguration view, @Context UriInfo uris);	
	
	@DELETE @Path("dashboards/{key}/views/{view}")
	void removeDashboardView(@PathParam("key") String key, @PathParam("key") String view, @Context UriInfo uris);		
	
	@DELETE @Path("dashboards/{key}")
	void removeDashboard(@PathParam("key") String key);	
}
