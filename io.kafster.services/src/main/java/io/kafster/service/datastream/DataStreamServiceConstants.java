/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.datastream;

public interface DataStreamServiceConstants {

	String EMTPY_STREAM  = "empty";
	
	String OFFSET_QUERY_PARAM = "offset";
	String COUNT_QUERY_PARAM = "count";
	String BEFORE_QUERY_PARAM = "before";
	String AFTER_QUERY_PARAM = "after";
	String FIELD_QUERY_PARAM = "field";
	
}
