/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.datastream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.DataStreamList;

@Path("")
public interface DataStreamService {
	
	@POST @Path("datastreams")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	DataStreamDetail uploadDataStream(Multipart definition, @Context UriInfo uris);	
	
	@GET @Path("datastreams")
	@Produces({MediaType.APPLICATION_JSON})
	DataStreamList getDataStreams(@Context UriInfo uris);		
	
	@GET @Path("datastreams/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	DataStreamDetail getDataStream(@PathParam("id") String dataset, @Context UriInfo uris);	
	
	@DELETE @Path("datastreams/{id}")
	void removeDataStream(@PathParam("id") String dataset, @Context UriInfo uris);		
	
	@GET @Path("datastreams/{id}/peek")
	@Produces({MediaType.WILDCARD})
	Response peek( @PathParam("id") String dataset, @Context UriInfo uris);	
	
}
