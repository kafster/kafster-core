/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.model;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.model.Model;
import io.kafster.model.ModelList;

@Path("")
public interface ModelService {

	@GET @Path("models")
	@Produces({MediaType.APPLICATION_JSON})
	ModelList getModels(@QueryParam("filter") String filter, @Context UriInfo uris);
	
	@GET @Path("models/{name}")
	@Produces({MediaType.APPLICATION_JSON})
	Model getModel(@PathParam("name") String name, @Context UriInfo uris);
	
	@POST @Path("models/{name}/schema")
	@Produces({MediaType.WILDCARD})
	Response getModelSchema(@PathParam("name") String name, @QueryParam("format") String format, @Context UriInfo uris);	
}
