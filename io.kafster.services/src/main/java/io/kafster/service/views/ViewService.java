/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.views;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.DataStreamDetail;
import io.kafster.model.ViewConfiguration;
import io.kafster.model.ViewDefinition;
import io.kafster.model.ViewDefinitionList;
import io.kafster.model.ViewList;

@Path("")
public interface ViewService {

	@GET @Path("views")
	@Produces({MediaType.APPLICATION_JSON})
	ViewList getViews(
			@QueryParam("definitions") @DefaultValue("true") boolean includeDefinitions, 
			@Context UriInfo uris);
	
	@POST @Path("views")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	ViewConfiguration addView(ViewConfiguration configuration, @Context UriInfo uris);	
	
	@GET @Path("views/view/{view}")
	@Produces({MediaType.APPLICATION_JSON})
	ViewConfiguration getView(@PathParam("view") String view, @Context UriInfo uris);
	
	@PUT @Path("views/view/{view}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	ViewConfiguration updateView(@PathParam("view") String view, ViewConfiguration configuration, @Context UriInfo uris);	
	
	@GET @Path("views/view/{view}/datastream")
	@Produces({MediaType.APPLICATION_JSON})
	DataStreamDetail getViewDataStream(@PathParam("view") String view, @Context UriInfo uris);		
	
	@PUT @Path("views/view/{view}/datastream")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	DataStreamDetail setViewDataStream(@PathParam("view") String view, DataStreamDetail datasource, @Context UriInfo uris);		
	
	@GET @Path("views/definitions")
	@Produces({MediaType.APPLICATION_JSON})
	ViewDefinitionList getViewDefinitions(@Context UriInfo uris);
	
	@POST @Path("views/definitions")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	Response uploadDefinition(Multipart definition, @Context UriInfo uris);		
	
	@GET @Path("views/definitions/{definition}")
	@Produces({MediaType.APPLICATION_JSON})
	ViewDefinition getViewDefinition(@PathParam("definition") String definition, @Context UriInfo uris);
	
	@GET @Path("views/definitions/{definition}/image")
	@Produces({MediaType.WILDCARD})
	Response getViewDefinitionImage(@PathParam("definition") String definition, @Context UriInfo uris, @Context Request request);
	
}


