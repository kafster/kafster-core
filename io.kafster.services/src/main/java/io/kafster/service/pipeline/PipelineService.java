/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.pipeline;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.ModelList;
import io.kafster.model.PipelineDestinationDetail;
import io.kafster.model.PipelineDetail;
import io.kafster.model.PipelineList;
import io.kafster.model.PipelineStageDescriptorList;
import io.kafster.model.PipelineStageDetail;
import io.kafster.model.PipelineStageList;

@Path("")
public interface PipelineService {

	@GET @Path("pipelines")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineList getPipelines(@QueryParam("query") String query, @Context UriInfo uris);
	
	@POST @Path("pipelines")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Response addPipeline(PipelineDetail pipeline, @Context UriInfo uris);
	
	@POST @Path("pipelines")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	Response uploadPipeline(Multipart upload, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineDetail getPipeline(@PathParam("key") String key, @Context UriInfo uris);
	
	@PUT @Path("pipelines/{key}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	PipelineDetail updatePipeline(@PathParam("key") String key, PipelineDetail pipeline, @Context UriInfo uris);	
	
	@DELETE @Path("pipelines/{key}")
	void deletePipeline(@PathParam("key") String key);	
	
	@GET @Path("pipelines/{key}/providers")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineStageDescriptorList getStageProviders(@PathParam("key") String key, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}/providers/{provider}/image")
	@Produces({MediaType.WILDCARD})
	Response getStageProviderImage(
			@PathParam("key") String key, 
			@PathParam("provider") String provider, 
			@Context UriInfo uris,
			@Context Request request);	
	
	@GET @Path("pipelines/{key}/stages")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineStageList getPipelineStages(@PathParam("key") String key, @Context UriInfo uris);
	
	@POST @Path("pipelines/{key}/stages")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	Response addPipelineStage(@PathParam("key") String key, PipelineStageDetail stage, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}/stages/{stage}")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineStageDetail getPipelineStage(@PathParam("key") String key, @PathParam("stage") String stage, @Context UriInfo uris);	
	
	@PUT @Path("pipelines/{key}/stages/{stage}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	PipelineStageDetail updatePipelineStage(@PathParam("key") String key, @PathParam("stage") String stage, PipelineStageDetail update, @Context UriInfo uris);		
	
	@DELETE @Path("pipelines/{key}/stages/{stage}")
	void deletePipelineStage(@PathParam("key") String key, @PathParam("stage") String stage);	
	
	@GET @Path("pipelines/{key}/in")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineDestinationDetail getPipelineInput(@PathParam("key") String key, @Context UriInfo uris);	
	
	@PUT @Path("pipelines/{key}/in")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	PipelineDestinationDetail updatePipelineInput(@PathParam("key") String key, PipelineDestinationDetail in, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}/in/models")
	@Produces({MediaType.APPLICATION_JSON})
	ModelList getPipelineInputModels(@PathParam("key") String key, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}/out")
	@Produces({MediaType.APPLICATION_JSON})
	PipelineDestinationDetail getPipelineOutput(@PathParam("key") String key, @Context UriInfo uris);
	
	@PUT @Path("pipelines/{key}/out")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	PipelineDestinationDetail updatePipelineOutput(@PathParam("key") String key, PipelineDestinationDetail in, @Context UriInfo uris);	
	
	@GET @Path("pipelines/{key}/out/models")
	@Produces({MediaType.APPLICATION_JSON})
	ModelList getPipelineOutputModels(@PathParam("key") String key, @Context UriInfo uris);	
}
