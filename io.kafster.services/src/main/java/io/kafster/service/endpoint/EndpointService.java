/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.model.EndpointDetail;
import io.kafster.model.HierarchicalEndpointList;

@Path("")
public interface EndpointService {

	@GET @Path("endpoints")
	@Produces({ MediaType.APPLICATION_JSON })
	HierarchicalEndpointList getEndpoints(
			@QueryParam("filter") String filter, 
			@QueryParam("provider") String provider, 
			@QueryParam("depth") @DefaultValue("1") Integer depth,
			@Context UriInfo uris);

	@POST @Path("endpoints")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	Response createEndpoint(EndpointDetail endpoint, @Context UriInfo uris);
	
	@GET
	@Path("endpoints/{key}")
	@Produces({ MediaType.APPLICATION_JSON })
	EndpointDetail getEndpoint(@PathParam("key") String key, @Context UriInfo uris);

	@PUT
	@Path("endpoints/{key}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	EndpointDetail updateEndpoint(@PathParam("key") String key, EndpointDetail endpoint, @Context UriInfo uris);

	@DELETE
	@Path("endpoints/{key}")
	void removeEndpoint(@PathParam("key") String key);

}
