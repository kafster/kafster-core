/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.service.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.kafster.endpoint.Endpoint.Role;
import io.kafster.jaxrs.multipart.Multipart;
import io.kafster.model.EndpointDetail;
import io.kafster.model.EndpointProviderDetail;
import io.kafster.model.EndpointProviderList;
import io.kafster.model.ExternalReference;
import io.kafster.model.HierarchicalEndpointList;

@Path("")
public interface EndpointProviderService {

	@GET @Path("providers")
	@Produces({ MediaType.APPLICATION_JSON })
	EndpointProviderList getProviders(@QueryParam("role") Role role, @Context UriInfo uris);
	
	@POST @Path("providers")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	@Produces({ MediaType.APPLICATION_JSON })
	Response uploadProviderDocument(Multipart multipart, @Context UriInfo uris);	
	
	@POST @Path("providers")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	Response importProviderDocument(ExternalReference reference, @Context UriInfo uris);

	@GET @Path("providers/{key}")
	@Produces({ MediaType.APPLICATION_JSON })
	EndpointProviderDetail getProvider(@PathParam("key") String key, @Context UriInfo uris);

	@GET @Path("providers/{key}/image")
	@Produces({ MediaType.WILDCARD })
	Response getProviderImage(@PathParam("key") String key, @Context UriInfo uris, @Context Request request);	
	
	@GET @Path("providers/{key}/children")
	@Produces({ MediaType.APPLICATION_JSON })
	EndpointProviderList getProviderChildren(@PathParam("key") String key, @QueryParam("filter") String filter, @Context UriInfo uris);
	
	@GET @Path("providers/{key}/endpoints")
	@Produces({ MediaType.APPLICATION_JSON })
	HierarchicalEndpointList getProviderEndpoints(
			@PathParam("key") String key,
			@QueryParam("filter") String filter, 
			@QueryParam("depth") @DefaultValue("1") Integer depth,
			@Context UriInfo uris);
	
	@POST @Path("providers/{key}/endpoints")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	Response createEndpoint(
			@PathParam("name") String key, 
			@QueryParam("type") String type,
			EndpointDetail endpoint, 
			@Context UriInfo uris);	
	
	@GET
	@Path("providers/{key}/endpoints/{endpoint}")
	@Produces({ MediaType.APPLICATION_JSON })
	EndpointDetail getEndpoint(@PathParam("key") String key, @PathParam("endpoint") String endpoint, @Context UriInfo uris);
	
	@PUT
	@Path("providers/{key}/endpoints/{endpoint}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	EndpointDetail updateEndpoint(
			@PathParam("key") String key, 
			@PathParam("endpoint") String endpoint, 
			EndpointDetail update, 
			@Context UriInfo uris);	
	
	@DELETE
	@Path("providers/{key}/endpoints/{endpoint}")
	void removeEndpoint(			
			@PathParam("key") String key, 
			@PathParam("endpoint") String endpoint);	
}

