/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.connect.deploy;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.connect.converters.ByteArrayConverter;
import org.apache.kafka.connect.runtime.distributed.DistributedConfig;
import org.apache.kafka.connect.sink.SinkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.kafka.config.BrokerConfiguration;
import io.kafster.kafka.connect.worker.ConnectWorker;
import io.kafster.kafka.stream.TopicNames;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.deploy.SubscriptionListener;

public class ConnectorProvisioner implements SubscriptionListener {
	
	private static final Logger log = LoggerFactory.getLogger(ConnectorProvisioner.class);
	
	private Map<String, ConnectWorker> workers = new HashMap<>();
	
	private int commitInterval = 30000;
	private BrokerConfiguration config;
	private TopicNames names;
	
	private boolean enabled = true;
	
	public void shutdown() {
		
		for (ConnectWorker worker : workers.values()) {
			worker.stop();
		}
		
		workers.clear();
	}
	
	@Override
	public void subscriptionAdded(Subscription subscription) {
		
		if (!enabled || (subscription == null)) {
			return;
		}
		
		String servers = config.getBootstrapServers();
		if (servers == null) {
			log.warn("No bootstrap servers configured.  Cannot start Connect Worker");
			return;
		}
		
		try {
	        Map<String,String> workerProps = conditionProperties(subscription.getTarget().getConfiguration());
	        
	        workerProps.put(DistributedConfig.GROUP_ID_CONFIG, subscription.getKey() + ".connect");
	        workerProps.put(DistributedConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
	        workerProps.put(DistributedConfig.OFFSET_COMMIT_INTERVAL_MS_CONFIG, String.valueOf(commitInterval));
	        
	        // Configure topics
	        //
	        workerProps.put(SinkConnector.TOPICS_CONFIG, names.getTopic(subscription));
	        workerProps.put(DistributedConfig.OFFSET_STORAGE_TOPIC_CONFIG, names.getConnectOffsetTopic(subscription));
	        workerProps.put(DistributedConfig.CONFIG_TOPIC_CONFIG, names.getConnectConfigurationTopic(subscription));
	        workerProps.put(DistributedConfig.STATUS_STORAGE_TOPIC_CONFIG, names.getConnectStatusTopic(subscription));
	        
	        workerProps.put(DistributedConfig.KEY_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
	        workerProps.put(DistributedConfig.VALUE_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
	        workerProps.put("key.converter.schemas.enable", "false");
	        workerProps.put("value.converter.schemas.enable", "false");
	        workerProps.put("internal.key.converter.schemas.enable", "false");
	        workerProps.put("internal.value.converter.schemas.enable", "false");
	        
			ConnectWorker worker = new ConnectWorker(workerProps, subscription);
			worker.start();
			
			workers.put(subscription.getKey(), worker);
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}
	
    protected Map<String,String> conditionProperties(Map<String,Object> props) {
    	Map<String,String> result = new HashMap<>();
    	props.forEach((k,v) -> result.put(k, v.toString()));
    	return result;
    }	

	@Override
	public void subscriptionUpdated(Subscription subscription) {

		if (!enabled || (subscription == null)) {
			return;
		}
		
		subscriptionRemoved(subscription);
		subscriptionAdded(subscription);
	}

	@Override
	public void subscriptionRemoved(Subscription subscription) {
		
		if (!enabled || (subscription == null)) {
			return;
		}
		
		ConnectWorker worker = workers.remove(subscription.getKey());
		if (worker != null) {
			worker.stop();
		}
	}

	public void setCommitInterval(int commitInterval) {
		this.commitInterval = commitInterval;
	}

	public void setConfig(BrokerConfiguration config) {
		this.config = config;
	}

	public void setNames(TopicNames names) {
		this.names = names;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
