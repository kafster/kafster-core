/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.connect.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.ConfigKey;
import org.apache.kafka.common.config.ConfigDef.Importance;

public class ConfigOptionsMapper {
	
	private static final String ORDER = "ui:order";
	
	private Set<Importance> importance = EnumSet.of(Importance.HIGH, Importance.MEDIUM);
	private Set<Pattern> excludes;

	private boolean includeDefaults = true;	

	public Map<String,Object> convert(ConfigDef config, String... groups) throws Exception {
		
		Map<String,Object> result = new HashMap<>();
		result = addDefaults(result);
		
		if (config != null) {
			
			Map<String,ConfigKey> keys = config.configKeys();
			if (keys != null) {
				Map<String,List<ConfigKey>> grouped = group(keys.values());
				
				for (String name : grouped.keySet()) {
					
					if (ArrayUtils.isEmpty(groups) || ArrayUtils.contains(groups, name)) {
						
						List<ConfigKey> group = grouped.get(name);
						group.sort((k1,k2) -> {
							// Bubble required properties to the top regardless of order in group
							//
							int k1Score = k1.hasDefault() ? k1.orderInGroup : group.size() + k1.orderInGroup;
							int k2Score = k2.hasDefault() ? k2.orderInGroup : group.size() + k2.orderInGroup;
							return k1Score - k2Score;
						});
						
						// Set ui:order.  There may be other values (defaults) in the list already.
						//
						List<String> order = (List<String>) result.putIfAbsent(ORDER, new ArrayList<>());
						order.addAll(group.stream().map(key -> key.name).collect(Collectors.toList()));
					}					
				}
			}
		}
		
		return result;
	}
	
	protected Map<String,List<ConfigKey>> group(Collection<ConfigKey> keys) {
		Map<String,List<ConfigKey>> result = new HashMap<>();
		for (ConfigKey key : keys) {
			String group = key.group == null ? "" : key.group;
			List<ConfigKey> list = result.computeIfAbsent(group, k -> new ArrayList<>());
			list.add(key);
		}
		return result;
	}	
	
	@SuppressWarnings("unchecked")
	protected Map<String,Object> addDefaults(Map<String,Object> options) {
		if (includeDefaults) {
			List<String> order = (List<String>) options.putIfAbsent(ORDER, new ArrayList<>());
			order.add("name");
			order.add("description");
		}
		return options;
	}

	public void setImportance(Set<Importance> importance) {
		this.importance = importance;
	}

	public void setExcludes(Set<Pattern> excludes) {
		this.excludes = excludes;
	}

	public void setIncludeDefaults(boolean includeDefaults) {
		this.includeDefaults = includeDefaults;
	}
}
