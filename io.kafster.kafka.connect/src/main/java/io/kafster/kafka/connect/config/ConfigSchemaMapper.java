/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.connect.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.ConfigKey;
import org.apache.kafka.common.config.ConfigDef.Importance;

import io.kafster.schema.PrimitiveTypes;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.simple.SimpleSchemaProperty;
import io.kafster.schema.simple.SimpleSchemaType;

public class ConfigSchemaMapper {
	
	private Set<Importance> importance = EnumSet.of(Importance.HIGH, Importance.MEDIUM);
	private Set<Pattern> excludes;

	private boolean includeDefaults = true;
	
	public SchemaType convert(ConfigDef config, String... groups) throws Exception {
		
		if (config != null) {
			
			Map<String,ConfigKey> keys = config.configKeys();
			if (keys != null) {
				SimpleSchemaType result = new SimpleSchemaType();
				result.setName(new QName(config.getClass().getSimpleName()));
				result.setProperties(getDefaultProperties());
				
				Map<String,List<ConfigKey>> grouped = group(keys.values());
				
				for (String name : grouped.keySet()) {
					
					if (ArrayUtils.isEmpty(groups) || ArrayUtils.contains(groups, name)) {
						
						List<ConfigKey> group = grouped.get(name);
						group.sort((k1,k2) -> {
							// Bubble required properties to the top regardless of order in group
							//
							int k1Score = k1.hasDefault() ? k1.orderInGroup : group.size() + k1.orderInGroup;
							int k2Score = k2.hasDefault() ? k2.orderInGroup : group.size() + k2.orderInGroup;
							return k1Score - k2Score;
						});
						
						for (ConfigKey key : group) {
							SchemaProperty property = convert(key);
							if (property != null) {
								result.getProperties().put(key.name, property);
							}
						}
					}
				}
				
				return result;
			}
		}
		
		return null;
	}
	
	protected Map<String,SchemaProperty> getDefaultProperties() {
		Map<String,SchemaProperty> result = new TreeMap<>();
		
		if (includeDefaults) {
			SimpleSchemaProperty name = new SimpleSchemaProperty();
			name.setTitle("Name");
			name.setType(PrimitiveTypes.STRING);
			name.setDescription("The Destination Name");
			
			SimpleSchemaProperty description = new SimpleSchemaProperty();
			description.setTitle("Description");
			description.setType(PrimitiveTypes.STRING);
			description.setDescription("A description of the Destination");
			
			result.put("name",  name);
			result.put("description",  description);
		}
		
		return result;
	}
	
	protected Map<String,List<ConfigKey>> group(Collection<ConfigKey> keys) {
		Map<String,List<ConfigKey>> result = new HashMap<>();
		for (ConfigKey key : keys) {
			String group = key.group == null ? "" : key.group;
			List<ConfigKey> list = result.computeIfAbsent(group, k -> new ArrayList<>());
			list.add(key);
		}
		return result;
	}
	
	protected SchemaProperty convert(ConfigKey key) {
		
		if ((key == null) || key.internalConfig || excluded(key)) {
			return null;
		}
		
		if ((key.importance != null) && !importance.contains(key.importance)) {
			return null;
		}
		
		SimpleSchemaProperty result = new SimpleSchemaProperty();
		result.setTitle(key.displayName);
		result.setDescription(key.documentation);
		
		if (!key.hasDefault()) {
			result.setMinCardinality(1L);
		}
		
		/*
		if ((key.dependents != null) && !key.dependents.isEmpty()) {
			result.setDependencies(key.dependents.toArray(new String[key.dependents.size()]));
		}
		*/
		
		if (key.recommender != null) {
			List<Object> values = key.recommender.validValues(key.name, Collections.emptyMap());
			if ((values != null) && !values.isEmpty()) {
				SimpleSchemaType type = new SimpleSchemaType();
				type.setEnum(true);
				type.setEnumValues(values.toArray(new Object[] {}));
				
				result.setType(type);
			}
		}
		else {
			switch(key.type) {
				case BOOLEAN:
					result.setType(PrimitiveTypes.BOOLEAN);
					break;
				case CLASS:
					result.setType(PrimitiveTypes.STRING);
					break;
				case DOUBLE:
					result.setType(PrimitiveTypes.DOUBLE);
					break;
				case INT:
					result.setType(PrimitiveTypes.INTEGER);
					break;
				case LIST:
					result.setType(PrimitiveTypes.STRING);
					break;
				case LONG:
					result.setType(PrimitiveTypes.LONG);
					break;
				case PASSWORD:
					result.setType(PrimitiveTypes.STRING);
					break;
				case SHORT:
					result.setType(PrimitiveTypes.SHORT);
					break;
				case STRING:
					result.setType(PrimitiveTypes.STRING);
					break;
				default:
					break;
			}
		}
		
		return result;
	}
	
	protected boolean excluded(ConfigKey key) {
		if (excludes != null) {
			for (Pattern pattern : excludes) {
				if (pattern.matcher(key.name).matches()) {
					return true;
				}
				if (pattern.matcher(key.displayName).matches()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void setImportance(Set<Importance> importance) {
		this.importance = importance;
	}

	public void setExcludes(Set<Pattern> excludes) {
		this.excludes = excludes;
	}

	public void setIncludeDefaults(boolean includeDefaults) {
		this.includeDefaults = includeDefaults;
	}
}
