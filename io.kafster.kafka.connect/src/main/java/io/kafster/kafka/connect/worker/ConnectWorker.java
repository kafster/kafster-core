/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.connect.worker;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.common.utils.SystemTime;
import org.apache.kafka.common.utils.Time;
import org.apache.kafka.connect.connector.policy.AllConnectorClientConfigOverridePolicy;
import org.apache.kafka.connect.runtime.ConnectorConfig;
import org.apache.kafka.connect.runtime.Herder;
import org.apache.kafka.connect.runtime.Worker;
import org.apache.kafka.connect.runtime.distributed.DistributedConfig;
import org.apache.kafka.connect.runtime.distributed.DistributedHerder;
import org.apache.kafka.connect.runtime.isolation.Plugins;
import org.apache.kafka.connect.runtime.rest.entities.ConnectorInfo;
import org.apache.kafka.connect.storage.ConfigBackingStore;
import org.apache.kafka.connect.storage.KafkaConfigBackingStore;
import org.apache.kafka.connect.storage.KafkaOffsetBackingStore;
import org.apache.kafka.connect.storage.KafkaStatusBackingStore;
import org.apache.kafka.connect.storage.StatusBackingStore;
import org.apache.kafka.connect.util.FutureCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.endpoint.Endpoint;
import io.kafster.subscription.Subscription;

public class ConnectWorker {
	
    private static final Logger log = LoggerFactory.getLogger(ConnectWorker.class);
    
    private static final int REQUEST_TIMEOUT_MS = 120000;

    private final Worker worker;
    private final DistributedHerder herder;
    
    private final AtomicBoolean shutdown = new AtomicBoolean(true);

    private final Subscription subscription;
    
	public ConnectWorker(Map<String, String> properties, Subscription subscription) throws Exception {

		DistributedConfig config = new DistributedConfig(properties);

		Time time = new SystemTime();

		worker = new Worker(
				UUID.randomUUID().toString(), 
				time, 
				new Plugins(properties), 
				config,
				offsetBackingStore(config),
				new AllConnectorClientConfigOverridePolicy());
		herder = new DistributedHerder(
				config, 
				time, 
				worker, 
				null, 
				statusBackingStore(time, config),
				configBackingStore(config), 
				"",
				new AllConnectorClientConfigOverridePolicy());

		this.subscription = subscription;
	}
    
    protected KafkaOffsetBackingStore offsetBackingStore(DistributedConfig config) {
        KafkaOffsetBackingStore result = new KafkaOffsetBackingStore();
        result.configure(config);  
        return result;
    }
    
    protected StatusBackingStore statusBackingStore(Time time, DistributedConfig config) {
        StatusBackingStore result = new KafkaStatusBackingStore(time, worker.getInternalValueConverter());
        result.configure(config);
        return result;
    }
    
    protected ConfigBackingStore configBackingStore(DistributedConfig config) {
    	return new KafkaConfigBackingStore(worker.getInternalValueConverter(), config, null);
    }

    public void start() throws Exception {
    	
    	if (shutdown.getAndSet(false)) {
    	
	        log.info("Starting [" + getClass().getSimpleName() + "] for [" + subscription.getName() + "][" + subscription.getKey() + "]");
	
	        worker.start();
	        herder.start();
	
	        log.info("Started [" + subscription.getKey() + "]");
	        
	        updateConfiguration(subscription.getTarget());
    	}
    }

    public void stop() {
    	
        if (!shutdown.getAndSet(true)) {

        	log.info("Stopping [" + getClass().getSimpleName() + "] for [" + subscription.getName() + "][" + subscription.getKey() + "]");
        	
            herder.stop();
            worker.stop();

            log.info("Stopped [" + subscription.getKey() + "]");
        }
    }
    
    public void updateConfiguration(Endpoint destination) throws Exception {
        
        Map<String,String> props = conditionProperties(destination.getConfiguration());

        String name = props.get(ConnectorConfig.NAME_CONFIG);
        if (name != null) {
        	FutureCallback<Herder.Created<ConnectorInfo>> cb = new FutureCallback<>();
            herder.putConnectorConfig(name, props, true, cb);
            cb.get(REQUEST_TIMEOUT_MS, TimeUnit.MILLISECONDS);  
        }
    }
    
    protected Map<String,String> conditionProperties(Map<String,Object> props) {
    	Map<String,String> result = new HashMap<>();
    	props.forEach((k,v) -> result.put(k, v.toString()));
    	return result;
    }
}

