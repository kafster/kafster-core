/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.connect.support;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigValue;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.connect.config.ConfigOptionsMapper;
import io.kafster.kafka.connect.config.ConfigSchemaMapper;
import io.kafster.schema.SchemaType;

public abstract class ConnectDestinationProviderSupport implements EndpointProvider {
	
	private SchemaType schema;
	private Map<String,Object> options;
	
	// Most connectors have a "Connector" group for the basic properties 
	//
	protected static final String CONNECTOR_GROUP = "Connector";
	
	protected abstract ConfigDef getConfigDef();

	protected SchemaType getConfigSchema(Set<Pattern> excludes, Set<Importance> importance, String... groups) throws Exception {
		
		if (schema == null) {
			ConfigSchemaMapper mapper = new ConfigSchemaMapper();
			mapper.setExcludes(excludes);
			mapper.setImportance(importance);
			
			schema = mapper.convert(getConfigDef(), groups);
		}
		
		return schema;
	}
	
	protected Map<String,Object> getOptions(Set<Pattern> excludes, Set<Importance> importance, String... groups) throws Exception {
		
		if (options == null) {
			ConfigOptionsMapper mapper = new ConfigOptionsMapper();
			mapper.setExcludes(excludes);
			mapper.setImportance(importance);
			
			options = mapper.convert(getConfigDef(), groups);
		}
		
		return options;
	}
	
	protected Map<String, String> validate(Map<String, Object> configuration) throws Exception {
		
		if (configuration == null) {
			throw new ConfigurationException("Null Configuration");
		}
		
		Map<String,String> props = new HashMap<>();
		configuration.forEach((k,v) -> props.put(k, String.valueOf(v)));
		
		List<ConfigValue> result = getConfigDef().validate(props);
		if (result != null) {
			for (ConfigValue value : result) {
				List<String> errors = value.errorMessages();
				if ((errors != null) && !errors.isEmpty()) {
					throw new ConfigurationException(String.join(",", errors));
				}
			}
		}
		
		return props;
	}
}
