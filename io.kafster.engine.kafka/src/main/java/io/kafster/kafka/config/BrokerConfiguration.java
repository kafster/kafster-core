/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.config;

import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.kafka.KafkaConstants;

public class BrokerConfiguration {
	
	private static final Logger log = LoggerFactory.getLogger(BrokerConfiguration.class);
	
	public static final String BOOTSTRAP_SERVERS_CONFIG = "bootstrap.servers";
	
	private ConfigurationAdmin configAdmin;
	private String selector;

	public Map<String,String> getProperties() {
		
		try {
			
			String filter = "(" + ConfigurationAdmin.SERVICE_FACTORYPID + "=" + KafkaConstants.FACTORY_PID + ")";
			if (selector != null) {
				filter = "(&(" + filter + selector + "))";
			}
			
			Configuration[] configs = configAdmin.listConfigurations(filter);
			if ((configs != null) && (configs.length > 0)) {
				// Use the first available
				//
				Configuration config = configs[0];
				if (config.getProperties() != null) {
					return asStringMap(config.getProperties());
				}
				else {
					log.warn("Null configuration found for [" + filter + "]");
				}				
			}
			else {
				log.warn("No broker configuration found for [" + filter + "]");
			}
		} 
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return Collections.emptyMap();
	}
	
	public String getBootstrapServers() {
		
		Map<String,String> props = getProperties();
		if (props != null) {
			return props.get(BOOTSTRAP_SERVERS_CONFIG);
		}
		
		return null;
	}	
	
	protected Map<String,String> asStringMap(Dictionary<String,Object> props) {
		Map<String,String> result = new HashMap<>();
		
		Enumeration<String> keys = props.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			result.put(key, props.get(key).toString());
		}
		
		return result;
	}
	
	public void setConfigAdmin(ConfigurationAdmin configAdmin) {
		this.configAdmin = configAdmin;
	}

	public void setSelector(String selector) {
		this.selector = selector;
	}	
}
