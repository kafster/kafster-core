/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

import io.kafster.jexl.annotation.JexlNamespace;
import io.kafster.jexl.annotation.JexlNamespaceOperation;

@JexlNamespace(prefix="digest", description="Generate digests using different algorithms")
public class DigestNamespace {
	
	private static final DigestUtils MD2 = new DigestUtils(MessageDigestAlgorithms.MD2);
	private static final DigestUtils MD5 = new DigestUtils(MessageDigestAlgorithms.MD5);
	private static final DigestUtils SHA1 = new DigestUtils(MessageDigestAlgorithms.SHA_1);
	private static final DigestUtils SHA224 = new DigestUtils(MessageDigestAlgorithms.SHA_224);
	private static final DigestUtils SHA256 = new DigestUtils(MessageDigestAlgorithms.SHA_256);
	private static final DigestUtils SHA384 = new DigestUtils(MessageDigestAlgorithms.SHA_384);
	private static final DigestUtils SHA512 = new DigestUtils(MessageDigestAlgorithms.SHA_512);

	@JexlNamespaceOperation(description="Get the MD2 digest for a value as a hex string")
	public String md2(String value) {
		return MD2.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the MD5 digest for a value as a hex string")
	public String md5(String value) {
		return MD5.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the SHA1 digest for a value as a hex string")
	public String sha1(String value) {
		return SHA1.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the SHA224 digest for a value as a hex string")
	public String sha224(String value) {
		return SHA224.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the SHA256 digest for a value as a hex string")
	public String sha256(String value) {
		return SHA256.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the SHA384 digest for a value as a hex string")
	public String sha384(String value) {
		return SHA384.digestAsHex(value);
	}
	
	@JexlNamespaceOperation(description="Get the SHA512 digest for a value as a hex string")
	public String sha512(String value) {
		return SHA512.digestAsHex(value);
	}
}
