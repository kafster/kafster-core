/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.interceptors;

import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

import io.kafster.kafka.factory.impl.InterceptorTopologyFactory.Interceptor;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.subscription.Subscription;

public class PipelineSetupInterceptor implements Interceptor {
	
	private Interceptor delegate;
	
	public PipelineSetupInterceptor() {
	}

	public PipelineSetupInterceptor(Interceptor delegate) {
		this.delegate = delegate;
	}
	
	public void setDelegate(Interceptor delegate) {
		this.delegate = delegate;
	}	

	@Override
	public KStream<byte[], StreamRecord> preprocess(KStream<byte[], byte[]> stream, Subscription subscription) throws Exception {
		
		KStream<byte[], StreamRecord> result = delegate.preprocess(stream, subscription);
		
		String as = subscription.getPipeline().getSource().getAs();
		if (as != null) {
			result = result.transformValues(pre(as));
		}
		
		return result;
	}

	@Override
	public KStream<byte[], byte[]> postprocess(KStream<byte[], StreamRecord> stream, Subscription subscription)	throws Exception {
		String as = subscription.getPipeline().getTarget().getAs();
		if (as != null) {
			stream = stream.transformValues(post(as));
		}
		return delegate.postprocess(stream, subscription);
	}
	
	private ValueTransformerSupplier<StreamRecord,StreamRecord> post(String as) {
		return () -> {
			return new ValueTransformer<StreamRecord,StreamRecord>() {

				@Override
				public void close() {
				}

				@Override
				public void init(ProcessorContext ctx) {
				}

				@Override
				public StreamRecord transform(StreamRecord in) {
					return in.copy(as);
				}
			};
		};
	}
	
	private ValueTransformerSupplier<StreamRecord,StreamRecord> pre(String as) {
		return () -> {
			return new ValueTransformer<StreamRecord,StreamRecord>() {

				@Override
				public void close() {
				}

				@Override
				public void init(ProcessorContext ctx) {
				}

				@Override
				public StreamRecord transform(StreamRecord in) {
					StreamRecord result = in.create();
					result.set(as, in);
					return result;
				}
			};
		};
	}	

}
