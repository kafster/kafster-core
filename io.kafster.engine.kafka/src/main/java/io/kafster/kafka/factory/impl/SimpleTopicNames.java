/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.impl;

import io.kafster.kafka.stream.TopicNames;
import io.kafster.subscription.Subscription;

public class SimpleTopicNames implements TopicNames {
	
	private static final String TOPIC_TEMPLATE = "subscription.%s";
	private static final String CONFIG_TOPIC_TEMPLATE = "subscription.%s.connect.config";
	private static final String OFFSET_TOPIC_TEMPLATE = "subscription.%s.connect.offset";
	private static final String STATUS_TOPIC_TEMPLATE = "subscription.%s.connect.status";

	@Override
	public String getTopic(Subscription subscription) {
		return String.format(TOPIC_TEMPLATE, subscription.getKey());
	}

	@Override
	public String getConnectOffsetTopic(Subscription subscription) {
		return String.format(OFFSET_TOPIC_TEMPLATE, subscription.getKey());
	}

	@Override
	public String getConnectConfigurationTopic(Subscription subscription) {
		return String.format(CONFIG_TOPIC_TEMPLATE, subscription.getKey());
	}

	@Override
	public String getConnectStatusTopic(Subscription subscription) {
		return String.format(STATUS_TOPIC_TEMPLATE, subscription.getKey());
	}

}
