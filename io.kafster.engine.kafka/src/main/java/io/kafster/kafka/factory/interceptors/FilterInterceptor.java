/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.interceptors;

import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointResolver;
import io.kafster.endpoint.EndpointResolver.ResolvedEndpoint;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.factory.impl.InterceptorTopologyFactory.Interceptor;
import io.kafster.kafka.runner.jexl.JexlHeaderContext;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.subscription.Subscription;

public class FilterInterceptor implements Interceptor {
	
	private Interceptor delegate;
	private JexlEngine jexl;
	private EndpointResolver resolver;
	
	public FilterInterceptor(Interceptor delegate) {
		this.delegate = delegate;
	}
	
	public FilterInterceptor() {
	}
	
	public void setDelegate(Interceptor delegate) {
		this.delegate = delegate;
	}
	
	public void setResolver(EndpointResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public KStream<byte[], StreamRecord> preprocess(KStream<byte[], byte[]> stream, Subscription subscription) throws Exception {
		
		Endpoint source = subscription.getSource();
		if ((source == null) || (source.getUri() == null)) {
			throw new ConfigurationException("Invalid source");
		}
		
		if (resolver != null) {
			ResolvedEndpoint resolved = resolver.resolve(subscription, source);
			if ((resolved != null) && (resolved.getFilter() != null)) {
				stream = stream.transform(transformer(resolved));
			}
		}
		
		return delegate.preprocess(stream, subscription);
	}

	@Override
	public KStream<byte[], byte[]> postprocess(KStream<byte[], StreamRecord> stream, Subscription subscription) throws Exception {
		return delegate.postprocess(stream, subscription);
	}
	
	protected TransformerSupplier<byte[],byte[],KeyValue<byte[],byte[]>> transformer(ResolvedEndpoint destination) {
		
		final JexlExpression expression = jexl.createExpression(destination.getFilter());
		
		return () -> {
			return new Transformer<byte[],byte[],KeyValue<byte[],byte[]>>() {

				private ProcessorContext ctx;
				
				@Override
				public void close() {
				}

				@Override
				public void init(ProcessorContext ctx) {
					this.ctx = ctx;
				}

				@Override
				public KeyValue<byte[], byte[]> transform(byte[] key, byte[] value) {
					JexlContext context = new JexlHeaderContext(ctx.headers());
					Object result = expression.evaluate(context);
					if (jexl.getArithmetic().toBoolean(result)) {
						return KeyValue.pair(key, value);
					}
					return null;
				}
			};
		};
	}

}
