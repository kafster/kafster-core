/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.jexl.annotation.JexlNamespace;
import io.kafster.jexl.annotation.JexlNamespaceOperation;

@JexlNamespace(prefix="net", description="General networking utilities")
public class NetNamespace {
	
	private static final Logger log = LoggerFactory.getLogger(NetNamespace.class);

	@JexlNamespaceOperation(description="Returns true if a string is an IPv6 address")
	public Boolean isIPv6(String ip) {
		try {
			return InetAddress.getByName(ip) instanceof Inet6Address;
		} catch (UnknownHostException e) {
			log.error("Could not resolve [" + ip + "]", e);
		}
		return false;
	}
	
	@JexlNamespaceOperation(description="Returns true if a string is an IPv4 address")
	public Boolean isIPv4(String ip) {
		try {
			return InetAddress.getByName(ip) instanceof Inet4Address;
		} catch (UnknownHostException e) {
			log.error("Could not resolve [" + ip + "]", e);
		}
		return false;
	}
	
	@JexlNamespaceOperation(description="Look up the canonical host name for a host or IP")
	public String lookupHostname(String ip) {
		try {
			InetAddress address = InetAddress.getByName(ip);
			return address.getCanonicalHostName();
		} 
		catch (UnknownHostException e) {
			log.error("Could not resolve [" + ip + "]", e);
		}
		return ip;
	}
	
	@JexlNamespaceOperation(description="Look up the IP address for a host name")
	public String lookupIp(String hostname) {
		try {
			InetAddress address = InetAddress.getByName(hostname);
			return address.getHostAddress();
		} 
		catch (UnknownHostException e) {
			log.error("Could not resolve [" + hostname + "]", e);
		}
		return hostname;
	}	
}
