/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointResolver;
import io.kafster.endpoint.EndpointResolver.ResolvedEndpoint;
import io.kafster.kafka.factory.TopologyFactory;
import io.kafster.kafka.stages.StageBuilder;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineStage;
import io.kafster.subscription.Subscription;

public abstract class AbstractTopologyFactory implements TopologyFactory {
	
	private static final String NAME_PROP = "name";
	
	private EndpointResolver resolver;	
	
	private Map<String,StageBuilder> builders = new ConcurrentHashMap<>();	

	public Topology create(Subscription subscription) throws Exception {
		
		Endpoint source = subscription.getSource();
		if (source == null) {
			return null;
		}
		
		ResolvedEndpoint sourceTopic = resolver.resolve(subscription, source);
		if ((sourceTopic == null) || (sourceTopic.getName() == null)) {
			return null;
		}
		
		Pipeline pipeline = subscription.getPipeline();
		if (pipeline == null) {
			return null;
		}
		
		Endpoint target = subscription.getTarget();
		if (target == null) {
			return null;
		}
		
		ResolvedEndpoint targetTopic = resolver.resolve(subscription, target);
		if ((targetTopic == null) || (targetTopic.getName() == null)) {
			return null;
		}
		
		StreamsBuilder builder = new StreamsBuilder();
		KStream<byte[],byte[]> stream = builder.stream(sourceTopic.getName());
		
		if (pipeline.getStages() != null) {
			
			KStream<byte[],StreamRecord> records = preprocess(stream, subscription);
			
			for (PipelineStage stage : pipeline.getStages()) {
				
				records = handle(stage, records);
				
				if (records == null) {
					throw new IllegalArgumentException("Could not process stage [" + stage + "]");
				}
			}
			
			stream = postprocess(records, subscription);
		}
		
		stream.to(targetTopic.getName());
		
		return builder.build();
	}	
	
	protected KStream<byte[],StreamRecord> handle(PipelineStage stage, KStream<byte[],StreamRecord> stream) throws Exception {
		StageBuilder builder = builders.get(stage.getMetadata().getName());
		if (builder != null) {
			return builder.build(stage, stream);
		}
		return null;
	}
	
	protected abstract KStream<byte[],StreamRecord> preprocess(KStream<byte[],byte[]> stream, Subscription subscription) throws Exception;
	protected abstract KStream<byte[],byte[]> postprocess(KStream<byte[],StreamRecord> stream, Subscription subscription) throws Exception;

	public void builderAdded(StageBuilder builder, Map<String,Object> props) {
		
		if (builder == null) {
			return;
		}
		
		String name = (String)props.get(NAME_PROP);
		if (name == null) {
			return;
		}
		
		this.builders.put(name, builder);
	}
	
	public void builderRemoved(StageBuilder builder, Map<String,Object> props) {
		
		if (builder == null) {
			return;
		}
		
		String name = (String)props.get(NAME_PROP);
		if (name == null) {
			return;
		}
		
		this.builders.remove(name);
	}

	public void setResolver(EndpointResolver resolver) {
		this.resolver = resolver;
	}	
}
