/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.impl;

import java.util.HashMap;
import java.util.Map;

import io.kafster.kafka.runner.TopologyRunner.TopologyStatus;
import io.kafster.subscription.SubscriptionStatus;
import io.kafster.subscription.SubscriptionStatus.State;

public class SubscriptionStatusAdapter implements SubscriptionStatus {
	
	private TopologyStatus status;
	
	@SuppressWarnings("serial")
	private static final Map<TopologyStatus.Status,State> STATUS = new HashMap<TopologyStatus.Status,State>( ) {{
		put(TopologyStatus.Status.CREATED, State.CREATED);
		put(TopologyStatus.Status.ERROR, State.ERROR);
		put(TopologyStatus.Status.NOT_RUNNING, State.NOT_RUNNING);
		put(TopologyStatus.Status.PENDING_SHUTDOWN, State.PENDING_SHUTDOWN);
		put(TopologyStatus.Status.QUEUED, State.QUEUED);
		put(TopologyStatus.Status.REBALANCING, State.REBALANCING);
		put(TopologyStatus.Status.RUNNING, State.RUNNING);
	}};
	
	public SubscriptionStatusAdapter(TopologyStatus status) {
		this.status = status;
	}

	public String getId() {
		return status.getId();
	}

	@Override
	public State getState() {
		return STATUS.get(status.getStatus());
	}

	@Override
	public String getStateDetail() {
		return status.getStatusDetail();
	}

	@Override
	public void setState(State state) {
	}

	@Override
	public void setStateDetail(String detail) {
	}
}
