/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringEscapeUtils;

import io.kafster.jexl.annotation.JexlNamespace;
import io.kafster.jexl.annotation.JexlNamespaceOperation;

@JexlNamespace(prefix="escape")
public class EscapeNamespace {

	@JexlNamespaceOperation(description="URL encode a string")
	public String urlEncode(String url) {
		try {
			return URLEncoder.encode(url, StandardCharsets.UTF_8.name());
		} 
		catch (UnsupportedEncodingException e) {
		}
		return url;
	}
	
	@JexlNamespaceOperation(description="URL decode a string")
	public String urlDecode(String url) {
		try {
			return URLDecoder.decode(url, StandardCharsets.UTF_8.name());
		} 
		catch (UnsupportedEncodingException e) {
		}
		return url;
	}
	
	@JexlNamespaceOperation(description="Returns a String value for a CSV column enclosed in double quotes, if required")
	public String escapeCsv(String str) {
		return StringEscapeUtils.escapeCsv(str);
	}
	
	@JexlNamespaceOperation(description="Returns a String value for an unescaped CSV column")
	public String unescapeCsv(String str) {
		return StringEscapeUtils.unescapeCsv(str);
	}	
	
	@JexlNamespaceOperation(description="Escapes the characters in a String using EcmaScript String rules")
	public String escapeEcmaScript(String str) {
		return StringEscapeUtils.escapeEcmaScript(str);
	}
	
	@JexlNamespaceOperation(description="Unescapes any EcmaScript literals found in the passed-in String")
	public String unescapeEcmaScript(String str) {
		return StringEscapeUtils.unescapeEcmaScript(str);
	}
	
	@JexlNamespaceOperation(description="Escapes the characters in a String using HTML entities")
	public String escapeHtml(String str) {
		return StringEscapeUtils.escapeHtml4(str);
	}
	
	@JexlNamespaceOperation(description="Unescapes a string containing entity escapes to a string containing the actual Unicode characters corresponding to the escapes")
	public String unescapeHtml(String str) {
		return StringEscapeUtils.unescapeHtml4(str);
	}
	
	@JexlNamespaceOperation(description="Escapes the characters in a String using Json String rules")
	public String escapeJson(String str) {
		return StringEscapeUtils.escapeJson(str);
	}
	
	@JexlNamespaceOperation(description="Unescapes any Json literals found in the String")
	public String unescapeJson(String str) {
		return StringEscapeUtils.unescapeJson(str);
	}	
	
	@JexlNamespaceOperation(description="Escapes the characters in a String using XML entities")
	public String escapeXml(String str) {
		return StringEscapeUtils.escapeXml11(str);
	}
	
	@JexlNamespaceOperation(description="Unescapes a string containing XML entity escapes to a string containing the actual Unicode characters corresponding to the escapes")
	public String unescapeXml(String str) {
		return StringEscapeUtils.unescapeXml(str);
	}	
}
