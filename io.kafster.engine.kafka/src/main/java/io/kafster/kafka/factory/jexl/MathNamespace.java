/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import io.kafster.jexl.annotation.JexlNamespace;

@JexlNamespace(prefix="math", description = "Math Utilities")
public class MathNamespace {

    private boolean strict = false;

    public void setStrict(boolean strict) {
        this.strict = strict;
    }

    public double sin(double a) {
        return StrictMath.sin(a); 
    }

    /**
     * Returns the trigonometric cosine of an angle. 
     * @see java.lang.Math#cos(double)
     */
    public double cos(double a) {
        return StrictMath.cos(a); 
    }

    /**
     * Returns the trigonometric tangent of an angle.  
     * @see java.lang.Math#tan(double)
     */
    public double tan(double a) {
        return StrictMath.tan(a); 
    }

    /**
     * Returns the arc sine of a value; the returned angle is in the
     * range -<i>pi</i>/2 through <i>pi</i>/2.  
     * @see java.lang.Math#asin(double)
     */
    public double asin(double a) {
        return StrictMath.asin(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the arc cosine of a value; the returned angle is in the
     * range 0.0 through <i>pi</i>.  
     * @see java.lang.Math#acos(double)
     */
    public double acos(double a) {
        return StrictMath.acos(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the arc tangent of a value; the returned angle is in the
     * range -<i>pi</i>/2 through <i>pi</i>/2.  
     * @see java.lang.Math#atan(double)
     */
    public double atan(double a) {
        return StrictMath.atan(a); // default impl. delegates to StrictMath
    }

    /**
     * Converts an angle measured in degrees to an approximately
     * equivalent angle measured in radians.  The conversion from
     * degrees to radians is generally inexact.
     * @see java.lang.Math#toRadians(double)
     */
    public double toRadians(double angdeg) {
        return Math.toRadians(angdeg);
    }

    /**
     * Converts an angle measured in radians to an approximately
     * equivalent angle measured in degrees.  The conversion from
     * radians to degrees is generally inexact; users should
     * <i>not</i> expect {@code cos(toRadians(90.0))} to exactly
     * equal {@code 0.0}.
     * @see java.lang.Math#toDegrees(double)
     */
    public double toDegrees(double angrad) {
        return Math.toDegrees(angrad);
    }

    /**
     * Returns Euler's number <i>e</i> raised to the power of a
     * {@code double} value.  
     * @see java.lang.Math#exp(double)
     */
    public double exp(double a) {
        return StrictMath.exp(a); 
    }

    /**
     * Returns the natural logarithm (base <i>e</i>) of a {@code double}
     * value.  
     * @see java.lang.Math#log(double)
     */
    public double log(double a) {
        return StrictMath.log(a); 
    }

    /**
     * Returns the base 10 logarithm of a {@code double} value.
     * @see java.lang.Math#log10(double)
     */
    public double log10(double a) {
        return StrictMath.log10(a); 
    }

    /**
     * Returns the correctly rounded positive square root of a
     * {@code double} value.
     * @see java.lang.Math#sqrt(double)
     */
    public double sqrt(double a) {
        return StrictMath.sqrt(a); 
    }

    /**
     * Returns the cube root of a {@code double} value.  For
     * positive finite {@code x}, {@code cbrt(-x) ==
     * -cbrt(x)}; that is, the cube root of a negative value is
     * the negative of the cube root of that value's magnitude.
     * @see java.lang.Math#cbrt(double)
     */
    public double cbrt(double a) {
        return StrictMath.cbrt(a);
    }

    /**
     * Computes the remainder operation on two arguments as prescribed
     * by the IEEE 754 standard.
     * @see java.lang.Math#IEEEremainder(double, double)
     */
    public double IEEEremainder(double f1, double f2) {
        return StrictMath.IEEEremainder(f1, f2); 
    }

    /**
     * Returns the smallest (closest to negative infinity)
     * {@code double} value that is greater than or equal to the
     * argument and is equal to a mathematical integer. 
     * @see java.lang.Math#ceil(double)
     */
    public double ceil(double a) {
        return StrictMath.ceil(a); 
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code double} value that is less than or equal to the
     * argument and is equal to a mathematical integer. 
     * @see java.lang.Math#floor(double)
     */
    public double floor(double a) {
        return StrictMath.floor(a); 
    }

    /**
     * Returns the {@code double} value that is closest in value
     * to the argument and is equal to a mathematical integer. 
     * @see java.lang.Math#rint(double)
     */
    public double rint(double a) {
        return StrictMath.rint(a); 
    }

    /**
     * Returns the angle <i>theta</i> from the conversion of rectangular
     * coordinates ({@code x},&nbsp;{@code y}) to polar
     * coordinates (r,&nbsp;<i>theta</i>).
     * @see java.lang.Math#atan2(double, double)
     */
    public double atan2(double y, double x) {
        return StrictMath.atan2(y, x); 
    }

    /**
     * Returns the value of the first argument raised to the power of the
     * second argument. 
     * @see java.lang.Math#pow(double, double)
     */
    public double pow(double a, double b) {
        return StrictMath.pow(a, b); 
    }

    /**
     * Returns the closest {@code int} to the argument, with ties
     * rounding to positive infinity.
     * @see java.lang.Math#round(float)
     */
    public int round(float a) {
        return strict ? StrictMath.round(a) : Math.round(a);
    }

    /**
     * Returns the closest {@code long} to the argument, with ties
     * rounding to positive infinity.
     * @see java.lang.Math#round(double)
     */
    public long round(double a) {
        return strict ? StrictMath.round(a) : Math.round(a);
    }

    /**
     * Returns a {@code double} value with a positive sign, greater
     * than or equal to {@code 0.0} and less than {@code 1.0}.
     * @see java.lang.Math#random()
     */
    public double random() {
        return Math.random();
    }

    /**
     * Returns the sum of its arguments,
     * throwing an exception if the result overflows an {@code int}.
     * @see java.lang.Math#addExact(int, int)
     */
    public int addExact(int x, int y) {
        return strict ? StrictMath.addExact(x, y) : Math.addExact(x, y);
    }

    /**
     * Returns the sum of its arguments,
     * throwing an exception if the result overflows a {@code long}.
     * @see java.lang.Math#addExact(long, long)
     */
    public long addExact(long x, long y) {
        return strict ? StrictMath.addExact(x, y) : Math.addExact(x, y);
    }

    /**
     * Returns the difference of the arguments,
     * throwing an exception if the result overflows an {@code int}.
     * @see java.lang.Math#subtractExact(int, int)
     */
    public int subtractExact(int x, int y) {
        return strict ? StrictMath.subtractExact(x, y) : Math.subtractExact(x, y);
    }

    /**
     * Returns the difference of the arguments,
     * throwing an exception if the result overflows a {@code long}.
     * @see java.lang.Math#subtractExact(long, long)
     */
    public long subtractExact(long x, long y) {
        return strict ? StrictMath.subtractExact(x, y) : Math.subtractExact(x, y);
    }

    /**
     * Returns the product of the arguments,
     * throwing an exception if the result overflows an {@code int}.
     * @see java.lang.Math#multiplyExact(int, int)
     */
    public int multiplyExact(int x, int y) {
        return strict ? StrictMath.multiplyExact(x, y) : Math.multiplyExact(x, y);
    }

    /**
     * Returns the product of the arguments,
     * throwing an exception if the result overflows a {@code long}.
     * @see java.lang.Math#multiplyExact(long, long)
     */
    public long multiplyExact(long x, long y) {
        return strict ? StrictMath.multiplyExact(x, y) : Math.multiplyExact(x, y);
    }

    /**
     * Returns the argument incremented by one, throwing an exception if the
     * result overflows an {@code int}.
     * @see java.lang.Math#incrementExact(int)
     */
    public int incrementExact(int a) {
        return Math.incrementExact(a);
    }

    /**
     * Returns the argument incremented by one, throwing an exception if the
     * result overflows a {@code long}.
     * @see java.lang.Math#incrementExact(long)
     */
    public long incrementExact(long a) {
        return Math.incrementExact(a);
    }

    /**
     * Returns the argument decremented by one, throwing an exception if the
     * result overflows an {@code int}.
     * @see java.lang.Math#decrementExact(int)
     */
    public int decrementExact(int a) {
        return Math.decrementExact(a);
    }

    /**
     * Returns the argument decremented by one, throwing an exception if the
     * result overflows a {@code long}.
     * @see java.lang.Math#decrementExact(long)
     */
    public long decrementExact(long a) {
        return Math.decrementExact(a);
    }

    /**
     * Returns the negation of the argument, throwing an exception if the
     * result overflows an {@code int}.
     * @see java.lang.Math#negateExact(int)
     */
    public int negateExact(int a) {
        return Math.negateExact(a);
    }

    /**
     * Returns the negation of the argument, throwing an exception if the
     * result overflows a {@code long}.
     * @see java.lang.Math#negateExact(long)
     */
    public long negateExact(long a) {
        return Math.negateExact(a);
    }

    /**
     * Returns the value of the {@code long} argument;
     * throwing an exception if the value overflows an {@code int}.
     * @see java.lang.Math#toIntExact(long)
     */
    public int toIntExact(long value) {
        return strict ? StrictMath.toIntExact(value) : Math.toIntExact(value);
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code int} value that is less than or equal to the algebraic quotient.
     * @see java.lang.Math#floorDiv(int, int)
     */
    public int floorDiv(int x, int y) {
        return strict ? StrictMath.floorDiv(x, y) : Math.floorDiv(x, y);
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code long} value that is less than or equal to the algebraic quotient.
     * @see java.lang.Math#floorDiv(long, long)
     */
    public long floorDiv(long x, long y) {
        return strict ? StrictMath.floorDiv(x, y) : Math.floorDiv(x, y);
    }

    /**
     * Returns the floor modulus of the {@code int} arguments.
     * @see java.lang.Math#floorMod(int, int)
     */
    public int floorMod(int x, int y) {
        return strict ? StrictMath.floorMod(x, y) : Math.floorMod(x, y);
    }

    /**
     * Returns the floor modulus of the {@code long} arguments.
     * @see java.lang.Math#floorMod(long, long)
     */
    public long floorMod(long x, long y) {
        return strict ? StrictMath.floorMod(x, y) : Math.floorMod(x, y);
    }

    /**
     * Returns the absolute value of an {@code int} value.
     * @see java.lang.Math#abs(int)
     */
    public int abs(int a) {
        return strict ? StrictMath.abs(a) : Math.abs(a);
    }

    /**
     * Returns the absolute value of a {@code long} value.
     * @see java.lang.Math#abs(long)
     */
    public long abs(long a) {
        return strict ? StrictMath.abs(a) : Math.abs(a);
    }

    /**
     * Returns the absolute value of a {@code float} value.
     * @see java.lang.Math#abs(float)
     */
    public float abs(float a) {
        return strict ? StrictMath.abs(a) : Math.abs(a);
    }

    /**
     * Returns the absolute value of a {@code double} value.
     * @see java.lang.Math#abs(double)
     */
    public double abs(double a) {
        return strict ? StrictMath.abs(a) : Math.abs(a);
    }

    /**
     * Returns the greater of two {@code int} values. 
     * @see java.lang.Math#max(int, int)
     */
    public int max(int a, int b) {
        return strict ? StrictMath.max(a, b) : Math.max(a, b);
    }

    /**
     * Returns the greater of two {@code long} values. 
     * @see java.lang.Math#max(long, long)
     */
    public long max(long a, long b) {
        return strict ? StrictMath.max(a, b) : Math.max(a, b);
    }

    /**
     * Returns the greater of two {@code float} values.  
     * @see java.lang.Math#max(float, float)
     */
    public float max(float a, float b) {
        return strict ? StrictMath.max(a, b) : Math.max(a, b);
    }

    /**
     * Returns the greater of two {@code double} values.  
     * @see java.lang.Math#max(double, double)
     */
    public double max(double a, double b) {
        return strict ? StrictMath.max(a, b) : Math.max(a, b);
    }

    /**
     * Returns the smaller of two {@code int} values. 
     * @see java.lang.Math#min(int, int)
     */
    public int min(int a, int b) {
        return strict ? StrictMath.min(a, b) : Math.min(a, b);
    }

    /**
     * Returns the smaller of two {@code long} values. 
     * @see java.lang.Math#min(long, long)
     */
    public long min(long a, long b) {
        return strict ? StrictMath.min(a, b) : Math.min(a, b);
    }

    /**
     * Returns the smaller of two {@code float} values.  
     * @see java.lang.Math#min(float, float)
     */
    public float min(float a, float b) {
        return strict ? StrictMath.min(a, b) : Math.min(a, b);
    }

    /**
     * Returns the smaller of two {@code double} values.  
     * @see java.lang.Math#min(double, double)
     */
    public double min(double a, double b) {
        return strict ? StrictMath.min(a, b) : Math.min(a, b);
    }

    /**
     * Returns the size of an ulp of the argument.  An ulp, unit in
     * the last place, of a {@code double} value is the positive
     * distance between this floating-point value and the {@code
     * double} value next larger in magnitude.  Note that for non-NaN
     * <i>x</i>, <code>ulp(-<i>x</i>) == ulp(<i>x</i>)</code>.
     * @see java.lang.Math#ulp(double)
     */
    public double ulp(double d) {
        return strict ? StrictMath.ulp(d) : Math.ulp(d);
    }

    /**
     * Returns the size of an ulp of the argument.  An ulp, unit in
     * the last place, of a {@code float} value is the positive
     * distance between this floating-point value and the {@code
     * float} value next larger in magnitude.  Note that for non-NaN
     * <i>x</i>, <code>ulp(-<i>x</i>) == ulp(<i>x</i>)</code>.
     * @see java.lang.Math#ulp(float)
     */
    public float ulp(float f) {
        return strict ? StrictMath.ulp(f) : Math.ulp(f);
    }

    /**
     * Returns the signum function of the argument; zero if the argument
     * is zero, 1.0 if the argument is greater than zero, -1.0 if the
     * argument is less than zero.
     * @see java.lang.Math#signum(double)
     */
    public double signum(double d) {
        return strict ? StrictMath.signum(d) : Math.signum(d);
    }

    /**
     * Returns the signum function of the argument; zero if the argument
     * is zero, 1.0f if the argument is greater than zero, -1.0f if the
     * argument is less than zero.
     * @see java.lang.Math#signum(float)
     */
    public float signum(float f) {
        return strict ? StrictMath.signum(f) : Math.signum(f);
    }

    /**
     * Returns the hyperbolic sine of a {@code double} value.
     * @see java.lang.Math#sinh(double)
     */
    public double sinh(double x) {
        return StrictMath.sinh(x);
    }

    /**
     * Returns the hyperbolic cosine of a {@code double} value.
     * @see java.lang.Math#cosh(double)
     */
    public double cosh(double x) {
        return StrictMath.cosh(x);
    }

    /**
     * Returns the hyperbolic tangent of a {@code double} value.
     * @see java.lang.Math#tanh(double)
     */
    public double tanh(double x) {
        return StrictMath.tanh(x);
    }

    /**
     * Returns sqrt(<i>x</i><sup>2</sup>&nbsp;+<i>y</i><sup>2</sup>)
     * without intermediate overflow or underflow.
     * @see java.lang.Math#hypot(double, double)
     */
    public static double hypot(double x, double y) {
        return StrictMath.hypot(x, y);
    }

    /**
     * Returns <i>e</i><sup>x</sup>&nbsp;-1.  
     * @see java.lang.Math#expm1(double)
     */
    public double expm1(double x) {
        return StrictMath.expm1(x);
    }

    /**
     * Returns the natural logarithm of the sum of the argument and 1.
     * @see java.lang.Math#log1p(double)
     */
    public double log1p(double x) {
        return StrictMath.log1p(x);
    }

    /**
     * Returns the first floating-point argument with the sign of the
     * second floating-point argument.  
     * @see java.lang.Math#copySign(double, double)
     * @see java.lang.StrictMath#copySign(double, double)
     */
    public double copySign(double magnitude, double sign) {
        return strict ? StrictMath.copySign(magnitude, sign) : Math.copySign(magnitude, sign);
    }

    /**
     * Returns the first floating-point argument with the sign of the
     * second floating-point argument.  
     * @see java.lang.Math#copySign(float, float)
     * @see java.lang.StrictMath#copySign(float, float)
     */
    public float copySign(float magnitude, float sign) {
        return strict ? StrictMath.copySign(magnitude, sign) : Math.copySign(magnitude, sign);
    }

    /**
     * Returns the unbiased exponent used in the representation of a
     * {@code float}.  
     * @see java.lang.Math#getExponent(float)
     */
    public int getExponent(float f) {
        return strict ? StrictMath.getExponent(f) : Math.getExponent(f);
    }

    /**
     * Returns the unbiased exponent used in the representation of a
     * {@code double}.  
     * @see java.lang.Math#getExponent(double)
     */
    public int getExponent(double d) {
        return strict ? StrictMath.getExponent(d) : Math.getExponent(d);
    }

    /**
     * Returns the floating-point number adjacent to the first
     * argument in the direction of the second argument.  If both
     * arguments compare as equal the second argument is returned.
     * @see java.lang.Math#nextAfter(double, double)
     */
    public double nextAfter(double start, double direction) {
        return strict ? StrictMath.nextAfter(start, direction) : Math.nextAfter(start, direction);
    }

    /**
     * Returns the floating-point number adjacent to the first
     * argument in the direction of the second argument.  If both
     * arguments compare as equal a value equivalent to the second argument
     * is returned.
     * @see java.lang.Math#nextAfter(float, double)
     */
    public float nextAfter(float start, double direction) {
        return strict ? StrictMath.nextAfter(start, direction) : Math.nextAfter(start, direction);
    }

    /**
     * Returns the floating-point value adjacent to {@code d} in
     * the direction of positive infinity.  
     * @see java.lang.Math#nextUp(double)
     */
    public double nextUp(double d) {
        return strict ? StrictMath.nextUp(d) : Math.nextUp(d);
    }

    /**
     * Returns the floating-point value adjacent to {@code f} in
     * the direction of positive infinity.  
     * @see java.lang.Math#nextUp(float)
     */
    public float nextUp(float f) {
        return strict ? StrictMath.nextUp(f) : Math.nextUp(f);
    }

    /**
     * Returns the floating-point value adjacent to {@code d} in
     * the direction of negative infinity.  
     * @see java.lang.Math#nextDown(double)
     */
    public double nextDown(double d) {
        return strict ? StrictMath.nextDown(d) : Math.nextDown(d);
    }

    /**
     * Returns the floating-point value adjacent to {@code f} in
     * the direction of negative infinity.  
     * @see java.lang.Math#nextDown(float)
     */
    public float nextDown(float f) {
        return strict ? StrictMath.nextDown(f) : Math.nextDown(f);
    }

    /**
     * Returns {@code d} &times;
     * 2<sup>{@code scaleFactor}</sup> rounded as if performed
     * by a single correctly rounded floating-point multiply to a
     * member of the double value set.  
     * @see java.lang.Math#scalb(double, int)
     */
    public double scalb(double d, int scaleFactor) {
        return strict ? StrictMath.scalb(d, scaleFactor) : Math.scalb(d, scaleFactor);
    }

    /**
     * Returns {@code f} &times;
     * 2<sup>{@code scaleFactor}</sup> rounded as if performed
     * by a single correctly rounded floating-point multiply to a
     * member of the float value set.  
     * @see java.lang.Math#scalb(float, int)
     */
    public static float scalb(float f, int scaleFactor) {
        return Math.scalb(f, scaleFactor);
    }

}
