/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.jexl.annotation.JexlNamespace;
import io.kafster.jexl.annotation.JexlNamespaceOperation;

@JexlNamespace(prefix="time", description="Time utilities")
public class TimeNamespace {
	
	private static final Logger log = LoggerFactory.getLogger(TimeNamespace.class);
	
	@JexlNamespaceOperation(description="Convert a long milliseconds since epoch (Unix Time) to an ISO8601 string")
	public String epochToIso8601(Object epoch) {
		if (epoch != null) {
			Long millis = 0L;
			if (epoch instanceof Long) {
				millis = (Long)epoch;
			}
			else if (epoch instanceof String) {
				try {
					millis = Long.parseLong((String)epoch);
				}
				catch (Exception e) {
					log.warn("Could not convert [" + epoch + "]", e);
				}
			}
			return ISODateTimeFormat.dateTime().print(millis);
		}
		return null;
	}
	
	@JexlNamespaceOperation(description="Convert an ISO8601 time string to a milliseconds since epoch value")
	public Long iso8601ToEpoch(String time) {
		if (time != null) {
			DateTime result = ISODateTimeFormat.dateTimeParser().parseDateTime(time);
			if (result != null) {
				return result.getMillis();
			}
		}
		return null;
	}	
	
	@JexlNamespaceOperation(description="Format a date using a format string")
	public String format(Object date, String format) {
		return null;
	}
}
