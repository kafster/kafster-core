/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

import io.kafster.jexl.annotation.JexlNamespace;
import io.kafster.jexl.annotation.JexlNamespaceOperation;

@JexlNamespace(prefix="str", description="General string utilities")
public class StringNamespace {
	
	@JexlNamespaceOperation(description="Abbreviates a String to the specified length using ellipses.")
	public String abbreviate(String string, int length) {
		return StringUtils.abbreviate(string, length);
	}
	
	@JexlNamespaceOperation(description="Capitalizes a String changing the first character to title case.")
	public String capitalize(String string) {
		return StringUtils.capitalize(string);
	}
	
	@JexlNamespaceOperation(description="Centers a String in a larger String of the specified size using the space character (' ').")
	public String center(String string, int size) {
		return StringUtils.center(string, size);
	}
	
	@JexlNamespaceOperation(description="Removes one newline from end of a String if it's there, otherwise leave it alone.")
	public String chomp(String string) {
		return StringUtils.chomp(string);
	}	
	
	@JexlNamespaceOperation(description="Remove the last character from a String.")
	public String chop(String string) {
		return StringUtils.chop(string);
	}
	
	@JexlNamespaceOperation(description="Compare two Strings lexicographically, returning 0 if the strings are equal (or both null), < 0 if the first is less than the second, > 0 otherwise")
	public int compare(String string1, String string2) {
		return StringUtils.compare(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Compare two Strings lexicographically, ignoring case differences, returning 0 if the strings are equal (or both null), < 0 if the first is less than the second, > 0 otherwise")
	public int compareIgnoreCase(String string1, String string2) {
		return StringUtils.compareIgnoreCase(string1, string2);
	}	
	
	@JexlNamespaceOperation(description="Check if the first string contains the second string")
	public boolean contains(String string1, String string2) {
		return StringUtils.contains(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string contains any character in the second string")
	public boolean containsAny(String string1, String string2) {
		return StringUtils.containsAny(string1, string2);
	}	
	
	@JexlNamespaceOperation(description="Check if the first string contains the second string, ignoring case")
	public boolean containsIgnoreCase(String string1, String string2) {
		return StringUtils.containsIgnoreCase(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string contains none of the characters in the second string")
	public boolean containsNone(String string1, String string2) {
		return StringUtils.containsNone(string1, string2);
	}	
	
	@JexlNamespaceOperation(description="Check if the first string only contains characters in the second string")
	public boolean containsOnly(String string1, String string2) {
		return StringUtils.containsOnly(string1, string2);
	}		
	
	@JexlNamespaceOperation(description="Check whether the given string contains any whitespace characters")
	public boolean containsWhitespace(String string1) {
		return StringUtils.containsWhitespace(string1);
	}
	
	@JexlNamespaceOperation(description="Count how many times the second string appears in the first string")
	public int countMatches(String string1, String string2) {
		return StringUtils.countMatches(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Returns either the first string, or if it is whitespace, empty (\"\") or null, the second string")
	public String defaultString(String string1, String string2) {
		return StringUtils.defaultIfBlank(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Delete all whitespaces from a string")
	public String deleteWhitespace(String string1) {
		return StringUtils.deleteWhitespace(string1);
	}	
	
	@JexlNamespaceOperation(description="Return the remainder of the second String, starting from where it's different from the first")
	public String difference(String string1, String string2) {
		return StringUtils.difference(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string ends with the second string")
	public boolean endsWith(String string1, String string2) {
		return StringUtils.endsWith(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string ends with any of the specified strings")
	public boolean endsWithAny(String string1, String... suffixes) {
		return StringUtils.endsWithAny(string1, suffixes);
	}
	
	@JexlNamespaceOperation(description="Check if the first string ends with the second string, ignoring case")
	public boolean endsWithIgnoreCase(String string1, String string2) {
		return StringUtils.endsWithIgnoreCase(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string equals the second string")
	public boolean equals(String string1, String string2) {
		return StringUtils.equals(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string equals any of the specified strings")
	public boolean equalsAny(String string1, String... suffixes) {
		return StringUtils.equalsAny(string1, suffixes);
	}
	
	@JexlNamespaceOperation(description="Check if the first string equals the second string, ignoring case")
	public boolean equalsIgnoreCase(String string1, String string2) {
		return StringUtils.equalsIgnoreCase(string1, string2);
	}
	
	@JexlNamespaceOperation(description="Check if the first string equals any of the specified strings, ignoring case")
	public boolean equalsAnyIgnoreCase(String string1, String... suffixes) {
		return StringUtils.equalsAnyIgnoreCase(string1, suffixes);
	}	
	
	@JexlNamespaceOperation(description="Returns the first value in a set of strings which is not empty (\"\"), null or whitespace only.")
	public String firstNonBlank(String... strings) {
		return StringUtils.firstNonBlank(strings);
	}	
	
	@JexlNamespaceOperation(description="Returns the first value in a set of strings which is not empty (\"\").")
	public String firstNonEmpty(String... strings) {
		return StringUtils.firstNonEmpty(strings);
	}	
	
	@JexlNamespaceOperation(description="Returns an empty byte array if string is null, the string as a byte array in the specified character set otherwise.")
	public byte[] getBytes(String str, String charset) throws UnsupportedEncodingException {
		return StringUtils.getBytes(str, charset);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of the second string in the first string, returning -1 if no match or null string input.")
	public int indexOf(String str1, String str2) {
		return StringUtils.indexOf(str1, str2);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of the second string in the first string, starting from the given offset, returning -1 if no match or null string input.")
	public int indexOf(String str1, String str2, int start) {
		return StringUtils.indexOf(str1, str2, start);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of any character in the second string in the first string, returning -1 if no match or null string input.")
	public int indexOfAny(String str1, String str2) {
		return StringUtils.indexOfAny(str1, str2);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of any character in the second string not in the first string, returning -1 if no match or null string input.")
	public int indexOfAnyBut(String str1, String str2) {
		return StringUtils.indexOfAnyBut(str1, str2);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of the second string in the first string, ignoring case, returning -1 if no match or null string input.")
	public int indexOfIgnoreCase(String str1, String str2) {
		return StringUtils.indexOfIgnoreCase(str1, str2);
	}	
	
	@JexlNamespaceOperation(description="Find the first index of the second string in the first string, starting from the given offset and ignoring case, returning -1 if no match or null string input.")
	public int indexOfIgnoreCase(String str1, String str2, int start) {
		return StringUtils.indexOfIgnoreCase(str1, str2, start);
	}
	
	@JexlNamespaceOperation(description="Returns true if all of the provided strings are empty (\"\") or null or whitespace only")
	public boolean isAllBlank(String... strings) {
		return StringUtils.isAllBlank(strings);
	}	
	
	@JexlNamespaceOperation(description="Returns true if all of the provided strings are empty (\"\") or null")
	public boolean isAllEmpty(String... strings) {
		return StringUtils.isAllEmpty(strings);
	}	
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only lowercase characters")
	public boolean isAllLowerCase(String string) {
		return StringUtils.isAllLowerCase(string);
	}	
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only upper case characters")
	public boolean isAllUpperCase(String string) {
		return StringUtils.isAllUpperCase(string);
	}
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only Unicode letters")
	public boolean isAlpha(String string) {
		return StringUtils.isAlpha(string);
	}
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only Unicode letters or digits")
	public boolean isAlphanumeric(String string) {
		return StringUtils.isAlphanumeric(string);
	}
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only Unicode letters or digits or space (\" \")")
	public boolean isAlphanumericSpace(String string) {
		return StringUtils.isAlphanumericSpace(string);
	}
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only Unicode letters or space (\" \")")
	public boolean isAlphaSpace(String string) {
		return StringUtils.isAlphaSpace(string);
	}
	
	@JexlNamespaceOperation(description="Checks if any of a set of strings is empty (\"\") or null.")
	public boolean isAnyEmpty(String... strings) {
		return StringUtils.isAnyEmpty(strings);
	}
	
	@JexlNamespaceOperation(description="Checks if the provided string contains only ASCII printable characters")
	public boolean isAsciiPrintable(String string) {
		return StringUtils.isAsciiPrintable(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string is empty (\"\"), null or whitespace only")
	public boolean isBlank(String string) {
		return StringUtils.isBlank(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string is empty (\"\") or null")
	public boolean isEmpty(String string) {
		return StringUtils.isEmpty(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string is empty (\"\") or null")
	public boolean isMixedCase(String string) {
		return StringUtils.isMixedCase(string);
	}	
	
	@JexlNamespaceOperation(description="Returns true if none of the provided strings are empty (\"\") or null or whitespace only")
	public boolean isNoneBlank(String... strings) {
		return StringUtils.isNoneBlank(strings);
	}	
	
	@JexlNamespaceOperation(description="Returns true if none of the provided strings are empty (\"\") or null")
	public boolean isNoneEmpty(String... strings) {
		return StringUtils.isNoneEmpty(strings);
	}	
	
	@JexlNamespaceOperation(description="Checks if a string is not empty (\"\"), null or whitespace only")
	public boolean isNotBlank(String string) {
		return StringUtils.isNotBlank(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string is not empty (\"\") or null")
	public boolean isNotEmpty(String string) {
		return StringUtils.isNotEmpty(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string contains only Unicode digits")
	public boolean isNumeric(String string) {
		return StringUtils.isNumeric(string);
	}	
	
	@JexlNamespaceOperation(description="Checks if a string contains only Unicode digits or space (' ')")
	public boolean isNumericSpace(String string) {
		return StringUtils.isNumericSpace(string);
	}
	
	@JexlNamespaceOperation(description="Checks if a string contains only whitespace")
	public boolean isWhitespace(String string) {
		return StringUtils.isWhitespace(string);
	}
	
	@JexlNamespaceOperation(description="Joins the elements of the provided array into a single string separated by the specified delimiter")
	public String join(Object[] objects, String delimiter) {
		return StringUtils.join(objects, delimiter);
	}	
	
	@JexlNamespaceOperation(description="Joins the elements of the provided array within the specified indices into a single string separated by the delimiter")
	public String join(Object[] objects, String delimiter, int start, int end) {
		return StringUtils.join(objects, delimiter, start, end);
	}		
}
