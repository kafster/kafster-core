/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.impl;

import java.util.Map;

import org.apache.kafka.streams.Topology;

import io.kafster.endpoint.Endpoint;
import io.kafster.kafka.factory.TopologyFactory;
import io.kafster.subscription.Subscription;

public class ContentTypeTopologyFactory implements TopologyFactory {
	
	private Map<String,TopologyFactory> factories;

	public Topology create(Subscription subscription) throws Exception {
		
		Endpoint source = subscription.getSource();
		if ((source == null) || (source.getMetadata() == null) || (source.getMetadata().getContentType() == null)) {
			return null;
		}
		
		TopologyFactory factory = factories.get(source.getMetadata().getContentType());
		if (factory == null) {
			return null;
		}
		
		return factory.create(subscription);
	}


	public void setFactories(Map<String, TopologyFactory> factories) {
		this.factories = factories;
	}
	
}
