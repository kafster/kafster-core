/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.impl;

import org.apache.kafka.streams.kstream.KStream;

import io.kafster.kafka.stream.StreamRecord;
import io.kafster.subscription.Subscription;

public class InterceptorTopologyFactory extends AbstractTopologyFactory {
	
	private Interceptor interceptor;

	@Override
	protected KStream<byte[], StreamRecord> preprocess(KStream<byte[], byte[]> stream, Subscription subscription) throws Exception {
		return interceptor.preprocess(stream, subscription);
	}

	@Override
	protected KStream<byte[], byte[]> postprocess(KStream<byte[], StreamRecord> stream, Subscription subscription) throws Exception {
		return interceptor.postprocess(stream, subscription);
	}
	
	public interface Interceptor {
		KStream<byte[], StreamRecord> preprocess(KStream<byte[], byte[]> stream, Subscription subscription) throws Exception;
		KStream<byte[], byte[]> postprocess(KStream<byte[], StreamRecord> stream, Subscription subscription) throws Exception;
	}

	public void setInterceptor(Interceptor interceptor) {
		this.interceptor = interceptor;
	}

}
