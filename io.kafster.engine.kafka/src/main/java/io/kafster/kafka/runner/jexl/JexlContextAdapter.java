/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.jexl;

import org.apache.commons.jexl3.JexlContext;

import io.kafster.kafka.stream.StreamRecord;

public class JexlContextAdapter implements JexlContext {
	
	private StreamRecord context;
	
	public JexlContextAdapter(StreamRecord context) {
		this.context = context;
	}

	@Override
	public Object get(String name) {
		return context.get(name);
	}

	@Override
	public boolean has(String name) {
		return context.get(name) != null;
	}

	@Override
	public void set(String name, Object value) {
		context.set(name, value);
	}

}
