/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.jexl;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.jexl3.JexlContext;

/**
 * A JexlContext that can be mapped easily to/from JSON.
 */
public class HierarchicalObjectMapContext implements JexlContext {
	
	private Map<String,Object> value;
	
	public HierarchicalObjectMapContext() {
		this.value = new TreeMap<>();
	}
	
	public HierarchicalObjectMapContext(Map<String,Object> value) {
		this.value = value;
	}
	
	public Map<String,Object> getAsMap() {
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object get(String path) {
		DottedName name = DottedName.from(path);
		if (name != null) {
			Map<String,Object> container = this.value;
			
			Iterator<String> segments = name.iterator();
			if (segments.hasNext()) {
				String segment = segments.next();
				
				while (segments.hasNext() && (container != null)) {
					container = (Map<String,Object>)container.get(segment);
					segment = segments.next();				
				}
				
				if ((container != null) && (segment != null)) {
					Object result = container.get(segment);
					if ((result instanceof Map) && segments.hasNext()) {
						// Force JEXL to ask for the leaf node
						//
						return null;
					}
					return result;
				}
			}
		}
		return null;
	}

	@Override
	public boolean has(String path) {
		return get(path) != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void set(String path, Object value) {
		DottedName name = DottedName.from(path);
		if (name != null) {
			Map<String,Object> container = this.value;
			
			Iterator<String> segments = name.iterator();
			String segment = segments.next();
			
			while (segments.hasNext()) {
				container = (Map<String,Object>)container.computeIfAbsent(segment, (key) -> new TreeMap<String,Object>());
				segment = segments.next();
			}
			
			if (container != null) {
				container.put(segment, value);
			}
		}
	}

	@Override
	public String toString() {
		return value.toString();
	}

}
