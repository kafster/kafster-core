/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner;

import java.util.Map;
import java.util.Properties;

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.streams.Topology;

public interface TopologyRunner {
	
	public interface TopologyStatus {
		
		enum Status { QUEUED, CREATED, REBALANCING, RUNNING, PENDING_SHUTDOWN, ERROR, NOT_RUNNING }
		
		Status getStatus();
		String getStatusDetail();
		String getId();
		
		Map<MetricName,? extends Metric> getMetrics();
		
	}

	TopologyStatus execute(Topology topology, Properties props);
	TopologyStatus shutdown(String key);
	
	TopologyStatus getStatus(String id);
	Iterable<TopologyStatus> getStatus();
}
