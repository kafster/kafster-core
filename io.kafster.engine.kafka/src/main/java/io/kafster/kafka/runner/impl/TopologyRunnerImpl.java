/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.impl;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.kafka.runner.TopologyRunner;
import io.kafster.kafka.runner.TopologyRunner.TopologyStatus.Status;

public class TopologyRunnerImpl implements TopologyRunner {
	
	private static final Logger log = LoggerFactory.getLogger(TopologyRunnerImpl.class);
	
	private Map<String,TopologyExecutor> topologies = new ConcurrentHashMap<>();
	
	public TopologyStatus execute(Topology topology, Properties props) {
		TopologyExecutor job = new TopologyExecutor(topology, props);
		job.start();
		
		TopologyStatus result = job.getStatus();
		topologies.put(result.getId(), job);
		return result;
	}

	@Override
	public TopologyStatus shutdown(String key) {
		TopologyExecutor executor = topologies.get(key);
		if (executor != null) {
			executor.shutdown();
			return executor.getStatus();
		}
		return null;
	}	
	
	public TopologyStatus getStatus(String id) {
		TopologyExecutor result = topologies.get(id);
		if (result != null) {
			return result.getStatus();
		}
		return null;
	}
	
	public Iterable<TopologyStatus> getStatus() {
		return topologies.values().stream().map(m -> m.getStatus()).collect(Collectors.toList());
	}
	
	public void shutdown() {
		topologies.forEach((id, executor) -> {
			try {
				executor.shutdown();
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		});
	}
	
	protected class TopologyExecutor implements Runnable, KafkaStreams.StateListener {
		
		private Topology topology;
		private Properties props;
		
		private SimpleTopologyStatus status;
		
		private KafkaStreams streams;
		
		public TopologyExecutor(Topology topology, Properties props) {
			this.topology = topology;
			this.props = props;
			
			// Use the application ID as the executor ID
			//
			this.status = new SimpleTopologyStatus(props.getProperty(StreamsConfig.APPLICATION_ID_CONFIG), Status.QUEUED);
		}
		
		public TopologyStatus getStatus() {
			return status;
		}
		
		public void shutdown() {
			if (streams != null) {
				streams.close();
			}
		}

		@Override
		public void run() {
			start();
		}
		
		public void start() {

			try {
				streams = new KafkaStreams(topology, props);
				streams.setStateListener(this);
				streams.start();				
			} 
			catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
				status.setStatus(Status.ERROR);
				status.setStatusDetail(e.getLocalizedMessage());
			}			
		}

		@Override
		public void onChange(State updated, State original) {
			switch (updated) {
				case CREATED:
					status.setStatus(Status.CREATED);
					break;
				case ERROR:
					status.setStatus(Status.ERROR);
					break;
				case NOT_RUNNING:
					status.setStatus(Status.NOT_RUNNING);
					break;
				case PENDING_SHUTDOWN:
					status.setStatus(Status.PENDING_SHUTDOWN);
					break;
				case REBALANCING:
					status.setStatus(Status.REBALANCING);
					break;
				case RUNNING:
					status.setStatus(Status.RUNNING);
					break;
				default:
					break;
			
			}
		}
		
	}
	
	protected class SimpleTopologyStatus implements TopologyStatus {
		
		private String id;
		private Status status;
		private String statusDetail;
		
		public SimpleTopologyStatus(String id, Status status) {
			this.status = status;
		}

		@Override
		public synchronized Status getStatus() {
			return status;
		}
		
		public synchronized void setStatus(Status status) {
			this.status = status;
		}

		@Override
		public synchronized String getStatusDetail() {
			return statusDetail;
		}
		
		public synchronized void setStatusDetail(String detail) {
			this.statusDetail = detail;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public Map<MetricName, ? extends Metric> getMetrics() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

}
