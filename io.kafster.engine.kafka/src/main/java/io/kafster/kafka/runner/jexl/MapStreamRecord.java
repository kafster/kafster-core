/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.jexl;

import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.kafka.stream.StreamRecord;

public class MapStreamRecord implements StreamRecord {
	
	private HierarchicalObjectMapContext context;
	
	private Map<String,Object> map;
	private ObjectMapper mapper;
	
	public MapStreamRecord(Map<String,Object> map, ObjectMapper mapper) {
		this.map = map;
		this.mapper = mapper;
		
		this.context = new HierarchicalObjectMapContext(map);
	}
	
	public Map<String,Object> getAsMap() {
		return context.getAsMap();
	}

	@Override
	public Object get(String path) {
		return context.get(path);
	}

	@Override
	public StreamRecord set(String path, Object value) {
		if (value instanceof MapStreamRecord) {
			value = ((MapStreamRecord)value).getAsMap();
		}
		context.set(path, value);
		return this;
	}

	@Override
	public StreamRecord copy() {
		return new MapStreamRecord(new TreeMap<>(map), mapper);
	}

	@Override
	public StreamRecord create() {
		return new MapStreamRecord(new TreeMap<>(), mapper);
	}

	@Override
	public StreamRecord copy(String property) {
		Object result = context.get(property);
		if (result instanceof Map) {
			return new MapStreamRecord((Map<String, Object>)result, mapper);
		}
		return null;
	}

}
