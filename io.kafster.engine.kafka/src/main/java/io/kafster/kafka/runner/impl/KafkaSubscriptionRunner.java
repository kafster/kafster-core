/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.impl;

import java.util.Properties;

import org.apache.commons.collections4.IterableUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.kafka.config.BrokerConfiguration;
import io.kafster.kafka.factory.TopologyFactory;
import io.kafster.kafka.factory.impl.SubscriptionStatusAdapter;
import io.kafster.kafka.runner.TopologyRunner;
import io.kafster.kafka.runner.TopologyRunner.TopologyStatus;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionStatus;

public class KafkaSubscriptionRunner {
	
	private static final Logger log = LoggerFactory.getLogger(KafkaSubscriptionRunner.class);
	
	private TopologyFactory factory;
	private TopologyRunner runner;
	private BrokerConfiguration brokerConfiguration;

	public SubscriptionStatus execute(Subscription subscription) throws Exception {
		Topology topology = factory.create(subscription);
		if (topology != null) {
			
			if (log.isTraceEnabled()) {
				log.trace("Created Kafka Streams Topology [" + topology.describe() + "]");
			}
			
			Properties props = new Properties();
	        props.put(StreamsConfig.APPLICATION_ID_CONFIG, subscription.getKey());
	        props.put(StreamsConfig.CLIENT_ID_CONFIG, subscription.getKey());
	        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerConfiguration.getBootstrapServers());
	        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass());
	        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass());
	        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, HeadersTimestampExtractor.class.getName());
	        
			return new SubscriptionStatusAdapter(runner.execute(topology, props));
		}
		return null;
	}

	public SubscriptionStatus shutdown(String key) throws Exception {
		TopologyStatus status = runner.shutdown(key);
		if (status != null) {
			return new SubscriptionStatusAdapter(status);
		}
		return null;
	}

	public Iterable<SubscriptionStatus> getSubscriptions() {
		return IterableUtils.transformedIterable(runner.getStatus(), s -> new SubscriptionStatusAdapter(s));
	}

	public SubscriptionStatus getSubscription(String key) {
		TopologyStatus status = runner.getStatus(key);
		if (status != null) {
			return new SubscriptionStatusAdapter(status);
		}
		return null;
	}

	public void setFactory(TopologyFactory factory) {
		this.factory = factory;
	}

	public void setRunner(TopologyRunner runner) {
		this.runner = runner;
	}

	public void setBrokerConfiguration(BrokerConfiguration brokerConfiguration) {
		this.brokerConfiguration = brokerConfiguration;
	}

}
