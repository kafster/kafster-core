/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.deploy;

import io.kafster.kafka.runner.impl.KafkaSubscriptionRunner;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.deploy.SubscriptionListener;

public class PipelineProvisioner implements SubscriptionListener {
	
	private KafkaSubscriptionRunner runner;	

	@Override
	public void subscriptionAdded(Subscription subscription) throws Exception {
		runner.execute(subscription);
	}

	@Override
	public void subscriptionUpdated(Subscription subscription) throws Exception {
		subscriptionRemoved(subscription);
		subscriptionAdded(subscription);
	}

	@Override
	public void subscriptionRemoved(Subscription subscription) throws Exception {
		runner.shutdown(subscription.getKey());
	}

	public void setRunner(KafkaSubscriptionRunner runner) {
		this.runner = runner;
	}

}
