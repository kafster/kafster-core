/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.deploy;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.kafka.stream.TopicNames;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.beans.SubscriptionConfiguration;
import io.kafster.subscription.deploy.SubscriptionListener;

/**
 * This provisioner creates the actual subscription topic, which is the real
 * target of the pipeline.
 */
public class TopicProvisioner implements SubscriptionListener {
	
	private static final Logger log = LoggerFactory.getLogger(TopicProvisioner.class);
	
	private Admin admin;
	private TopicNames names;
	private ObjectMapper mapper;
	
	private int defaultPartitions = 1;
	private short defaultReplication = 1;
	
	private boolean deleteOnRemove = false;

	@Override
	public void subscriptionAdded(Subscription subscription) {
		
		if (subscription == null) {
			return;
		}
		
		try {
			
			String topic = names.getTopic(subscription);
			if (topic != null) {
				
				DescribeTopicsResult result = admin.describeTopics(Collections.singletonList(topic));
				if ((result == null) || !result.values().containsKey(topic)) {
					
					try {
						int partitions = defaultPartitions;
						short replicationFactor = defaultReplication;
					
						Map<String,String> properties = new HashMap<>();
						
						if (subscription.getConfiguration() != null) {
							SubscriptionConfiguration config = mapper.convertValue(subscription.getConfiguration(), SubscriptionConfiguration.class);
							
							partitions = Optional.of(config.getPartitions()).orElse(partitions);
							replicationFactor = Optional.of(config.getReplicationFactor()).orElse(replicationFactor);
							properties = Optional.of(config.getProperties()).orElse(properties);
						}
						
						NewTopic request = new NewTopic(topic, partitions, replicationFactor);
						request.configs(properties);
						
						CreateTopicsResult created = admin.createTopics(Collections.singletonList(request));
						created.all().get();
					} 
					catch (Exception e) {
						log.error(e.getLocalizedMessage(), e);				
					} 
				}
			}
		}
		catch (Exception e) {
			
		}
	}

	@Override
	public void subscriptionUpdated(Subscription subscription) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void subscriptionRemoved(Subscription subscription) {
		// TODO Auto-generated method stub
		
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public void setNames(TopicNames names) {
		this.names = names;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

}
