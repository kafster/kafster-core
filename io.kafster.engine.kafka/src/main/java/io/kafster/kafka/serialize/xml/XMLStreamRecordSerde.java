/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.serialize.xml;

import java.io.ByteArrayInputStream;
import java.util.function.Supplier;

import javax.xml.parsers.DocumentBuilder;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import io.kafster.kafka.stream.StreamRecord;

public class XMLStreamRecordSerde implements Serde<StreamRecord> {
	
	private static final Logger log = LoggerFactory.getLogger(XMLStreamRecordSerde.class);
	
	private Supplier<DocumentBuilder> builder;

	@Override
	public Deserializer<StreamRecord> deserializer() {
		return new Deserializer<StreamRecord>() {
			@Override
			public StreamRecord deserialize(String topic, byte[] content) {
				if (content != null) {
					try {
						Document dom = builder.get().parse(new ByteArrayInputStream(content));
						return new DOMStreamRecord(dom);
					}
					catch (Exception e) {
						log.error(e.getLocalizedMessage(), e);
					}
				}					
				return null;
			}
		};
	}

	@Override
	public Serializer<StreamRecord> serializer() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setBuilder(Supplier<DocumentBuilder> builder) {
		this.builder = builder;
	}

}
