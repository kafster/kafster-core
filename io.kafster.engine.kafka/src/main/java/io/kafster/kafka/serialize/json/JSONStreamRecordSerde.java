/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.serialize.json;

import java.util.Map;
import java.util.TreeMap;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.kafka.runner.jexl.MapStreamRecord;
import io.kafster.kafka.stream.StreamRecord;

public class JSONStreamRecordSerde implements Serde<StreamRecord> {
	
	private static final Logger log = LoggerFactory.getLogger(JSONStreamRecordSerde.class);

	private ObjectMapper mapper;
	
	public JSONStreamRecordSerde() {
	}
	
	public JSONStreamRecordSerde(ObjectMapper mapper) {
		this.mapper = mapper;
	}
	
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Deserializer<StreamRecord> deserializer() {
		return new Deserializer<StreamRecord>() {
			@Override
			public StreamRecord deserialize(String topic, byte[] content) {
				if (content != null) {
					
					try {
						Map<String, Object>  result = mapper.readValue(content, new TypeReference<TreeMap<String,Object>>(){});
						if (result != null) {
							return new MapStreamRecord(result, mapper);
						}
					} 
					catch (Exception e) {
						log.error(e.getLocalizedMessage(), e);
					}
				}					
				return null;
			}
		};
	}

	@Override
	public Serializer<StreamRecord> serializer() {
		return new Serializer<StreamRecord>() {
			@Override
			public byte[] serialize(String topic, StreamRecord record) {
				if (record instanceof MapStreamRecord) {
					try {
						return mapper.writeValueAsBytes(((MapStreamRecord)record).getAsMap());
					} 
					catch (JsonProcessingException e) {
						log.error(e.getLocalizedMessage(), e);
					}
				}
				return null;
			}
		};
	}

}
