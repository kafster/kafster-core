/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.runner.jexl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.jexl.JexlEngineFactoryBean;
import io.kafster.kafka.serialize.json.ObjectMapperFactoryBean;

public class JEXLTests {
	
	protected JexlEngine getEngine() throws Exception {
		JexlEngineFactoryBean factory = new JexlEngineFactoryBean();
		factory.setStrict(true);
		
		return factory.getObject();
	}
	
	protected ObjectMapper getMapper() throws Exception {
		ObjectMapperFactoryBean factory = new ObjectMapperFactoryBean();
		return factory.getObject();
	}
	
	protected SimpleType getTestObject() {
		SimpleType object = new SimpleType();
		object.setA("a1");
		object.setB(1234);
		
		SimpleType other1 = new SimpleType();
		other1.setA("A2");
		other1.setB(890);
		
		object.setOther(other1);
		
		SimpleType other2 = new SimpleType();
		other2.setB(5678);
		
		other1.setOther(other2);
		
		return object;
	}

	@Test
	public void testSimpleDottedName() throws Exception {
		
		DottedName name = DottedName.from("a.b.c.d");
		
		final AtomicInteger segments = new AtomicInteger(0);
		
		StringBuilder builder = new StringBuilder();
		name.forEach(segment -> {
			if (segments.getAndIncrement() > 0) {
				builder.append(".");
			}
			builder.append(segment);
		});
		
		assertEquals(4, segments.get());
		assertEquals("a.b.c.d", builder.toString());
	}
	
	@Test
	public void testGetExpression() throws Exception {
		
		Map<String,Object> data = getMapper().convertValue(getTestObject(), new TypeReference<TreeMap<String,Object>>(){});
		
		JexlContext context = new HierarchicalObjectMapContext(data);
		JexlEngine engine = getEngine();
		
		JexlExpression expression = engine.createExpression("a");
		Object result = expression.evaluate(context);
		
		assertEquals("a1", result);
		
		expression = engine.createExpression("b");
		result = expression.evaluate(context);
		
		assertEquals(1234, result);
		
		expression = engine.createExpression("other.a");
		result = expression.evaluate(context);
		
		assertEquals("A2", result);
		
		expression = engine.createExpression("other.other.b");
		result = expression.evaluate(context);
		
		assertEquals(5678, result);
	}
	
	@Test
	public void testSetExpression() throws Exception {
		
		Map<String,Object> data = getMapper().convertValue(getTestObject(), new TypeReference<TreeMap<String,Object>>(){});
		
		JexlContext context = new HierarchicalObjectMapContext(data);
		JexlEngine engine = getEngine();
		
		JexlExpression expression = engine.createExpression("a='42'");
		expression.evaluate(context);
		
		expression = engine.createExpression("b=42");
		expression.evaluate(context);
		
		expression = engine.createExpression("other.a='this is the other'");
		expression.evaluate(context);
		
		expression = engine.createExpression("other.other.b=1234567890");
		expression.evaluate(context);
		
		expression = engine.createExpression("other.other.c=12.5");
		expression.evaluate(context);
		
		SimpleType result = getMapper().convertValue(data, SimpleType.class);
		
		assertEquals("42", result.getA());
		assertEquals(42, result.getB());
		assertEquals("this is the other", result.getOther().getA());
		assertEquals(1234567890, result.getOther().getOther().getB());
		assertEquals(12.5f, result.getOther().getOther().getC());
	}
}
