/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.jexl3.JexlArithmetic;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;
import org.junit.jupiter.api.Test;

public class StringTests extends TestSupport {

	private static Object NAMESPACE = new StringNamespace();
	private static JexlArithmetic MATH = new JexlArithmetic(true);

	@Test
	public void testAbbreviate() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:abbreviate(string1, 6)");
		
		MapContext context = new MapContext();
		context.set("string1", "0123456789");
		
		Object result = expr.evaluate(context);
		assertNotNull(result);
		assertEquals("012...", result.toString());
	}
	
	@Test
	public void testCapitalize() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:capitalize(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcdefg");
		
		Object result = expr.evaluate(context);
		assertNotNull(result);
		assertEquals("Abcdefg", result.toString());
	}	
	
	@Test
	public void testCenter() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:center(string1, 8)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcd");
		
		Object result = expr.evaluate(context);
		assertNotNull(result);
		assertEquals("  abcd  ", result.toString());
	}
	
	@Test
	public void testChomp() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:chomp(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcdefg\n");
		
		Object result = expr.evaluate(context);
		assertNotNull(result);
		assertEquals("abcdefg", result.toString());
	}
	
	@Test
	public void testChop() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:chop(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcdefg");
		
		Object result = expr.evaluate(context);
		assertNotNull(result);
		assertEquals("abcdef", result.toString());
	}
	
	@Test
	public void testCompare() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:compare(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcdefg");
		
		context.set("string2", "abcdefg");
		assertTrue(MATH.equals(expr.evaluate(context), 0));
		
		context.set("string2", "1234567");
		assertTrue(MATH.greaterThan(expr.evaluate(context), 0));
		
		context.set("string2", "tuvwxyz");
		assertTrue(MATH.lessThan(expr.evaluate(context), 0));		
	}	
	
	@Test
	public void testCompareIgnoreCase() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:compareIgnoreCase(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcDEfg");
		
		context.set("string2", "abcDEfg");
		assertTrue(MATH.equals(expr.evaluate(context), 0));		
		
		context.set("string2", "abcdefg");
		assertTrue(MATH.equals(expr.evaluate(context), 0));			
	}
	
	@Test
	public void testContains() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:contains(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcDEfg");
		
		context.set("string2", "abcDEfg");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string2", "abc");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));	
		
		context.set("string2", "xyz");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));	
	}
	
	@Test
	public void testContainsAny() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:containsAny(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcDEfg");
		
		context.set("string2", "1234567a");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string2", "1234567z");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));		
	}
	
	@Test
	public void testContainsIgnoreCase() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:containsIgnoreCase(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcDEfg");
		
		context.set("string2", "abc");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string2", "ABC");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));			
		
		context.set("string2", "xyz");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));		
	}	
	
	@Test
	public void testContainsNone() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:containsNone(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcDEfg");
		
		context.set("string2", "xyz");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string2", "abc");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));		
	}
	
	@Test
	public void testContainsOnly() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:containsOnly(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcabcabc");
		
		context.set("string2", "abc");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string2", "ab");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));		
	}	
	
	@Test
	public void testContainsWhitespace() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:containsWhitespace(string1)");
		
		MapContext context = new MapContext();
		
		context.set("string1", "abcabc abc");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));
		
		context.set("string1", " abcabcabc");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));

		context.set("string1", "abcabcabc ");
		assertTrue(MATH.toBoolean(expr.evaluate(context)));		
		
		context.set("string1", "abcabcabc");
		assertFalse(MATH.toBoolean(expr.evaluate(context)));		
	}
	
	@Test
	public void testCountMatches() throws Exception {
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("str:countMatches(string1,string2)");
		
		MapContext context = new MapContext();
		context.set("string1", "abcabc abcaa");
		
		context.set("string2", "abc");
		assertTrue(MATH.equals(expr.evaluate(context), 3));
		
		context.set("string2", "a");
		assertTrue(MATH.equals(expr.evaluate(context), 5));		
	}	
}
