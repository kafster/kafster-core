/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

import org.apache.commons.jexl3.JexlEngine;

import io.kafster.jexl.JexlEngineFactoryBean;
import io.kafster.jexl.beans.SimpleNamespaceRegistration;

public class TestSupport {
	
	private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();
	private static final char[] APHANUM_ARRAY = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
	
	protected JexlEngine getJexl(Object... namespaces) throws Exception {
		
		SimpleNamespaceRegistration registration = new SimpleNamespaceRegistration();
		registration.setNamespaces(new HashSet<>(Arrays.asList(namespaces)));
		
		JexlEngineFactoryBean factory = new JexlEngineFactoryBean();
		factory.addNamespace(registration, Collections.emptyMap());
		
		return factory.getObject();
	}
	
	protected String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for (int j = 0; j < bytes.length; j++) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = HEX_ARRAY[v >>> 4];
	        hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	protected String generateRandomString(int length) {
		
		Random random = new Random();
		
		StringBuilder result = new StringBuilder();
		for (int i = 0 ; i < length ; i++) {
			result.append(APHANUM_ARRAY[random.nextInt(APHANUM_ARRAY.length)]);
		}
		return result.toString();
	}
}
