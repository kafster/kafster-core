/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.kafka.factory.jexl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.MessageDigest;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;
import org.junit.jupiter.api.Test;

public class DigestTests extends TestSupport {
	
	private static final Object NAMESPACE = new DigestNamespace();

	@Test
	public void testMD2() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:md2(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("md2").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}
	
	@Test
	public void testMD5() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:md5(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("md5").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}	
	
	@Test
	public void testSHA1() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:sha1(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("sha1").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}	
	
	@Test
	public void testSHA224() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:sha224(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("sha-224").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}	
	
	@Test
	public void testSHA256() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:sha256(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("sha-256").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}
	
	@Test
	public void testSHA384() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:sha384(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("sha-384").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}	
	
	@Test
	public void testSHA512() throws Exception {
		
		String value = generateRandomString(256);
		
		JexlEngine engine = getJexl(NAMESPACE);
		JexlExpression expr = engine.createExpression("digest:sha512(string1)");
		
		MapContext context = new MapContext();
		context.set("string1", value);
		
		String expected = bytesToHex(MessageDigest.getInstance("sha-512").digest(value.getBytes()));
		assertEquals(expected, expr.evaluate(context));	
	}	
}
