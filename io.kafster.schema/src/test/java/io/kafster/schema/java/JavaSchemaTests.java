/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.Test;

import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.annotation.SchemaAccessType;
import io.kafster.schema.annotation.SchemaAccessorType;
import io.kafster.schema.util.SchemaUtils;

public class JavaSchemaTests {
	
	@SchemaAccessorType(SchemaAccessType.FIELDS)
	class SimpleBean {
		private String name;
		private String id;
	}

	@Test
	public void testSimpleSchema() throws Exception {
		
		SchemaType type = SchemaUtils.getSchema(SimpleBean.class);
		assertNotNull(type);
		
		Map<String,SchemaProperty> properties = type.getProperties();
		assertNotNull(properties);
		
		assertTrue(properties.containsKey("name"));
		assertTrue(properties.containsKey("id"));
	}
}

