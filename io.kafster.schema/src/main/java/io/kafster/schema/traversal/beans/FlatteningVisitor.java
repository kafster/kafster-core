/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.traversal.beans;

import javax.xml.namespace.QName;

import io.kafster.schema.MutableSchema;
import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;
import io.kafster.schema.simple.SimpleSchema;
import io.kafster.schema.traversal.SchemaVisitor;

public class FlatteningVisitor implements SchemaVisitor<Schema> {
	
	private MutableSchema schema;

	public FlatteningVisitor() {
		this(new SimpleSchema());
	}

	public FlatteningVisitor(MutableSchema schema) {
		this.schema = schema;;
	}
	
	@Override
	public Schema complete() {
		return schema;
	}

	@Override
	public SchemaVisitor<Schema> name(QName name) {
		return this;
	}

	@Override
	public SchemaVisitor<Schema> title(String title) {
		return this;
	}

	@Override
	public SchemaVisitor<Schema> description(String description) {
		return this;
	}

	@Override
	public TypeVisitor<Schema> primitive(SchemaType type) {
		if (type.getName() != null) {
			try {
				schema.addType(type.getName(), type);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	@Override
	public TypeVisitor<Schema> collection(SchemaType type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeVisitor<Schema> object(SchemaType type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeVisitor<Schema> reference(SchemaType type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeVisitor<Schema> enumeration(SchemaType type) {
		// TODO Auto-generated method stub
		return null;
	}


}
