/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.traversal;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaModel;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.traversal.SchemaVisitor.PropertyVisitor;
import io.kafster.schema.traversal.SchemaVisitor.TypeVisitor;

public class SchemaWalker {
	
	private static final int DEFAULT_MAX_RECURSION = 16;
	
	private int maxRecursion = DEFAULT_MAX_RECURSION;
	
	public SchemaWalker() {
	}
	
	public SchemaWalker(int maxRecursion) {
		this.maxRecursion = maxRecursion;
	}

	public <T> T walk(SchemaType model, SchemaVisitor<T> visitor) throws Exception {
		walk(model, visitor, new WalkerContext());
		return visitor.complete();
	}
	
	protected void walk(SchemaType type, SchemaVisitor<?> visitor, WalkerContext context) throws Exception {
		
		if (!type.isPrimitive() && context.visited(type.getName())) {
			return;
		}
		
		if (context.push() > maxRecursion) {
			throw new IllegalStateException("Maximum Recursion Depth [" + maxRecursion + "] exceeded");
		}
		
		if (type.isCollection()) {
			TypeVisitor<?> collection = visitor.collection(type);
			collection = collection(type, collection, context);
			collection.complete();
		}
		else if (type.isEnum()) {
			TypeVisitor<?> enumeration = visitor.enumeration(type);
			enumeration = enumeration(type, enumeration, context);
			enumeration.complete();
		}
		else if (type.isPrimitive()) {
			TypeVisitor<?> primitive = visitor.primitive(type);
			primitive = primitive(type, primitive, context);
			primitive.complete();
		}
		else if (type.isRef()) {
			TypeVisitor<?> reference = visitor.reference(type);
			reference = ref(type, reference, context);
			reference.complete();
		}
		else {
			TypeVisitor<?> object = visitor.object(type);
			object = object(type, object, context);
			object.complete();
		}
		
		context.pop();
	}
	
	protected TypeVisitor<?> collection(SchemaType type, TypeVisitor<?> visitor, WalkerContext context) throws Exception {
		base(type, visitor, context);
		
		SchemaType collectionType = type.getCollectionType();
		if (collectionType != null) {
			walk(collectionType, visitor, context);
		}
		
		return visitor;
	}
	
	protected TypeVisitor<?> enumeration(SchemaType type, TypeVisitor<?> visitor, WalkerContext context) throws Exception {
		base(type, visitor, context);
		return visitor;
	}
	
	protected TypeVisitor<?> primitive(SchemaType type, TypeVisitor<?> visitor, WalkerContext context) throws Exception {
		base(type, visitor, context);
		return visitor;
	}
	
	protected TypeVisitor<?> ref(SchemaType type, TypeVisitor<?> visitor, WalkerContext context) throws Exception {
		base(type, visitor, context);
		return visitor.reference(type);
	}
	
	protected TypeVisitor<?> object(SchemaType type, TypeVisitor<?> visitor, WalkerContext context) throws Exception {
		
		base(type, visitor, context);
		
		if (type.getMaxExclusive() != null) {
			visitor = visitor.maxExclusive(type.getMaxExclusive());
		}
		
		if (type.getMaxInclusive() != null) {
			visitor = visitor.maxInclusive(type.getMaxInclusive());
		}	
		
		if (type.getMinExclusive() != null) {
			visitor = visitor.minExclusive(type.getMinExclusive());
		}
		
		if (type.getMinInclusive() != null) {
			visitor = visitor.minInclusive(type.getMinInclusive());
		}
		
		Map<String, SchemaProperty> properties = type.getProperties();
		if (properties != null) {
			for (String name : properties.keySet()) {
				PropertyVisitor<?> child = visitor.property(name);
				property(properties.get(name), child, context);
				child.complete();
			}
		}
		
		return visitor;
	}
	
	protected void property(SchemaProperty property, PropertyVisitor<?> visitor, WalkerContext context) throws Exception {
		base(property, visitor, context);
		
		if (property.getMaxCardinality() != null) {
			visitor.maxOccurs(property.getMaxCardinality());
		}
		
		if (property.getMinCardinality() != null) {
			visitor.minOccurs(property.getMinCardinality());
		}
		
		SchemaType type = property.getType();
		if (type != null) {
			walk(type, visitor, context);
		}
	}
	
	protected void base(SchemaModel type, SchemaVisitor<?> visitor, WalkerContext context) throws Exception {
		
		if (type != null) {
			
			if (type.getName() != null) {
				visitor = visitor.name(type.getName());
			}
			
			if (type.getDescription() != null) {
				visitor = visitor.description(type.getDescription());
			}
			
			if (type.getTitle() != null) {
				visitor = visitor.title(type.getTitle());
			}
		}
	}	
	
	public void setMaxRecursion(int maxRecursion) {
		this.maxRecursion = maxRecursion;
	}
	
    protected class WalkerContext {
    	
    	private Set<QName> visited = new HashSet<>();
    	private int depth = 0;
    	
    	public boolean visited(QName name) {
    		return !visited.add(name);
    	}
    	
    	int push() {
    		return ++depth;
    	}
    	
    	int pop() {
    		return --depth;
    	}
    }	

}
