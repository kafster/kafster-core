/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.traversal.beans;

import javax.xml.namespace.QName;

import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaType;
import io.kafster.schema.traversal.SchemaVisitor;

public class CopyVisitor implements SchemaVisitor<SchemaType> {
	
	private MutableSchemaType type;
	
	public CopyVisitor(MutableSchemaType type) {
		this.type = type;
	}

	protected CopyVisitor() {
	}

	@Override
	public SchemaType complete() {
		return type;
	}
	
	@Override
	public SchemaVisitor<SchemaType> name(QName name) {
		type.setName(name);
		return this;
	}

	@Override
	public SchemaVisitor<SchemaType> title(String title) {
		type.setTitle(title);
		return this;
	}

	@Override
	public SchemaVisitor<SchemaType> description(String description) {
		type.setDescription(description);
		return this;
	}

	@Override
	public TypeVisitor<SchemaType> primitive(SchemaType type) {
		this.type.setPrimitive(true);
		this.type.setName(type.getName());
		return new InternalTypeVisitor();
	}

	@Override
	public TypeVisitor<SchemaType> collection(SchemaType type) {
		this.type.setCollection(true);
		return new InternalTypeVisitor();
	}

	@Override
	public TypeVisitor<SchemaType> object(SchemaType type) {
		this.type.setName(type.getName());
		return new InternalTypeVisitor();
	}

	@Override
	public TypeVisitor<SchemaType> reference(SchemaType type) {
		this.type.setRef(true);
		this.type.setReference(type.getReference());
		return new InternalTypeVisitor();
	}

	@Override
	public TypeVisitor<SchemaType> enumeration(SchemaType type) {
		this.type.setEnum(true);
		this.type.setEnumValues(type.getEnumValues());
		return new InternalTypeVisitor();
	}

	protected class InternalTypeVisitor extends CopyVisitor implements TypeVisitor<SchemaType> {

		@Override
		public TypeVisitor<SchemaType> format(String format) {
			type.setFormat(format);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> pattern(String pattern) {
			type.setPattern(pattern);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> maxLength(Integer maxLength) {
			type.setMaxLength(maxLength);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> minLength(Integer minLength) {
			type.setMinLength(minLength);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> maxExclusive(Number maxExclusive) {
			type.setMaxExclusive(maxExclusive);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> minExclusive(Number minExclusive) {
			type.setMinExclusive(minExclusive);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> maxInclusive(Number maxInclusive) {
			type.setMaxInclusive(maxInclusive);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> minInclusive(Number minInclusive) {
			type.setMinInclusive(minInclusive);
			return this;
		}

		@Override
		public TypeVisitor<SchemaType> multipleOf(Number multipleOf) {
			type.setMultipleOf(multipleOf);
			return this;
		}

		@Override
		public PropertyVisitor<SchemaType> property(String name) {
			MutableSchemaProperty property = type.addProperty(name);
			if (property != null) {
				return new InternalPropertyVisitor(property);
			}
			return null;
		}
	}
	
	protected class InternalPropertyVisitor extends CopyVisitor implements PropertyVisitor<SchemaType> {

		private MutableSchemaProperty property;
		private SchemaType type;
		
		protected InternalPropertyVisitor(MutableSchemaProperty property) {
			this.property = property;
		}

		@Override
		public TypeVisitor<SchemaType> primitive(SchemaType type) {
			// TODO Auto-generated method stub
			return super.primitive(type);
		}

		@Override
		public TypeVisitor<SchemaType> object(SchemaType type) {
			// TODO Auto-generated method stub
			return super.object(type);
		}

		@Override
		public TypeVisitor<SchemaType> reference(SchemaType type) {
			// TODO Auto-generated method stub
			return super.reference(type);
		}

		@Override
		public TypeVisitor<SchemaType> enumeration(SchemaType type) {
			// TODO Auto-generated method stub
			return super.enumeration(type);
		}
		
		@Override
		public PropertyVisitor<SchemaType> dependencies(String... dependencies) {
			// TODO Auto-generated method stub
			return this;
		}

		@Override
		public PropertyVisitor<SchemaType> maxOccurs(Long maxOccurs) {
			property.setMaxCardinality(maxOccurs);
			return this;
		}

		@Override
		public PropertyVisitor<SchemaType> minOccurs(Long minOccurs) {
			property.setMinCardinality(minOccurs);
			return this;
		}

		@Override
		public SchemaType complete() {
			return type;
		}

	}
}
