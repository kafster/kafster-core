/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.traversal.beans;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaType;
import io.kafster.schema.traversal.SchemaVisitor;

public class DelegatingVisitor<T> implements SchemaVisitor<T> {

	private SchemaVisitor<T> delegate;
	
	public DelegatingVisitor(SchemaVisitor<T> delegate) {
		this.delegate = delegate;
	}
	
	protected SchemaVisitor<T> getDelegate() {
		return delegate;
	}
	
	@Override
	public T complete() {
		return getDelegate().complete();
	}

	@Override
	public SchemaVisitor<T> name(QName name) {
		return getDelegate().name(name);
	}

	@Override
	public SchemaVisitor<T> title(String title) {
		return getDelegate().title(title);
	}

	@Override
	public SchemaVisitor<T> description(String description) {
		return getDelegate().description(description);
	}

	@Override
	public TypeVisitor<T> primitive(SchemaType type) {
		return getDelegate().primitive(type);
	}

	@Override
	public TypeVisitor<T> collection(SchemaType type) {
		return getDelegate().collection(type);
	}

	@Override
	public TypeVisitor<T> object(SchemaType type) {
		return getDelegate().object(type);
	}

	@Override
	public TypeVisitor<T> reference(SchemaType type) {
		return getDelegate().reference(type);
	}

	@Override
	public TypeVisitor<T> enumeration(SchemaType type) {
		return getDelegate().enumeration(type);
	}
	
	public class DelegatingTypeVisitor extends DelegatingVisitor<T> implements TypeVisitor<T> {

		public DelegatingTypeVisitor(SchemaVisitor<T> delegate) {
			super(delegate);
		}

		@Override
		public TypeVisitor<T> format(String format) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> pattern(String pattern) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> maxLength(Integer maxLength) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> minLength(Integer minLength) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> maxExclusive(Number maxExclusive) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> minExclusive(Number minExclusive) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> maxInclusive(Number maxInclusive) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> minInclusive(Number minInclusive) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public TypeVisitor<T> multipleOf(Number multipleOf) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PropertyVisitor<T> property(String name) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

}
