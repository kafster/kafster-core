/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.traversal;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaType;

public interface SchemaVisitor<T> {

	T complete();
	
	SchemaVisitor<T> name(QName name);
	SchemaVisitor<T> title(String title);
	SchemaVisitor<T> description(String description);
	
	TypeVisitor<T> primitive(SchemaType type);
	TypeVisitor<T> collection(SchemaType type);
	TypeVisitor<T> object(SchemaType type);
	TypeVisitor<T> reference(SchemaType type);
	TypeVisitor<T> enumeration(SchemaType type);
	
	interface TypeVisitor<T> extends SchemaVisitor<T> {
		
		TypeVisitor<T> format(String format);
		TypeVisitor<T> pattern(String pattern);
		TypeVisitor<T> maxLength(Integer maxLength);
		TypeVisitor<T> minLength(Integer minLength);
		TypeVisitor<T> maxExclusive(Number maxExclusive);
		TypeVisitor<T> minExclusive(Number minExclusive);
		TypeVisitor<T> maxInclusive(Number maxInclusive);
		TypeVisitor<T> minInclusive(Number minInclusive);
		TypeVisitor<T> multipleOf(Number multipleOf);	
		
		PropertyVisitor<T> property(String name);
	}
	
	interface PropertyVisitor<T> extends SchemaVisitor<T> {
		
		PropertyVisitor<T> dependencies(String... dependencies);
		PropertyVisitor<T> maxOccurs(Long maxOccurs);
		PropertyVisitor<T> minOccurs(Long minOccurs);		
	}
}

