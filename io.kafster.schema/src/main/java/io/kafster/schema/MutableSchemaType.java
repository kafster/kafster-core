/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema;

public interface MutableSchemaType extends MutableSchemaModel, SchemaType {

	void setDependencies(Iterable<Dependency> dependencies);
	void setEnumValues(Object[] values);
	void setFormat(String format);
	void setMaxExclusive(Number maxExclusive);
	void setMaxInclusive(Number maxInclusive);
	void setMaxLength(Integer length);
	void setMinExclusive(Number minExclusive);
	void setMinInclusive(Number minInclusive);
	void setMultipleOf(Number multipleOf);
	void setMinLength(Integer minLength);
	void setPattern(String pattern);
	void setReference(String ref);
	
	void setAny(boolean isAny);
	void setCollection(boolean isCollection);
	void setEnum(boolean isEnum);
	void setPrimitive(boolean isPrimitive);
	void setRef(boolean isRef);
	
	MutableSchemaProperty addProperty(String name);
}
