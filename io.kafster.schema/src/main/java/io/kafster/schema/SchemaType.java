/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema;

import java.util.Map;

public interface SchemaType extends SchemaModel {
	
	boolean isCollection();	
	boolean isPrimitive();
	boolean isAny();
	boolean isRef();
	boolean isEnum();
	
	Object[] getEnumValues();	
	SchemaType getCollectionType();
	
	String getReference();
	
	Number getMaxInclusive();
	Number getMaxExclusive();
	Number getMinInclusive();
	Number getMinExclusive();
	Number getMultipleOf();

	Integer getMinLength();
	Integer getMaxLength();	
	String getPattern();
	String getFormat();

	Map<String,SchemaProperty> getProperties();
	
	Iterable<Dependency> getDependencies();	
}
