/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.model;

import javax.xml.namespace.QName;

import org.apache.commons.lang3.ArrayUtils;

import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.SchemaType;

public class JSONSchemaProperty extends JSONSchemaModel implements MutableSchemaProperty {
	
	private String name;
	private JSONSchemaType parent;
	
	private Long maxCardinality = Long.MAX_VALUE;

	public JSONSchemaProperty(String name, JSONSchema model, JSONSchemaType parent) {
		super(model);
		this.name = name;
		this.parent = parent;
	}

	@Override
	public SchemaType getType() {
		return new JSONSchemaType(getModel());
	}

	@Override
	public Long getMaxCardinality() {
		return maxCardinality;
	}

	@Override
	public Long getMinCardinality() {
		return ArrayUtils.contains(parent.getModel().getRequired(), name) ? 1L : 0L;
	}

	@Override
	public void setMaxCardinality(Long maxCardinality) {
		this.maxCardinality = maxCardinality;
	}

	@Override
	public void setMinCardinality(Long minCardinality) {
		
		parent.getModel().setRequired(ArrayUtils.removeElement(getModel().getRequired(), name));
		
		if (minCardinality > 0) {
			parent.getModel().setRequired(ArrayUtils.add(getModel().getRequired(), name));
		}
	}

	@Override
	public void setType(SchemaType type) {
		
		try {
			JSONSchemaType json = JSONSchemaConverter.convert(type);
			if (json != null)  {
				setModel(json.getModel());
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public QName getName() {
		return new QName(name);
	}

}
