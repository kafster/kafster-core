/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Feature;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

public class JSONSchema {

    private String $id;
    private String $schema;
    private String $ref;
    private String $comment;
    private String title;
    private String description;
    private Boolean readOnly;
    private Object[] examples;
    private Number multipleOf;
    private Number maximum;
    private Number exclusiveMaximum;
    private Number minimum;
    private Number exclusiveMinimum;
    private Integer maxLength;
    private Integer minLength;
    private String pattern;
    private JSONSchema additionalItems;
    
    @JsonFormat(with={Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED})
    private JSONSchema[] items;
    private Integer maxItems;
    private Integer minItems;
    private Boolean uniqueItems;
    private JSONSchema contains;
    private Integer maxProperties;
    private Integer minProperties;
    private String[] required;
    private JSONSchema additionalProperties;
    private Map<String,JSONSchema> definitions;
    private Map<String,JSONSchema> properties;
    private Map<String,JSONSchema> patternProperties;
    private List<String> dependencies;
    
    @JsonProperty("default")
    private JsonNode _default;
    
    @JsonProperty("enum")
    private List<String> _enum;
    
    @JsonProperty("const")
    private Object _const;
    
    @JsonFormat(with={Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY,Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED})
    private String[] type;
    
    private String format;
    private String contentMediaType;
    private String contentEncoding;
    private List<JSONSchema> allOf;
    private List<JSONSchema> anyOf;
    private List<JSONSchema> oneOf;
    private JSONSchema not;
    
    @JsonProperty("if")
    private JSONSchema _if;
    
    @JsonProperty("then")
    private JSONSchema _then;
    
    @JsonProperty("else")
    private JSONSchema _else;
    
    public JSONSchema() {
    }

    public JSONSchema(JSONSchema other) {
        this.$id = other.$id;
        this.$schema = other.$schema;
        this.$ref = other.$ref;
        this.$comment = other.$comment;
        this.title = other.title;
        this.description = other.description;
        this.readOnly = other.readOnly;
        this.examples = other.examples;
        this.multipleOf = other.multipleOf;
        this.maximum = other.maximum;
        this.exclusiveMaximum = other.exclusiveMaximum;
        this.minimum = other.minimum;
        this.exclusiveMinimum = other.exclusiveMinimum;
        this.maxLength = other.maxLength;
        this.minLength = other.minLength;
        this.pattern = other.pattern;
        this.additionalItems = other.additionalItems;
        this.items = other.items;
        this.maxItems = other.maxItems;
        this.minItems = other.minItems;
        this.uniqueItems = other.uniqueItems;
        this.contains = other.contains;
        this.maxProperties = other.maxProperties;
        this.minProperties = other.minProperties;
        this.required = other.required;
        this.additionalProperties = other.additionalProperties;
        this.definitions = (other.definitions == null) ? null : new HashMap<>(other.definitions);
        this.properties = (other.properties == null) ? null : new HashMap<>(other.properties);
        this.patternProperties = (other.patternProperties == null) ? null : new HashMap<>(other.patternProperties);
        this.dependencies = (other.dependencies == null) ? null : new ArrayList<>(other.dependencies);
        this._default = other._default;
        this._enum = new ArrayList<>(other._enum);
        this._const = other._const;
        this.type = other.type;
        this.format = other.format;
        this.contentMediaType = other.contentMediaType;
        this.contentEncoding = other.contentEncoding;
        this.allOf = (other.allOf == null) ? null : new ArrayList<>(other.allOf);
        this.anyOf = (other.anyOf == null) ? null : new ArrayList<>(other.anyOf);
        this.oneOf = (other.oneOf == null) ? null : new ArrayList<>(other.oneOf);
        this.not = other.not;
        this._if = other._if;
        this._then = other._then;
        this._else = other._else;
    }
    
	public String get$id() {
		return $id;
	}

	public void set$id(String $id) {
		this.$id = $id;
	}

	public String get$schema() {
		return $schema;
	}

	public void set$schema(String $schema) {
		this.$schema = $schema;
	}

	public String get$ref() {
		return $ref;
	}

	public void set$ref(String $ref) {
		this.$ref = $ref;
	}

	public String get$comment() {
		return $comment;
	}

	public void set$comment(String $comment) {
		this.$comment = $comment;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Object[] getExamples() {
		return examples;
	}

	public void setExamples(Object[] examples) {
		this.examples = examples;
	}

	public Number getMultipleOf() {
		return multipleOf;
	}

	public void setMultipleOf(Number multipleOf) {
		this.multipleOf = multipleOf;
	}

	public Number getMaximum() {
		return maximum;
	}

	public void setMaximum(Number maximum) {
		this.maximum = maximum;
	}

	public Number getExclusiveMaximum() {
		return exclusiveMaximum;
	}

	public void setExclusiveMaximum(Number exclusiveMaximum) {
		this.exclusiveMaximum = exclusiveMaximum;
	}

	public Number getMinimum() {
		return minimum;
	}

	public void setMinimum(Number minimum) {
		this.minimum = minimum;
	}

	public Number getExclusiveMinimum() {
		return exclusiveMinimum;
	}

	public void setExclusiveMinimum(Number exclusiveMinimum) {
		this.exclusiveMinimum = exclusiveMinimum;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public Integer getMinLength() {
		return minLength;
	}

	public void setMinLength(Integer minLength) {
		this.minLength = minLength;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public JSONSchema getAdditionalItems() {
		return additionalItems;
	}

	public void setAdditionalItems(JSONSchema additionalItems) {
		this.additionalItems = additionalItems;
	}

	public JSONSchema[] getItems() {
		return items;
	}

	public void setItems(JSONSchema[] items) {
		this.items = items;
	}

	public Integer getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(Integer maxItems) {
		this.maxItems = maxItems;
	}

	public Integer getMinItems() {
		return minItems;
	}

	public void setMinItems(Integer minItems) {
		this.minItems = minItems;
	}

	public Boolean getUniqueItems() {
		return uniqueItems;
	}

	public void setUniqueItems(Boolean uniqueItems) {
		this.uniqueItems = uniqueItems;
	}

	public JSONSchema getContains() {
		return contains;
	}

	public void setContains(JSONSchema contains) {
		this.contains = contains;
	}

	public Integer getMaxProperties() {
		return maxProperties;
	}

	public void setMaxProperties(Integer maxProperties) {
		this.maxProperties = maxProperties;
	}

	public Integer getMinProperties() {
		return minProperties;
	}

	public void setMinProperties(Integer minProperties) {
		this.minProperties = minProperties;
	}

	public String[] getRequired() {
		return required;
	}

	public void setRequired(String[] required) {
		this.required = required;
	}

	public JSONSchema getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(JSONSchema additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public Map<String, JSONSchema> getDefinitions() {
		return definitions;
	}

	public void setDefinitions(Map<String, JSONSchema> definitions) {
		this.definitions = definitions;
	}

	public Map<String, JSONSchema> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, JSONSchema> properties) {
		this.properties = properties;
	}

	public Map<String, JSONSchema> getPatternProperties() {
		return patternProperties;
	}

	public void setPatternProperties(Map<String, JSONSchema> patternProperties) {
		this.patternProperties = patternProperties;
	}

	public List<String> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<String> dependencies) {
		this.dependencies = dependencies;
	}

	public JsonNode getDefault() {
		return _default;
	}

	public void setDefault(JsonNode _default) {
		this._default = _default;
	}

	public List<String> getEnum() {
		return _enum;
	}

	public void setEnum(List<String> _enum) {
		this._enum = _enum;
	}

	public Object getConst() {
		return _const;
	}

	public void setConst(Object _const) {
		this._const = _const;
	}

	public String[] getType() {
		return type;
	}

	public void setType(String[] type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getContentMediaType() {
		return contentMediaType;
	}

	public void setContentMediaType(String contentMediaType) {
		this.contentMediaType = contentMediaType;
	}

	public String getContentEncoding() {
		return contentEncoding;
	}

	public void setContentEncoding(String contentEncoding) {
		this.contentEncoding = contentEncoding;
	}

	public List<JSONSchema> getAllOf() {
		return allOf;
	}

	public void setAllOf(List<JSONSchema> allOf) {
		this.allOf = allOf;
	}

	public List<JSONSchema> getAnyOf() {
		return anyOf;
	}

	public void setAnyOf(List<JSONSchema> anyOf) {
		this.anyOf = anyOf;
	}

	public List<JSONSchema> getOneOf() {
		return oneOf;
	}

	public void setOneOf(List<JSONSchema> oneOf) {
		this.oneOf = oneOf;
	}

	public JSONSchema getNot() {
		return not;
	}

	public void setNot(JSONSchema not) {
		this.not = not;
	}

	public JSONSchema getIf() {
		return _if;
	}

	public void setIf(JSONSchema _if) {
		this._if = _if;
	}

	public JSONSchema getThen() {
		return _then;
	}

	public void setThen(JSONSchema _then) {
		this._then = _then;
	}

	public JSONSchema getElse() {
		return _else;
	}

	public void setElse(JSONSchema _else) {
		this._else = _else;
	}
}
