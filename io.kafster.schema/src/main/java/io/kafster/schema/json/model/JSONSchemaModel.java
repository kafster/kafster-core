/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.model;

import javax.xml.namespace.QName;

import io.kafster.schema.MutableSchemaModel;

public class JSONSchemaModel implements MutableSchemaModel {
	
	public static final String ARRAY_TYPE = "array";
	public static final String OBJECT_TYPE = "object";
	
	private JSONSchema model;
	
	protected JSONSchemaModel(JSONSchema model) {
		this.model = model;
	}
	
	public JSONSchema getModel() {
		return model;
	}
	
	protected void setModel(JSONSchema model) {
		this.model = model;
	}

	@Override
	public QName getName() {
		String[] types = model.getType();
		if ((types != null) && (types.length > 0)) {
			return new QName(types[0]);
		}
		return null;
	}

	@Override
	public String getTitle() {
		return model.getTitle();
	}

	@Override
	public String getDescription() {
		return model.getDescription();
	}

	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	@Override
	public void setName(QName name) {
		model.setType(new String[] { name.getLocalPart() });
	}

	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

}
