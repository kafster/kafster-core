/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json;

import java.io.IOException;
import java.io.Writer;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.schema.SchemaType;
import io.kafster.schema.io.SchemaTypeWriter;
import io.kafster.schema.json.model.JSONSchemaConverter;
import io.kafster.schema.json.model.JSONSchemaType;

public class JSONSchemaWriter implements SchemaTypeWriter {
	
	private ObjectMapper mapper;

	@Override
	public void write(SchemaType type, Writer writer) throws IOException {
		try {
			write(JSONSchemaConverter.convert(type), writer);
		}
		catch (IOException e) {
			throw e;
		}
		catch (Exception e) {
			throw new IOException(e);
		}
	}

	protected void write(JSONSchemaType type, Writer writer) throws IOException {
		
		if (type.getModel() != null) {
			mapper.writeValue(writer, type.getModel());
		}
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}
}
