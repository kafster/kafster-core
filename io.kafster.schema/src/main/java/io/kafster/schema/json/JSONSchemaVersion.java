/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json;

public enum JSONSchemaVersion {

	DRAFT03("http://json-schema.org/draft-03/schema#"),
	DRAFT04("http://json-schema.org/draft-04/schema#"),
	DRAFT06("http://json-schema.org/draft-06/schema#"),
	DRAFT07("http://json-schema.org/draft-07/schema#");
	
	private String uri;
	
	JSONSchemaVersion(String uri) {
		this.uri = uri;
	}
	
	public String getUri() {
		return uri;
	}
	
	public String toString() {
		return uri;
	}
}
