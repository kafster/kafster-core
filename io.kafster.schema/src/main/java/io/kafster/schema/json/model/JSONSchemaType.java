/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import io.kafster.schema.Dependency;
import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.json.JSONSchemaPrimitiveTypes;

public class JSONSchemaType extends JSONSchemaModel implements MutableSchemaType {
	
	public JSONSchemaType() {
		super(new JSONSchema());
	}

	public JSONSchemaType(JSONSchema model) {
		super(model);
	}
	
	public JSONSchemaType(JSONSchemaType model) {
		super(new JSONSchema(model.getModel()));
	}

	@Override
	public boolean isCollection() {
		return ArrayUtils.contains(getModel().getType(), ARRAY_TYPE);
	}

	@Override
	public boolean isPrimitive() {
		return JSONSchemaPrimitiveTypes.ALL.contains(this);
	}

	@Override
	public boolean isAny() {
		return ArrayUtils.contains(getModel().getType(), OBJECT_TYPE);
	}

	@Override
	public boolean isRef() {
		return getModel().get$ref() != null;
	}

	@Override
	public boolean isEnum() {
		List<String> options = getModel().getEnum();
		return (options != null) && !options.isEmpty();
	}

	@Override
	public Object[] getEnumValues() {
		List<String> options = getModel().getEnum();
		if (options != null) {
			return options.toArray(new String[options.size()]);
		}
		return null;
	}

	@Override
	public SchemaType getCollectionType() {
		JSONSchema[] items = getModel().getItems();
		if ((items != null) && (items.length > 0)) {
			return new JSONSchemaType(items[0]);
		}
		return null;
	}

	@Override
	public String getReference() {
		return getModel().get$ref();
	}

	@Override
	public Number getMaxInclusive() {
		return getModel().getMaximum();
	}

	@Override
	public Number getMaxExclusive() {
		return getModel().getExclusiveMaximum();
	}

	@Override
	public Number getMinInclusive() {
		return getModel().getMinimum();
	}

	@Override
	public Number getMinExclusive() {
		return getModel().getExclusiveMinimum();
	}

	@Override
	public Integer getMinLength() {
		return getModel().getMinLength();
	}

	@Override
	public Integer getMaxLength() {
		return getModel().getMaxLength();
	}

	@Override
	public String getPattern() {
		return getModel().getPattern();
	}

	@Override
	public String getFormat() {
		return getModel().getFormat();
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		
		Map<String,JSONSchema> properties = getModel().getProperties();
		if (properties != null) {
			Map<String,SchemaProperty> result = new HashMap<>();
			properties.forEach((k,v) -> result.put(k, new JSONSchemaProperty(k, properties.get(k), this)));
			return result;
		}
		return null;
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDependencies(Iterable<Dependency> dependdencies) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnumValues(Object[] values) {
		
		if (values == null) {
			getModel().setEnum(null);
		}
		else {
			getModel().setEnum(Arrays.stream(values).map(v -> String.valueOf(v)).collect(Collectors.toList()));
		}
		
	}

	@Override
	public void setFormat(String format) {
		getModel().setFormat(format);
	}

	@Override
	public void setMaxExclusive(Number maxExclusive) {
		getModel().setExclusiveMaximum(maxExclusive);
	}

	@Override
	public void setMaxInclusive(Number maxInclusive) {
		getModel().setMaximum(maxInclusive);
	}

	@Override
	public void setMaxLength(Integer length) {
		getModel().setMaxLength(length);
	}

	@Override
	public void setMinExclusive(Number minExclusive) {
		getModel().setExclusiveMinimum(minExclusive);
	}

	@Override
	public void setMinInclusive(Number minInclusive) {
		getModel().setMinimum(minInclusive);
	}

	@Override
	public void setMinLength(Integer minLength) {
		getModel().setMinLength(minLength);
	}

	@Override
	public void setPattern(String pattern) {
		getModel().setPattern(pattern);
	}

	@Override
	public void setReference(String ref) {
		getModel().set$ref(ref);
	}

	@Override
	public void setAny(boolean isAny) {
		
		getModel().setType(ArrayUtils.removeElement(getModel().getType(), OBJECT_TYPE));
		
		if (isAny) {
			getModel().setType(ArrayUtils.add(getModel().getType(), OBJECT_TYPE));
		}		
	}

	@Override
	public void setCollection(boolean isCollection) {
		
		getModel().setType(ArrayUtils.removeElement(getModel().getType(), ARRAY_TYPE));
		
		if (isCollection) {
			getModel().setType(ArrayUtils.add(getModel().getType(), ARRAY_TYPE));
		}
	}

	@Override
	public void setEnum(boolean isEnum) {
		if (isEnum) {
			getModel().setEnum(Collections.emptyList());
		}
		else {
			getModel().setEnum(null);
		}
	}

	@Override
	public void setPrimitive(boolean isPrimitive) {
	}

	@Override
	public void setRef(boolean isRef) {
	}

	@Override
	public Number getMultipleOf() {
		return getModel().getMultipleOf();
	}

	@Override
	public void setMultipleOf(Number multipleOf) {
		getModel().setMultipleOf(multipleOf);
	}

	@Override
	public MutableSchemaProperty addProperty(String name) {
		
		Map<String,JSONSchema> properties = getModel().getProperties();
		if (properties == null) {
			properties = new HashMap<>();
			getModel().setProperties(properties);
		}
		
		JSONSchemaProperty result = new JSONSchemaProperty(name, new JSONSchema(), this);
		properties.put(name, result.getModel());
		
		return result;
	}
	
	public static JSONSchemaType derivedFrom(SchemaType base) {
		
		if (base instanceof JSONSchemaType) {
			return new JSONSchemaType((JSONSchemaType)base);
		}
		return null;
	}

}
