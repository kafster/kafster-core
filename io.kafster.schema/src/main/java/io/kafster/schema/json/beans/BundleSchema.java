/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.beans;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;
import io.kafster.schema.io.SchemaTypeReader;

public class BundleSchema implements Schema, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(BundleSchema.class);
	
	private List<String> locations;
	private boolean recurse = false;
	private String namespace;
	private SchemaTypeReader reader;
	
	private Map<QName,SchemaType> types;
	
	private BundleContext ctx;

	@Override
	public Optional<SchemaType> getType(QName name) throws Exception {
		return Optional.ofNullable(getModels().get(name));
	}

	@Override
	public Iterable<? extends SchemaType> getTypes() throws Exception {
		return getModels().values();
	}
	
	protected synchronized Map<QName,SchemaType> getModels() {
		if (types == null) {
			types = loadTypes();
		}
		return types;
	}

	protected Map<QName,SchemaType> loadTypes() {
		Map<QName,SchemaType> result = new HashMap<>();
		
		for (String location : locations) {
			URL url = ctx.getBundle().getEntry(location);
			if (url != null) {
				result.putAll(loadTypes(url));
			}
			else {
				Enumeration<URL> urls = ctx.getBundle().findEntries(location, "*.json", recurse);
				if (urls != null) {
					while (urls.hasMoreElements()) {
						result.putAll(loadTypes(urls.nextElement()));
					}
				}
			}
		}
		
		return result;
	}
	
	protected Map<QName,SchemaType> loadTypes(URL resource) {
		
		Map<QName,SchemaType> result = new HashMap<>();
		
		try {
			
			SchemaType type = reader.read(new InputStreamReader(resource.openStream()));
			if (type != null) {
				QName name = type.getName();
				if (namespace != null) {
					name = new QName(namespace, name.getLocalPart());
				}
				
				result.put(name, type);
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return result;
	}

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}

	public void setLocations(String locations) {
		this.locations = Arrays.asList(StringUtils.split(locations, ':'));
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setReader(SchemaTypeReader reader) {
		this.reader = reader;
	}
}


