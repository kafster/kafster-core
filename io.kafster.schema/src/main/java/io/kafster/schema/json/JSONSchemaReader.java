/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json;

import java.io.IOException;
import java.io.Reader;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.schema.SchemaType;
import io.kafster.schema.io.SchemaTypeReader;
import io.kafster.schema.json.model.JSONSchema;
import io.kafster.schema.json.model.JSONSchemaType;

public class JSONSchemaReader implements SchemaTypeReader {
	
	private ObjectMapper mapper;

	@Override
	public SchemaType read(Reader reader) throws IOException {
		JSONSchema schema = mapper.readValue(reader, JSONSchema.class);
		if (schema != null) {
			return new JSONSchemaType(schema);
		}
		return null;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

}
