/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.model;

import io.kafster.schema.SchemaType;
import io.kafster.schema.traversal.SchemaWalker;
import io.kafster.schema.traversal.beans.CopyVisitor;

public class JSONSchemaConverter {
	
	private static SchemaWalker walker = new SchemaWalker();

	public static JSONSchemaType convert(SchemaType type) throws Exception {
		
		if (type instanceof JSONSchemaType) {
			return (JSONSchemaType)type;
		}
		
		JSONSchemaType result = new JSONSchemaType(new JSONSchema());
		walker.walk(type, new CopyVisitor(result));
		return result;
	}
}
