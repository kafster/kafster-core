/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json.jackson;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import io.kafster.schema.SchemaType;
import io.kafster.schema.json.model.JSONSchemaConverter;
import io.kafster.schema.json.model.JSONSchemaType;

public class JSONSchemaSerializer extends JsonSerializer<SchemaType> {
	
	private static final Logger log = LoggerFactory.getLogger(JSONSchemaSerializer.class);

	@Override
	public void serialize(SchemaType type, JsonGenerator generator, SerializerProvider provider) throws IOException {
		
		try {
			JSONSchemaType json = JSONSchemaConverter.convert(type);
			if (json != null) {
				generator.writeObject(json.getModel());
			}
		}
		catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
			throw e;
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new IOException(e);
		}
	}

}
