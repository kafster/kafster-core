/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.json;

import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaType;
import io.kafster.schema.json.model.JSONSchema;
import io.kafster.schema.json.model.JSONSchemaType;

public interface JSONSchemaPrimitiveTypes {

	SchemaType STRING = new JSONSchemaPrimitiveType("string");
	SchemaType NUMBER = new JSONSchemaPrimitiveType("number");
	SchemaType INTEGER = new JSONSchemaPrimitiveType("integer");
	SchemaType OBJECT = new JSONSchemaPrimitiveType("object");
	SchemaType ARRAY = new JSONSchemaPrimitiveType("array");
	SchemaType BOOLEAN = new JSONSchemaPrimitiveType("boolean");
	SchemaType NULL = new JSONSchemaPrimitiveType("null");
	
	SchemaType DATE_TIME = new JSONSchemaPrimitiveType("string", "date-time");
	SchemaType TIME = new JSONSchemaPrimitiveType("string", "time");
	SchemaType DATE = new JSONSchemaPrimitiveType("string", "date");
	SchemaType DURATION = new JSONSchemaPrimitiveType("string", "duration");
	
	SchemaType EMAIL = new JSONSchemaPrimitiveType("string", "email");
	SchemaType IDN_EMAIL = new JSONSchemaPrimitiveType("string", "idn-email");
	
	SchemaType HOSTNAME= new JSONSchemaPrimitiveType("string", "hostname");
	SchemaType IDN_HOSTNAME = new JSONSchemaPrimitiveType("string", "idn-hostname");	
	
	SchemaType IPv4 = new JSONSchemaPrimitiveType("string", "ipv4");
	SchemaType IPv6 = new JSONSchemaPrimitiveType("string", "ipv6");		
	
	SchemaType UUID = new JSONSchemaPrimitiveType("string", "uuid");
	SchemaType URI = new JSONSchemaPrimitiveType("string", "uri");
	SchemaType URI_REFERENCE = new JSONSchemaPrimitiveType("string", "uri-reference");
	SchemaType IRI = new JSONSchemaPrimitiveType("string", "iri");
	SchemaType IRI_REFERENCE = new JSONSchemaPrimitiveType("string", "iri-reference");
	
	SchemaType URI_TEMPLATE = new JSONSchemaPrimitiveType("string", "uri-template");
	SchemaType JSON_POINTER = new JSONSchemaPrimitiveType("string", "json-pointer");
	SchemaType RELATIVE_JSON_POINTER = new JSONSchemaPrimitiveType("string", "relative-json-pointer");
	
	SchemaType REGEX = new JSONSchemaPrimitiveType("string", "regex");
	
	@SuppressWarnings("serial")
	Set<SchemaType> ALL = new HashSet<SchemaType>() {{
		add(ARRAY);
		add(BOOLEAN);
		add(DATE);
		add(DATE_TIME);
		add(DURATION);
		add(EMAIL);
		add(HOSTNAME);
		add(IDN_EMAIL);
		add(IDN_HOSTNAME);
		add(INTEGER);
		add(IPv4);
		add(IPv6);
		add(IRI);
		add(IRI_REFERENCE);
		add(JSON_POINTER);
		add(NULL);
		add(NUMBER);
		add(OBJECT);
		add(REGEX);
		add(RELATIVE_JSON_POINTER);
		add(STRING);
		add(TIME);
		add(URI);
		add(URI_REFERENCE);
		add(URI_TEMPLATE);
		add(UUID);
	}};

	class JSONSchemaPrimitiveType extends JSONSchemaType {
		
		JSONSchemaPrimitiveType(String name) {
			super(new JSONSchema());
			setName(new QName(name));
		}
		
		JSONSchemaPrimitiveType(String name, String format) {
			super(new JSONSchema());
			setName(new QName(name));
			setFormat(format);
		}
	}
}
