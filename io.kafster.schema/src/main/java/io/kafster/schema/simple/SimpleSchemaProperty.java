/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.simple;

import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.SchemaType;

public class SimpleSchemaProperty extends SimpleModel implements MutableSchemaProperty {
	
	private SchemaType type;
	private Long maxCardinality;
	private Long minCardinality;

	@Override
	public SchemaType getType() {
		return type;
	}

	@Override
	public Long getMaxCardinality() {
		return maxCardinality;
	}

	@Override
	public Long getMinCardinality() {
		return minCardinality;
	}

	public void setType(SchemaType type) {
		this.type = type;
	}

	public void setMaxCardinality(Long maxCardinality) {
		this.maxCardinality = maxCardinality;
	}

	public void setMinCardinality(Long minCardinality) {
		this.minCardinality = minCardinality;
	}


}
