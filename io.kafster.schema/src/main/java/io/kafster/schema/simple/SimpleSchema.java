/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.simple;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;

import io.kafster.schema.MutableSchema;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaType;

public class SimpleSchema implements MutableSchema {
	
	private Map<QName,SchemaType> types;
	
	public SimpleSchema() {
	}

	public SimpleSchema(SchemaType... types) {
		for (SchemaType type : types) {
			addType(type);
		}
	}
	
	@Override
	public Optional<SchemaType> getType(QName name) throws Exception {
		return Optional.ofNullable(types.get(name));
	}

	@Override
	public Iterable<? extends SchemaType> getTypes() throws Exception {
		return types.values();
	}
	
	public void setTypes(Map<QName,SchemaType> types) {
		this.types = types;
	}
	
	public void addType(SchemaType type) {
		if (types == null) { 
			types = new HashMap<>();
		}
		types.put(type.getName(), type);
	}

	@Override
	public MutableSchemaType addType(QName qname) throws Exception {
		MutableSchemaType result = new SimpleSchemaType();
		result.setName(qname);
		
		types.put(qname, result);
		return result;
	}

	@Override
	public SchemaType addType(QName qname, SchemaType type) throws Exception {
		types.put(qname, type);
		return type;
	}

}
