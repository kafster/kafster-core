/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.simple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.kafster.schema.Dependency;
import io.kafster.schema.MutableSchemaProperty;
import io.kafster.schema.MutableSchemaType;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;

public class SimpleSchemaType extends SimpleModel implements MutableSchemaType {
	
	private String ref;
	private boolean isCollection;
	private boolean isPrimitive;
	private boolean isAny;
	private boolean isEnum;
	private boolean isRef;
	private SchemaType collectionType;
	private Object[] enumValues;
	private Number maxInclusive;
	private Number maxExclusive;
	private Number minInclusive;
	private Number minExclusive;
	private Number multipleOf;
	private Integer minLength;
	private Integer maxLength;
	private String pattern;
	private String format;
	private Iterable<Dependency> dependencies;
	private Map<String, SchemaProperty> properties;

	@Override
	public boolean isCollection() {
		return isCollection;
	}

	@Override
	public boolean isPrimitive() {
		return isPrimitive;
	}

	@Override
	public boolean isAny() {
		return isAny;
	}

	@Override
	public boolean isEnum() {
		return isEnum;
	}

	@Override
	public Object[] getEnumValues() {
		return enumValues;
	}

	@Override
	public Number getMaxInclusive() {
		return maxInclusive;
	}

	@Override
	public Number getMaxExclusive() {
		return maxExclusive;
	}

	@Override
	public Number getMinInclusive() {
		return minInclusive;
	}

	@Override
	public Number getMinExclusive() {
		return minExclusive;
	}

	@Override
	public Integer getMinLength() {
		return minLength;
	}

	@Override
	public Integer getMaxLength() {
		return maxLength;
	}

	@Override
	public String getPattern() {
		return pattern;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		return properties;
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		return dependencies;
	}

	@Override
	public boolean isRef() {
		return isRef;
	}

	@Override
	public String getReference() {
		return ref;
	}

	@Override
	public SchemaType getCollectionType() {
		return collectionType;
	}

	public void setReference(String ref) {
		this.ref = ref;
	}

	public void setCollection(boolean isCollection) {
		this.isCollection = isCollection;
	}

	public void setPrimitive(boolean isPrimitive) {
		this.isPrimitive = isPrimitive;
	}

	public void setAny(boolean isAny) {
		this.isAny = isAny;
	}

	public void setEnum(boolean isEnum) {
		this.isEnum = isEnum;
	}

	public void setRef(boolean isRef) {
		this.isRef = isRef;
	}

	public void setCollectionType(SchemaType collectionType) {
		this.collectionType = collectionType;
	}

	public void setEnumValues(Object[] enumValues) {
		this.enumValues = enumValues;
	}

	public void setMaxInclusive(Number maxInclusive) {
		this.maxInclusive = maxInclusive;
	}

	public void setMaxExclusive(Number maxExclusive) {
		this.maxExclusive = maxExclusive;
	}

	public void setMinInclusive(Number minInclusive) {
		this.minInclusive = minInclusive;
	}

	public void setMinExclusive(Number minExclusive) {
		this.minExclusive = minExclusive;
	}

	public void setMinLength(Integer minLength) {
		this.minLength = minLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public void setProperties(Map<String, SchemaProperty> properties) {
		this.properties = properties;
	}

	@Override
	public void setDependencies(Iterable<Dependency> dependencies) {
		this.dependencies = dependencies;		
	}

	@Override
	public MutableSchemaProperty addProperty(String name) {
		
		if (properties == null) {
			properties = new HashMap<>();
		}
		
		MutableSchemaProperty result = new SimpleSchemaProperty();
		properties.put(name, result);
		
		return result;
	}

	@Override
	public Number getMultipleOf() {
		return multipleOf;
	}

	@Override
	public void setMultipleOf(Number multipleOf) {
		this.multipleOf = multipleOf;
	}

}
