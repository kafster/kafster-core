/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.util;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;

import io.kafster.schema.annotation.SchemaName;

public class QNameUtils {
	
	private static final String DEFAULT = "##default";

	public static QName forClass(Class<?> clazz) {
		
		String name = clazz.getSimpleName();
		String namespace = clazz.getPackage().getName();
		
		XmlSchema annotation = clazz.getPackage().getAnnotation(XmlSchema.class);
		if ((annotation != null) && (annotation.namespace() != null)) {
			namespace = annotation.namespace();
		}		
		
		XmlRootElement element = clazz.getAnnotation(XmlRootElement.class);
		if (element != null) {
			
			if (!StringUtils.isBlank(element.name()) && !StringUtils.equals(DEFAULT, element.name())) {
				name = element.name();
			}
			
			if (!StringUtils.isBlank(element.namespace()) && !StringUtils.equals(DEFAULT, element.namespace())) {
				namespace = element.namespace();
			}
		}	
		
		XmlType type = clazz.getAnnotation(XmlType.class);
		if (type != null) {
			
			if (!StringUtils.isBlank(type.name()) && !StringUtils.equals(DEFAULT, type.name())) {
				name = type.name();
			}
			
			if (!StringUtils.isBlank(type.namespace()) && !StringUtils.equals(DEFAULT, type.namespace())) {
				namespace = type.namespace();
			}
		}
		
		SchemaName schemaName = clazz.getAnnotation(SchemaName.class);
		if (schemaName != null) {
			if (!StringUtils.isEmpty(schemaName.name())) {
				name = schemaName.name();
			}
			if (!StringUtils.isEmpty(schemaName.namespace())) {
				namespace = schemaName.namespace();
			}
		}
		
		return new QName(namespace, name);
	}
}
