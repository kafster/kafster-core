/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema;

import java.net.URI;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import io.kafster.schema.simple.SimpleSchemaType;

public interface PrimitiveTypes {
	
	SchemaType ANY = new PrimitiveType(Object.class);
	SchemaType BOOLEAN = new PrimitiveType(Boolean.class);
	SchemaType BYTE = new PrimitiveType(Byte.class);
	SchemaType CHAR = new PrimitiveType(Character.class);
	SchemaType SHORT = new PrimitiveType(Short.class);
	SchemaType INTEGER = new PrimitiveType(Integer.class);
	SchemaType NUMBER = new PrimitiveType(Number.class);
	SchemaType LONG = new PrimitiveType(Long.class);
	SchemaType DOUBLE = new PrimitiveType(Double.class);
	SchemaType FLOAT = new PrimitiveType(Float.class);
	SchemaType STRING = new PrimitiveType(String.class);
	SchemaType DATE = new PrimitiveType(Date.class);
	SchemaType URI = new PrimitiveType(URI.class);
	
	@SuppressWarnings("serial")
	Set<SchemaType> ALL = new HashSet<SchemaType>() {{
		add(ANY);
		add(BOOLEAN);
		add(BYTE);
		add(CHAR);
		add(SHORT);
		add(INTEGER);
		add(NUMBER);
		add(LONG);
		add(DOUBLE);
		add(FLOAT);
		add(STRING);
		add(DATE);
		add(URI);
	}};
	
	class PrimitiveType extends SimpleSchemaType implements SchemaType {

		private Class<?> type;
		
		PrimitiveType(Class<?> type) {
			this.type = type;
		}
		
		@Override
		public boolean isPrimitive() {
			return true;
		}

		@Override
		public QName getName() {
			return new QName(type.getSimpleName());
		}

		@Override
		public String getTitle() {
			return type.getSimpleName();
		}
		
	}
}
