/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.beans;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.namespace.QName;

import org.apache.commons.collections4.IterableUtils;

import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;

public class CompositeSchema implements Schema {
	
	private List<Schema> schemas = new CopyOnWriteArrayList<>();

	@Override
	public Iterable<SchemaType> getTypes() throws Exception {
		
		Iterable<SchemaType> result = IterableUtils.emptyIterable();
		
		if (schemas != null) {
			for (Schema schema : schemas) {
				result = IterableUtils.chainedIterable(result, schema.getTypes());
			}
		}
		
		return result;
	}

	@Override
	public Optional<SchemaType> getType(QName name) throws Exception {
		for (Schema schema : schemas) {
			Optional<SchemaType> result = schema.getType(name);
			if (result.isPresent()) {
				return result;
			}
		}
		return Optional.empty();
	}

	public void setSchemas(List<Schema> schemas) {
		this.schemas.addAll(schemas);
	}

	public void addSchema(Schema schema, Map<String,Object> props) {
		if (schema != null) {
			schemas.add(schema);
		}
	}
	
	public void removeSchema(Schema schema, Map<String,Object> props) {
		if (schema != null) {
			schemas.remove(schema);
		}
	}

}
