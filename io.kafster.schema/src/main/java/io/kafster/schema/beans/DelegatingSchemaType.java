/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.beans;

import java.util.Map;

import io.kafster.schema.Dependency;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;

public class DelegatingSchemaType extends DelegatingSchemaModel implements SchemaType {
	
	public DelegatingSchemaType() {
	}

	public DelegatingSchemaType(SchemaType delegate) {
		super(delegate);
	}
	
	protected SchemaType getDelegate() {
		return (SchemaType)super.getDelegate();
	}
	
	@Override
	public boolean isCollection() {
		return getDelegate().isCollection();
	}

	@Override
	public boolean isPrimitive() {
		return getDelegate().isPrimitive();
	}

	@Override
	public boolean isAny() {
		return getDelegate().isAny();
	}

	@Override
	public boolean isRef() {
		return getDelegate().isRef();
	}

	@Override
	public boolean isEnum() {
		return getDelegate().isEnum();
	}

	@Override
	public Object[] getEnumValues() {
		return getDelegate().getEnumValues();
	}

	@Override
	public SchemaType getCollectionType() {
		return getDelegate().getCollectionType();
	}

	@Override
	public String getReference() {
		return getDelegate().getReference();
	}

	@Override
	public Number getMaxInclusive() {
		return getDelegate().getMaxInclusive();
	}

	@Override
	public Number getMaxExclusive() {
		return getDelegate().getMaxExclusive();
	}

	@Override
	public Number getMinInclusive() {
		return getDelegate().getMinInclusive();
	}

	@Override
	public Number getMinExclusive() {
		return getDelegate().getMinExclusive();
	}

	@Override
	public Number getMultipleOf() {
		return getDelegate().getMultipleOf();
	}

	@Override
	public Integer getMinLength() {
		return getDelegate().getMinLength();
	}

	@Override
	public Integer getMaxLength() {
		return getDelegate().getMaxLength();
	}

	@Override
	public String getPattern() {
		return getDelegate().getPattern();
	}

	@Override
	public String getFormat() {
		return getDelegate().getFormat();
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		return getDelegate().getProperties();
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		return getDelegate().getDependencies();
	}

}
