/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.beans;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaModel;

public class DelegatingSchemaModel implements SchemaModel {
	
	private SchemaModel delegate;
	
	public DelegatingSchemaModel() {
	}

	public DelegatingSchemaModel(SchemaModel delegate) {
		this.delegate = delegate;
	}
	
	protected SchemaModel getDelegate() {
		return delegate;
	}

	@Override
	public QName getName() {
		return getDelegate().getName();
	}

	@Override
	public String getTitle() {
		return getDelegate().getTitle();
	}

	@Override
	public String getDescription() {
		return getDelegate().getDescription();
	}

}
