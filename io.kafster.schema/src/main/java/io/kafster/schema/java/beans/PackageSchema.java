/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java.beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.gemini.blueprint.context.BundleContextAware;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kafster.schema.Schema;
import io.kafster.schema.SchemaType;
import io.kafster.schema.java.JavaSchemaGenerator;
import io.kafster.schema.util.QNameUtils;

public class PackageSchema implements Schema, BundleContextAware {
	
	private static final Logger log = LoggerFactory.getLogger(PackageSchema.class);
	
	private Map<QName,SchemaType> models;
	private JavaSchemaGenerator generator = new JavaSchemaGenerator();
	
	private BundleContext ctx;
	
	private List<String> packages;

	@Override
	public Optional<SchemaType> getType(QName name) {
		return Optional.ofNullable(getModels().get(name));
	}
	
	@Override
	public Iterable<? extends SchemaType> getTypes() throws Exception {
		return getModels().values();
	}		
	
	protected synchronized Map<QName,SchemaType> getModels() {
		if (models == null) {
			models = loadTypes();
		}
		return models;
	}
	
	protected Map<QName,SchemaType> loadTypes() {
		
		Map<QName,SchemaType> result = new HashMap<>();
		
		if (packages != null) {
			for (String pkg : packages) {
				result.putAll(loadPackage(pkg));
			}
		}		
		
		return result;
	}
	
	protected Map<QName,SchemaType> loadPackage(String pkg) {
		
		Map<QName,SchemaType> result = new HashMap<>();
		
		try {
			
			List<Class<?>> classes = loadClasses(pkg);
			if (!classes.isEmpty()) {
				
				for (Class<?> clazz : classes) {
					
					Schema schema = generator.generate(clazz);
					if (schema != null) {
						QName name = QNameUtils.forClass(clazz);
						schema.getType(name).ifPresent(model -> result.put(name, model));
					}
				}
			}
			else {
				log.warn("No types found in package [" + pkg + "]");
			}
		}
		catch (Throwable t) {
			log.error(t.getLocalizedMessage(), t);
		}
		
		return result;
	}
	
	protected List<Class<?>> loadClasses(String pkg) throws IOException {
		
		List<Class<?>> result = new ArrayList<>();
		
		URL index = loadResource(pkg, "jaxb.index");
		if (index != null) {
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(index.openStream()));
			
			String name = StringUtils.trimToNull(reader.readLine());
			while (name != null) {
				
				// Don't fail if a class doesn't load
				//
				try {
					Class<?> clazz = loadClass(pkg, name);
					if (clazz != null) {
						result.add(clazz);
					}
					else {
						log.warn("Could not load class [" + pkg + "][" + name + "]");
					}
				}
				catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
				
				name = StringUtils.trimToNull(reader.readLine());
			}
		}
		else {
			log.warn("No index found in package [" + pkg + "]");
		}
		
		return result;
	}
	
	protected URL loadResource(String pkg, String resource) {
		
		String name = pkg.replace('.', '/') + "/" + resource;
		
		URL result = ctx.getBundle().getResource(name);
		if (result == null) {
			result = ctx.getBundle().getEntry(name);
		}
		
		if (result == null) {
			result = Thread.currentThread().getContextClassLoader().getResource(name);
		}
		
		return result;
	}
	
	protected Class<?> loadClass(String pkg, String name) throws ClassNotFoundException {
		
		String fullname = pkg + "." + name;
		
		Class<?> result = ctx.getBundle().loadClass(fullname);
		if (result == null) {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			result = loader.loadClass(fullname);
		}
		
		return result;
	}	

	@Override
	public void setBundleContext(BundleContext ctx) {
		this.ctx = ctx;
	}
	
	public void setPackages(String packages) {
		this.packages = Arrays.asList(StringUtils.split(packages, ':'));
	}
	
}
