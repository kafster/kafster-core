/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java.introspect;

import java.util.Map;

import io.kafster.schema.SchemaProperty;
import io.kafster.schema.annotation.SchemaAccessorType;
import io.kafster.schema.java.introspect.impl.FieldIntrospector;
import io.kafster.schema.java.introspect.impl.PropertiesIntrospector;

public interface JavaInstrospection {

	Map<String,SchemaProperty> getProperties(Class<?> clazz);
	
	static JavaInstrospection forClass(Class<?> clazz) {
		SchemaAccessorType annotation = clazz.getAnnotation(SchemaAccessorType.class);
		if (annotation != null) {
			switch (annotation.value()) {
				case FIELDS:
					return new FieldIntrospector();
	
				case PROPERTIES:
				default:
					break;
			}
		}
		return new PropertiesIntrospector();
	}
}
