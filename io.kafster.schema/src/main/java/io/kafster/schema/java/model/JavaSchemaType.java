/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java.model;

import java.util.Collection;
import java.util.Map;

import io.kafster.schema.Dependency;
import io.kafster.schema.PrimitiveTypes;
import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.java.introspect.JavaInstrospection;

public class JavaSchemaType extends JavaSchemaModel implements SchemaType {
	
	private JavaInstrospection introspection;

	public JavaSchemaType(Class<?> clazz, JavaInstrospection introspection) {
		super(clazz);
		this.introspection = introspection;
	}

	@Override
	public boolean isCollection() {
		return Collection.class.isAssignableFrom(getModel()) || getModel().isArray();
	}

	@Override
	public boolean isPrimitive() {
		return getPrimitive().map(p -> true).orElse(getModel().isPrimitive());
	}

	@Override
	public boolean isAny() {
		return false;
	}

	@Override
	public boolean isRef() {
		return false;
	}

	@Override
	public boolean isEnum() {
		return getModel().isEnum();
	}

	@Override
	public Object[] getEnumValues() {
		return getModel().getEnumConstants();
	}

	@Override
	public SchemaType getCollectionType() {
		if (isCollection()) {
			
			if (getModel().isArray()) {
				return getPrimitive(getModel().getComponentType()).orElse(PrimitiveTypes.ANY);
			}
			
			return PrimitiveTypes.ANY;
		}
		return null;
	}

	@Override
	public String getReference() {
		return null;
	}

	@Override
	public Number getMaxInclusive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number getMaxExclusive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number getMinInclusive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number getMinExclusive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMinLength() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxLength() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPattern() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, SchemaProperty> getProperties() {
		if (!isPrimitive()) {
			return introspection.getProperties(getModel());
		}
		return null;
	}

	@Override
	public Iterable<Dependency> getDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number getMultipleOf() {
		// TODO Auto-generated method stub
		return null;
	}

}
