/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java.introspect.impl;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import io.kafster.schema.SchemaProperty;
import io.kafster.schema.SchemaType;
import io.kafster.schema.annotation.SchemaIgnore;
import io.kafster.schema.java.introspect.JavaInstrospection;
import io.kafster.schema.java.model.JavaSchemaModel;
import io.kafster.schema.java.model.JavaSchemaProperty;
import io.kafster.schema.java.model.JavaSchemaType;

public class PropertiesIntrospector implements JavaInstrospection {

	@Override
	public Map<String, SchemaProperty> getProperties(Class<?> clazz) {

		Map<String, SchemaProperty> properties = new HashMap<>();

		try {
			BeanInfo info = Introspector.getBeanInfo(clazz);
			if (info != null) {

				PropertyDescriptor[] props = info.getPropertyDescriptors();
				if ((props != null) && (props.length > 0)) {

					for (PropertyDescriptor prop : props) {

						Method method = prop.getReadMethod();
						if ((method != null) && method.isAnnotationPresent(SchemaIgnore.class)) {
							continue;
						}

						method = prop.getWriteMethod();
						if ((method != null) && method.isAnnotationPresent(SchemaIgnore.class)) {
							continue;
						}

						properties.put(prop.getName(), convert(prop));
					}
				}
			}
		} 
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		return properties;
	}
	
	protected SchemaProperty convert(PropertyDescriptor property) {
		
		SchemaType type = JavaSchemaModel.PRIMITIVES.get(property.getPropertyType());
		if (type == null) {
			type = new JavaSchemaType(property.getPropertyType(), this);
		}
		
		return new JavaSchemaProperty(new QName(property.getName()), type, property.getPropertyType());
	}

}
