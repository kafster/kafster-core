/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java;

import io.kafster.schema.Schema;
import io.kafster.schema.java.introspect.JavaInstrospection;
import io.kafster.schema.java.model.JavaSchemaType;
import io.kafster.schema.simple.SimpleSchema;

public class JavaSchemaGenerator {

	public Schema generate(Class<?> type) {
		return new SimpleSchema(new JavaSchemaType(type, JavaInstrospection.forClass(type)));
	}
}
