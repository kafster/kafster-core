/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.schema.java.model;

import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;

import io.kafster.schema.PrimitiveTypes;
import io.kafster.schema.SchemaModel;
import io.kafster.schema.SchemaType;
import io.kafster.schema.util.QNameUtils;

public class JavaSchemaModel implements SchemaModel {
	
    public static Map<Class<?>, SchemaType> PRIMITIVES = new HashMap<>();
	static {
		PRIMITIVES.put(Boolean.class, PrimitiveTypes.BOOLEAN);
		PRIMITIVES.put(boolean.class, PrimitiveTypes.BOOLEAN);
		PRIMITIVES.put(Byte.class, PrimitiveTypes.BYTE);
		PRIMITIVES.put(byte.class, PrimitiveTypes.BYTE);
		PRIMITIVES.put(Character.class, PrimitiveTypes.CHAR);
		PRIMITIVES.put(char.class, PrimitiveTypes.CHAR);
		PRIMITIVES.put(Date.class, PrimitiveTypes.DATE);
		PRIMITIVES.put(Calendar.class, PrimitiveTypes.DATE);
		PRIMITIVES.put(Double.class, PrimitiveTypes.DOUBLE);
		PRIMITIVES.put(double.class, PrimitiveTypes.DOUBLE);
		PRIMITIVES.put(Float.class, PrimitiveTypes.FLOAT);
		PRIMITIVES.put(float.class, PrimitiveTypes.FLOAT);
		PRIMITIVES.put(Integer.class, PrimitiveTypes.INTEGER);
		PRIMITIVES.put(int.class, PrimitiveTypes.INTEGER);
		PRIMITIVES.put(Long.class, PrimitiveTypes.LONG);
		PRIMITIVES.put(long.class, PrimitiveTypes.LONG);
		PRIMITIVES.put(Number.class, PrimitiveTypes.NUMBER);
		PRIMITIVES.put(Short.class, PrimitiveTypes.SHORT);
		PRIMITIVES.put(short.class, PrimitiveTypes.SHORT);
		PRIMITIVES.put(String.class, PrimitiveTypes.STRING);
		PRIMITIVES.put(URI.class, PrimitiveTypes.URI);
		PRIMITIVES.put(URL.class, PrimitiveTypes.URI);
	}	
	
	private Class<?> clazz;
	
	protected JavaSchemaModel(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	protected Class<?> getModel() {
		return clazz;
	}
	
	protected Optional<SchemaType> getPrimitive() {
		return getPrimitive(clazz);
	}
	
	protected Optional<SchemaType> getPrimitive(Class<?> clazz) {
		return Optional.ofNullable(PRIMITIVES.get(clazz));
	}	

	@Override
	public QName getName() {
		SchemaType primitive = PRIMITIVES.get(clazz);
		if (primitive != null) {
			return primitive.getName();
		}
		
		return QNameUtils.forClass(clazz);
	}

	@Override
	public String getTitle() {
		QName name = getName();
		if (name != null) {
			return name.getLocalPart();
		}
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
