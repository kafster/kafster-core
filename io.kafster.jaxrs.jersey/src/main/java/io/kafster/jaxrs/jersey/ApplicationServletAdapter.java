/*
 * Copyright (c) 2022 kafster.io
 *
 * This file is licensed to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.jaxrs.jersey;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationServletAdapter {

	private Logger log = LoggerFactory.getLogger(ApplicationServletAdapter.class);
	
	private HttpService service;
	private List<Object> providers;
	private Map<String,Object> properties;
	private Map<String,HttpContext> contexts = new ConcurrentHashMap<String,HttpContext>();
	
	public void setHttpService(HttpService service) {
		this.service = service;
	}
	
	public void setProviders(List<Object> providers) {
		this.providers = providers;
	}
	
	public void setProperties(Map<String,Object> props) {
		this.properties = props;
	}
	
	public void applicationAdded(Application application, Map<String,Object> props) {
		
		if (application == null) {
			return;
		}		
		
		String alias = getAlias(application, props);
		if (alias == null) {
			log.warn("Could not determine Application alias, ignoring.");
			return;
		}
		
		ClassLoader original = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
					
			Dictionary<Object,Object> dict = new Hashtable<>();
			
			if (props != null) {
				props.forEach((key,value) -> dict.put(key, value));
			}
			
			if (properties != null) {
				properties.forEach((key,value) -> dict.put(key, value));
			}
			
			ApplicationServlet container = new ApplicationServlet(getResourceConfig(application));
			service.registerServlet(alias, container, dict, getHttpContext(application, props));
		} 
		catch (Throwable e) {
			log.error("Error loading Application [" + alias + "]", e);
		}
		finally {
			Thread.currentThread().setContextClassLoader(original);
		}
	}
	
	public void applicationRemoved(Application application, Map<String,Object> props) {
		
		if (application == null) {
			return;
		}
		
		String alias = getAlias(application, props);
		if (alias == null) {
			log.warn("Could not determine Application alias, ignoring.");
			return;
		}
		
		service.unregister(alias);
 	}
	
	protected String getAlias(Application application, Map<String,Object> props) {
		
		ApplicationPath path = application.getClass().getAnnotation(ApplicationPath.class);
		if (path != null) {
			return path.value();
		}
		
		String alias = (String)props.get("path");
		if (alias == null) {
			alias = (String)props.get("alias");
		}
		
		String context = (String)props.get("context");
		if (context != null) {
			if (alias == null) {
				alias = context;
			}
			else {
				alias = StringUtils.removeEnd(context, "/") + StringUtils.prependIfMissing(alias, "/");
			}
		}
		
		return alias;
	}	
	
	protected String getContext(Application application, Map<String,Object> props) {
		String context = (String)props.get("context");
		if (context == null) {
			context = getAlias(application, props);
		}
		
		return StringUtils.prependIfMissing(context, "/");
	}
	
	protected HttpContext getHttpContext(Application application, Map<String,Object> props) {
		String ctx = getContext(application, props);
		if (ctx != null) {
			synchronized (contexts) {
				HttpContext result = contexts.get(ctx);
				if (result == null) {
					result = service.createDefaultHttpContext();
					contexts.put(ctx, result);
				}
				return result;
			}
		}
		return null;
	}
	
	protected ResourceConfig getResourceConfig(Application application) throws Exception {
		return ResourceConfig.forApplication(new DelegatingApplication(application));
	}
	
	@SuppressWarnings("serial")
	private class ApplicationServlet extends ServletContainer {

		public ApplicationServlet(ResourceConfig config) throws Exception {
			super(config);
		}

		@Override
		protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				if (request.getMethod().equalsIgnoreCase("options")) {
					response.setStatus(HttpServletResponse.SC_OK);
					return;
				}
				super.service(request, response);
			}
			catch (ServletException e) {
				log.error(e.getLocalizedMessage(), e);
				throw e;
			}
			catch (IOException e) {
				log.error(e.getLocalizedMessage(), e);
				throw e;
			}			
			catch (Throwable e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ServletException(e);
			}
		}

	}
	
	private class DelegatingApplication extends Application {

		private Application app;
		
		public DelegatingApplication(Application app) throws Exception {
			this.app = app;
		}
		
		@Override
		public Set<Class<?>> getClasses() {
			return app.getClasses();
		}

		@Override
		public Set<Object> getSingletons() {
			Set<Object> result = new HashSet<>(app.getSingletons());
			if (providers != null) {
				result.addAll(providers);
			}
			return result;
		}
		
	}	
}
