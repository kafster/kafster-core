/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages;

import java.util.Arrays;
import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class NothingSerde<T> implements Serializer<T>, Deserializer<T>, Serde<T> {

    @Override
    public void configure(final Map<String, ?> configuration, final boolean isKey) {

    }

    @Override
    public T deserialize(final String topic, final byte[] bytes) {
      if (bytes != null) {
        throw new IllegalArgumentException("Expected [" + Arrays.toString(bytes) + "] to be null.");
      } else {
        return null;
      }
    }

    @Override
    public byte[] serialize(final String topic, final T data) {
      if (data != null) {
        throw new IllegalArgumentException("Expected [" + data + "] to be null.");
      } else {
        return null;
      }
    }

    @Override
    public void close() {

    }

    @Override
    public Serializer<T> serializer() {
      return this;
    }

    @Override
    public Deserializer<T> deserializer() {
      return this;
    }
  }
