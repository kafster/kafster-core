/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.endpoint.Endpoint;
import io.kafster.endpoint.EndpointResolver;
import io.kafster.endpoint.beans.SimpleEndpoint;
import io.kafster.engine.stages.builders.AggregationBuilder;
import io.kafster.engine.stages.builders.FilterBuilder;
import io.kafster.engine.stages.builders.FlatMapBuilder;
import io.kafster.engine.stages.builders.MapBuilder;
import io.kafster.engine.stages.model.AggregateConfiguration;
import io.kafster.engine.stages.model.FieldMapping;
import io.kafster.engine.stages.model.FieldMappingOperation;
import io.kafster.engine.stages.model.FieldMappingOperation.Operation;
import io.kafster.engine.stages.model.FilterConfiguration;
import io.kafster.engine.stages.model.FlatMapConfiguration;
import io.kafster.engine.stages.model.GroupBy;
import io.kafster.engine.stages.model.MapConfiguration;
import io.kafster.engine.stages.model.WindowConfiguration;
import io.kafster.kafka.factory.TopologyFactory;
import io.kafster.kafka.factory.impl.InterceptorTopologyFactory;
import io.kafster.kafka.factory.interceptors.PipelineSetupInterceptor;
import io.kafster.kafka.factory.interceptors.SerdeInterceptor;
import io.kafster.kafka.serialize.json.JSONStreamRecordSerde;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.beans.SimplePipeline;
import io.kafster.pipeline.beans.SimplePipelineDestination;
import io.kafster.pipeline.beans.SimplePipelineStage;
import io.kafster.pipeline.beans.SimplePipelineStageMetadata;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.beans.SimpleSubscription;

public class PipelineTests {
	
	@Rule
    public TemporaryFolder temp = new TemporaryFolder();
	
	private TopologyFactory factory;
	private ObjectMapper mapper;
	private JexlEngine jexl;
	
	private static List<byte[]> VALUES = Arrays.asList(
			"{\"key\":\"123456\",\"integer1\":2,\"integer2\":2,\"float1\":1.5,\"array1\":[1,2,3,4]}".getBytes(),
			"{\"key\":\"123456\",\"integer1\":4,\"integer2\":2,\"float1\":2.5,\"array1\":[6,7,8]}".getBytes()
	);
	
	private static final String INPUT_TOPIC = "input";
	private static final String OUTPUT_TOPIC = "output";
	
	protected ObjectMapper getMapper() {
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper;
	}
	
	protected JexlEngine getJexl() {
		if (jexl == null) {
			jexl = new JexlBuilder().create();
		}
		return jexl;
	}
	
	protected Topology createTopology(Subscription subscription) throws Exception {
		if (factory == null) {
			
			JSONStreamRecordSerde serde = new JSONStreamRecordSerde();
			serde.setMapper(getMapper());
			
			SerdeInterceptor interceptor = new SerdeInterceptor();
			interceptor.setSerde(serde);
			
			InterceptorTopologyFactory result = new InterceptorTopologyFactory();
			result.setInterceptor(new PipelineSetupInterceptor(interceptor));
			
			Map<String,Object> props = new HashMap<>();
			
			AggregationBuilder aggregate = new AggregationBuilder();
			aggregate.setMapper(getMapper());
			aggregate.setJexl(getJexl());
			props.put("name", "default:aggregate");
			
			result.builderAdded(aggregate, props);
			
			FlatMapBuilder flatmap = new FlatMapBuilder();
			flatmap.setMapper(getMapper());
			flatmap.setJexl(getJexl());
			props.put("name", "default:flatmap");
			
			result.builderAdded(flatmap, props);	
			
			MapBuilder map = new MapBuilder();
			map.setMapper(getMapper());
			map.setJexl(getJexl());
			props.put("name", "default:map");
			
			result.builderAdded(map, props);	
			
			FilterBuilder filter = new FilterBuilder();
			filter.setMapper(getMapper());
			filter.setJexl(getJexl());
			props.put("name", "default:filter");
			
			result.builderAdded(filter, props);	
			
			result.setResolver(new EndpointResolver() {

				@Override
				public ResolvedEndpoint resolve(Subscription subscription, Endpoint destination)	throws Exception {
					return new ResolvedEndpoint() {
						@Override
						public String getName() {
							return destination.getUri();
						}

						@Override
						public String getFilter() {
							return null;
						}
					};
				}
			});
			
			factory = result;
		}
		
		Topology topology = factory.create(subscription);
		System.out.println(topology.describe());
		
		return topology;
	}

	protected Subscription createSubscription(PipelineStage... stages) {
		SimpleSubscription subscription = new SimpleSubscription();
		subscription.setKey(UUID.randomUUID().toString());
		subscription.setName("test-subscription");
		
		SimplePipeline pipeline = new SimplePipeline();
		pipeline.setStages(Arrays.asList(stages));
		pipeline.setKey(UUID.randomUUID().toString());
		pipeline.setName("test-pipeline");
		pipeline.setSource(new SimplePipelineDestination("in"));
		pipeline.setTarget(new SimplePipelineDestination("out"));
		
		subscription.setPipeline(pipeline);
		
		SimpleEndpoint source = new SimpleEndpoint();
		source.setUri(INPUT_TOPIC);
		subscription.setSource(source);
		
		SimpleEndpoint target = new SimpleEndpoint();
		target.setUri(OUTPUT_TOPIC);
		subscription.setTarget(target);
		
		return subscription;
	}
	
	protected PipelineStage createStage(String name, Object configuration) {
		
		SimplePipelineStageMetadata metadata = new SimplePipelineStageMetadata();
		metadata.setName(name);
		
		Map<String,Object> props = getMapper().convertValue(configuration, new TypeReference<HashMap<String,Object>>(){});
		
		SimplePipelineStage result = new SimplePipelineStage();
		result.setConfiguration(props);
		result.setKey(UUID.randomUUID().toString());
		result.setMetadata(metadata);
		return result;
	}
	
	private Properties createTopologyConfiguration() throws IOException {
		Properties result = new Properties();
		result.put(StreamsConfig.APPLICATION_ID_CONFIG, "pipeline-test");
		result.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy config");
		result.put(StreamsConfig.BUILT_IN_METRICS_VERSION_CONFIG, StreamsConfig.METRICS_LATEST);
		result.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass());
		result.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass());

		// Use a temporary directory for storing state, which will be automatically
		// removed after the test.
		result.put(StreamsConfig.STATE_DIR_CONFIG, temp.newFile().getAbsolutePath());
		return result;
	}
	
	// @Test Temporarily slip (rocksDB)
	public void testAggregate() throws Exception {
		
		GroupBy groupBy = new GroupBy();
		groupBy.setAs("out.key");
		groupBy.setExpression("in.key");
		
		WindowConfiguration window = new WindowConfiguration();
		window.setAdvance(10000L);
		window.setDuration(10000L);
		
		AggregateConfiguration configuration = new AggregateConfiguration();
		configuration.setGroupBy(groupBy);
		configuration.setWindow(window);
		configuration.setMappings(Arrays.asList(
			new FieldMappingOperation("in.integer1","out.integer1-avg",Operation.AVERAGE), 
			new FieldMappingOperation("in.integer2","out.integer2-avg",Operation.AVERAGE),
			new FieldMappingOperation("in.float1","out.float1-avg",Operation.AVERAGE),
			new FieldMappingOperation("in.integer1","out.integer1-sum",Operation.SUM), 
			new FieldMappingOperation("in.integer2","out.integer2-sum",Operation.SUM),
			new FieldMappingOperation("in.float1","out.float1-sum",Operation.SUM),
			new FieldMappingOperation("in.integer1","out.integer1-max",Operation.MAX), 
			new FieldMappingOperation("in.integer2","out.integer2-max",Operation.MAX),
			new FieldMappingOperation("in.float1","out.float1-max",Operation.MAX)
		));	
		
		String result = "{"
				+ "\"key\":\"123456\","
				+ "\"integer1-avg\":3,"
				+ "\"integer2-avg\":2,"
				+ "\"float1-avg\":2.0,"
				+ "\"integer1-sum\":6,"
				+ "\"integer2-sum\":4,"
				+ "\"float1-sum\":4.0,"
				+ "\"integer1-max\":4,"
				+ "\"integer2-max\":2,"
				+ "\"float1-max\":2.5"				
				+ "}";
		
		Subscription subscription = createSubscription(createStage("default:aggregate", configuration));
		
	    try (final TopologyTestDriver driver = new TopologyTestDriver(createTopology(subscription), createTopologyConfiguration())) {

	        TestInputTopic<byte[], byte[]> input = driver.createInputTopic(INPUT_TOPIC, new ByteArraySerializer(), new ByteArraySerializer());
	        TestOutputTopic<byte[], byte[]> output = driver.createOutputTopic(OUTPUT_TOPIC, new ByteArrayDeserializer(), new ByteArrayDeserializer());

	        for (byte[] value : VALUES) {
	        	input.pipeInput(value, Instant.now());
	        }
	        
	        List<byte[]> results = output.readValuesToList();
	        assertThat(results.size(), equalTo(1));

	        Map<String,Object> expected = getMapper().readValue(result.getBytes(), new TypeReference<HashMap<String,Object>>(){});
	        Map<String,Object> actual = getMapper().readValue(output.readValue(), new TypeReference<HashMap<String,Object>>(){});
	        
	        assertThat(actual, equalTo(expected));
	      }		
	}
	
	@Test
	public void testFilter() throws Exception {

		FilterConfiguration configuration = new FilterConfiguration();
		configuration.setExpression("in.integer1 lt 4");

		Subscription subscription = createSubscription(createStage("default:filter", configuration));

		try (TopologyTestDriver driver = new TopologyTestDriver(createTopology(subscription), createTopologyConfiguration())) {

			TestInputTopic<Void, byte[]> input = driver.createInputTopic(INPUT_TOPIC, new NothingSerde<Void>(),	new ByteArraySerializer());
			TestOutputTopic<Void, byte[]> output = driver.createOutputTopic(OUTPUT_TOPIC, new NothingSerde<Void>(),	new ByteArrayDeserializer());

			input.pipeValueList(VALUES);

			List<byte[]> results = output.readValuesToList();
			assertThat(results.size(), equalTo(1));
		}
	}	
	
	@Test
	public void testFlatmap() throws Exception {

		FlatMapConfiguration configuration = new FlatMapConfiguration();
		configuration.setMapping(new FieldMapping("in.array1", "out.arrayVal"));

		Subscription subscription = createSubscription(createStage("default:flatmap", configuration));

		try (TopologyTestDriver driver = new TopologyTestDriver(createTopology(subscription), createTopologyConfiguration())) {

			TestInputTopic<Void, byte[]> input = driver.createInputTopic(INPUT_TOPIC, new NothingSerde<Void>(),	new ByteArraySerializer());
			TestOutputTopic<Void, byte[]> output = driver.createOutputTopic(OUTPUT_TOPIC, new NothingSerde<Void>(),	new ByteArrayDeserializer());

			input.pipeValueList(VALUES);

			List<byte[]> results = output.readValuesToList();
			assertThat(results.size(), equalTo(7));

			List<Integer> expected = Arrays.asList(1, 2, 3, 4, 6, 7, 8);
			List<Integer> actual = results.stream().map(v -> parse(v).get("arrayVal").asInt()).collect(Collectors.toList());

			assertThat(actual, equalTo(expected));
		}
	}
	
	protected JsonNode parse(byte[] content) {
		try {
			return getMapper().readTree(content);
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testMap() throws Exception {

		MapConfiguration configuration = new MapConfiguration();
		configuration.setMappings(Arrays.asList(
				new FieldMapping("in.integer1 + 2","out.integer1")
		));

		Subscription subscription = createSubscription(createStage("default:map", configuration));

		try (TopologyTestDriver driver = new TopologyTestDriver(createTopology(subscription),	createTopologyConfiguration())) {

			TestInputTopic<Void, byte[]> input = driver.createInputTopic(INPUT_TOPIC, new NothingSerde<Void>(),	new ByteArraySerializer());
			TestOutputTopic<Void, byte[]> output = driver.createOutputTopic(OUTPUT_TOPIC, new NothingSerde<Void>(),	new ByteArrayDeserializer());

			input.pipeValueList(VALUES);

			List<byte[]> results = output.readValuesToList();
			assertThat(results.size(), equalTo(2));
		}
	}	
	
	@Test
	public void testPassThru() throws Exception {
	
	}	
}
