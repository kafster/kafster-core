/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.builders;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.engine.stages.model.FieldMapping;
import io.kafster.engine.stages.model.MapConfiguration;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.runner.jexl.JexlContextAdapter;
import io.kafster.kafka.stages.StageBuilder;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.pipeline.PipelineStage;

public class MapBuilder implements StageBuilder {
	
	private static final Logger log = LoggerFactory.getLogger(MapBuilder.class);
	
	private ObjectMapper mapper;
	private JexlEngine jexl;	

	@Override
	public KStream<byte[], StreamRecord> build(PipelineStage config, KStream<byte[], StreamRecord> stream) throws Exception {
		
		MapConfiguration map = mapper.convertValue(config.getConfiguration(), MapConfiguration.class);
		if (!validate(map)) {
			throw new ConfigurationException("Map configuration Error");
		}
		
		return stream.mapValues(mapper(map));
	}
	
	protected boolean validate(MapConfiguration config) throws Exception {
		
		if ((config == null) || (config.getMappings() == null)) {
			return false;
		}
			
		for (FieldMapping mapping : config.getMappings()) {
			
			if ((mapping.getFrom() == null) || (mapping.getAs() == null)) {
				return false;
			}
		}
		
		return true;
	}
	
	private ValueMapper<StreamRecord, StreamRecord> mapper(MapConfiguration config) {
		
		final List<MappingWithExpression> mappings = new ArrayList<>();
		
		for (FieldMapping mapping : config.getMappings()) {
			
			JexlExpression expr = jexl.createExpression(mapping.getFrom());
			if (expr != null) {
				mappings.add(new MappingWithExpression(mapping, expr));
			}
			else {
				log.warn("Could not create expression [" + mapping.getFrom() + "]");
			}
		}
		
		return (value) -> {
			
			JexlContext context = new JexlContextAdapter(value);
			
			for (MappingWithExpression mapping : mappings) {
				Object result = mapping.expression.evaluate(context);
				if (result != null) {
					value.set(mapping.mapping.getAs(), result);
				}
			}
			
			return value;
		};
	}
	
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setJexl(JexlEngine jexl) {
		this.jexl = jexl;
	}
	
	private class MappingWithExpression {
		private FieldMapping mapping;
		private JexlExpression expression;
		
		public MappingWithExpression(FieldMapping mapping, JexlExpression expression) {
			this.mapping = mapping;
			this.expression = expression;
		}
	}

}
