/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import io.kafster.schema.annotation.SchemaAccessType;
import io.kafster.schema.annotation.SchemaAccessorType;

@XmlRootElement
@SchemaAccessorType(SchemaAccessType.FIELDS)
public class AggregateConfiguration extends StageConfiguration {

	private GroupBy groupBy;
	private WindowConfiguration window;
	
	private List<FieldMappingOperation> mappings;
	
	public GroupBy getGroupBy() {
		return groupBy;
	}
	
	public void setGroupBy(GroupBy groupBy) {
		this.groupBy = groupBy;
	}
	
	public WindowConfiguration getWindow() {
		return window;
	}
	
	public void setWindow(WindowConfiguration window) {
		this.window = window;
	}
	
	public List<FieldMappingOperation> getMappings() {
		return mappings;
	}
	
	public void setMappings(List<FieldMappingOperation> mappings) {
		this.mappings = mappings;
	}
}
