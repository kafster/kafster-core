/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.builders;

import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.engine.stages.model.FieldMapping;
import io.kafster.engine.stages.model.FlatMapConfiguration;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.runner.jexl.JexlContextAdapter;
import io.kafster.kafka.stages.StageBuilder;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.pipeline.PipelineStage;

public class FlatMapBuilder implements StageBuilder {
	
	private static final Logger log = LoggerFactory.getLogger(FlatMapBuilder.class);
	
	private ObjectMapper mapper;
	private JexlEngine jexl;
	
	@Override
	public KStream<byte[], StreamRecord> build(PipelineStage config, KStream<byte[], StreamRecord> stream) throws Exception {
		FlatMapConfiguration flatmap = mapper.convertValue(config.getConfiguration(), FlatMapConfiguration.class);
		if (!validate(flatmap)) {
			throw new ConfigurationException("FlatMap configuration Error");
		}
		
		return stream.flatMapValues(mapper(flatmap));
	}
	
	protected boolean validate(FlatMapConfiguration config) throws Exception {
		return ((config.getMapping() != null) && (config.getMapping().getFrom() != null) && (config.getMapping().getAs() != null));
	}	
		
	private ValueMapper<StreamRecord, Iterable<? extends StreamRecord>> mapper(FlatMapConfiguration config) {
		
		FieldMapping mapping = config.getMapping();
		
		JexlExpression expr = jexl.createExpression(mapping.getFrom());
		
		return (value) -> {
			
			Object result = expr.evaluate(new JexlContextAdapter(value));
			if (result != null) {
				Iterator<?> iterable = jexl.getUberspect().getIterator(result);
				if (iterable != null) {
					return new Iterable<StreamRecord>() {
						@Override
						public Iterator<StreamRecord> iterator() {
							return IteratorUtils.transformedIterator(iterable, v -> value.copy().set(mapping.getAs(), v));
						}
					};					
				}
				else {
					log.warn("Could not iterate across [" + mapping.getFrom() + "][" + result + "]");
				}
			}
			
			return Collections.singletonList(value);
		};
	}		

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setJexl(JexlEngine jexl) {
		this.jexl = jexl;
	}

}
