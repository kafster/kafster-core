/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.builders;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import org.apache.commons.jexl3.JexlArithmetic;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Initializer;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.WindowStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.engine.stages.model.AggregateConfiguration;
import io.kafster.engine.stages.model.FieldMapping;
import io.kafster.engine.stages.model.GroupBy;
import io.kafster.engine.stages.model.WindowConfiguration;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.runner.jexl.JexlContextAdapter;
import io.kafster.kafka.runner.jexl.MapStreamRecord;
import io.kafster.kafka.serialize.json.JSONStreamRecordSerde;
import io.kafster.kafka.stages.StageBuilder;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.pipeline.PipelineStage;

public class AggregationBuilder implements StageBuilder {
	
	private static final Logger log = LoggerFactory.getLogger(AggregationBuilder.class);
	
	private static final String COUNT_FIELD = "__count";
	private static final String MEAN_PREFIX = "__mean.";
	private static final String DSQUARED_PREFIX = "__dsquared.";
	
	private ObjectMapper mapper;
	private JexlEngine jexl;
	
	private JexlArithmetic math = new JexlArithmetic(true);

	@Override
	public KStream<byte[], StreamRecord> build(PipelineStage stage, KStream<byte[], StreamRecord> stream) throws Exception {
		
		AggregateConfiguration configuration = mapper.convertValue(stage.getConfiguration(), AggregateConfiguration.class);
		if (!validate(configuration)) {
			throw new ConfigurationException("Invalid Aggregation configuration");
		}
		
		WindowConfiguration window = configuration.getWindow();
		
		Duration size = Duration.ofMillis(window.getDuration());
		Duration advance = Duration.ofMillis(window.getAdvance());
		Duration grace = Duration.ofMillis(Optional.ofNullable(window.getGracePeriod()).orElse(0L));
		
		KTable<Windowed<byte[]>, StreamRecord> windowed = stream
				.groupBy(key(configuration), Grouped.with(Serdes.ByteArray(), new JSONStreamRecordSerde(mapper)))
				.windowedBy(TimeWindows.ofSizeAndGrace(size, grace).advanceBy(advance))
				.aggregate(initializer(), aggregator(configuration), materialized());
		
		return windowed.toStream((k,v) -> k.key());
	}
	
	protected boolean validate(AggregateConfiguration config) throws Exception {
		
		return ((config != null) &&
				(config.getWindow() != null) && 
				(config.getWindow().getAdvance() != null) &&
				(config.getWindow().getDuration() != null) && 
				(config.getGroupBy() != null));
	}	

	protected KeyValueMapper<byte[],StreamRecord,byte[]> key(AggregateConfiguration aggregate) throws Exception {
		
		GroupBy groupBy = aggregate.getGroupBy();
		if (groupBy.getExpression() != null) {
			
			final JexlExpression expression = jexl.createExpression(groupBy.getExpression());
			
			return (k,v) -> {
				JexlContextAdapter context = new JexlContextAdapter(v);
				Object result = expression.evaluate(context);
				if (result != null) {
					String val = String.valueOf(result);
					if (groupBy.getAs() != null) {
						context.set(groupBy.getAs(), val);
					}
					log.trace("Record key [" + val + "]");
					return val.getBytes();
				}
				
				log.warn("Could not calculate key for [" + groupBy.getExpression() + "]");
				return new byte[]{};
			};
		}
		
		if (groupBy.getFields() != null) {
			
			final List<JexlExpression> expressions = new ArrayList<>();
			for (String expr : groupBy.getFields()) {
				expressions.add(jexl.createExpression(expr));
			}
			
			return (k,v) -> {
				JexlContextAdapter context = new JexlContextAdapter(v);
				
				StringBuilder builder = new StringBuilder();
				for (JexlExpression expr : expressions) {
					Object value = expr.evaluate(context);
					if (value != null) {
						if (builder.length() > 0) {
							builder.append(':');
						}
						builder.append(value);
					}					
				}
				
				String val = builder.toString();
				if (groupBy.getAs() != null) {
					context.set(groupBy.getAs(), val);
				}
				return val.getBytes();				
			};
		}
		
		throw new ConfigurationException("Could not determine grouping key");
	}
	
	protected Initializer<StreamRecord> initializer() {
		return () -> {
			StreamRecord result = new MapStreamRecord(new TreeMap<>(), mapper);
			result.set(COUNT_FIELD, Integer.valueOf(0));
			return result;
		};
	}
	
	protected Materialized<byte[],StreamRecord,WindowStore<Bytes,byte[]>> materialized() {
		return Materialized.with(Serdes.ByteArray(), new JSONStreamRecordSerde(mapper));
	}
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> aggregator(AggregateConfiguration config) {
		
		final List<Aggregator<byte[],StreamRecord,StreamRecord>> aggregators = new ArrayList<>();
		
		if (config.getMappings() != null) {
			
			config.getMappings().forEach(mapping -> {
				switch(mapping.getOperation()) {
					case AVERAGE:
						aggregators.add(avg(mapping));
						break;
					case COUNT:
						aggregators.add(count(mapping));
						break;
					case FIRST:
						aggregators.add(first(mapping));
						break;
					case LAST:
						aggregators.add(last(mapping));
						break;
					case MAX:
						aggregators.add(max(mapping));
						break;
					case MIN:
						aggregators.add(min(mapping));
						break;
					case STDDEV:
						aggregators.add(stddev(mapping));
						break;
					case SUM:
						aggregators.add(sum(mapping));
						break;
					default:
						break;
				}
			});
		}
		
		return (key, current, aggregate) -> {
			
			aggregators.forEach(aggregator -> aggregator.apply(key, current, aggregate));

			Integer count = (Integer)aggregate.get(COUNT_FIELD);
			aggregate.set(COUNT_FIELD, Integer.valueOf(++count));
			
			return aggregate;
		};
	}
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> avg(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				
				Object mean = aggregate.get(mapping.getAs());
				if (mean != null) {
					Integer count = (Integer)aggregate.get(COUNT_FIELD);
					
					Object differential = math.divide(math.subtract(value, mean), count+1);
					mean = math.add(mean, differential);
				}
				else {
					mean = value;
				}
				
				aggregate.set(mapping.getAs(), mean);
			}	
			
			return aggregate;
		};
	}
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> stddev(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {		
		
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				
				Integer count = (Integer)aggregate.get(COUNT_FIELD);
				
				Object mean = aggregate.get(MEAN_PREFIX + mapping.getAs());
				Object dSquared = aggregate.get(DSQUARED_PREFIX + mapping.getAs());
				
				if (mean == null) {
					mean = Long.valueOf(0);
				}
				
				if (dSquared == null) {
					dSquared = Long.valueOf(0);
				}
					
				Object differential = math.divide(math.subtract(value, mean), count+1);
				Object newMean = math.add(mean, differential);
				
				Object dSquaredIncrement = math.multiply(math.subtract(value, newMean), math.subtract(value, mean));
				Object newDSquared = math.add(dSquared, dSquaredIncrement);
				
				aggregate.set(mapping.getAs(), sqrt(newDSquared));
				
				aggregate.set(MEAN_PREFIX + mapping.getAs(), newMean);
				aggregate.set(DSQUARED_PREFIX + mapping.getAs(), newDSquared);
			}
			
			return aggregate;
		};
	}	
	
	protected Object sqrt(Object value) {
		if (value instanceof Number) {
			return Math.sqrt(((Number)value).doubleValue());
		}
		return null;
	}
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> count(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {	
			
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				Long count = (Long)aggregate.get(mapping.getAs());
				if (count != null) {
					aggregate.set(mapping.getAs(), ++count);
				}
				else {
					aggregate.set(mapping.getAs(), Long.valueOf(1));
				}
			}
			
			return aggregate;
		};		
	}	
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> first(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			
			Object first = aggregate.get(mapping.getAs());
			if (first == null) {
				Object value = from.evaluate(new JexlContextAdapter(current));
				if (value != null) {
					aggregate.set(mapping.getAs(), value);
				}
			}
			
			return aggregate;
		};		
	}
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> last(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				aggregate.set(mapping.getAs(), value);
			}
			return aggregate;
		};		
	}	
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> max(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				Object max = aggregate.get(mapping.getAs());
				if (max != null) {
					aggregate.set(mapping.getAs(), math.lessThan(max, value) ? value : max);
				}
				else {
					aggregate.set(mapping.getAs(), value);
				}
			}
			return aggregate;
		};	
	}	
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> min(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				Object min = aggregate.get(mapping.getAs());
				if (min != null) {
					aggregate.set(mapping.getAs(), math.lessThan(value, min) ? value : min);
				}
				else {
					aggregate.set(mapping.getAs(), value);
				}
			}
			return aggregate;
		};	
	}	
	
	protected Aggregator<byte[],StreamRecord,StreamRecord> sum(FieldMapping mapping) {
		
		final JexlExpression from = jexl.createExpression(mapping.getFrom());
		
		return (key, current, aggregate) -> {
			Object value = from.evaluate(new JexlContextAdapter(current));
			if (value != null) {
				Object agg = aggregate.get(mapping.getAs());
				if (agg != null) {
					aggregate.set(mapping.getAs(), math.add(value, agg));
				}
				else {
					aggregate.set(mapping.getAs(), value);
				}
			}
			return aggregate;
		};	
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setJexl(JexlEngine jexl) {
		this.jexl = jexl;
	}	
}
