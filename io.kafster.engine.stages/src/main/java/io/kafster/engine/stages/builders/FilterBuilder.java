/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.builders;

import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.kafka.streams.kstream.KStream;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.kafster.engine.stages.model.FilterConfiguration;
import io.kafster.error.ConfigurationException;
import io.kafster.kafka.runner.jexl.JexlContextAdapter;
import io.kafster.kafka.stages.StageBuilder;
import io.kafster.kafka.stream.StreamRecord;
import io.kafster.pipeline.PipelineStage;

public class FilterBuilder implements StageBuilder {
	
	private ObjectMapper mapper;
	private JexlEngine jexl;

	@Override
	public KStream<byte[], StreamRecord> build(PipelineStage stage, KStream<byte[], StreamRecord> stream) throws Exception {
		
		FilterConfiguration configuration = mapper.convertValue(stage.getConfiguration(), FilterConfiguration.class);
		if (!validate(configuration)) {
			throw new ConfigurationException("Invalid Filter stage configuration");
		}
		
		final JexlExpression expression = jexl.createExpression(configuration.getExpression());
		
		return stream.filter((k,v) -> {
			Object result = expression.evaluate(new JexlContextAdapter(v));
			return jexl.getArithmetic().toBoolean(result);
		});
	}
	
	protected boolean validate(FilterConfiguration config) throws Exception {
		return ((config != null) && (config.getExpression() != null));
	}		
	
	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setJexl(JexlEngine jexl) {
		this.jexl = jexl;
	}

}
