/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.engine.stages.model;

import javax.xml.bind.annotation.XmlRootElement;

import io.kafster.schema.annotation.SchemaAccessType;
import io.kafster.schema.annotation.SchemaAccessorType;

@XmlRootElement
@SchemaAccessorType(SchemaAccessType.FIELDS)
public class FieldMappingOperation extends FieldMapping {

	public enum Operation {
		AVERAGE,
		COUNT,
		FIRST,
		LAST,
		MAX,
		MIN,
		STDDEV,
		SUM
	}
	
	private Operation operation;
	
	public FieldMappingOperation() {
	}

	public FieldMappingOperation(String from, String as) {
		super(from, as);
	}	
	
	public FieldMappingOperation(String from, String as, Operation operation) {
		super(from, as);
		this.operation = operation;
	}	

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
}
