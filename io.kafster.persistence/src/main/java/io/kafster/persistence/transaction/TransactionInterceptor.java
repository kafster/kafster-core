/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.transaction;

import java.lang.reflect.Method;
import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import io.kafster.persistence.transaction.TransactionContext.Transaction;

public class TransactionInterceptor implements MethodInterceptor  {
	
	private static final Logger log = LoggerFactory.getLogger(TransactionInterceptor.class);
	
	private boolean force = false;
	private TransactionContext context;

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		
		if (context == null) {
			return invocation.proceed();
		}
		
		Transactional annotation = getAnnotation(invocation.getMethod());
		if (!force && (annotation == null)) {
			return invocation.proceed();
		}
		else {
			
			boolean rollback = false;
		
			Transaction tx = context.getTransaction();
			try {
				if ((tx != null) && !tx.isOpen()) {
					tx.open();
				}
				
				return invocation.proceed();
			}
			catch (Throwable e) {
				
				log.error(e.getLocalizedMessage(), e);
				
				if ((annotation == null) || !ArrayUtils.contains(annotation.noRollbackFor(), e)) {
					rollback = true;
				}
				else {
					log.info("Not rolling back [" + e + "] because it is in exception list");
				}
				
				throw e;
			}
			finally {
				if ((tx != null) && tx.isOpen()) {
					if (rollback) {
						tx.rollback(); 
					}
					else { 
						tx.commit(); 
					}
					
					tx.close();
				}
			}
		}
	}
	
	protected Transactional getAnnotation(Method method) {
		Transactional annotation = method.getAnnotation(Transactional.class);
		if (annotation == null) {
			annotation = method.getClass().getAnnotation(Transactional.class);
		}
		return annotation;
	}
	
	public void setForce(boolean force) {
		this.force = force;
	}

	public void setContext(TransactionContext context) {
		this.context = context;
	}
	
	public void contextAdded(TransactionContext context, Map<String,Object> props) {
		if (context != null) {
			this.context = context;
		}
	}
	
	public void contextRemoved(TransactionContext context, Map<String,Object> props) {
		if (context != null) {
			this.context = null;
		}
	}	

}
