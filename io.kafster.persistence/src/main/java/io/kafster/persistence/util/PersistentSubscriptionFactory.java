/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.time.Instant;
import java.util.UUID;

import io.kafster.persistence.SubscriptionRepository;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionBuilder;
import io.kafster.subscription.SubscriptionFactory;
import io.kafster.subscription.SubscriptionStatus.State;
import io.kafster.subscription.beans.SimpleSubscription;
import io.kafster.subscription.beans.SimpleSubscriptionBuilder;
import io.kafster.subscription.beans.SimpleSubscriptionStatus;

public class PersistentSubscriptionFactory implements SubscriptionFactory {

	private SubscriptionRepository repository;

	@Override
	public SubscriptionBuilder createSubscription() throws Exception {
		return new PersistentSubscriptionBuilder();
	}
	
	protected class PersistentSubscriptionBuilder extends SimpleSubscriptionBuilder {

		@Override
		public Subscription build() throws Exception {
			
			SimpleSubscription subscription = getSubscription();
			if (subscription.getStatus() == null) {
				subscription.setStatus(new SimpleSubscriptionStatus(State.CREATED));
			}
			if (subscription.getCreated() == null) {
				subscription.setCreated(Instant.now());
			}
			if (subscription.getModified() == null) {
				subscription.setModified(Instant.now());
			}
			
			String key = subscription.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
				subscription.setKey(key);
			}
			
			return repository.store(key, subscription);
		}
	}

	public void setRepository(SubscriptionRepository repository) {
		this.repository = repository;
	}
}
