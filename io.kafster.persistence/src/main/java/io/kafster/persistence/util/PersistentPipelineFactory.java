/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import io.kafster.persistence.PipelineRepository;
import io.kafster.pipeline.Pipeline;
import io.kafster.pipeline.PipelineBuilder;
import io.kafster.pipeline.PipelineFactory;
import io.kafster.pipeline.PipelineStage;
import io.kafster.pipeline.PipelineStageBuilder;
import io.kafster.pipeline.PipelineStageMetadata;
import io.kafster.pipeline.beans.SimplePipeline;
import io.kafster.pipeline.beans.SimplePipelineDestination;
import io.kafster.pipeline.beans.SimplePipelineStage;
import io.kafster.schema.SchemaType;

public class PersistentPipelineFactory implements PipelineFactory {
	
	private PipelineRepository repository;

	@Override
	public PipelineBuilder builder() {
		return new PersistentPipelineBuilder();
	}

	public void setRepository(PipelineRepository repository) {
		this.repository = repository;
	}
	
	protected class PersistentPipelineBuilder implements PipelineBuilder {
		
		private SimplePipeline pipeline;
		
		public PersistentPipelineBuilder() {
			this.pipeline = new SimplePipeline();
		}

		@Override
		public PipelineBuilder key(String key) {
			pipeline.setKey(key);
			return this;
		}

		@Override
		public PipelineBuilder name(String name) {
			pipeline.setName(name);
			return this;
		}

		@Override
		public PipelineBuilder description(String description) {
			pipeline.setDescription(description);
			return this;
		}

		@Override
		public PipelineBuilder version(String version) {
			pipeline.setVersion(version);
			return this;
		}

		@Override
		public PipelineBuilder modified(Instant modified) {
			pipeline.setModified(modified);
			return this;
		}

		@Override
		public PipelineBuilder created(Instant created) {
			pipeline.setCreated(created);
			return this;
		}

		@Override
		public PipelineBuilder source(String as) {
			pipeline.setSource(new SimplePipelineDestination(as));
			return this;
		}

		@Override
		public PipelineBuilder source(String as, SchemaType type) {
			pipeline.setSource(new SimplePipelineDestination(as, type));
			return this;
		}

		@Override
		public PipelineBuilder target(String as) {
			pipeline.setTarget(new SimplePipelineDestination(as));
			return this;
		}

		@Override
		public PipelineBuilder target(String as, SchemaType type) {
			pipeline.setTarget(new SimplePipelineDestination(as, type));
			return this;
		}

		@Override
		public PipelineStageBuilder stage(PipelineStageMetadata metadata) throws Exception {
			return new PersistentStageBuilder(metadata);
		}

		@Override
		public PipelineBuilder stage(PipelineStage stage) throws Exception {
			pipeline.addStage(stage);
			return this;
		}

		@Override
		public Pipeline build() throws Exception {
			String key = pipeline.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
				pipeline.setKey(key);
			}
			return repository.store(key, pipeline);
		}

		
	}
	
	protected class PersistentStageBuilder implements PipelineStageBuilder {
		
		private SimplePipelineStage stage;
		
		public PersistentStageBuilder(PipelineStageMetadata metadata) {
			this.stage = new SimplePipelineStage(metadata);
		}

		@Override
		public PipelineStageBuilder condition(String expression) {
			stage.setCondition(expression);
			return this;
		}

		@Override
		public PipelineStageBuilder key(String key) {
			stage.setKey(key);
			return this;
		}

		@Override
		public PipelineStageBuilder configuration(Map<String, Object> configuration) {
			stage.setConfiguration(configuration);
			return this;
		}

		@Override
		public PipelineStage build() {
			return stage;
		}
		
	}

}
