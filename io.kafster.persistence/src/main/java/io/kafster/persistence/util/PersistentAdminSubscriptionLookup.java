/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.util.Optional;

import io.kafster.persistence.SubscriptionRepository;
import io.kafster.subscription.Subscription;
import io.kafster.subscription.SubscriptionLookup;
import io.kafster.subscription.SubscriptionStatus.State;

public class PersistentAdminSubscriptionLookup implements SubscriptionLookup {
	
	private SubscriptionRepository repository;
	
	private static final State[] STATES = {
		State.AVAILABLE,
		State.QUEUED,
		State.REBALANCING,
		State.RUNNING,
		State.NOT_RUNNING
	};

	@Override
	public Optional<Subscription> getSubscription(String query) throws Exception {
		return repository.fetch(query);
	}

	@Override
	public Iterable<? extends Subscription> getSubscriptions() throws Exception {
		return repository.getByState(STATES);
	}
	
	public void setRepository(SubscriptionRepository repository) {
		this.repository = repository;
	}

}
