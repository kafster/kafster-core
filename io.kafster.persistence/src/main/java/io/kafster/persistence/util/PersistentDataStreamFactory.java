/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.kafster.datastream.DataStream;
import io.kafster.datastream.DataStreamFactory;
import io.kafster.datastream.Record;
import io.kafster.io.ResourceLookup;
import io.kafster.persistence.DataStreamRepository;

public class PersistentDataStreamFactory implements DataStreamFactory {
	
	private DataStreamRepository repository;
	private ResourceLookup resources;

	@Override
	public DataStreamBuilder create() throws Exception {
		return new NodeDataStreamBuilder();
	}
	
	public void setRepository(DataStreamRepository repository) {
		this.repository = repository;
	}
	
	public void setResources(ResourceLookup resources) {
		this.resources = resources;
	}
	
	private class NodeDataStreamBuilder implements DataStreamBuilder {
		
		private SimpleDataStream stream = new SimpleDataStream();

		@Override
		public DataStream build() throws Exception {
			String key = stream.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
			}
			
			/*
			DataStream result = repository.create(key);
			result.setName(stream.getName());
			result.setDescription(stream.getDescription());
			result.setContentType(stream.getContentType());
			
			if (stream.getStream() != null) {
				Resource resource = resources.lookup(key);
				IOUtils.copy(stream.getStream(), resource.getOutputStream());
				result.setRef(resource.getName());
			}
			else if (stream.getRecords() != null) {
				
			}
			
			return repository.store(key, result);
			*/
			return null;
		}

		@Override
		public DataStreamBuilder key(String key) {
			stream.setKey(key);
			return this;
		}

		@Override
		public DataStreamBuilder name(String name) {
			stream.setName(name);
			return this;
		}

		@Override
		public DataStreamBuilder description(String description) {
			stream.setDescription(description);
			return this;
		}

		@Override
		public DataStreamBuilder contentType(String contentType) {
			stream.setContentType(contentType);
			return this;
		}

		@Override
		public DataStreamBuilder record(Record record) {
			if (stream.getRecords() == null) {
				stream.setRecords(new ArrayList<>());
			}
			stream.getRecords().add(record);
			return this;
		}

		@Override
		public DataStreamBuilder records(Iterable<? extends Record> records) {
			records.forEach(record -> record(record));
			return this;
		}

		@Override
		public DataStreamBuilder records(InputStream records) {
			stream.setStream(records);
			return this;
		}
		
	}
	
	private class SimpleDataStream implements DataStream {
		
		private String key;
		private String name;
		private String description;
		private String contentType;
		
		private List<Record> records;
		private InputStream stream;

		@Override
		public String getKey() {
			return key;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ReaderBuilder getReader() {
			return null;
		}
		
		@Override
		public String getContentType() {
			return contentType;
		}	
		
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		
		public List<Record> getRecords() {
			return records;
		}
		
		public InputStream getStream() {
			return stream;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public void setRecords(List<Record> records) {
			this.records = records;
		}

		public void setStream(InputStream stream) {
			this.stream = stream;
		}
		
	}

}
