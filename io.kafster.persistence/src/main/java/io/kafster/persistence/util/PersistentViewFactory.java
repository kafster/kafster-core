/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.kafster.persistence.ViewRepository;
import io.kafster.view.View;
import io.kafster.view.ViewFactory;
import io.kafster.view.ViewFactory.ViewBuilder;
import io.kafster.view.ViewMetadata;
import io.kafster.view.beans.SimpleView;

public class PersistentViewFactory implements ViewFactory {
	
	private ViewRepository repository;

	@Override
	public ViewBuilder createView() {
		return new SimpleViewBuilder();
	}
	
	protected class SimpleViewBuilder implements ViewBuilder {

		private SimpleView view = new SimpleView();
		
		@Override
		public View build() throws Exception {
			String key = view.getKey();
			if (key == null) {
				key = UUID.randomUUID().toString();
				view.setKey(key);
			}
			return repository.store(key, view);
		}

		@Override
		public ViewBuilder metadata(ViewMetadata metadata) {
			view.setMetadata(metadata);
			return this;
		}

		@Override
		public ViewBuilder configuration(String name, Object value) {
			if (view.getConfiguration() == null) {
				view.setConfiguration(new HashMap<>());
			}
			view.getConfiguration().put(name, value);
			return this;
		}

		@Override
		public ViewBuilder configuration(Map<String, Object> configuration) {
			view.setConfiguration(configuration);
			return this;
		}

		@Override
		public ViewBuilder created(Instant created) {
			view.setCreated(created);
			return this;
		}

		@Override
		public ViewBuilder modified(Instant modified) {
			view.setModified(modified);
			return this;
		}

		@Override
		public ViewBuilder key(String key) {
			view.setKey(key);
			return this;
		}

		@Override
		public ViewBuilder name(String name) {
			view.setName(name);
			return this;
		}

		@Override
		public ViewBuilder description(String description) {
			view.setDescription(description);
			return this;
		}
		
	}

	public void setRepository(ViewRepository repository) {
		this.repository = repository;
	}

}
