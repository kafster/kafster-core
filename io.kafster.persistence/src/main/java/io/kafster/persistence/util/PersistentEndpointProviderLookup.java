/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.persistence.util;

import java.util.Optional;

import io.kafster.endpoint.EndpointProvider;
import io.kafster.endpoint.EndpointProviderLookup;
import io.kafster.persistence.EndpointProviderRepository;

public class PersistentEndpointProviderLookup implements EndpointProviderLookup {
	
	private EndpointProviderRepository repository;

	@Override
	public Iterable<? extends EndpointProvider> getProviders() throws Exception {
		return repository.fetch(p -> true);
	}

	@Override
	public Optional<EndpointProvider> getProvider(String query) throws Exception {
		return repository.fetch(query);
	}

	public void setRepository(EndpointProviderRepository repository) {
		this.repository = repository;
	}

}
