/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.kafster.k8s.client.config;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import io.kafster.k8s.client.ClientSupplier;

public class ClientManagedServiceFactory implements ManagedServiceFactory {
	
	private Map<String,ClientRegistration> clients = new ConcurrentHashMap<>();

	@Override
	public String getName() {
		return "Kubernetes Client Factory";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleted(String pid) {
		ClientRegistration registration = clients.remove(pid);
		if (registration != null) {
			registration.unregister();
		}
	}
	
	private class ClientRegistration {
		private ClientSupplier client;
		private ServiceRegistration<ClientSupplier> registration;
		
		public void unregister() {
			if (registration != null) {
				registration.unregister();
				registration = null;
			}
		}
	}

}
